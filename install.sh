#!/usr/bin/env bash
sudo apt-get update  -y
sudo apt-get upgrade   -y

#add swap
sudo fallocate -l 4G /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab

sudo pip install -r requirements.txt
sudo apt-get install -y mysql-client
sudo apt-get install -y unzip xvfb libxi6 libgconf-2-4
sudo apt-get install -y default-jdk
sudo apt-get install  -y chromium-chromedriver
sudo chmod -R 0777 driver