from elasticsearch import Elasticsearch
from lib.mws_listing import *

from lib.blacklist import *
from lib.elastic_product import ElasticProduct
from lib.general import clean_files
from lib.listing import clear_by_brand, eu
from lib.listing_report import get_all_listing_report
from lib.mail import *
from lib.mws_feed import submit_flat_file_to_amazon
from lib.utils.dropbox_utils import upload_to_dropbox
from lib.utils.simple_logger import SimpleLogger
from listing import parse_all_listing_report
from local import *

logger = SimpleLogger.get_stream_logger('source_price_updater')

rootDir = os.path.dirname(os.path.realpath(__file__))
feedsFileDir = rootDir + "/feeds"
if os.path.isdir(feedsFileDir) is False:
    os.makedirs(feedsFileDir)

dataFileDir = rootDir + "/data"
if os.path.isdir(dataFileDir) is False:
    os.makedirs(dataFileDir)

elastic = ElasticProduct(host="104.196.114.179", port=8080, http_auth=('elasticuser', 'KbersRiseUp153'))
price_log_es = Elasticsearch(hosts="35.231.9.43", port=8080, http_auth=('elasticuser', 'KbersRiseUp153'))


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="", help='Source ASIN country')
    parser.add_argument('-t', '--type', type=str, default="inventory", help='Feed Type: inventory or autpricing')
    parser.add_argument('-f', '--file', type=str, help='file path')
    parser.add_argument('-d', '--download_inventory', type=int, default=1)
    parser.add_argument('-hp', '--high_price', type=int, default=1)
    parser.add_argument('-m', '--mode', type=str, default='production', help='mode: production, debug')
    parser.add_argument('-sb', '--sync_blacklist', type=int, default=1, help='sync blacklist')
    command_args = parser.parse_args()
    return command_args


def remove_out_stocks(account_info, region, database, database_name, pro_type="product"):
    region = region.lower()
    if region == 'us':
        pro_table = 'products'
        inv_table = 'inventory'
    else:
        pro_table = 'products_%s' % region
        inv_table = 'inventory_%s' % region
    # remove out of stock items
    try:
        sql = "UPDATE " + pro_table + " p JOIN " + inv_table + " i ON i.sku = p.sku SET p.status = 1 WHERE i.qty > 0"
        database.execute_sql(sql)
    except:
        pass

    qty_file = feedsFileDir + '/' + pro_type + '-qty-' + datetime.datetime.utcnow().strftime('%m%d-%H%M') + ".txt"
    sql = "SELECT sku FROM " + pro_table + " WHERE real_new_price_to_update = 0 AND status > 0"
    print sql
    cursor = database.execute_sql(sql)
    rows = cursor.fetchall()
    if len(rows) > 0:
        with open(qty_file, 'wb') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter="\t", quotechar='|', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerow(["sku", "quantity"])
            for row in rows:
                csv_writer.writerow([row[0], 0])

            # upload_to_dropbox(qty_file, database_name)

            try:
                result = submit_flat_file_to_amazon(region, qty_file, "_POST_FLAT_FILE_INVLOADER_DATA_",
                                                    account_info.mws_access_key,
                                                    account_info.mws_secret_key, account_info.seller_id)
                # sendMail(database_name + " uploaded qty file", result)
            except:
                print traceback.format_exc()
                # sendMail(database_name + " fail to upload qty file", traceback.format_exc())

            try:
                sql = "UPDATE " + pro_table + " SET status = 0 WHERE real_new_price_to_update = 0 AND status > 0"
                database.execute_sql(sql)
            except:
                pass


def remove_merged_book(region, database):
    region = region.lower()
    if region == 'us':
        inv_table = 'all_listings'
    else:
        inv_table = 'all_listings_%s' % region

    sql = "update " + inv_table + " set status = -5 where product_type ='book' and sku not like %s and asin not like %s"

    try:
        database.execute_sql(sql, ['%zd%', 'B%'])
    except:
        pass


def remove_out_stock_from_inventory(account_info, region, database, database_name):
    region = region.lower()
    if region == 'us':
        inv_table = 'all_listings'
    else:
        inv_table = 'all_listings_%s' % region

    qty_file = feedsFileDir + '/high-price-qty-' + datetime.datetime.utcnow().strftime('%m%d-%H%M') + ".txt"
    sql = "SELECT sku,asin,price FROM " + inv_table + " WHERE price >2000"
    print sql
    cursor = database.execute_sql(sql)
    rows = cursor.fetchall()
    print len(rows)
    if len(rows) > 0:
        with open(qty_file, 'wb') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter="\t", quotechar='|', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerow(["sku", "product-id", "product-id-type", "add-delete"])
            for row in rows:
                csv_writer.writerow([row[0], row[1], "ASIN", "x"])
                print row[0], row[1]

            # upload_to_dropbox(qty_file, database_name)
            try:
                upload_to_dropbox(qty_file, database_name)
            except:
                pass

            try:
                result = submit_flat_file_to_amazon(region, qty_file, "_POST_FLAT_FILE_INVLOADER_DATA_",
                                                    account_info.mws_access_key,
                                                    account_info.mws_secret_key, account_info.seller_id, auth_token=account_info.mws_auth_token)
                # sendMail(database_name + " uploaded high price qty file", result)
            except:
                print traceback.format_exc()
                # sendMail(database_name + " fail to upload qty file", traceback.format_exc())

            try:
                sql = "UPDATE " + inv_table + " SET status = -3 WHERE price >2000 AND status >= 0"
                database.execute_sql(sql)
            except:
                pass


def amazon_generated_sku(sku):
    parts = sku.split("-")
    if len(parts) != 3:
        return False
    if len(parts[0]) == 2 and len(parts[1]) == 4 and len(parts[2]) == 4:
        return True
    return False


if __name__ == "__main__":
    args = parse_args()
    logger.info(args)
    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    get_lock("clean_listing_%s" % (args.account.lower()))

    feedType = "_POST_FLAT_FILE_INVLOADER_DATA_"

    accountId = args.account
    if accountId is None or len(accountId) == 0:
        accountId = sids[0]

    database_name = accountId
    databaseName = database_name
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    accountInfo = Account.get(name=accountId)

    if accountInfo is None:
        logger.error("No account info for " + accountId + " found")
        sys.exit()

    clean_files(hours=3)
    if args.type == "file":
        sys.exit()

    region = args.country if len(args.country) > 0 else accountInfo.country

    countries = region.split(",")
    print countries

    regions = set()
    for country in countries:
        if country in eu:
            regions.add('uk')
        else:
            regions.add(country)

        if args.download_inventory > 0:
            get_all_listing_report(country, accountId, accountInfo.mws_access_key, accountInfo.mws_secret_key,
                                   accountInfo.seller_id, auth_token=accountInfo.mws_auth_token,
                                   callback=parse_all_listing_report)

    for region in regions:
        clear_by_brand(accountInfo, region)

    for country in countries:
        asyncBlacklist(accountId, country, sync_db=args.sync_blacklist)

    for country in countries:
        if args.high_price > 0 and args.mode == 'production':
            try:
                remove_out_stock_from_inventory(accountInfo, region=region, database_name=database_name,
                                                database=database)
            except:
                print traceback.format_exc()
                pass

        if args.type == "highprice":
            continue

        if args.mode == 'production':
            remove_merged_book(country, database=database)

        cursor = None
        sql = ""
        db_table = ""
        if args.type == "inactive":
            db_table = "inactive_listings"
            if country.upper() != "US":
                db_table = db_table + "_" + country.lower()

            try:
                sql = "select sku,asin from " + db_table + " where price >= 2000 or mapped = -1 or (asin not like %s and sku not like %s)"
                cursor = database.execute_sql(sql, ["B%", "%ZD%"])
            except:
                print traceback.format_exc()

        elif args.type == "autopricing":
            db_table = "products"
            if country.upper() != "US":
                db_table = db_table + "_" + country.lower()

            sql = "update " + db_table + " i join blacklist b on b.asin = i.real_asin set status = -5 where status >-5 and b.asin is not null"
            try:
                database.execute_sql(sql)
            except:
                print traceback.format_exc()

            try:
                sql = "select sku,fake_asin from " + db_table + " where status = -5"
                cursor = database.execute_sql(sql)
            except:
                print traceback.format_exc()
        else:

            db_table = "inventory"
            all_listing_table = "all_listings"
            if country.upper() != "US":
                db_table = db_table + "_" + country.lower()
                all_listing_table = all_listing_table + "_" + country.lower()

            if all_listings_table_existed(all_listing_table, database):
                db_table = all_listing_table
            try:
                # sql = "select sku,asin from " + db_table + " where  (asin not like %s and sku not like %s)  or status = -5"
                # if country.upper() == "US":
                #     sql = sql + " or (status<0 and product_type = 'product' and status >-5)"
                # cursor = database.execute_sql(sql, ["B%", "%ZD%"])
                sql = "select sku,asin from " + db_table + " where  status = -5"
                cursor = database.execute_sql(sql)
            except:
                print traceback.format_exc()

        logger.info(sql)

        if cursor is not None:
            rows = cursor.fetchall()
            print len(rows), " asins found to be removed for %s." % country

            if len(rows) == 0:
                continue

            list_file = feedsFileDir + '/remove-listings-' + datetime.datetime.utcnow().strftime('%m%d-%H%M') + ".txt"

            with open(list_file, 'wb') as csvfile:
                csvwriter = csv.writer(csvfile, delimiter="\t", quotechar='|', quoting=csv.QUOTE_MINIMAL)
                csvwriter.writerow(["sku", "product-id", "product-id-type", "add-delete"])

                for row in rows:
                    if "fba" in row[0].lower() or amazon_generated_sku(row[0]):
                        continue
                    csvwriter.writerow([row[0], row[1], "ASIN", "x"])

            try:
                if args.mode == 'production':
                    result = submit_flat_file_to_amazon(country, list_file, feedType, accountInfo.mws_access_key,
                                                        accountInfo.mws_secret_key, accountInfo.seller_id,
                                                        auth_token=accountInfo.mws_auth_token)
                    sendMail(database_name + "-" + country + " uploaded listing removal file", result)

                if args.type != "inactive":
                    try:
                        sql = "update " + db_table + "  set status = -10 where status = -5"
                        database.execute_sql(sql)
                    except:
                        print traceback.format_exc()
            except:
                print traceback.format_exc()
                sendMail(database_name + "-" + country + " fail to upload listing file", traceback.format_exc())

                # try:
                #     remove_out_stocks(accountInfo, region=region, database_name=database_name, database=database)
                # except:
                #     print traceback.format_exc()

            try:
                upload_to_dropbox(list_file, accountId)
            except:
                pass

                # if args.mode == 'production':
                #     print "remove price logs from es server"
                #     with open(list_file, 'wb') as csvfile:
                #         elastic_actions = []
                #         for row in rows:
                #             if "fba" in row[0].lower() or amazon_generated_sku(row[0]):
                #                 continue
                #             sku = row[0]
                #             asin = row[1]
                #             sku_info = SkuParser.parse_sku(sku)
                #             condition = sku_info['item_condition']
                #             act = dict(
                #                 _op_type='delete',
                #                 _index="retail_%s" % condition.lower(),
                #                 _id="%s-%s%s" % (asin, accountInfo.code, region),
                #                 _type=region,
                #             )
                #             elastic_actions.append(act)
                #
                #             if len(elastic_actions) > 2000:
                #                 try:
                #                     print "bulk process %s records from es server" % len(elastic_actions)
                #                     helpers.bulk(price_log_es, elastic_actions)
                #                     elastic_actions = []
                #                 except:
                #                     elastic_actions = []
                #                     pass
                #                     # print traceback.format_exc()
                #
                #         if len(elastic_actions) > 0:
                #             try:
                #                 print "bulk process %s records from es server" % len(elastic_actions)
                #                 helpers.bulk(price_log_es, elastic_actions)
                #                 elastic_actions = []
                #             except:
                #                 elastic_actions = []
                # print traceback.format_exc()
