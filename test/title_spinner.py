import csv
import sys
import os

from abbreviate import Abbreviate

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

replace_map = {
    'Pack of': 'pk',
    'pack': 'pk',
    '-inch': "'",
    ' inch': "'",
    '-Inch': "'",
    ' Inch': "'",
    'feet': 'ft'
}

abbr = Abbreviate()


def process(title, brand):
    by_brand = 'by %s' % brand
    if by_brand.lower() in title.lower():
        title = title.replace(by_brand, '')
    else:
        title = title + ' ' + by_brand

    title = abbr.abbreviate(title)

    for k, v in replace_map.iteritems():
        title = title.replace(k, v)
        title = title.replace(k.capitalize(), v)

    return title


def write_to_file(list_file, data):

    with open(list_file, 'ab') as csvfile:
        for asin in data:
            try:
                if isinstance(asin, basestring):
                    csvfile.write(asin + "\n")
                elif isinstance(asin, dict):
                    line = asin.values()
                    csvfile.write("\t".join(line) + "\n")
                else:
                    csvfile.write("\t".join(asin) + "\n")
            except:
                pass

if __name__ == "__main__":
    file_name = 'kitchen'
    list_file = lib_dir + "data/new_fake/raw/%s.csv" % file_name
    dest_file = lib_dir + "data/new_fake/raw/p-%s.csv" % file_name
    print list_file
    limit = 100
    with open(list_file) as f:
        reader = csv.reader(f, delimiter='\t')
        no = 0
        rows = []
        for line in reader:
            try:
                asin = line[0]
                brand = line[1]
                image = line[2]
                title = line[3]
                # print asin, brand, title, image
                no = no + 1
                print no
                print title
                rows.append([asin,brand,process(title, brand),image])
            except:
                pass

        write_to_file(dest_file,rows)
