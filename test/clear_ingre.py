from lib.listing import clear_file_by_ingredients

if __name__ == "__main__":
    test_file = 'data/all-listings-2019-02-12-12-25.csv'
    region = 'uk'
    clear_file_by_ingredients(file_path=test_file, region=region, test=True)
