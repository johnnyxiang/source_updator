import traceback
from datetime import datetime, timedelta

from lib.utils.offer_service import OfferService
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
from lib.utils.offers import ESOfferFilter

offer_service = OfferService('35.245.123.47', 80, 'elasticuser', 'KbersRiseUp153')
filter_cond = {
    'rate': 85,
    'review': 50,
    'domestic': True,
    'shipping_time': 5,
    'subcondition': 70
}

offer_filter = ESOfferFilter(**filter_cond)

offer_service_price_finder = OfferServicePriceFinder(offer_service=offer_service, offer_filter=offer_filter)

if __name__ == '__main__':
    min_sellers = 2
    earliest_date = datetime.utcnow() - timedelta(hours=60)
    asins = ['B002VDW9B2',
             'B00XBILOIU',
             'B00095M3Q0',
             'B00II58NJE',
             'B0090R8AGK',
             'B0027PA178',
             'B0044307Y8',
             'B01BLF2LHC',
             'B004D5ORDE',
             'B007YCN2G6',
             'B00II58JMK',
             'B00008XPFR',
             'B00D8LEHZC',
             'B00N9J49WE',
             'B006S679C2']
    source_country = 'us'
    item_condition = 'new'
    try:
        offers_info = offer_service_price_finder.find_offer_for_asins(asins, country=source_country,
                                                                      condition=item_condition)
        for asin in asins:
            now = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
            try:

                if asin not in offers_info:
                    print asin, 'no offer info'
                    continue

                offer = offers_info[asin]
                if offer is None or 'product_price' not in offer:
                    print asin, 'no offer info'
                    continue

                total_offers = offer['total_offers'] if 'total_offers' in offer else offer['offers']
                if total_offers < min_sellers:
                    print asin, 'only %s seller(s), min %s' % (total_offers, min_sellers)
                    continue

                price = round(offer['product_price'] + offer['shipping_price'], 2)

                if not offer['expired']:
                    print ([round(offer['product_price'] + offer['shipping_price'], 2)])
                else:
                    print asin, 'too old offer info', earliest_date


            except:
                print traceback.format_exc()

    except:
        print traceback.format_exc()
