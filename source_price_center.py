import socket
import sys
import argparse
import threading
import traceback
import datetime
import time
from lib.utils.simple_logger import SimpleLogger
from lib import mws_product
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
from lib.models import *
from local import sids
from multiprocessing import Process, Queue, current_process, Pool

logger = SimpleLogger.get_stream_logger('source_price_updater')
offer_service_price_finder = OfferServicePriceFinder()


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-d', '--condition', type=str, default="all", help='Source ASIN condition')
    parser.add_argument('-t', '--type', type=str, default="product", help='Product Type: product, book or cd')
    parser.add_argument('-stock', '--stock', type=int, default=1, help='stock')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-o', '--offset', type=int, default=0, help='offset')
    parser.add_argument('-s', '--size', type=int, default=120, help='page size')
    parser.add_argument('-m', '--mode', type=str, default="both", help='running mode')
    parser.add_argument('-lock', '--lock', type=int, default=1, help='lock db table')
    parser.add_argument('-mh', '--max_hour', type=int, default=4, help='max hour')
    parser.add_argument('-asins', '--asins', type=str, default=None, help='asins')
    parser.add_argument('-ta', '--target_account', type=str, default="", help='Target Account SID')
    parser.add_argument('-tc', '--target_country', type=str, default="us", help='Target Source ASIN country')
    parser.add_argument('-w', '--workers', type=int, default=2, help='max workers')
    parser.add_argument('-db', '--target_database', type=str, default=None, help='Target Database')
    command_args = parser.parse_args()
    return command_args


def check_by_es(isbns, db_table, earliest_date, condition, country='us'):
    # check es server
    try:
        values = []
        rows = []
        found = []
        offers_info = offer_service_price_finder.find_offer_for_asins(isbns, country, condition)

        if offers_info is not None:

            for asin in isbns:
                try:

                    if asin not in offers_info or offers_info[asin] is None or 'product_price' not in offers_info[asin]:
                        continue

                    offer = offers_info[asin]
                    # sometimes, event query with new condition, used condition offers returned
                    if condition.lower() == "new" and offer['item_condition'].lower() != 'new':
                        continue

                    if 'product_price' in offer:
                        offer_time_str = offer['time'][:19]
                        offer_time = datetime.datetime.strptime(offer_time_str, '%Y-%m-%dT%H:%M:%S')
                        if offer_time > earliest_date:
                            values.append("('" + asin + "'," + str(
                                round(offer['product_price'] + offer['shipping_price'],
                                      2)) + ",'" + offer_time_str + "')")
                            found.append(asin)
                            rows.append([asin, round(offer['product_price'] + offer['shipping_price'], 2), offer_time_str])
                            print asin, condition, round(offer['product_price'] + offer['shipping_price'], 2), offer_time_str
                    else:
                        pass
                except Exception as e:
                    print traceback.format_exc()

            if len(rows) > 0:
                if accountDB.is_closed():
                    accountDB.connect()

                with accountDB.atomic():
                    price_field = 'price' if condition == 'new' else 'used_price'
                    for row in rows:
                        update_sql = " update " + db_table + " set " + price_field + " = %s, price_last_checked=%s where isbn = %s"
                        accountDB.execute_sql(update_sql, [row[1], row[2], row[0]])
                        # update_sql = update_sql + ",".join(values)
                        # update_sql = update_sql + " ON DUPLICATE KEY UPDATE price=VALUES(price),price_last_checked=VALUES(price_last_checked);"
                        # print sql
                        # accountDB.execute_sql(update_sql)

            isbns = [item for item in isbns if item not in found]

    except Exception as e:
        print traceback.format_exc()

    return isbns


def worker(productApi, condition, country, db_table, ptype, work_queue, done_queue):
    try:
        for isbns in iter(work_queue.get, 'STOP'):
            try:
                check_by_mws(isbns, productApi, condition, country, db_table, ptype)
            except:
                print traceback.format_exc()
                pass
            done_queue.put("%s - %s" % (current_process().name, isbns))
    except Exception, e:
        done_queue.put("%s failed  with: %s" % (current_process().name, e.message))
    return True


def check_by_mws_multiple_process(isbns, productApi, condition, country, db_table, ptype, max_worker=3):
    if max_worker == 1:
        check_by_mws(isbns, productApi, condition, country, db_table, ptype)
        return

    work_queue = Queue()
    done_queue = Queue()
    processes = []

    chunks = [isbns[x:x + 20] for x in xrange(0, len(isbns), 20)]
    for chunk in chunks:
        work_queue.put(chunk)

    workers = min(max_worker, len(chunks))
    start = time.time()

    print "get price for %s isbns, used %s works" % (len(isbns), workers)
    for w in xrange(workers):
        p = Process(target=worker, args=(productApi, condition, country, db_table, ptype, work_queue, done_queue))
        p.start()
        processes.append(p)
        work_queue.put('STOP')

    TIMEOUT = 30
    start = time.time()
    while time.time() - start <= TIMEOUT:
        if any(p.is_alive() for p in processes):
            time.sleep(.1)  # Just to avoid hogging the CPU
        else:
            # All the processes are done, break now.
            break
    else:
        # We only enter this if we didn't 'break' above.
        print("timed out, killing all processes")
        for p in processes:
            p.terminate()
            p.join()

    # for p in processes:
    #     p.join()

    # done_queue.put('STOP')
    # for status in iter(done_queue.get, 'STOP'):
    #     print status

    print "get price for %s isbns, used %s works, took %s seconds" % (len(isbns), workers, time.time() - start)
    pass


def check_by_mws(isbns, productApi, condition, country, db_table, ptype):
    if condition.lower() == 'used':
        condition = 'any'

    try:
        rows = {'new': [], 'any': [], 'verygood': []}
        chunks = [isbns[x:x + 20] for x in xrange(0, len(isbns), 20)]
        offers_info = {}
        offers_to_save = {'new': {}, 'any': {}, 'verygood': {}}
        current_time = time.time()

        avg = True if ptype.lower() == 'book' and condition.lower() == 'any' else False
        # print avg
        for chunk in chunks:
            if len(chunk) > 0:
                try:
                    chunk_offer_info = mws_product.getPriceForAsinsFromMws(productApi, chunk, condition, country, product_type=ptype,
                                                                           avg=avg)
                    # print chunk_offer_info
                    for key, value in chunk_offer_info.iteritems():
                        offers_info[key] = value
                    time.sleep(1)
                except:
                    print traceback.format_exc()
                    if "AccessDenied" in traceback.format_exc():
                        print "AccessDenied"
                        time.sleep(60 * 60 * 2)
                        sys.exit()
                        # offers_info.update(chunk_offer_info)

        print "fetch price for %d asins, took %s secs" % (len(isbns), time.time() - current_time)
        # print offers_info

        if condition.lower() == 'all':
            conditions = ['new', 'any']
        elif condition.lower() == 'any':
            conditions = ['any', 'verygood']
        else:
            conditions = [condition]
            # offers = {condition: offers_info}

        offers = offers_info

        for asin in isbns:
            now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
            for cond in conditions:
                try:
                    offers_info = offers[cond]
                    if asin not in offers_info or 'product_price' not in offers_info[asin]:
                        offer = {}
                        offer["product_code"] = "p" if ptype == "product" else "c"
                        offer["country_code"] = country
                        offer['time'] = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
                        offer['asin'] = asin
                        offer['offers'] = 0
                        offer['has_offer'] = 'n'
                        offer['item_condition'] = cond

                        offers_to_save[cond][asin] = offer
                        print asin, cond, 'no offer info'
                        rows[cond].append([asin, 0, now])
                        continue

                    offer = offers_info[asin]
                    # print offer

                    if 'product_price' in offer:
                        # print pid, asin,offer['product_price'],offer['shipping_price'],offer['time']
                        offer["product_code"] = "p" if ptype == "product" else "c"
                        offer["country_code"] = country
                        offer['time'] = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
                        offer['asin'] = asin
                        offers_to_save[cond][asin] = offer
                        # found = found + 1

                        print asin, cond, round(offer['product_price'] + offer['shipping_price'], 2)

                        rows[cond].append([asin, round(offer['product_price'] + offer['shipping_price'], 2), now])
                    else:
                        print asin, cond, 'no offer info'
                        rows[cond].append([asin, 0, now])
                except Exception as e:
                    print asin, cond, 'no offer info'
                    rows[cond].append([asin, 0, now])
                    print traceback.format_exc()

        current_time = time.time()

        for cond in conditions:
            if cond == 'verygood':
                continue
            with accountDBSave.atomic():
                price_field = 'price' if cond == 'new' else 'used_price'
                for row in rows[cond]:
                    update_sql = " update " + db_table + " set " + price_field + " = %s, price_last_checked=%s where isbn = %s"
                    accountDBSave.execute_sql(update_sql, [row[1], row[2], row[0]])
                    # update_sql = " update " + db_table + " set price_last_checked=%s where isbn = %s"
                    # accountDBSave.execute_sql(update_sql, [row[2], row[0]])
            print "saving price for %d asins, took %s secs" % (len(rows[cond]), time.time() - current_time)
            break

        # print country, condition, offers_to_save

        for cond in conditions:
            offers_to_save_es = offers_to_save[cond] if cond in offers_to_save else {}
            print cond, country, len(offers_to_save_es)
            if len(offers_to_save_es) > 0:
                try:
                    # threading.Thread(target=offer_service_price_finder.save_offers, args=(offers_to_save_es, country, cond))
                    offer_service_price_finder.save_offers(offers_to_save_es, country, cond)
                except:
                    print traceback.format_exc()


    except:
        print traceback.format_exc()


def process(country, productApi, ptype, condition, size, limit, mode, lock, offset, max_hour=6, stock=True, max_worker=3,
            target_db_table=None, asins=None):
    condition = condition.lower()

    if target_db_table is not None:
        db_table = target_db_table
    elif ptype == "product":
        db_table = "source_price_products"
    elif ptype == "cd":
        db_table = "source_price_cds"
    else:
        db_table = "source_price_books"

    if target_db_table is not None:
        index_type = target_db_table
    else:
        index_type = ptype

    if stock is False and target_db_table is None:
        db_table = db_table + "_ofs"
        index_type = ptype + "_ofs"

    country = country.lower()
    if country != "us" and target_db_table is None:
        db_table = db_table + "_" + country

    if ptype == "book" and condition != 'all':
        db_table = db_table + "_" + condition

    found = 0
    pindex = 0
    min_id = 0

    if mode == "es":
        max_hour = max(36, max_hour)

    if asins is not None:
        asins = args.asins.split(",")
        check_by_mws(asins, productApi, condition, country, db_table, ptype)
        return

    while True:
        # with accountDB.atomic() as transaction:
        try:
            if accountDB.is_closed():
                accountDB.connect()

            with accountDB.atomic():
                earliest_date = (datetime.datetime.utcnow() - datetime.timedelta(hours=max_hour))
                # last_date = earliest_date.strftime('%Y-%m-%d %H:%M:%S')
                if mode == "es":
                    last_id = min_id
                elif mode == "by_date":
                    last_id = 0
                else:
                    last_id = get_last_id(index_type, condition, country)

                print last_id, index_type, condition, country, mode

                if last_id == 999999999:
                    print 'stop signal found, STOP'
                    break

                sql = "select isbn,price_last_checked,id  from %s where id > %s " % (db_table, last_id)
                # sql = sql + " and `condition` = '%s' " % condition

                if mode == "by_date":
                    sql = sql + " order by price_last_checked asc"
                else:
                    sql = sql + " order by id asc"
                    # sql = sql + " and price_last_checked < '%s' order by id asc" % last_date

                sql = sql + " limit %d" % size

                print sql

                cursor = accountDB.execute_sql(sql)
                rows = cursor.fetchall()
                print "%s rows found" % len(rows)
                if len(rows) == 0:
                    if mode == "es" or mode == "by_date":
                        break
                    # reset and restart
                    print "save id ", index_type, 0, condition, country
                    save_last_id(index_type, 0, condition, country)
                    continue

                isbns = [str(row[0]).zfill(10) for row in rows]

                for row in rows:
                    min_date = row[1]
                    min_id = row[2]
                    pindex = pindex + 1

                if mode != "es" and mode != "by_date":
                    if min_id == 0:
                        min_id = last_id + len(rows)
                    print "saving id ", index_type, min_id, condition, country
                    save_last_id(index_type, min_id, condition, country)

            print "loading ", min_id, min_date, len(isbns), '; total found ', found

            if len(isbns) > 0:
                if condition != 'all' and max_hour > 0:
                    isbns = check_by_es(isbns, db_table, earliest_date, condition=condition, country=country)
                else:
                    pass
                    # isbns_any = check_by_es(isbns, db_table, earliest_date, condition='any', country=country)
                    # isbns_new = check_by_es(isbns, db_table, earliest_date, condition='new', country=country)
                    # isbns = list(set(isbns_any + isbns_new))
                print len(isbns)

            if mode.lower() == "es":
                continue

            if len(isbns) > 0:
                check_by_mws_multiple_process(isbns, productApi, condition, country, db_table, ptype, max_worker=max_worker)

            if limit != 0 and pindex >= limit:
                break
        except:
            print traceback.format_exc()
            # time.sleep(30)
            break


def merge_two_dicts(x, y):
    z = x.copy()  # start with x's keys and values
    z.update(y)  # modifies z with y's keys and values & returns None
    return z


def get_lock(process_name):
    global lock_socket  # Without this our lock gets garbage collected
    lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    try:
        lock_socket.bind('\0' + process_name)
        print 'I got the lock for %s' % (process_name)
    except socket.error:
        print 'lock exists'
        sys.exit()


if __name__ == "__main__":

    args = parse_args()

    print args
    if args.account not in sids:
        print "account not supported"
        sys.exit(0)
    condition = args.condition.lower()
    country = args.country.upper()
    offset = args.offset
    limit = args.limit
    ptype = args.type
    max_hour = args.max_hour
    offset = args.offset
    lock = True if args.lock > 0 else False
    stock = True if args.stock > 0 else False

    if ptype == "book":
        if condition not in ['used', 'new']:
            condition = "all"
    else:
        condition = "new"

    get_lock("source_price_center_%s_%s" % (args.account.lower(), args.mode.lower()))

    if args.mode == "d" or args.mode == "debug":
        debug = True
    else:
        debug = False

    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        logger.error("No account info for " + args.account + " found")
        sys.exit()

    productApi = mws_product.getProductApi(accountInfo.seller_id, accountInfo.mws_access_key,
                                           accountInfo.mws_secret_key, country, auth_token=accountInfo.mws_auth_token)

    size = args.size

    process(country, productApi, ptype, condition, size, limit, args.mode, lock, offset, max_hour, stock, max_worker=args.workers,
            target_db_table=args.target_database, asins=args.asins)
