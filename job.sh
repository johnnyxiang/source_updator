#!/bin/bash
args=("$@")
count=0
dir="$PWD"
echo "$dir"
USER_HOME=$(eval echo ~)

if [ ${#args[0]} = 2 ]
then

    echo "Start - Report End "
    ${USER_HOME}/projects/venv/bin/python ${USER_HOME}/projects/repricing/send_report_monitor.py ${args[0]}
    ${USER_HOME}/projects/venv/bin/python ${USER_HOME}/projects/repricing/send_account_info.py ${args[0]}
    ${USER_HOME}/projects/venv/bin/python ${USER_HOME}/projects/repricing/center_file.py ${args[0]}
    cd  ${USER_HOME}/projects/repricing/ && ${USER_HOME}/projects/venv/bin/fab upload_asin
    cd  ${USER_HOME}/projects/repricing/ && ${USER_HOME}/projects/venv/bin/fab set_env upload_retail_asin
    cd  ${USER_HOME}/projects/repricing/ && ${USER_HOME}/projects/venv/bin/fab download_no_match_asins
    echo "End"
fi

