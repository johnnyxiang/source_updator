import sys
import os
import argparse
import platform
import traceback
from datetime import datetime
from lib.utils.simple_logger import SimpleLogger
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
from lib.utils.process_checker import ProcessChecker
import requests
from config import *
from lib.models import *
import time
import csv
import subprocess
from lib.utils.asin_matcher import AsinMatcher

# Python version must be greater than or equal 2.7
if sys.version_info < (2, 7):
    print("ERROR: Repricing requires Python Version 2.7 or above...exiting.")
    sys.exit(1)
else:
    PY2 = bool(sys.version_info.major == 2)

logger = SimpleLogger.get_stream_logger('source_price_updator')

offer_service_price_finder = OfferServicePriceFinder()

asin_matcher = AsinMatcher()


def write_to_file(file_name, data):
    list_file = "data/%s.txt" % file_name
    with open(list_file, 'ab') as csvfile:
        for asin in data:
            csvfile.write(asin + "\n")


if __name__ == "__main__":
    db_table = "source_price_books"
    last_id = 5452060
    size = 100
    found = 0
    while True:
        # with accountDB.atomic() as transaction:
        asins = set()
        try:

            sql = "select isbn,price_last_checked,id  from %s where id > %s " % (db_table, last_id)
            # sql = sql + " and `condition` = '%s' " % condition

            sql = sql + " order by id asc"

            sql = sql + " limit %d" % size

            print sql

            cursor = accountDB.execute_sql(sql)
            rows = cursor.fetchall()

            if len(rows) == 0:
                break

            isbns = [str(row[0]).zfill(10) for row in rows]

            for row in rows:
                min_date = row[1]
                min_id = row[2]

            last_id = min_id

            source_asins = asin_matcher.match_asins_by_api(isbns)
            if len(source_asins) > 0:
                source_asins = [item['asin'] for item in source_asins]
                asins = asins | set(source_asins)

            print "loading ", min_id, len(isbns), '; total found ', found + len(asins)
        except:
            print traceback.format_exc()
            # break

        if len(asins) > 0:
            write_to_file('all-asins', asins)
            found = found + len(asins)
