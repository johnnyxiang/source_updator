import sys
import argparse
import datetime

from lib.general import regions, get_lock
from lib.models import *
import os
import csv
from lib import mws_report
import traceback
from local import *
import StringIO
from lib.utils.exchange_rate import ExchangeRate

rootDir = os.path.dirname(os.path.realpath(__file__))

listingFileDir = rootDir + "/settlement"
if os.path.isdir(listingFileDir) is False:
    os.makedirs(listingFileDir)

account_code = ""

accountInfo = None


def get_settlement_report(seller_account, region=None, fromdate=None, todate=None):
    reportType = '_GET_V2_SETTLEMENT_REPORT_DATA_FLAT_FILE_'

    if region is None:
        region = seller_account.country.upper()

    mws_report.get_report_request_list(reportType, region, seller_account.seller_id, seller_account.mws_access_key,
                                       seller_account.mws_secret_key, auth_token=seller_account.mws_auth_token, fromdate=fromdate,
                                       todate=todate, callback=parse_settlement_report)


def parse_settlement_report(data, region, seller_id):
    fileName = rootDir + "/settlement/settlement-" + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%I") + ".csv"
    print fileName
    w = open(fileName, "wb")
    writer = csv.writer(w, delimiter=',')

    f = StringIO.StringIO(data)
    reader = csv.reader(f, delimiter="\t")

    index = 0
    for row in reader:
        index = index + 1
        # print row
        writer.writerow(row)

    w.close()

    # return 
    parse_settlement_report_file(fileName, seller_id, region)
    try:
        os.remove(fileName)
    except:
        pass

    return


def parse_settlement_report_file(file, seller_id, region):
    global accountInfo
    if os.path.isfile(file):
        fileName = file
    else:
        fileName = rootDir + "/settlement/" + file + ".csv"
    if accountInfo is None:
        accountInfo = Account.get(seller_id=seller_id)
    account_code = accountInfo.name.upper()

    row_index = 0
    total = 0
    header = {}
    exist_check = False
    header_cols = ['settlement-id', 'settlement-start-date', 'settlement-end-date', 'deposit-date', 'total-amount', 'currency',
                   'transaction-type', 'order-id', 'merchant-order-id', 'adjustment-id', 'shipment-id', 'marketplace-name',
                   'fulfillment-id', 'posted-date', 'order-item-code', 'merchant-order-item-id', 'merchant-adjustment-item-id', 'sku',
                   'quantity-purchased', 'other-amount']

    marketplace = None
    reserved = 0
    with open(fileName, "r") as f:
        reader = csv.reader(f, delimiter=",")
        for row in reader:
            if "settlement-id" in row:
                for idx, val in enumerate(row):
                    header[val.lower()] = idx
                continue
            row_data = {"account_code": account_code, "country": region}
            for col in header_cols:
                try:
                    key = col.replace('-', '_')
                    row_data[key] = row[header[col.lower()]]
                    if ' UTC' in row_data[key]:
                        row_data[key] = row_data[key].replace(' UTC', '').replace('+00:00', '')
                    if 'date' in col:
                        row_data[key] = row_data[key].replace(' UTC', '').replace('+00:00', '')
                        try:
                            row_data[key] = datetime.datetime.strptime(row_data[key], '%d.%m.%Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
                        except:
                            try:
                                row_data[key] = datetime.datetime.strptime(row_data[key], '%Y-%m-%dT%H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
                            except:
                                pass
                            pass
                except:
                    pass
                    # print row_data[col]
            # print row, row_data
            if "marketplace_name" in row_data and row_data["marketplace_name"].strip() != "":
                marketplace = row_data["marketplace_name"]

            if 'transaction_type' in row_data and row_data['transaction_type'] == 'Current Reserve Amount':
                reserved = -float(row_data['other_amount'])

                # print reserved

    with open(fileName, "r") as f:
        reader = csv.reader(f, delimiter=",")
        for row in reader:
            # print row
            row_index = row_index + 1

            if "settlement-id" in row:
                for idx, val in enumerate(row):
                    header[val.lower()] = idx
                continue

            if exist_check is False:
                sql = 'SELECT settlement_start_date,settlement_end_date,total_amount FROM transactions WHERE `settlement_id` = %s'
                cursor = omsDB.execute_sql(sql, [row[header['settlement-id']]])
                res = cursor.fetchone()

                if res is not None:
                    print 'existed', res[0], res[1], res[2]
                    return

                exist_check = True

            row_data = {"account_code": account_code, "marketplace": marketplace}
            for col in header_cols:
                try:
                    key = col.replace('-', '_')
                    row_data[key] = row[header[col.lower()]]
                    if ' UTC' in row_data[key]:
                        row_data[key] = row_data[key].replace(' UTC', '').replace('+00:00', '')
                    if 'date' in col:
                        row_data[key] = row_data[key].replace(' UTC', '').replace('+00:00', '')
                        try:
                            row_data[key] = datetime.datetime.strptime(row_data[key], '%d.%m.%Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
                        except:
                            try:
                                row_data[key] = datetime.datetime.strptime(row_data[key], '%Y-%m-%dT%H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
                            except:
                                pass
                            pass
                except:
                    pass
                    # print row_data[col]

            if "order_id" not in row_data or row_data["order_id"].strip() != "" or row_data[
                "transaction_type"].strip() != "":
                continue

            row_data['reserved'] = reserved
            del row_data['other_amount']

            print row_data
            # continue

            if float(row_data["total_amount"]) == 0:
                try:
                    sql = "DELETE FROM transactions WHERE settlement_id=%s AND account_code=%s"
                    omsDB.execute_sql(sql, [row_data["settlement_id"], account_code])
                except:
                    pass

                continue

            total = total + 1

            if row_data["currency"] == "USD":
                rate = 1.0
            else:
                rate = ExchangeRate.get_exchange_rate(row_data["currency"], currency='USD')
                rate = rate * 0.96

            row_data["exchange_rate"] = rate
            row_data["total_amount_usd"] = float(row_data["total_amount"]) * rate

            print row_data
            # print header,row, header_cols,row_data
            try:
                sql = "INSERT INTO transactions (" + (','.join('`' + k + '`' for k in row_data.keys())) + ") value (" + (
                    ','.join("%s" for col in
                             row_data.keys())) + ") ON DUPLICATE KEY UPDATE marketplace = VALUES (marketplace),total_amount = VALUES (total_amount),reserved = VALUES (reserved)"

                # print sql
                omsDB.execute_sql(sql, [k if k != "" else None for k in row_data.values()])

            except:
                print traceback.format_exc()
                return
                pass

                # os.remove(fileName)


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default=None, help='Source country')
    parser.add_argument('-after', '--posted_after', type=str, default=None, help='posted_after')
    parser.add_argument('-before', '--posted_before', type=str, default=None, help='posted_after')
    command_args = parser.parse_args()
    return command_args


def main(args, region=None):
    print args
    global accountInfo

    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        print("No account info for " + args.account + " found")
        sys.exit()

    if region is None:
        try:
            region = args.country.upper()
        except:
            region = accountInfo.country.upper()

    try:
        fromdate = args.posted_after
    except:
        fromdate = None

    try:
        todate = args.posted_before
    except:
        todate = None

    get_settlement_report(accountInfo, region=region, fromdate=fromdate, todate=todate)


if __name__ == "__main__":
    # parse_settlement_report_file(rootDir + "/test/data/15584138658017985.csv", seller_id="A2P33C1SB3BOHA", region="UK")
    # sys.exit(0)
    args = parse_args()

    get_lock("settlement_%s" % (args.account.lower()))
    main(args)
