import argparse
import subprocess
import sys
import time
import traceback
from datetime import datetime, timedelta

from config import *
from lib import mws_product
from lib.general import APP_ROOT_DIR
from lib.models import *
from lib.utils.offer_service import OfferService
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
from lib.utils.offers import ESOfferFilter
from lib.utils.simple_logger import SimpleLogger
# Python version must be greater than or equal 2.7
from lib.utils.sku_parser import SkuParser
from local import sids

if sys.version_info < (2, 7):
    print("ERROR: Repricing requires Python Version 2.7 or above...exiting.")
    sys.exit(1)
else:
    PY2 = bool(sys.version_info.major == 2)

logger = SimpleLogger.get_stream_logger('source_price_updator')

offer_service = OfferService('35.245.123.47', 80, 'elasticuser', 'KbersRiseUp153')
filter_cond = {
    'rate': 85,
    'review': 50,
    'domestic': True,
    'shipping_time': 5,
    'subcondition': 70
}

offer_filter = ESOfferFilter(**filter_cond)

offer_service_price_finder = OfferServicePriceFinder(offer_service=offer_service, offer_filter=offer_filter)

rootDir = os.path.dirname(os.path.realpath(__file__))
priceFileDir = rootDir + "/prices"

if not os.path.isdir(priceFileDir):
    os.makedirs(priceFileDir)


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='ASIN country')
    parser.add_argument('-s', '--source_country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-d', '--condition', type=str, default="new", help='Source ASIN condition')
    parser.add_argument('-t', '--type', type=str, default="product", help='Product Type: product, book or cd')
    parser.add_argument('-o', '--offset', type=int, default=0, help='Start offset')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-m', '--mode', type=str, default="production", help='running mode')
    parser.add_argument('-ps', '--page_size', type=int, default=500, help='page size')
    parser.add_argument('-sku', '--sku', type=str, default=None, help='sku pattern')
    parser.add_argument('-e', '-expired_only', type=int, default=0, help='expired offers only')
    command_args = parser.parse_args()
    return command_args


def create_update_table(db_name, database_connection):
    sql = "CREATE TABLE " + db_name + " (" \
                                      "id INT(11) NOT NULL AUTO_INCREMENT," \
                                      "sku VARCHAR(40) NOT NULL DEFAULT ''," \
                                      "real_new_price_to_update DECIMAL(9,2) DEFAULT '0.00'," \
                                      "real_price_last_checked DATETIME NOT NULL DEFAULT '2015-10-01 00:00:00'," \
                                      "PRIMARY KEY (`id`)," \
                                      "UNIQUE KEY sku (`sku`)" \
                                      ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;"

    try:
        database_connection.execute_sql(sql)
    except:
        pass
        # print traceback.format_exc()


def main(args):
    logger.info(args)
    database_name = args.account
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    condition = args.condition.lower()
    country = args.country

    offset = args.offset
    limit = args.limit
    ptype = args.type
    pindex = 0

    try:
        page_size = args.page_size
    except:
        page_size = 500

    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        logger.error("No account info for " + args.account + " found")
        sys.exit()

    if ptype == "product":
        db_table = "products"
    elif ptype == "cd":
        db_table = "cds"
    else:
        db_table = "books"

    if country.upper() != "US":
        db_table = db_table + "_" + args.country.lower()

    minId = args.offset

    update_db_table = db_table + "_update"

    parsed_file_name = APP_ROOT_DIR + "/listings/" + update_db_table + ".sql"
    writer = open(parsed_file_name, "wb")
    writer.write('SET autocommit=0;\n')
    writer.write('SET unique_checks=0;\n')
    writer.write('SET foreign_key_checks=0;\n\n')

    expired_offers = []
    sku_asin_mapping = {}
    earliest_date = datetime.utcnow() - timedelta(hours=60)
    earliest_date_str = earliest_date.strftime('%Y-%m-%d %H:%M:%S')

    if hasattr(args, 'expired_only') and args.expired_only > 0:
        expired_only = 1
    elif hasattr(args, 'expired_only') and args.expired_only < 0:
        expired_only = -1
    else:
        expired_only = 0

    default_min_sellers = accountInfo.min_sellers
    default_max_price = accountInfo.max_price
    repricing_settings = {}
    for setting in AutoRepricingSetting.select().where(AutoRepricingSetting.account_code == accountInfo.name):
        repricing_settings[setting.sku.lower()] = setting

    while True:
        sql = "select real_asin,sku,real_new_price,id,`condition`  from " + db_table + " where  status > -5 and  id > " + str(
            minId) + " and real_asin <> 'None' and real_asin is not null "
        if expired_only > 0:
            sql = sql + " and real_price_last_checked <= '%s'" % earliest_date_str
        if args.sku is not None:
            sql = sql + " and sku like %s"

        if condition.lower() != "all":
            sql = sql + " and `condition` = '" + condition + "' "
        if args.mode == "new":
            sql = sql + " and real_new_price_to_update = 0 "
        sql = sql + " order by id asc limit  %s" % page_size

        print sql
        if args.sku is not None:
            cursor = database.execute_sql(sql, ['%' + args.sku + '%'])
        else:
            cursor = database.execute_sql(sql)
        rows = cursor.fetchall()

        if rows is None or len(rows) == 0:
            break

        item_by_source = {}

        for row in rows:
            asin = str(row[0]).zfill(10)
            sku_info = SkuParser.parse_sku(row[1], country)
            source_country = sku_info['source']
            item_condition = sku_info['item_condition']
            if source_country not in item_by_source:
                item_by_source[source_country] = {}

            if item_condition not in item_by_source[source_country]:
                item_by_source[source_country][item_condition] = []

            item_by_source[source_country][item_condition].append(
                {'id': row[3], 'asin': asin, 'sku': row[1], 'current_source_price': row[2], 'condition': row[4]})

            pindex = pindex + 1
            minId = row[3]

        print "loading ", minId

        for source_country, condition_items in item_by_source.iteritems():
            if len(condition_items) > 0:
                data_rows = []
                for item_condition, items in condition_items.iteritems():
                    if len(items) == 0:
                        continue
                    try:
                        offers_info = offer_service_price_finder.find_offer_for_asins([p['asin'] for p in items],
                                                                                      country=source_country,
                                                                                      condition=item_condition)
                        for p in items:
                            now = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
                            try:
                                asin = p['asin']
                                pid = p['id']
                                sku = p['sku']
                                sku_pattern = SkuParser.parse_sku_pattern(sku).lower()

                                if sku_pattern in repricing_settings:
                                    min_sellers = repricing_settings[sku_pattern].min_sellers
                                    max_price = repricing_settings[sku_pattern].max_price
                                else:
                                    min_sellers = default_min_sellers
                                    max_price = default_max_price

                                if asin not in offers_info:
                                    print pid, asin, 'no offer info'
                                    data_rows.append([sku, 0, now])
                                    continue
                                offer = offers_info[asin]
                                if offer is None or 'product_price' not in offer:
                                    print pid, asin, 'no offer info'
                                    data_rows.append([sku, 0, now])
                                    continue

                                # sometimes, event query with new condition, used condition offers returned
                                if condition.lower() == "new" and offer['item_condition'].lower() != 'new':
                                    print pid, asin, 'no offer info'
                                    data_rows.append([sku, 0, now])
                                    continue

                                total_offers = offer['total_offers'] if 'total_offers' in offer else offer['offers']
                                if total_offers < min_sellers:
                                    print pid, asin, sku_pattern, 'only %s seller(s), min %s' % (
                                        total_offers, min_sellers)
                                    data_rows.append([sku, 0, now])
                                    continue

                                price = round(offer['price'], 2)
                                if price > max_price:
                                    print pid, asin, sku_pattern, 'offer price %s, max %s' % (price, max_price)
                                    data_rows.append([sku, 0, now])
                                    continue

                                offer_time_str = offer['time'][:19]
                                if not offer['expired']:
                                    data_rows.append([sku, round(offer['price'], 2), offer_time_str])
                                else:
                                    print pid, asin, 'too old offer info', earliest_date, offer_time_str
                                    expired_offers.append(asin)
                                    sku_asin_mapping[asin] = sku
                            except:
                                print traceback.format_exc()
                    except:
                        print traceback.format_exc()

                if len(data_rows) > 0:
                    writer.write(
                        'INSERT INTO ' + update_db_table + ' (sku,real_new_price_to_update,real_price_last_checked) VALUES \n')
                    lines = []
                    for row in data_rows:
                        line = '("' + row[0] + '","' + str(row[1]) + '","' + str(row[2]) + '")'
                        lines.append(line)
                    value_line = ",\n".join(lines);
                    value_line = value_line + ";\n"
                    writer.write(value_line)
        if limit != 0 and pindex >= limit:
            break

    writer.write('COMMIT;\n')
    writer.write('SET unique_checks=1;\n')
    writer.write('SET foreign_key_checks=1;\n')
    writer.close()

    create_update_table(update_db_table, database)

    try:
        sql = "TRUNCATE TABLE " + update_db_table
        print sql
        database.execute_sql(sql)
    except:
        print traceback.format_exc()

    time.sleep(20)

    try:
        sql = "mysql --user=" + databaseUser + " --password=" + databasePW + " -h" + databaseHost + " " + database_name + " < " + parsed_file_name
        print sql
        subprocess.call(sql, shell=True)

        sql = "UPDATE %s p JOIN %s pu ON pu.sku = p.sku " \
              "SET p.`real_new_price_to_update` = pu.`real_new_price_to_update`, " \
              "p.real_price_last_checked = pu.real_price_last_checked" % (db_table, update_db_table)

        print sql
        database.execute_sql(sql)
    except:
        print traceback.format_exc()


if __name__ == "__main__":
    args = parse_args()
    main(args)
