#
import argparse

from lib.models import omsDB


def parse_args():
    parser = argparse.ArgumentParser(description='Cronjob')
    parser.add_argument('-j', '--job', type=str, help='Source country')
    command_args = parser.parse_args()
    return command_args


def update_order_item_sku_pattern():
    sql = 'UPDATE  amazon_order_items ' \
          'SET sku_pattern =  REPLACE(sellersku,concat("-",SUBSTRING_INDEX(sellersku, "-", -1)),"")  ' \
          'WHERE sku_pattern IS NULL OR sku_pattern = ""'
    print sql
    omsDB.execute_sql(sql)

    sql = 'UPDATE  amazon_order_items SET fba = 1 WHERE sellersku LIKE % OR sellersku LIKE %'
    omsDB.execute_sql(sql, ['%fba%', '%resell%'])


if __name__ == "__main__":
    args = parse_args()

    if args.job == 'order_item_sku':
        update_order_item_sku_pattern()
