import argparse
import datetime
from lib.models import *
import csv
import traceback
from lib.utils.sku_parser import SkuParser
import subprocess
import StringIO
import unicodedata
from config import *
from local import *
from lib.listing_report import *

listingFileDir = APP_ROOT_DIR + "/listings"
if not os.path.isdir(listingFileDir):
    os.makedirs(listingFileDir)

feedsFileDir = APP_ROOT_DIR + "/feeds"
if not os.path.isdir(feedsFileDir):
    os.makedirs(feedsFileDir)

csv.field_size_limit(sys.maxsize)
remote_db = "local"


def check_create_inventory_table(table_name):
    sql = "CREATE TABLE " + table_name + " (" \
                                         "id int(11) unsigned NOT NULL AUTO_INCREMENT," \
                                         "sku varchar(100) DEFAULT NULL," \
                                         "price decimal(9,2) DEFAULT NULL," \
                                         "asin varchar(10) DEFAULT NULL," \
                                         "`condition` varchar(20) DEFAULT 'New'," \
                                         "status tinyint(11) DEFAULT '1' COMMENT '-3 = out of stock'," \
                                         "shipping decimal(9,2) DEFAULT '4.00'," \
                                         "product_type varchar(20) DEFAULT 'book'," \
                                         "offers int(4) DEFAULT '0'," \
                                         "lowest_price decimal(9,2) DEFAULT '0.00'," \
                                         "qty int(11) DEFAULT '0'," \
                                         "source varchar(10) DEFAULT 'us'," \
                                         "zd tinyint(1) DEFAULT '0'," \
                                         "PRIMARY KEY (`id`)," \
                                         "UNIQUE KEY sku (`sku`)," \
                                         "KEY status (`status`)," \
                                         "KEY asin (`asin`)," \
                                         "KEY `condition` (`condition`)" \
                                         ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4"

    print sql
    try:
        if remote_db == "data":
            dataDB.execute_sql(sql)
        else:
            database.execute_sql(sql)
    except:
        if "exists" not in traceback.format_exc():
            print traceback.format_exc()


def check_create_product_data_table(table_name):
    sql = "CREATE TABLE `" + table_name + "` (" \
                                          "`id` int(11) unsigned NOT NULL AUTO_INCREMENT," \
                                          "`sku` varchar(120) DEFAULT ''," \
                                          "`asin` varchar(10) DEFAULT NULL," \
                                          "`name` varchar(500) DEFAULT NULL," \
                                          "PRIMARY KEY (`id`)," \
                                          "FULLTEXT KEY `name` (`name`)," \
                                          "UNIQUE KEY `sku` (`sku`)" \
                                          ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;"

    print sql
    try:
        if remote_db == "data":
            dataDB.execute_sql(sql)
        else:
            database.execute_sql(sql)
    except:
        if "exists" not in traceback.format_exc():
            print traceback.format_exc()


def check_create_inactive_listing_table(table_name):
    sql = "CREATE TABLE %s (" \
          "id int(11) unsigned NOT NULL AUTO_INCREMENT," \
          "sku varchar(100) DEFAULT NULL," \
          "asin varchar(10) DEFAULT NULL," \
          "price decimal(9,2) DEFAULT '0.00'," \
          "qty int(3) DEFAULT '0'," \
          "mapped tinyint(4) DEFAULT '0'," \
          "PRIMARY KEY (`id`)," \
          "KEY mapped (`mapped`)," \
          "KEY sku (`sku`)" \
          ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;" % table_name

    print sql
    try:
        database.execute_sql(sql)
    except:
        if "exists" not in traceback.format_exc():
            print traceback.format_exc()


def parse_inventory_report_file(region, account_id, file_path):
    region = region.upper()

    if os.path.isfile(file_path):
        file_name = file_path
        parsed_file_name = file_path + ".sql"
    else:
        file_name = APP_ROOT_DIR + "/listings/" + file_path + ".csv"
        parsed_file_name = APP_ROOT_DIR + "/listings/" + file_path + ".sql"

    print file_name

    f = open(file_name, "r")
    reader = csv.reader(f, delimiter=",")

    writer = open(parsed_file_name, "wb")

    writer.write('SET autocommit=0;\n')
    writer.write('SET unique_checks=0;\n')
    writer.write('SET foreign_key_checks=0;\n\n')

    rows = []

    for row in reader:
        if row[0] == "sku":
            continue
        rows.append(row)

    max_feed_size = 1000
    chunks = [rows[x:x + max_feed_size] for x in xrange(0, len(rows), max_feed_size)]

    total = 0

    # if region == "US":
    #     table_name = 'inventory'
    # else:
    #     table_name = 'inventory_%s' % region.lower()

    if region.upper() == "US":
        table_name = 'all_listings'
    else:
        table_name = 'all_listings_%s' % region.lower()

    if remote_db == "data":
        table_name = table_name + "_" + account_id
        database_name = "data"
    else:
        database_name = account_id

    check_create_inventory_table(table_name)

    for chunk in chunks:
        writer.write(
            'INSERT INTO ' + table_name + ' (sku,asin,`condition`,price,qty,`status`,product_type,source,zd) VALUES \n')
        # print row
        lines = []
        for row in chunk:
            try:
                total = total + 1
                sku = row[0]
                asin = row[1]
                if len(asin) > 10:
                    continue
                try:
                    price = row[2]
                except:
                    price = 0
                try:
                    qty = row[3]
                except:
                    qty = 0

                if total % 1000 == 0:
                    print total, sku, asin, qty

                try:
                    if int(row[3]) > 0:
                        status = 1
                    else:
                        status = -3
                except:
                    status = -3

                product_type = SkuParser.parse_type(row[0])

                try:
                    condition = SkuParser.parse_condition(row[0])
                except Exception as e:
                    condition = 'New'

                sku_parts = SkuParser.parse_sku(row[0])

                zd = "1" if sku_parts['zd'] else "0"

                line = '("' + sku + '","' + asin + '","' + condition + '","' + str(price) + '","' + str(qty) + '","' + str(
                    status) + '","' + product_type + '","' + sku_parts['source'] + '","' + str(zd) + '")'
                lines.append(line)
            except:
                pass

        value_line = ",\n".join(lines)
        value_line = value_line + ";\n"

        writer.write(value_line)

    writer.write('COMMIT;\n')
    writer.write('SET unique_checks=1;\n')
    writer.write('SET foreign_key_checks=1;\n')
    writer.close()

    try:
        sql = 'TRUNCATE TABLE ' + table_name
        print sql
        if remote_db == "data":
            dataDB.execute_sql(sql)
        else:
            database.execute_sql(sql)
    except:
        print traceback.format_exc()

    time.sleep(20)

    try:

        sql = "mysql -f --user=" + databaseUser + " --password=" + databasePW + " -h" + databaseHost + " " + database_name + " < " + parsed_file_name

        subprocess.call(sql, shell=True)
    except:
        print traceback.format_exc()

    save_listing_stats(region, account_id, file_name)


def parse_inactive_inventory_report_file(region, account_id, file):
    if os.path.isfile(file):
        file_name = file
        parsed_file_name = file + ".sql"
    else:
        file_name = APP_ROOT_DIR + "/listings/" + file + ".csv"
        parsed_file_name = APP_ROOT_DIR + "/listings/" + file + ".sql"

    print file_name

    f = open(file_name, "r")
    reader = csv.reader(f, delimiter=",")

    writer = open(parsed_file_name, "wb")

    writer.write('SET autocommit=0;\n')
    writer.write('SET unique_checks=0;\n')
    writer.write('SET foreign_key_checks=0;\n\n')

    rows = []

    header = {'sku': 3, 'asin': 16, 'price': 4, 'qty': 5}
    for row in reader:
        if row[0] == "item-name":
            index = 0
            for name in row:
                if 'sku' in name:
                    header['sku'] = index
                elif name == 'product-id' or 'asin' in name:
                    header['asin'] = index
                elif name == 'price':
                    header['price'] = index
                elif name == 'quantity':
                    header['qty'] = index
                index = index + 1
            continue
        rows.append(row)

    max_feed_size = 5000
    chunks = [rows[x:x + max_feed_size] for x in xrange(0, len(rows), max_feed_size)]

    total = 0

    if region.upper() == "US":
        table_name = 'inactive_listings'
    else:
        table_name = 'inactive_listings_%s' % region.lower()

    try:
        check_create_inactive_listing_table(table_name)
    except:
        pass

    for chunk in chunks:
        writer.write('INSERT INTO ' + table_name + ' (sku,asin,price,qty) VALUES \n')
        # print row
        lines = []
        for row in chunk:
            try:
                total = total + 1
                sku = row[header['sku']]
                asin = row[header['asin']]

                if len(asin) > 10:
                    continue

                price = row[header['price']]
                qty = row[header['qty']]

                try:
                    qty = int(qty)
                except:
                    qty = 0

                if total % 1000 == 0:
                    print total, sku, asin, qty

                line = '("' + sku + '","' + asin + '","' + str(price) + '","' + str(qty) + '")'
                lines.append(line)
            except:
                pass

        value_line = ",\n".join(lines);
        value_line = value_line + ";\n"

        writer.write(value_line)

    writer.write('COMMIT;\n')
    writer.write('SET unique_checks=1;\n')
    writer.write('SET foreign_key_checks=1;\n')
    writer.close()

    try:
        sql = "TRUNCATE TABLE " + table_name
        print sql
        database.execute_sql(sql)
    except:
        print traceback.format_exc()

    time.sleep(20)

    try:
        sql = "mysql -f  --user=" + databaseUser + " --password=" + databasePW + " -h" + databaseHost + " " + account_id + " < " + parsed_file_name
        print sql
        subprocess.call(sql, shell=True)

    except:
        print traceback.format_exc()


def parse_active_listing_report_file(region, account_id, file):
    if os.path.isfile(file):
        file_name = file
        parsed_file_name = file + ".sql"
    else:
        file_name = APP_ROOT_DIR + "/listings/" + file + ".csv"
        parsed_file_name = APP_ROOT_DIR + "/listings/" + file + ".sql"

    print file_name

    f = open(file_name, "r")
    reader = csv.reader(f, delimiter=",")

    writer = open(parsed_file_name, "wb")

    writer.write('SET autocommit=0;\n')
    writer.write('SET unique_checks=0;\n')
    writer.write('SET foreign_key_checks=0;\n\n')

    rows = []

    header = {'sku': 3, 'asin': 16, 'price': 4, 'qty': 5}
    for row in reader:
        if row[0] == "item-name":
            index = 0
            for name in row:
                if 'sku' in name:
                    header['sku'] = index
                elif name == 'product-id' or 'asin' in name:
                    header['asin'] = index
                elif name == 'price':
                    header['price'] = index
                elif name == 'quantity':
                    header['qty'] = index
                index = index + 1
            continue
        sku = row[header['sku']]
        sku_info = SkuParser.parse_sku(sku, region)
        if sku_info['product_code'] != 'p':
            continue
        rows.append(row)

    max_feed_size = 100
    chunks = [rows[x:x + max_feed_size] for x in xrange(0, len(rows), max_feed_size)]

    total = 0

    if region.upper() == "US":
        table_name = 'product_data'
    else:
        table_name = 'product_data_%s' % region.lower()

    try:
        check_create_product_data_table(table_name)
    except:
        pass

    for chunk in chunks:
        writer.write('INSERT INTO ' + table_name + ' (sku,asin,name) VALUES \n')
        # print row
        lines = []
        for row in chunk:
            try:
                sku = row[header['sku']]
                total = total + 1
                asin = row[header['asin']]
                name = row[0].strip()
                name = name.replace("'", "")
                name = name.replace('"', '')

                try:
                    name = (name.decode('iso-8859-1')).encode("utf-8")
                except:
                    # print name
                    continue

                name = name.strip()

                if len(name) > 250:
                    name = name[:250]

                name = name.replace("'", "")
                name = name.replace('"', '')

                if total % 1000 == 0:
                    print total, sku, asin, name

                line = '("' + sku + '","' + asin + '","' + name + '")'
                lines.append(line)
            except:
                pass

        value_line = ",\n".join(lines)
        value_line = value_line + ";\n"

        writer.write(value_line)

    writer.write('COMMIT;\n')
    writer.write('SET unique_checks=1;\n')
    writer.write('SET foreign_key_checks=1;\n')
    writer.close()

    try:
        sql = "TRUNCATE TABLE " + table_name
        print sql
        database.execute_sql(sql)
    except:
        print traceback.format_exc()

    time.sleep(20)

    try:
        sql = "mysql -f  --user=" + databaseUser + " --password=" + databasePW + " -h" + databaseHost + " " + account_id + " < " + parsed_file_name
        print sql
        subprocess.call(sql, shell=True)

    except:
        print traceback.format_exc()


def parse_all_listing_report(data, region, account_id):
    file_name = APP_ROOT_DIR + "/listings/all-listings-" + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M") + ".csv"

    # fw = codecs.open(file_name, "w", "utf-8")
    # fw = open(file_name, "w")
    # fw.write(data)
    # fw.close()

    w = open(file_name, "wb")
    writer = csv.writer(w, delimiter=',')

    f = StringIO.StringIO(data)
    reader = csv.reader(f, delimiter="\t")

    for row in reader:
        writer.writerow(row)

    w.close()

    parse_all_listing_report_file(region, account_id, file_name)

    return


def process_chunk(sql_file, rows, table_name, header):
    writer = open(sql_file, "ab")
    if os.path.isfile(sql_file) is False:
        writer.write('SET autocommit=0;\n')
        writer.write('SET unique_checks=0;\n')
        writer.write('SET foreign_key_checks=0;\n\n')

    max_feed_size = 50000
    chunks = [rows[x:x + max_feed_size] for x in xrange(0, len(rows), max_feed_size)]
    for chunk in chunks:
        writer.write('INSERT INTO ' + table_name + ' (sku,asin,`condition`,price,qty,`status`,product_type,source,zd) VALUES \n')
        # print row
        lines = []
        total = 0
        for row in chunk:
            total = total + 1
            try:
                sku = row[header['sku']]
                asin = row[header['asin']]
                if len(asin) > 10:
                    asin = asin[:10]
                price = row[header['price']]
                if len(price) == 0:
                    price = 0
                qty = row[header['qty']]
                if len(qty) == 0:
                    qty = '0'

                if total % 1000 == 0:
                    print total, sku, asin, qty

                if int(qty) > 0:
                    status = 1
                else:
                    status = -3

                product_type = SkuParser.parse_type(sku)

                try:
                    condition = SkuParser.parse_condition(sku)
                except:
                    condition = 'New'

                sku_parts = SkuParser.parse_sku(sku)

                zd = "1" if sku_parts['zd'] else "0"

                line = '("' + sku + '","' + asin + '","' + condition + '","' + str(price) + '","' + str(qty) + '","' + str(
                    status) + '","' + product_type + '","' + sku_parts['source'] + '","' + str(zd) + '")'
                lines.append(line)
            except:
                print row, traceback.format_exc()

        value_line = ",\n".join(lines)
        value_line = value_line + ";\n"
        writer.write(value_line)

    writer.close()


def commit_chunk_process(account_id, sql_file):
    writer = open(sql_file, "ab")
    writer.write('COMMIT;\n')
    writer.write('SET unique_checks=1;\n')
    writer.write('SET foreign_key_checks=1;\n')
    writer.close()

    try:
        sql = "mysql -f  --user=" + databaseUser + " --password=" + databasePW + " -h" + databaseHost + " " + account_id + " < " + sql_file
        print sql
        subprocess.call(sql, shell=True)
    except:
        print traceback.format_exc()


def parse_all_listing_report_file(region, account_id, file):
    if region.upper() == "US":
        table_name = 'all_listings'
    else:
        table_name = 'all_listings_%s' % region.lower()
    try:
        check_create_inventory_table(table_name)
    except:
        pass

    if os.path.isfile(file):
        file_name = file
    else:
        file_name = APP_ROOT_DIR + "/listings/" + file + ".csv"

    print file_name

    try:
        sql = "TRUNCATE TABLE " + table_name
        print sql
        database.execute_sql(sql)
    except:
        print traceback.format_exc()

    f = open(file_name, "r")
    reader = csv.reader(f, delimiter=",")
    rows = []
    header = {'sku': 3, 'asin': 16, 'price': 4, 'qty': 5}
    max_feed_size = 100000
    sql_file = APP_ROOT_DIR + "/listings/%d.sql" % int(time.time())
    batch_no = 1
    for row in reader:
        if row[0] == "item-name":
            index = 0
            for name in row:
                if 'sku' in name:
                    header['sku'] = index
                elif name == 'product-id' or 'asin' in name:
                    header['asin'] = index
                elif name == 'price':
                    header['price'] = index
                elif name == 'quantity':
                    header['qty'] = index
                index = index + 1
            continue

        rows.append(row)
        if len(rows) >= max_feed_size:
            print batch_no, len(rows)
            process_chunk(sql_file, rows, table_name, header)
            rows = []
            batch_no += 1

    f.close()

    commit_chunk_process(account_id, sql_file)

    save_listing_stats(region, account_id, file)


def save_listing_stats(region, account_id, file):
    if os.path.isfile(file):
        file_name = file
    else:
        file_name = APP_ROOT_DIR + "/listings/" + file + ".csv"

    print file_name

    f = open(file_name, "r")
    reader = csv.reader(f, delimiter=",")
    header = {'sku': 3, 'asin': 16, 'price': 4, 'qty': 5}
    sku_by_groups = dict()
    row_no = 0
    for row in reader:
        row_no += 1
        if row_no == 1:
            if row[0] == "item-name":
                index = 0
                for name in row:
                    if 'sku' in name:
                        header['sku'] = index
                    elif name == 'product-id' or 'asin' in name:
                        header['asin'] = index
                    elif name == 'price':
                        header['price'] = index
                    elif name == 'quantity':
                        header['qty'] = index
                    index = index + 1
            else:
                header = {'sku': 0, 'asin': 1, 'price': 2, 'qty': 3}
            continue
        try:
            sku = row[header['sku']]
            qty = row[header['qty']]
            sku_replace = sku.replace("_", "-")
            sku_key = sku_replace[:sku_replace.rfind("-")]

            if sku_key not in sku_by_groups:
                sku_info = SkuParser.parse_sku(sku, region)
                sku_by_groups[sku_key] = dict(total=0, active=0, condition=sku_info['item_condition'], product_code=sku_info['product_code'])

            if len(qty) > 0 and int(qty) > 0:
                sku_by_groups[sku_key]['active'] += 1

            sku_by_groups[sku_key]['total'] += 1
        except:
            pass

    f.close()

    today = datetime.datetime.utcnow().date()
    days_30 = (datetime.datetime.utcnow() - datetime.timedelta(days=30)).date().strftime("%Y-%m-%d")
    channel = salesChannels[region.upper()]

    # clear first
    query = ListingStat.delete().where(ListingStat.country == region, ListingStat.account == account_id, ListingStat.date == today)
    print query.execute()

    try:
        sql = 'UPDATE  orders ' \
              'SET sku_pattern =  REPLACE(sku,concat("-",SUBSTRING_INDEX(sku, "-", -1)),"") ' \
              'WHERE sku_pattern IS NULL'
        omsDB.execute_sql(sql)

    except:
        print traceback.format_exc()

    for sku_group, data in sku_by_groups.iteritems():
        if data['total'] < 100:
            continue

        try:
            listing_stat = ListingStat.get(ListingStat.country == region, ListingStat.sku == sku_group, ListingStat.account == account_id,
                                           ListingStat.date == today)
        except:
            listing_stat = ListingStat()
            listing_stat.sku = sku_group
            listing_stat.account = account_id
            listing_stat.date = today
            listing_stat.country = region.upper()

        listing_stat.total = data['total']
        listing_stat.active = data['active']
        listing_stat.condition = data['condition']
        listing_stat.product_type = data['product_code']
        # orders in last 30 days
        sku_replace = sku_group.replace("-", "_")
        sql = "SELECT count(*) FROM orders WHERE (sku_pattern = %s OR sku_pattern = %s ) AND sheet_date>= %s AND channel = %s"
        # print sql, sku_group, sku_replace, days_30, channel
        cursor = omsDB.execute_sql(sql, [sku_group, sku_replace, days_30, channel])
        d = cursor.fetchone()
        listing_stat.orders = d[0]
        listing_stat.save()

        data['orders'] = listing_stat.orders
        print sku_group, data


def parse_inventory_report(data, region, account_id):
    file_name = APP_ROOT_DIR + "/listings/inventory-" + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M") + ".csv"

    w = open(file_name, "wb")
    writer = csv.writer(w, delimiter=',')

    f = StringIO.StringIO(data)
    reader = csv.reader(f, delimiter="\t")

    for row in reader:
        writer.writerow(row)

    w.close()

    parse_inventory_report_file(region, account_id, file_name)

    return


def parse_inactive_inventory_report(data, region, account_id):
    file_name = APP_ROOT_DIR + "/listings/inactive-" + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M") + ".csv"

    w = open(file_name, "wb")
    writer = csv.writer(w, delimiter=',')

    f = StringIO.StringIO(data)
    reader = csv.reader(f, delimiter="\t")

    for row in reader:
        writer.writerow(row)

    w.close()

    parse_inactive_inventory_report_file(region, account_id, file_name)

    return


def parse_active_listing_report(data, region, account_id):
    file_name = APP_ROOT_DIR + "/listings/active-" + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M") + ".csv"

    w = open(file_name, "wb")
    writer = csv.writer(w, delimiter=',')

    f = StringIO.StringIO(data)
    reader = csv.reader(f, delimiter="\t")

    for row in reader:
        writer.writerow(row)

    w.close()

    parse_active_listing_report_file(region, account_id, file_name)

    return


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')

    parser.add_argument('-c', '--country', type=str, default=None, help='country of the inventory')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-t', '--type', type=str, default="inventory", help='report type: inventory,inactive,active,all')
    parser.add_argument('-f', '--file', type=str, default=None, help='File')
    parser.add_argument('-r', '--remote', type=str, default="local", help='report type: inventory,inactive')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()

    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    clean_files(hours=12)
    accountInfo = Account.get(name=args.account)
    databaseName = args.account
    database_name = args.account
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    country = args.country if args.country is not None and len(args.country) > 0 else accountInfo.country

    remote_db = args.remote

    countries = country.split(",")
    print countries
    for country in countries:

        if args.type.lower() == "inactive":
            if args.file is not None:
                parse_inactive_inventory_report_file(region=country, account_id=accountInfo.name, file=args.file)
            else:
                get_inactive_inventory_report(country, accountInfo.name, accountInfo.mws_access_key, accountInfo.mws_secret_key,
                                              accountInfo.seller_id, auth_token=accountInfo.mws_auth_token,
                                              callback=parse_inactive_inventory_report)
        elif args.type.lower() == "all":
            if args.file is not None:
                parse_all_listing_report_file(region=country, account_id=accountInfo.name, file=args.file)
            else:
                get_all_listing_report(country, accountInfo.name, accountInfo.mws_access_key, accountInfo.mws_secret_key,
                                       accountInfo.seller_id, auth_token=accountInfo.mws_auth_token,
                                       callback=parse_all_listing_report)
        elif args.type.lower() == "active":
            if args.file is not None:
                parse_active_listing_report_file(region=country, account_id=accountInfo.name, file=args.file)
            else:
                get_active_listing_report(country, accountInfo.name, accountInfo.mws_access_key, accountInfo.mws_secret_key,
                                          accountInfo.seller_id, auth_token=accountInfo.mws_auth_token,
                                          callback=parse_active_listing_report)
        elif args.type.lower() == 'stats':
            if args.file is not None:
                save_listing_stats(region=country, account_id=accountInfo.name, file=args.file)
            else:
                print 'please provide listing file'
        else:
            if args.file is not None:
                parse_inventory_report_file(region=country, account_id=accountInfo.name, file_path=args.file)
            else:
                get_inventory_report(country, accountInfo.name, accountInfo.mws_access_key, accountInfo.mws_secret_key,
                                     accountInfo.seller_id, auth_token=accountInfo.mws_auth_token,
                                     callback=parse_inventory_report)
