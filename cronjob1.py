import random

from crontab import CronTab
import sys

from lib.general import get_lock
from lib.models import *
import argparse
from local import *


class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', type=str, default="all", help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='Country')
    command_args = parser.parse_args()
    return command_args


def delete_if_existed(cron, command, account=None):
    for item in cron:
        if command == item.command:
            item.delete()

        # print item.command, account, command
        if command in item.command:
            if account is not None and account not in item.command:
                continue
            item.delete()


def process_account(account, cron):
    # update
    try:
        account_id = account.id
    except:
        account_id = random.randint(1, 60)

    command = "cd %s && git reset --hard origin/master && git pull origin master && sudo pip install -r requirements.txt  && sudo sh install.sh" % APP_ROOT_DIR
    delete_if_existed(cron, 'sh install.sh', None)
    job = cron.new(command=command)
    job.hour.every(1)
    job.minute.on(account_id % 60)

    # cronjob
    command = 'python  %s/cronjob1.py -a %s' % (APP_ROOT_DIR, account.name)
    delete_if_existed(cron, 'cronjob1.py', account.name)
    cron.new(command=command).hour.every(2)
    job.minute.on(account_id % 60)

    command = "ps aux | grep chrome | grep -v 'grep chrome' | awk '{print $2}' | xargs sudo kill -9"
    delete_if_existed(cron, 'chrome', None)
    job = cron.new(command=command)
    job.hour.every(6)
    job.minute.on(account_id % 60 + 10 if account.id % 60 < 50 else account.id % 60 - 10)

    delete_if_existed(cron, 'buybox_price_center.py', account.name)

    command = "ps aux | grep buybox | grep -v 'grep buybox' | awk '{print $2}' | xargs sudo kill -9"
    delete_if_existed(cron, 'grep buybox', None)
    job = cron.new(command=command)
    job.hour.every(6)
    job.minute.on(account_id % 60 + 10 if account.id % 60 < 50 else account.id % 60 - 10)

    command = "ps aux | grep source_price_center | grep -v 'grep source_price_center' | awk '{print $2}' | xargs sudo kill -9"
    delete_if_existed(cron, 'grep source_price_center', None)
    job = cron.new(command=command)
    job.hour.every(6)
    job.minute.on(account_id % 60 + 10 if account.id % 60 < 50 else account.id % 60 - 10)

    # update tracking status
    # command = 'python %s/tracking_updator.py -t order -a %s' % (APP_ROOT_DIR, account.name)
    delete_if_existed(cron, 'tracking_updator.py', account.name)
    # job = cron.new(command=command)
    # job.hour.every(4)
    # job.minute.on(account_id % 60)


if __name__ == "__main__":

    args = parse_args()
    print args

    cron = CronTab(user=True)

    if args.account == "all":
        for sid in sids:
            if sid in sids:
                account = Account.get(name=sid)
                process_account(account, cron)
    else:
        if args.account not in sids:
            print "account not supported"
            sys.exit(0)

        account = Account.get(name=args.account)
        process_account(account, cron)

    cron.write()
    for item in cron:
        print item
        item.enable()
