import json

import argparse
import traceback

import datetime
from dateutil import tz
from dateutil import parser

from lib.general import *
from lib.models import Account, AmazonOrderItem, AmazonOrder
from lib.mws import mws
from local import *
from lib.utils.exchange_rate import ExchangeRate

root_dir = os.path.dirname(os.path.realpath(__file__))

from_zone = tz.gettz('UTC')
to_zone = tz.gettz('America/New_York')


def get_order_api(seller_account, region=None):
    if region is None:
        region = seller_account.region

    order_api = mws.Orders(seller_account.mws_access_key, seller_account.mws_secret_key,
                           seller_account.seller_id, region=region, version="2013-09-01",
                           auth_token=seller_account.mws_auth_token)
    return order_api


def date_range(start_date, end_date):
    for n in range(int((end_date - start_date).days) + 1):
        yield end_date - datetime.timedelta(n)


def list_orders(seller_account, region=None, date_from=None, order_only=False):
    if region is None:
        region = seller_account.region

    region = region.upper()
    order_ids = []
    key = (seller_account.name + "-" + region).lower()
    flag_file = root_dir + "/" + key + "-Order-LastRequest.flag"

    if date_from is not None:
        created_after = parser.parse(date_from).strftime('%Y-%m-%dT%H:%M:%S')
    else:
        try:
            with open(flag_file) as f:
                created_after = f.readlines()

            created_after = created_after[0].replace("Z", "").strip()

            print created_after
        except:
            dt = datetime.datetime.utcnow() - datetime.timedelta(minutes=20)
            created_after = dt.strftime('%Y-%m-%dT%H:%M:%S')

    mkt_ids = get_marketplace_ids(region)

    print "list orders for %s %s since %s" % (seller_account.name, region, created_after)
    order_api = get_order_api(seller_account, region)
    try:
        orders = order_api.list_orders(mkt_ids, lastupdatedafter=created_after)
        orders_data = orders.parsed
        if "Order" in orders_data["Orders"]:
            order_dict = orders_data["Orders"]["Order"]
            if "AmazonOrderId" in order_dict:
                order_dict = [order_dict]

            for order in order_dict:
                order_obj = save_order_to_db(order, seller_account)
                if order_obj is not None and order_obj.OrderStatus != "Pending":
                    order_ids.append(order_obj.AmazonOrderId)

        try:
            if orders_data["LastUpdatedBefore"]["value"] is not None \
                    and orders_data["LastUpdatedBefore"]["value"] != "":
                file = open(flag_file, "w")
                file.write(orders_data["LastUpdatedBefore"]["value"])
        except:
            print traceback.format_exc()
            pass

    except:
        print traceback.format_exc()
        pass

    while True:
        try:
            if 'NextToken' in orders_data:
                nextToken = orders_data['NextToken']['value']
                print nextToken
                orders = order_api.list_orders_by_next_token(nextToken)
                orders_data = orders.parsed
                if "Order" in orders_data["Orders"]:
                    order_dict = orders_data["Orders"]["Order"]
                    if "AmazonOrderId" in order_dict:
                        order_dict = [order_dict]

                    for order in order_dict:
                        order_obj = save_order_to_db(order, seller_account)
                        if order_obj is not None and order_obj.OrderStatus != "Pending":
                            order_ids.append(order_obj.AmazonOrderId)

                try:
                    if orders_data["LastUpdatedBefore"]["value"] is not None \
                            and orders_data["LastUpdatedBefore"]["value"] != "":
                        file = open(flag_file, "w")
                        file.write(orders_data["LastUpdatedBefore"]["value"])
                except:
                    print traceback.format_exc()
                    pass
            else:
                break
        except:
            print traceback.format_exc()
            if "throttled" in traceback.format_exc():
                time.sleep(30)
            else:
                break

    if len(order_ids) > 0 and order_only is False:
        list_order_items(order_ids, seller_account, region)


def list_order_items(order_ids, seller_account, region):
    if isinstance(order_ids, basestring):
        order_ids = [order_ids]

    for orderId in order_ids:
        items = get_order_api(seller_account, region).list_order_items(orderId)
        items_data = items.parsed

        # print itemsData
        items = items_data["OrderItems"]["OrderItem"]
        if "ASIN" in items:
            items = [items]

        AmazonOrderId = orderId

        for item in items:
            try:
                item_obj = AmazonOrderItem.get(AmazonOrderItem.OrderItemId == item["OrderItemId"]["value"])
            except:
                item_obj = AmazonOrderItem()
                item_obj.AmazonOrderId = AmazonOrderId
                item_obj.OrderItemId = item["OrderItemId"]["value"]

            try:
                item_obj.ASIN = item["ASIN"]["value"]
                item_obj.SellerSKU = item["SellerSKU"]["value"]
                item_obj.Title = item["Title"]["value"]
                item_obj.QuantityOrdered = int(item["QuantityOrdered"]["value"])

                if "ItemPrice" in item:
                    item_obj.ItemPrice = float(item["ItemPrice"]["Amount"]["value"])
                    item_obj.Currency = item["ItemPrice"]["CurrencyCode"]["value"]
                if "ShippingPrice" in item:
                    item_obj.ShippingPrice = float(item["ShippingPrice"]["Amount"]["value"])
                # item_obj.dataJSON = json.dumps(item)
                item_obj.exchange_rate = ExchangeRate.get_exchange_rate(item_obj.Currency, currency='USD',
                                                                        endpoint=datetime.datetime.utcnow().date().isoformat())

                if 'fba' in item_obj.SellerSKU.lower() or 'resell' in item_obj.SellerSKU.lower():
                    item_obj.fba = 1
                else:
                    item_obj.fba = 0

                print item_obj.ASIN, item_obj.AmazonOrderId
            except:
                print traceback.format_exc()

            try:
                item_obj.save()
            except:
                print traceback.format_exc()

        time.sleep(1)


def save_order_to_db(order, seller_account):
    try:
        order_obj = AmazonOrder.get(AmazonOrder.AmazonOrderId == order["AmazonOrderId"]["value"])
        print "updated", order["AmazonOrderId"]["value"], order["OrderStatus"]["value"]
    except:
        # print traceback.format_exc()
        order_obj = AmazonOrder()
        order_obj.AmazonOrderId = order["AmazonOrderId"]["value"]
        print "new", order["AmazonOrderId"]["value"], order["OrderStatus"]["value"]

    try:
        utc = parser.parse(order["PurchaseDate"]["value"])
        utc.replace(tzinfo=from_zone)
        # Convert time zone
        local_date = utc.astimezone(to_zone)
        order_obj.PurchaseDate = local_date
        order_obj.purchase_date = local_date.date()
        order_obj.LastUpdateDate = parser.parse(order["LastUpdateDate"]["value"])
        order_obj.OrderStatus = order["OrderStatus"]["value"]
        order_obj.SalesChannel = order["SalesChannel"]["value"]

        if 'IsBusinessOrder' in order:
            order_obj.businessOrder = 1 if order['IsBusinessOrder']['value'] == 'true' else 0

        if "BuyerName" in order:
            order_obj.BuyerName = order["BuyerName"]["value"]
        if "BuyerEmail" in order:
            order_obj.BuyerEmail = order["BuyerEmail"]["value"]

        if "OrderTotal" in order:
            order_obj.OrderTotal = float(order["OrderTotal"]["Amount"]["value"])
            order_obj.Currency = order["OrderTotal"]["CurrencyCode"]["value"]

            if order_obj.Currency == "USD":
                order_obj.OrderTotalUSD = order_obj.OrderTotal
            else:
                try:
                    rate = ExchangeRate.get_exchange_rate(order_obj.Currency, currency='USD',
                                                          endpoint=order_obj.PurchaseDate.date().isoformat())
                    order_obj.OrderTotalUSD = order_obj.OrderTotal * rate
                except:
                    pass
            # order_obj.dataJSON = json.dumps(order)
            order_obj.selfOrder = 1 if order_obj.OrderTotal <= 2 else 0
    except:
        print traceback.format_exc()

    account_code = seller_account.name
    try:
        if order_obj.SalesChannel.lower() == 'amazon.com':
            account_code = account_code.replace('ca', 'us')
    except:
        pass

    order_obj.account_code = account_code

    try:
        order_obj.save()
    except:
        print traceback.format_exc()

    return order_obj


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-c', '--country', type=str, default=None, help='country of the inventory')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-d', '--date_from', help='Date from')
    parser.add_argument('-oo', '--order_only', type=int, default=0, help='Date from')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()

    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    account_info = Account.get(name=args.account)

    if account_info is None:
        print "No account info for " + args.account + " found"
        sys.exit()
    region = args.country
    if region is None:
        region = account_info.country

    order_only = True if args.order_only > 0 else False
    list_orders(account_info, region, args.date_from, order_only=order_only)
