import string
import traceback

from lib.google.sheet_api import SheetAPI
from lib.models import *
import argparse
import datetime
from dateutil import parser

spreadsheet_id = "1VNL3uMJagogGv69MbSuHf6HSLg1HKO-08ETNSz9oye4"
sheet_api = SheetAPI()


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-d', '--date_from', help='Date from')
    command_args = parser.parse_args()
    return command_args


def process_emails(account_code, date_from, debug=False):
    account_info = Account.get(name=account_code)

    if account_info is None:
        print "No account info for " + args.account + " found"
        return

    database_name = account_info.name.lower()
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    start_of_day = datetime.datetime(date_from.year, date_from.month, date_from.day)
    end_of_day = start_of_day + datetime.timedelta(days=1)

    emails = AccountEmail.select().where(AccountEmail.subject.contains('supplier refund follow up job'),
                                         (AccountEmail.created_at >= start_of_day) & (AccountEmail.created_at < end_of_day))

    for email in emails:
        subject = ''.join(filter(lambda x: x in string.printable, email.subject))
        print subject, email.fromAddress, email.toAddress


if __name__ == "__main__":
    args = parse_args()
    if args.date_from is None:
        date_from = datetime.datetime.now()
    else:
        date_from = parser.parse(args.date_from)

    # count_emails('401EU', date_from)

    template_sheet = sheet_api.service.open_by_key(spreadsheet_id).worksheet('Template')
    values_list = template_sheet.col_values(1)

    for account in values_list:
        if account.lower() == 'account' or len(account) == 0:
            continue
        try:
            process_emails(account, date_from)
        except:
            print traceback.format_exc()
            pass
