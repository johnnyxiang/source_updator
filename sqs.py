import argparse
import csv
import datetime
import ntpath
import sys
import time

from elasticsearch import Elasticsearch, helpers
from lxml import etree
from bs4 import BeautifulSoup
from lib import mail
from lib.elastic_product import ElasticProduct
from lib.general import marketplaceIdList, currencies, get_lock
from lib.models import *
import os
from lib.mws import mws
import boto.sqs
import numpy as np
from lib.mws.mws import MWS
from lib.utils.helper import Helper
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
from local import *
from lib.utils.price import *
import traceback
from multiprocessing import Process, Queue, current_process
import threading
from os import listdir
from os.path import isfile, join
import sqlite3 as lite

lock = threading.Lock()

dir = os.path.dirname(os.path.dirname(__file__))
offer_service_price_finder = OfferServicePriceFinder()

rootDir = os.path.dirname(os.path.realpath(__file__))
feedsFileDir = rootDir + "/feeds/sqs"
if os.path.isdir(feedsFileDir) is False:
    os.makedirs(feedsFileDir)

sites = {}
for site in Account.select().where(Account.seller_id != None):
    sites[site.seller_id] = site.min_profit_rate

lite_con = None


def get_mapping_db_connection(create_table=False, delete_existed=False):
    if delete_existed is True:
        try:
            os.remove(feedsFileDir + '/mapping.db')
        except:
            pass

    try:
        lite_con = lite.connect(feedsFileDir + '/mapping.db')
    except lite.Error, e:
        print "Error %s:" % e.args[0]
        sys.exit(1)

    if create_table is True and lite_con is not None:
        try:
            cur = lite_con.cursor()
            cur.execute("SELECT * FROM mapping LIMIT 1")
            lite_con.commit()
        except:
            print traceback.format_exc()
            cur = lite_con.cursor()
            cur.execute('CREATE TABLE mapping(sku TEXT PRIMARY KEY, asin TEXT)')
            lite_con.commit()
            pass

    return lite_con


class Subscriptions(MWS):
    """ Amazon MWS NOTIFICATION API """

    URI = '/Subscriptions/2013-07-01'
    VERSION = '2013-07-01'
    NS = "{http://mws.amazonaws.com/Subscriptions/2013-07-01}"

    def register_destination(self, marketplaceId, attributeList, deliveryChannel="SQS"):
        """ Returns information on available inventory """

        data = dict(Action='RegisterDestination',
                    MarketplaceId=marketplaceId
                    )

        data.update({"Destination.DeliveryChannel": deliveryChannel})
        data.update(self.enumerate_dict('Destination.AttributeList.member.', attributeList))

        return self.make_request(data, "POST")

    def send_test_notification_to_destination(self, marketplaceId, attributeList, deliveryChannel="SQS"):
        data = dict(Action="SendTestNotificationToDestination",
                    MarketplaceId=marketplaceId)

        data.update({"Destination.DeliveryChannel": deliveryChannel})
        data.update(self.enumerate_dict('Destination.AttributeList.member.', attributeList))

        return self.make_request(data, "POST")

    def create_subscription(self, marketplaceId, attributeList, notificationType="AnyOfferChanged", isEnabled="true",
                            deliveryChannel="SQS"):
        data = dict(Action="CreateSubscription",
                    MarketplaceId=marketplaceId)

        data.update({"Subscription.Destination.DeliveryChannel": deliveryChannel})
        data.update({"Subscription.NotificationType": notificationType})
        data.update({"Subscription.IsEnabled": isEnabled})

        data.update(self.enumerate_dict('Subscription.Destination.AttributeList.member.', attributeList))

        return self.make_request(data, "POST")


def getSqsQueueId(region="UK"):
    if region in sqsQueueIDs:
        return sqsQueueIDs[region]

    return None


asin_sku_mapping = {}


def getAttributeList(region="UK"):
    if region in sqsQueueUrls:
        attributeList = [
            {"Key": "sqsQueueUrl", "Value": sqsQueueUrls[region]}
        ]

        return attributeList

    return None


def register(region, access_key, secret_key, seller_id):
    region = region.upper()

    destination_attribute_list = getAttributeList(region)
    if destination_attribute_list is None:
        return

    subscription_api = Subscriptions(access_key, secret_key, seller_id, region=region, version="2013-07-01")

    from lib.general import marketplaceIdList
    subscription_api.register_destination(marketplaceIdList[region], destination_attribute_list)

    print destination_attribute_list, region
    response = subscription_api.create_subscription(marketplaceIdList[region], destination_attribute_list)

    print response._response_dict


def count_messages(region, send_mail=0, purge=1):
    sqsQueueID = getSqsQueueId(region)

    purge = int(purge)
    send_mail = int(send_mail)

    if sqsQueueID is None:
        return

    if region in sqsQueueUrls:
        qurl = sqsQueueUrls[region]
    else:
        return

    parts = qurl.split(".")
    region_code = parts[1]

    conn = boto.sqs.connect_to_region(region_code, aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)

    # print conn, region_code,aws_access_key_id,aws_secret_access_key
    q = conn.get_queue(sqsQueueID)
    count = q.count()
    print count

    if send_mail > 0:
        mail.sendMail('message count: ' + str(count), 'message count: ' + str(count))

    if purge > 0 or count > 100000:
        q.purge()
    return


def aworker(region, message_queue, done_queue, account_info, database, readonly=False):
    # print 232
    try:
        if readonly is True:
            readonly_message_process(message_queue, region, account_info)
        else:
            read_message_process(message_queue, region, account_info, database)
        done_queue.put("Done")

    except Exception, e:
        done_queue.put("%s failed with: %s" % (current_process().name, e.message))

    return True


def read_messages(region, account_info, database, processNo=3, readonly=False):
    sqsQueueID = getSqsQueueId(region)

    if sqsQueueID is None:
        return

    if region in sqsQueueUrls:
        qurl = sqsQueueUrls[region]
    else:
        return

    parts = qurl.split(".")
    region_code = parts[1]

    print qurl, region_code
    conn = boto.sqs.connect_to_region(region_code, aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)

    # print conn, region_code,aws_access_key_id,aws_secret_access_key
    q = conn.get_queue(sqsQueueID)

    q.set_message_class(boto.sqs.message.RawMessage)

    if processNo == 1:
        if readonly is True:
            readonly_message_process(q, region, account_info)
        else:
            read_message_process(q, region, account_info, database)
        return

    workers = int(processNo)
    work_queue = Queue()
    done_queue = Queue()
    processes = []

    for w in xrange(workers):
        p = Process(target=aworker, args=(region, q, done_queue, account_info, database, readonly))
        p.start()

        processes.append(p)
        work_queue.put('STOP')

    for p in processes:
        p.join()

    done_queue.put('STOP')

    for status in iter(done_queue.get, 'STOP'):
        print status


default_intervals = {
    'uk': 15,
    'de': 20,
    'fr': 20,
    'es': 25,
    'it': 25
}

upload_wait = 0


def write_to_file(region, data, account_info, interval=None):
    global upload_wait
    region = region.lower()
    if interval is None:
        try:
            interval = default_intervals[region]
        except:
            interval = 15

    lock.acquire()  # thread blocks at this line until it can obtain lock
    files = [f for f in listdir(feedsFileDir) if isfile(join(feedsFileDir, f))]
    list_file = None
    for file_name in files:
        if region in file_name:
            list_file = feedsFileDir + "/" + file_name
            break
    date_format = '%Y-%m-%d-%H%M%S'

    new_file = False
    if list_file is None:
        list_file = feedsFileDir + "/%s-%s.csv" % (region.lower(), datetime.datetime.utcnow().strftime(date_format))
        new_file = True

    with open(list_file, 'ab') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter="\t", quotechar='|', quoting=csv.QUOTE_MINIMAL)
        if new_file is True:
            csv_writer.writerow(["sku", "price", "quantity", "minimum-seller-allowed-price",
                                 "maximum-seller-allowed-price"])
        for row in data:
            if 'sku' not in row or len(str(row['sku'])) == 0:
                continue

            if region.upper() in ["DE", "IT"]:
                csv_writer.writerow([row['sku'], row['price'], '3', '9,99', '1999,00'])
            else:
                csv_writer.writerow([row['sku'], row['price'], '3', 9.99, 1999.00])

    basename = ntpath.basename(list_file)
    basename = basename.replace(".csv", "").replace(region + "-", "")
    creation_date = datetime.datetime.strptime(basename, date_format)
    print creation_date
    if creation_date < (datetime.datetime.utcnow() - datetime.timedelta(minutes=interval + upload_wait)):
        try:
            submit_price_xml_file_to_amazon(list_file, account_info.seller_id, account_info.mws_access_key, account_info.mws_secret_key,
                                            region=region)
            # os.remove(list_file)
            upload_wait = 0
        except:
            if "Exceeded" in traceback.format_exc():
                upload_wait = 10
            print traceback.format_exc()

    lock.release()


def get_source_prices(asins, region, condition="new"):
    earliest_date = (datetime.datetime.utcnow() - datetime.timedelta(hours=36))

    # print asins, region
    offers_info = offer_service_price_finder.find_offer_for_asins(asins, region, condition)
    source_prices = {}

    for asin in asins:
        source_prices[asin] = None

    if offers_info is not None:
        for asin in asins:
            try:
                if asin not in offers_info or offers_info[asin] is None or 'product_price' not in offers_info[asin]:
                    continue

                offer = offers_info[asin]

                if 'product_price' in offer:
                    offer_time_str = offer['time'][:19]
                    offer_time = datetime.datetime.strptime(offer_time_str, '%Y-%m-%dT%H:%M:%S')
                    if offer_time >= earliest_date:
                        source_prices[asin] = round(offer['product_price'] + offer['shipping_price'], 2)
                else:
                    pass
            except:
                print traceback.format_exc()

    return source_prices


def init_asin_sku_mapping(region, database_connect):
    connection = get_mapping_db_connection(create_table=True, delete_existed=True)
    db_table = "all_listings"
    if region.lower() in ['uk', 'de', 'fr', 'es', 'it']:
        db_table = db_table + "_uk"
    elif region.lower() != 'us':
        db_table = db_table + "_" + region.lower()

    min_id = 0
    while True:
        sql = "select sku,asin,id from " + db_table + " where id > %s limit 5000"
        cursor = database_connect.execute_sql(sql, [min_id])
        rows = cursor.fetchall()
        if len(rows) == 0:
            break

        cur = connection.cursor()
        for row in rows:
            # asin_sku_mapping[row[1]] = row[0]
            min_id = row[2]
            try:
                cur.execute("INSERT INTO mapping (sku,asin) VALUES (:Sku,:asin)", {"Sku": row[0], "asin": row[1]})
                print min_id, row[0], row[1]
            except:
                print traceback.format_exc()
                pass
        connection.commit()

        print "mapping ", len(asin_sku_mapping)


def get_sku(asin):
    global lite_con
    if lite_con is None:
        lite_con = get_mapping_db_connection()

    try:
        cur = lite_con.cursor()
        cur.execute("SELECT sku FROM mapping WHERE asin=:asin", {"asin": asin})
        lite_con.commit()
        row = cur.fetchone()
        if row is not None:
            return row[0]

    except:
        print traceback.format_exc()
        pass

    return None


elastic_actions = []
esclient = Elasticsearch(hosts='104.196.114.179', port=8080, http_auth=('elasticuser', 'KbersRiseUp153'))


def process_bulk(elastic_actions):
    try:
        helpers.bulk(esclient, elastic_actions)
    except:
        if 'blocked by' in traceback.format_exc():
            ElasticProduct.fix_read_only_settings()
            helpers.bulk(esclient, elastic_actions)

        print traceback.format_exc()


def prepare_competitor_es_data(country_code, asin, condition, competitor_offer, offer_count):
    global elastic_actions
    country_code = country_code.lower()
    if condition.lower() == 'used':
        condition = 'any'
    offer_data = {
        'asin': asin,
        'item_condition': competitor_offer['item_condition'],
        'product_price': competitor_offer['product_price'],
        'shipping_price': competitor_offer['shipping_price'],
        'is_prime': competitor_offer['prime'],
        'rate': competitor_offer['rate'],
        'feedback': competitor_offer['feedback']
    }

    offer_data['offers'] = offer_count
    offer_data['has_offer'] = 'y'
    offer_data['time'] = datetime.datetime.utcnow()
    offer_data['country_code'] = country_code
    act = dict(
        _op_type='index',
        _index="competitor_%s" % condition.lower(),
        _id=asin,
        _type=country_code,
    )

    for i in offer_data:
        act[i] = offer_data[i]

    # print act
    elastic_actions.append(act)

    if len(elastic_actions) >= 50:
        process_bulk(elastic_actions)
        elastic_actions = []


def readonly_message_process(message_queue, country_code, account_info):
    helper = Helper()
    while True:
        beginning_time = time.time()
        messages = message_queue.get_messages(num_messages=10)
        time_cost = int(time.time() - beginning_time)
        print "fetched %s messages, took %s sec(s)" % (len(messages), time_cost)

        if len(messages) < 1:
            break
        asins = []
        notifications = []
        for m in messages:
            try:
                body = m.get_body()
                soup = BeautifulSoup(body, 'html.parser')
                notifications_p = helper.find_tags(soup, "anyofferchangednotification")
                asins_p = [helper.find_tag(notification, "offerchangetrigger asin").text.strip() for notification in notifications_p]
                asins = asins + asins_p
                notifications = notifications + notifications_p
            except:
                pass

        if len(asins) == 0:
            continue

        for notification in notifications:
            asin = helper.find_tag(notification, "offerchangetrigger asin").text.strip()
            item_condition = helper.find_tag(notification, "offerchangetrigger itemcondition").text.strip()
            offers = notification.select("offers offer")

            listings = []
            for offer in offers:
                # print offer
                sellerId = helper.find_tag(offer, "sellerid").text
                subcondition = helper.find_tag(offer, "subcondition").text if helper.find_tag(offer, "subcondition") is not None else 'new'
                if subcondition == 'acceptable':
                    continue
                # my listing
                if sellerId == account_info.seller_id or sellerId in sites:
                    continue
                else:
                    # print offer
                    rating_text = helper.find_tag(offer, "sellerfeedbackrating sellerpositivefeedbackrating").text
                    try:
                        rating = int(helper.find_tag(offer, "sellerfeedbackrating sellerpositivefeedbackrating").text)
                    except:
                        rating = 0

                    # if rating != "Less than 70%" and rating != "70-79%" and rating != "Less than 80%" and rating != "N/A":
                    if rating >= 80:
                        listing_price = float(helper.find_tag(offer, "listingprice amount").text)
                        if helper.find_tag(offer, "shipping amount") is not None:
                            shipping_price = float(helper.find_tag(offer, "shipping amount").text)
                        else:
                            shipping_price = 0
                        try:
                            feedback_count = int(helper.find_tag(offer, "sellerfeedbackrating feedbackcount").text)
                        except:
                            feedback_count = 0

                        try:
                            prime = helper.find_tag(offer, "primeinformation isprime").text
                            if prime == 'false':
                                prime = 'n'
                            else:
                                prime = 'y'
                        except:
                            prime = 'n'

                        offer_data = {
                            'product_price': listing_price,
                            'shipping_price': shipping_price,
                            'item_condition': item_condition,
                            'rate': rating_text,
                            'prime': prime,
                            'feedback': feedback_count,
                        }

                        prepare_competitor_es_data(country_code, asin, item_condition, offer_data, len(offers))

                        break
        try:
            message_queue.delete_message_batch(messages)
        except:
            pass

        time_cost = int(time.time() - beginning_time)
        print "Done processing %s messages, took %s sec(s)" % (len(messages), time_cost)


def read_message_process(message_queue, region, account_info, database_connect, source_region='US', readonly=False):
    helper = Helper()
    while True:
        beginning_time = time.time()
        messages = message_queue.get_messages(num_messages=10)
        time_cost = int(time.time() - beginning_time)
        print "fetched %s messages, took %s sec(s)" % (len(messages), time_cost)

        if len(messages) < 1:
            break

        rows_to_upload = []
        asins = []
        notifications = []
        for m in messages:
            try:
                body = m.get_body()
                soup = BeautifulSoup(body, 'html.parser')
                notifications_p = helper.find_tags(soup, "anyofferchangednotification")
                asins_p = [helper.find_tag(notification, "offerchangetrigger asin").text.strip() for notification in notifications_p]
                asins = asins + asins_p
                notifications = notifications + notifications_p
            except:
                pass

        if len(asins) == 0:
            continue

        current_time = time.time()
        source_prices = get_source_prices(asins, source_region)
        time_cost = int(time.time() - current_time)
        print "fetched source prices for %s asins, took %s sec(s)" % (len(asins), time_cost)

        for notification in notifications:
            asin = helper.find_tag(notification, "offerchangetrigger asin").text.strip()
            item_condition = helper.find_tag(notification, "offerchangetrigger itemcondition").text.strip()
            if item_condition == "used":
                continue

            my_sku = get_sku(asin)
            if my_sku is None:
                # print 'not find sku for ', asin
                continue

            if asin not in source_prices or source_prices[asin] is None:
                # print 'not find source price for ', asin
                continue

            source_price = source_prices[asin]

            if source_price is None or source_price == 0:
                # print 'not find source price for ', asin
                continue

            my_current_price = 0
            my_current_shipping = 0
            my_feedback = 0
            # current_time = time.time()
            offers = notification.select("offers offer")
            listings = []
            for offer in offers:
                # print offer
                sellerId = helper.find_tag(offer, "sellerid").text
                subcondition = helper.find_tag(offer, "subcondition").text if helper.find_tag(offer, "subcondition") is not None else 'new'
                if subcondition != 'new':
                    continue
                # my listing
                if sellerId == account_info.seller_id:
                    listing_price = float(helper.find_tag(offer, "listingprice amount").text)
                    if helper.find_tag(offer, "shipping amount") is not None:
                        shipping_price = float(helper.find_tag(offer, "shipping amount").text)
                    else:
                        shipping_price = 0

                    try:
                        feedback_count = int(helper.find_tag(offer, "sellerfeedbackrating feedbackcount").text)
                    except:
                        feedback_count = 0

                    my_current_price = listing_price
                    my_current_shipping = shipping_price
                    my_feedback = feedback_count
                else:
                    if sellerId in sites:
                        if sites[sellerId] > sites[account_info.seller_id]:
                            continue
                    try:
                        rating = int(helper.find_tag(offer, "sellerfeedbackrating sellerpositivefeedbackrating").text)
                    except:
                        rating = 0

                    # if rating != "Less than 70%" and rating != "70-79%" and rating != "Less than 80%" and rating != "N/A":
                    if rating >= 80:
                        listing_price = float(helper.find_tag(offer, "listingprice amount").text)
                        if helper.find_tag(offer, "shipping amount") is not None:
                            shipping_price = float(helper.find_tag(offer, "shipping amount").text)
                        else:
                            shipping_price = 0
                        try:
                            feedback_count = int(helper.find_tag(offer, "sellerfeedbackrating feedbackcount").text)
                        except:
                            feedback_count = 0

                        listings.append([round(listing_price + shipping_price, 2), feedback_count, listing_price])
                if my_current_price > 0 and len(listings) > 7:
                    break

            # print "got %s offers" % len(listings), time.time() - current_time
            # print myCurrentPrice,myCurrentShipping,listings
            # listings = np.array(listings)
            #
            # if len(listings) >= 15:
            #     # continue
            #     mean = sum(listings[0:-1, 0]) / len(listings[0:-1, 0])
            #     listings = np.array(
            #         [listing.tolist() for listing in listings if abs(listing[0] / mean - 1.1) < 0.5 and listing[1] > 10])
            #
            # listings = listings.tolist()

            if len(listings) == 0 or my_current_price is None or my_current_price == 0:
                continue

            min_price = get_min_price(source_price, region, account_info.min_profit, account_info.min_profit_rate)
            min_accepted_price = min_price + my_current_shipping
            ideal_price = get_price(min_price)
            new_price = ideal_price
            my_current_price = new_price
            my_current_landed_price = new_price + my_current_shipping

            if listings[0][0] <= my_current_landed_price:
                new_price_caled = False
                num = 0
                for listing in listings:
                    if listing[0] == my_current_landed_price and listing[1] < my_feedback:
                        continue

                    num = num + 1
                    if num > 3:
                        break

                    if my_current_landed_price >= listing[0] > min_accepted_price:
                        new_price = listing[0] - 0.01 - my_current_shipping
                        new_price_caled = True
                        break
                    elif my_current_price >= listing[2] > min_price and (listing[2] + my_current_shipping) > min_accepted_price:
                        new_price = listing[2] - 0.01
                        new_price_caled = True
                        break

                if new_price_caled is False:
                    new_price = min_accepted_price - my_current_shipping
            else:
                if listings[0][0] - 0.01 - my_current_shipping - ideal_price <= 400:
                    new_price = listings[0][0] - 0.01 - my_current_shipping
                else:
                    new_price = ideal_price

            # print "done", time.time() - current_time

            if new_price >= min_price:
                new_price = round(new_price, 2)
                d = {'asin': asin, 'sku': my_sku, 'price': new_price}
                # print d
                rows_to_upload.append(d)

        try:
            message_queue.delete_message_batch(messages)
        except:
            pass

        current_time = time.time()
        write_to_file(region, rows_to_upload, account_info)
        time_cost = int(time.time() - current_time)
        print "Wrote %s records to file, took %s sec(s)" % (len(rows_to_upload), time_cost)

        time_cost = int(time.time() - beginning_time)
        print "Done processing %s messages, took %s sec(s)" % (len(messages), time_cost)


def upload(rows_to_upload, seller_id, access_key, secret_key, region):
    print 'uploading ', len(rows_to_upload), " data to amazon"
    list_file = feedsFileDir + '/repricing-feed-' + datetime.datetime.utcnow().strftime('%m%d-%H%M') + ".txt"

    with open(list_file, 'w') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter="\t", quotechar='|', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(["sku", "price", "quantity", "minimum-seller-allowed-price",
                             "maximum-seller-allowed-price"])

        for row in rows_to_upload:
            if region.upper() in ["DE", "IT"]:
                csv_writer.writerow([row['sku'], row['price'], '3', '9,99', '1999,00'])
            else:
                csv_writer.writerow([row['sku'], row['price'], '3', 9.99, 1999.00])

    try:
        submit_price_xml_file_to_amazon(list_file, seller_id, access_key, secret_key, region=region)
    except:
        print traceback.format_exc()

    pass


def submit_price_xml_file_to_amazon(file, seller_id, access_key, secret_key, region="UK"):
    region = region.upper()
    marketId = marketplaceIdList[region]
    mktids = [marketId]

    xsi = "http://www.w3.org/2001/XMLSchema-instance"

    root = etree.Element("AmazonEnvelope")

    header = etree.SubElement(root, "Header")
    DocumentVersion = etree.SubElement(header, "DocumentVersion")
    DocumentVersion.text = str(1.01)
    MerchantIdentifier = etree.SubElement(header, "MerchantIdentifier")
    MerchantIdentifier.text = seller_id

    MessageType = etree.SubElement(root, "MessageType")
    MessageType.text = "Price"

    with open(file, 'r') as f:
        reader = csv.reader(f, delimiter="\t")
        index = 0
        for row in reader:
            if row[0] == "sku":
                continue
            index = index + 1

            Message = etree.SubElement(root, "Message")
            MessageID = etree.SubElement(Message, "MessageID")
            MessageID.text = str(index)

            Price = etree.SubElement(Message, "Price")

            SKU = etree.SubElement(Price, "SKU")
            SKU.text = str(row[0])

            StandardPrice = etree.Element("StandardPrice", currency=currencies[region])
            StandardPrice.text = row[1].replace(",", ".")
            Price.append(StandardPrice)

    xmlToSubmit = '<?xml version="1.0" encoding="UTF-8" ?>'
    xmlToSubmit = xmlToSubmit + etree.tostring(root)

    if sys.platform == "win32":
        print xmlToSubmit
        return

    feedApi = mws.Feeds(access_key, secret_key, seller_id, region=region, version="2009-01-01")
    response = feedApi.submit_feed(xmlToSubmit, "_POST_PRODUCT_PRICING_DATA_", marketplaceids=mktids)
    # print response._response_dict
    print 'Submitted product feed: ', response._response_dict

    try:
        os.remove(file)
    except:
        pass


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-t', '--type', type=str, default="product", help='Product Type: product, book or cd')
    parser.add_argument('-m', '--method', type=str, default="product", help='method: register, count, read, readonly')
    parser.add_argument('-p', '--process_no', type=int, default=3, help='method: register, count, read')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()
    print args
    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    country = args.country.upper()
    method = args.method

    get_lock("notification_%s_%s_%s" % (args.account.lower(), country, args.method.lower()))

    account_info = Account.get(name=args.account)

    if account_info is None:
        print "No account info for " + args.account + " found"
        sys.exit()

    database_name = args.account
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    if method == "register":
        register(country, account_info.mws_access_key, account_info.mws_secret_key, account_info.seller_id)
    elif method == "count":
        count_messages(country)
    elif method == 'mapping':
        init_asin_sku_mapping(country, database)
    elif method == 'read':
        read_messages(country, account_info, database, args.process_no)
    else:
        read_messages(country, account_info, database, args.process_no, readonly=True)
