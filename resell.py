import argparse
import csv

import datetime

import StringIO
import requests

from lib.models import WarehouseTracking, Order, SupplierRefund


def parse_args():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-d', '--tracking_date', type=str, default=None, help='Date')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":

    args = parse_args()

    url = "http://dsi.dovertmt.club/index/resellandreturn"
    if args.account is not None:
        url = url + "?account=%s" % args.account
    r = requests.get(url)

    f = StringIO.StringIO(r.content)
    reader = csv.reader(f, delimiter=",")

    row_no = 0
    for row in reader:

        if len(row) == 0 or row[0].lower() == 'account':
            continue
        row_no = row_no + 1
        amazon_order_id = row[1]
        try:
            order = Order.get(Order.amazon_order_id == amazon_order_id)
        except:
            continue

        try:
            refund = SupplierRefund.get(SupplierRefund.fulfill_order_id == order.fulfill_order_id)
        except:
            refund = SupplierRefund()
            refund.fulfill_order_id = order.fulfill_order_id

        status = row[2].decode('gb2312')
        if 'stay' in status.lower():
            refund.refund_status = "Resell"
            refund.is_supplier_refunded = 1

        if refund.notes is None:
            refund.notes = row[3].decode('gb2312')

        if len(row[4]) > 0:
            refund.return_tracking = row[4].replace('#', '')

        try:
            refund.save()
        except:
            pass
        print row_no, row[0], row[1], status, row[3].decode('gb2312')
