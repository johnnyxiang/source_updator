import argparse
import traceback

from lib.general import *
from lib.models import Account
from lib.mws_feed import submit_flat_file_to_amazon
from local import *


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-c', '--country', type=str, default=None, help='country of the inventory')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-f', '--file', type=str, default=None, help='File')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()

    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    account_info = Account.get(name=args.account)

    if account_info is None:
        print "No account info for " + args.account + " found"
        sys.exit()

    file = args.file
    if os.path.isfile(file):
        file_name = file
    else:
        file_name = APP_ROOT_DIR + "/feeds/" + file + ".txt"

    region = args.country

    try:
        result = submit_flat_file_to_amazon(region, file_name, "_POST_FLAT_FILE_INVLOADER_DATA_",
                                            account_info.mws_access_key,
                                            account_info.mws_secret_key, account_info.seller_id, auth_token=account_info.mws_auth_token)

    except:
        print traceback.format_exc()
