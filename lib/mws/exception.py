class MissingAuthInfoError(ValueError):
	retry = False
	"""No mws auth info found for a specific marketplace
	"""
	def __init__(self, marketplace):
		self.marketplace = marketplace

class MissingActiveAuthError(ValueError):
	retry = False

	"""No active auth found for a specific marketplace
	"""

	def __init__(self, marketplace):
		self.marketplace = marketplace

class AwsAccessKeyNotFoundError(ValueError):
	retry = False
	"""InvalidAccesskeyId error
	"""

class AwsAccessKeyInactiveError(ValueError):
	retry = False
	"""AccessDenied error
	"""

class MarketplaceNotFoundError(ValueError):
	retry = False
	"""Marketplace is not valid
	"""

class MwsError:
	# Refer: https://docs.developer.amazonservices.com/en_US/dev_guide/DG_Errors.html
	error_codes = [
		{
			'code': 'InputStreamDisconnected',
			'http_status': '400',
			'mws_desc': 'There was an error reading the input stream.'
		},
		{
			'code': 'InvalidParameterValue',
			'http_status': '400',
			'mws_desc': 'An invalid parameter value was used, or the request size exceeded the maximum accepted size, or the request expired.'
		},
		{
			'code': 'AccessDenied',
			'http_status': '401',
			'mws_desc': 'Access was denied.'
		},
		{
			'code': 'InvalidAccessKeyId',
			'http_status': '403',
			'mws_desc': 'An invalid AWSAccessKeyId value was used.'
		},
		{
			'code': 'SignatureDoesNotMatch',
			'http_status': '403',
			'mws_desc': "The signature used does not match the server's calculated signature value."
		},
		{
			'code': 'InvalidAddress',
			'http_status': '404',
			'mws_desc': 'An invalid API section or operation value was used, or an invalid path was used.'
		},
		{
			'code': 'InternalError',
			'http_status': '500',
			'mws_desc': 'There was an internal service failure.'
		},
		{
			'code': 'QuotaExceeded',
			'http_status': '503',
			'mws_desc': 'The total number of requests in an hour was exceeded.'
		},
		{
			'code': 'RequestThrottled',
			'http_status': '503',
			'mws_desc': 'The frequency of requests was greater than allowed.'
		}
	]