mws_region_endpoint_mapping = {
    'na': {
        'api_endpoint': 'mws.amazonservices.com',
    },
    'br': {
        'api_endpoint': 'mws.amazonservices.com'
    },
    'eu': {
        'api_endpoint': 'mws-eu.amazonservices.com'
    },
    'in': {
        'api_endpoint': 'mws.amazonservices.in'
    },
    'cn': {
        'api_endpoint': 'mws.amazonservices.com.cn'
    },
    'jp': {
        'api_endpoint': 'mws.amazonservices.jp'
    },
    'au': {
        'api_endpoint': 'mws.amazonservices.com.au'
    }
}

mws_region_marketplace_mapping = {
    'ca': {
        'marketplace': 'A2EUQ1WTGCTBG2',
        'region': 'na'
    },
    'mx': {
        'marketplace': 'A1AM78C64UM0Y8',
        'region': 'na'
    },
    'us': {
        'marketplace': 'ATVPDKIKX0DER',
        'region': 'na'
    },
    'br': {
        'marketplace': 'A2Q3Y263D00KWC',
        'region': 'br'
    },
    'de': {
        'marketplace': 'A1PA6795UKMFR9',
        'region': 'eu'
    },
    'es': {
        'marketplace': 'A1RKKUPIHCS9HS',
        'region': 'eu'
    },
    'fr': {
        'marketplace': 'A13V1IB3VIYZZH',
        'region': 'eu'
    },
    'it': {
        'marketplace': 'APJ6JRA9NG5V4',
        'region': 'eu'
    },
    'uk': {
        'marketplace': 'A1F83G8C2ARO7P',
        'region': 'eu'
    },
    'in': {
        'marketplace': 'A21TJRUUN4KGV',
        'region': 'in'
    },
    'jp': {
        'marketplace': 'A1VC38T7YXB528',
        'region': 'jp'
    },
    'cn': {
        'marketplace': 'AAHKV2X7AFYLW',
        'region': 'cn'
    },
    'au': {
        'marketplace': 'A39IBJ37TRP1C6',
        'region': 'au'
    }
}

def is_valid_marketplace(marketplace):
    return marketplace in mws_region_marketplace_mapping

def is_invalid_marketplace(marketplace):
    return not is_valid_marketplace(marketplace)

def get_region(marketplace):
    if is_invalid_marketplace(marketplace):
        raise ValueError('Invalid marketplace %s' % marketplace)

    return mws_region_marketplace_mapping[marketplace]['region']

def get_host(marketplace):
    if is_invalid_marketplace(marketplace):
        raise ValueError('Invalid marketplace %s' % marketplace)

    return mws_region_endpoint_mapping[get_region(marketplace)]['api_endpoint']

def get_marketplace_id(marketplace):
    if is_invalid_marketplace(marketplace):
        raise ValueError('Invalid marketplace %s' % marketplace)

    return mws_region_marketplace_mapping[marketplace]['marketplace']