import time
import re
import boto.mws.connection

from dropshipping import mws
from dropshipping.mws import marketplace_mapping
from dropshipping.mws import response_parser
from dropshipping.mws.utils import list_unique
from dropshipping.mws.utils import LowestOfferListing
from dropshipping.mws import exception

class AmazonMWS(object):
    def __init__(self):
        self.auth = {}

    def connect(self, aws_access_key_id, aws_secret_access_key, merchant, marketplace):
        host = marketplace_mapping.get_host(marketplace)
        auth_pair = {
            'aws_access_key_id': aws_access_key_id,
            'aws_secret_access_key': aws_secret_access_key,
            'Merchant': merchant,
            'SellerId': merchant,
            'host': host
        }
        connection = boto.mws.connection.MWSConnection(**auth_pair)

        self.add_auth(auth_pair, connection, marketplace, True)

    def get_products_service_status(self, marketplace='us'):
        authes = self.get_authes(marketplace)
        if len(authes) <= 0:
            return dict()

        auth = authes[0]
        resp = auth['connection'].get_products_service_status()
        return response_parser.GetProductsServiceStatusParser.parse(resp)

    def get_lowest_offer_listings_for_asin(
            self, asin_list=[], marketplace='us', condition='All', **kw):
        if asin_list is None or len(asin_list) <= 0:
            raise ValueError("No asin supplied")

        if not self.is_valid_condition(condition):
            raise ValueError("Condition \"%s\" is invalid" % condition)

        lowest_offers = dict()
        active_auth = self.get_active_auth(marketplace)
        marketplace_id = marketplace_mapping.get_marketplace_id(marketplace)
        while True:
            try:
                query = {
                    'MarketplaceId': marketplace_id,
                    'ASINList': list_unique(asin_list)
                }
                if (condition is not None and condition.lower() != 'all' and
                        condition.lower() != 'any'):
                    query['ItemCondition'] = condition

                query.update(kw)

                resp = active_auth['connection'].get_lowest_offer_listings_for_asin(**query)
                parsed_resp = response_parser.GetLowestOfferListingsForAsinParser.parse(resp)
                for offer_listings in parsed_resp['GetLowestOfferListingsForASINResult']:
                    lowest_offer_listing = LowestOfferListing(offer_listings)
                    lowest_offers[lowest_offer_listing.get_asin()] = lowest_offer_listing

                break
            except boto.mws.exception.ResponseError as e:
                retry = self.handle_response_error(e, marketplace=active_auth['marketplace'])
                if retry:
                    continue
                else:
                    break
            except Exception as e:
                mws.log.exception(e)
                raise e

        return lowest_offers

    def get_competitive_pricing_for_asin(self, asin_list, marketplace='us'):
        if len(asin_list) <= 0:
            raise ValueError("No asin supplied")

        active_auth = self.get_active_auth(marketplace)
        marketplace_id = marketplace_mapping.get_marketplace_id(marketplace)
        while True:
            try:
                query = {
                    'MarketplaceId': marketplace_id,
                    'ASINList': list_unique(asin_list)
                }
                resp = active_auth['connection'].get_competitive_pricing_for_asin(**query)
                competitive_pricing = \
                    response_parser.GetCompetitivePricingForASINResultParser.parse(resp)
                break
            except boto.mws.exception.ResponseError as e:
                retry = self.handle_response_error(e, marketplace=active_auth['marketplace'])
                if retry:
                    continue
                else:
                    return dict()
            except Exception as e:
                mws.log.exception(e)
                raise e

        return competitive_pricing

    def get_report(self, marketplace, report_id):
        max_retry = 3
        active_auth = self.get_active_auth(marketplace)
        resp = None
        while max_retry > 0:
            try:
                resp = active_auth['connection'].get_report(ReportId=report_id)
                break
            except Exception as e:
                max_retry -= 1
                mws.log.exception(e)

        return resp

    def get_inventory_report_id(self, marketplace):
        request_report_result = self.request_report(
            marketplace, '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_')
        if 'ReportRequestId' not in request_report_result['ReportRequestInfo']:
            return None

        max_retry = 20
        report_request_id = request_report_result['ReportRequestInfo']['ReportRequestId']
        while max_retry > 0:
            max_retry -= 1
            time.sleep(10)

            report_request_list_result = self.get_report_request_list_for_request_id(
                marketplace, report_request_id)
            report_request_info_found = None
            for report_request_info in report_request_list_result['ReportRequestInfo']:
                if (report_request_info['ReportRequestId'] == report_request_id and
                        report_request_info['ReportType'] == '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_'):
                    report_request_info_found = report_request_info
                    break

            if report_request_info_found is None:
                continue

            if (report_request_info_found['ReportProcessingStatus'] in
                    ['_SUBMITTED_', '_IN_PROGRESS_']):
                time.sleep(60)
                continue

            if (report_request_info_found['ReportProcessingStatus'] in
                    ['_CANCELLED_', '_DONE_NO_DATA_']):
                report_id = None
            else:
                report_id = report_request_info_found['GeneratedReportId']

            return report_id

    def request_report(self, marketplace, report_type, start_date=None, end_date=None):
        marketplace_id = marketplace_mapping.get_marketplace_id(marketplace)
        query = {
            'ReportType': report_type,
            'MarketplaceIdList': [marketplace_id]
        }
        if start_date is not None:
            query['StartDate'] = start_date
        if end_date is not None:
            query['EndDate'] = end_date

        max_retry = 3
        active_auth = self.get_active_auth(marketplace)
        resp = None
        while max_retry > 0:
            try:
                resp = active_auth['connection'].request_report(**query)
                break
            except Exception as e:
                max_retry -= 1
                mws.log.exception(e)

        return response_parser.RequestReportParser.parse(resp)

    def get_report_request_list_for_request_id(self, marketplace, report_request_id):
        if type(report_request_id) is list:
            report_request_id_list = report_request_id
        else:
            report_request_id_list = [report_request_id]

        return self.get_report_request_list(marketplace, report_request_id_list)

    def get_report_request_list(self, marketplace, report_request_id_list,
                                report_type_list=None,
                                report_processing_status_list=None,
                                max_count=10):
        query = {
            'ReportRequestIdList': report_request_id_list
        }
        if report_type_list is not None:
            query['ReportTypeList'] = report_type_list
        if report_processing_status_list is not None:
            query['ReportProcessingStatusList'] = report_processing_status_list
        if max_count > 0 and max_count <= 100:
            query['MaxCount'] = max_count

        max_retry = 3
        active_auth = self.get_active_auth(marketplace)
        resp = None
        while max_retry > 0:
            try:
                resp = active_auth['connection'].get_report_request_list(**query)
                break
            except Exception as e:
                max_retry -= 1
                mws.log.exception(e)

        return response_parser.GetReportRequestListParser.parse(resp)

    def submit_price_and_quantity_feed(self, marketplace, feed):
        query = {
            'FeedType':'_POST_FLAT_FILE_PRICEANDQUANTITYONLY_UPDATE_DATA_',
            'content_type': 'text/plain',
            'FeedContent': feed,
            'MarketplaceIdList': [marketplace_mapping.get_marketplace_id(marketplace)]
        }

        max_retry = 3
        active_auth = self.get_active_auth(marketplace)
        resp = None
        while max_retry > 0:
            try:
                resp = active_auth['connection'].submit_feed(**query)
                break
            except Exception as e:
                max_retry -= 1
                mws.log.exception(e)

        return response_parser.SubmitFeedParser.parse(resp)

    def submit_inventory_loader_feed(self, marketplace, feed):
        query = {
            'FeedType': '_POST_FLAT_FILE_INVLOADER_DATA_',
            'content_type': 'text/plain',
            'FeedContent': feed,
            'MarketplaceIdList': [marketplace_mapping.get_marketplace_id(marketplace)]
        }

        max_retry = 3
        active_auth = self.get_active_auth(marketplace)
        resp = None
        while max_retry > 0:
            try:
                resp = active_auth['connection'].submit_feed(**query)
                break
            except Exception as e:
                max_retry -= 1
                mws.log.exception(e)
                time.sleep(3)

        return response_parser.SubmitFeedParser.parse(resp)

    def is_valid_condition(self, condition):
        """Check if condition is valid.
        Valid conditions are ['New', 'Used', 'Collectible', 'Refurbished', 'Club']
        """
        valid_conditions = ['new', 'used', 'collectible', 'refurbished', 'club']
        if condition.lower() in valid_conditions:
            return True
        else:
            return bool(condition.lower() == 'all' or condition.lower() == 'any')

    def add_auth(self, auth_pair, connection, marketplace, active):
        region = marketplace_mapping.get_region(marketplace)
        if region not in self.auth:
            self.auth[region] = []
            self.auth[region].append({
                'auth_pair': auth_pair,
                'marketplace_id': marketplace_mapping.get_marketplace_id(marketplace),
                'marketplace': marketplace,
                'connection': connection,
                'active': active
            })

    def has_active_auth(self, marketplace):
        try:
            region = marketplace_mapping.get_region(marketplace)
        except ValueError as e:
            return False

        if region not in self.auth or len(self.auth[region]) <= 0:
            return False

        return len([item for item in self.auth[region] if item['active']]) > 0

    def get_authes(self, marketplace):
        try:
            region = marketplace_mapping.get_region(marketplace)
        except ValueError as e:
            raise mws.exception.MarketplaceNotFoundError(e.message)

        if region not in self.auth or len(self.auth[region]) <= 0:
            raise mws.exception.MissingAuthInfoError(marketplace)

        return self.auth[region]

    def get_active_auth(self, marketplace):
        authes = self.get_authes(marketplace)
        active_auth = next((item for item in authes if item['active']), None)
        if active_auth is None:
            raise mws.exception.MissingActiveAuthError(marketplace)

        return active_auth

    def deactivate_auth(self, marketplace):
        try:
            region = marketplace_mapping.get_region(marketplace)
        except ValueError as e:
            return

        if region not in self.auth:
            return
        else:
            for auth in self.auth[region]:
                auth['active'] = False

    def activate_auth(self, marketplace):
        try:
            region = marketplace_mapping.get_region(marketplace)
        except ValueError as e:
            return

        if region not in self.auth:
            return
        else:
            for auth in self.auth[region]:
                auth['active'] = True

    def handle_response_error(self, e, **kw):
        error_info = self.extract_error_info(e)
        mws.log.error('[%s] %s' % (self.__class__.__name__, error_info))
        mws.log.exception(e)

        if 'error_code' not in error_info:
            return False

        if error_info['error_code'] == 'RequestThrottled':
            time.sleep(2)
            return True

        if error_info['error_code'] == 'QuotaExceeded':
            time.sleep(30)
            return True

        if error_info['error_code'] == 'InvalidAccessKeyId':
            if 'marketplace' in kw:
                self.deactivate_auth(kw.get('marketplace'))

            return False

        if error_info['error_code'] == 'AccessDenied':
            if 'marketplace' in kw:
                self.deactivate_auth(kw.get('marketplace'))

            return False

        if error_info['error_code'] == 'InvalidParameterValue':
            result = re.findall(
                "^Seller \[([A-Za-z0-9]+)\] does not have access to the given marketplace \[([A-Za-z0-9]+)\]",
                error_info['message'])
            if len(result) > 0 or error_info['message'].find('AWSAccessKeyId') != -1:
                if 'marketplace' in kw:
                    self.deactivate_auth(kw.get('marketplace'))

            return False

        return False

    def extract_error_info(self, e):
        error_info = dict()
        target_fields = ['error_code', 'status', 'retry', 'reason', 'message', 'body']
        for target_field in target_fields:
            if hasattr(e, target_field):
                error_info[target_field] = getattr(e, target_field)

        return error_info
