from .. import mws

# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 15:42:07 2012

Borrowed from https://github.com/timotheus/ebaysdk-python

@author: pierre
"""

import xml.etree.ElementTree as ET
import re



def list_unique(l):
    """Return a new list with duplicate items removed, not reserve order
    """
    return list({}.fromkeys(l).keys())
    
class object_dict(dict):
    """object view of dict, you can
    >>> a = object_dict()
    >>> a.fish = 'fish'
    >>> a['fish']
    'fish'
    >>> a['water'] = 'water'
    >>> a.water
    'water'
    >>> a.test = {'value': 1}
    >>> a.test2 = object_dict({'name': 'test2', 'value': 2})
    >>> a.test, a.test2.name, a.test2.value
    (1, 'test2', 2)
    """
    def __init__(self, initd=None):
        if initd is None:
            initd = {}
        dict.__init__(self, initd)

    def __getattr__(self, item):

        d = self.__getitem__(item)

        if isinstance(d, dict) and 'value' in d and len(d) == 1:
            return d['value']
        else:
            return d

    # if value is the only key in object, you can omit it
    def __setstate__(self, item):
        return False

    def __setattr__(self, item, value):
        self.__setitem__(item, value)

    def getvalue(self, item, value=None):
        return self.get(item, {}).get('value', value)


class xml2dict(object):

    def __init__(self):
        pass

    def _parse_node(self, node):
        node_tree = object_dict()
        # Save attrs and text, hope there will not be a child with same name
        if node.text:
            node_tree.value = node.text
        for (k, v) in node.attrib.items():
            k, v = self._namespace_split(k, object_dict({'value':v}))
            node_tree[k] = v
        #Save childrens
        for child in node.getchildren():
            tag, tree = self._namespace_split(child.tag,
                                              self._parse_node(child))
            if tag not in node_tree:  # the first time, so store it in dict
                node_tree[tag] = tree
                continue
            old = node_tree[tag]
            if not isinstance(old, list):
                node_tree.pop(tag)
                node_tree[tag] = [old]  # multi times, so change old dict to a list
            node_tree[tag].append(tree)  # add the new one

        return node_tree

    def _namespace_split(self, tag, value):
        """
        Split the tag '{http://cs.sfsu.edu/csc867/myscheduler}patients'
        ns = http://cs.sfsu.edu/csc867/myscheduler
        name = patients
        """
        result = re.compile("\{(.*)\}(.*)").search(tag)
        if result:
            value.namespace, tag = result.groups()

        return (tag, value)

    def parse(self, file):
        """parse a xml file to a dict"""
        f = open(file, 'r')
        return self.fromstring(f.read())

    def fromstring(self, s):
        """parse a string"""
        t = ET.fromstring(s)
        root_tag, root_tree = self._namespace_split(t.tag, self._parse_node(t))
        return object_dict({root_tag: root_tree})

def list_unique(l):
    """Return a new list with duplicate items removed, not reserve order
    """
    return list({}.fromkeys(l).keys())

class OfferListing(object):
    def __init__(self, offer_listing):
        self.offer_listing = offer_listing.copy()

    def get_condition(self):
        condition_key = 'condition'
        if not hasattr(self, condition_key):
            setattr(self, condition_key, None)
            qualifiers = self.offer_listing['Qualifiers']
            if 'ItemCondition' in qualifiers:
                setattr(self, condition_key, qualifiers['ItemCondition'])

        return getattr(self, condition_key)

    def get_subcondition(self):
        subcondition_key = 'subcondition'
        if not hasattr(self, subcondition_key):
            setattr(self, subcondition_key, None)
            qualifiers = self.offer_listing['Qualifiers']
            if 'ItemSubcondition' in qualifiers:
                setattr(self, subcondition_key, qualifiers['ItemSubcondition'])

        return getattr(self, subcondition_key)

    def get_ships_domestically(self):
        ships_domestically_key = 'ships_domestically'
        if not hasattr(self, ships_domestically_key):
            setattr(self, ships_domestically_key, None)
            qualifiers = self.offer_listing['Qualifiers']
            if 'ShipsDomestically' in qualifiers:
                if qualifiers['ShipsDomestically'] == 'False':
                    setattr(self, ships_domestically_key, False)
                else:
                    setattr(self, ships_domestically_key, True)

        return getattr(self, ships_domestically_key)

    def get_shipping_time(self):
        shipping_time_key = 'shipping_time'
        if not hasattr(self, shipping_time_key):
            setattr(self, shipping_time_key, None)
            qualifiers = self.offer_listing['Qualifiers']
            if 'ShippingTime' in qualifiers and 'Max' in qualifiers['ShippingTime']:
                setattr(self, shipping_time_key, qualifiers['ShippingTime']['Max'])

        return getattr(self, shipping_time_key)

    def get_rating(self):
        rating_key = 'rating'
        if not hasattr(self, rating_key):
            setattr(self, rating_key, None)
            qualifiers = self.offer_listing['Qualifiers']
            if 'SellerPositiveFeedbackRating' in qualifiers:
                if qualifiers['SellerPositiveFeedbackRating'] == 'Just Launched':
                    rating = {
                        'min': 0,
                        'max': 69
                    }
                elif qualifiers['SellerPositiveFeedbackRating'] == 'Less than 70%':
                    rating = {
                        'min': 60,
                        'max': 69
                    }
                else:
                    rating_min, rating_max = \
                        qualifiers['SellerPositiveFeedbackRating'][:-1].split('-')
                    rating = {
                        'min': int(rating_min),
                        'max': int(rating_max)
                    }
                setattr(self, rating_key, rating)

        return getattr(self, rating_key)

    def get_rating_str(self):
        rating_str_key = 'rating_str'
        if not hasattr(self, rating_str_key):
            setattr(self, rating_str_key, None)
            qualifiers = self.offer_listing['Qualifiers']
            if 'SellerPositiveFeedbackRating' in qualifiers:
                setattr(self, rating_str_key, qualifiers['SellerPositiveFeedbackRating'])

        return getattr(self, rating_str_key)

    def get_feedback_count(self):
        feedback_count_key = 'feedback_count'
        if not hasattr(self, feedback_count_key):
            setattr(self, feedback_count_key, None)
            if 'SellerFeedbackCount' in self.offer_listing:
                setattr(self, feedback_count_key, int(self.offer_listing['SellerFeedbackCount']))

        return getattr(self, feedback_count_key)

    def get_landed_price(self):
        landed_price_key = 'landed_price'
        if not hasattr(self, landed_price_key):
            setattr(self, landed_price_key, None)
            price = self.offer_listing['Price']
            if 'LandedPrice' in price:
                landed_price = dict()
                if 'CurrencyCode' in price['LandedPrice']:
                    landed_price['currency'] = price['LandedPrice']['CurrencyCode']
                else:
                    mws.log.info('[LandedPriceNoCurrency] %s' % self.offer_listing)

                if 'Amount' in price['LandedPrice']:
                    landed_price['amount'] = float(price['LandedPrice']['Amount'])
                else:
                    mws.log.info('[LandedPriceNoAmount] %s' % self.offer_listing)

                setattr(self, landed_price_key, landed_price)

        return getattr(self, landed_price_key)

    def get_listing_price(self):
        listing_price_key = 'listing_price'
        if not hasattr(self, listing_price_key):
            setattr(self, listing_price_key, None)
            price = self.offer_listing['Price']
            if 'ListingPrice' in price:
                listing_price = dict()
                if 'CurrencyCode' in price['ListingPrice']:
                    listing_price['currency'] = price['ListingPrice']['CurrencyCode']
                else:
                    mws.log.info('[ListingPriceNoCurrency] %s' % self.offer_listing)

                if 'Amount' in price['ListingPrice']:
                    listing_price['amount'] = float(price['ListingPrice']['Amount'])
                else:
                    mws.log.info('[ListingPriceNoAmount] %s' % self.offer_listing)

                setattr(self, listing_price_key, listing_price)

        return getattr(self, listing_price_key)

    def get_shipping_price(self):
        shipping_price_key = 'shipping_price'
        if not hasattr(self, shipping_price_key):
            setattr(self, shipping_price_key, None)
            price = self.offer_listing['Price']
            if 'Shipping' in price:
                shipping_price = dict()
                if 'CurrencyCode' in price['Shipping']:
                    shipping_price['currency'] = price['Shipping']['CurrencyCode']
                else:
                    mws.log.info('[ShippingPriceNoCurrency] %s' % self.offer_listing)

                if 'Amount' in price['Shipping']:
                    shipping_price['amount'] = float(price['Shipping']['Amount'])
                else:
                    mws.log.info('[ShippingPriceNoAmount] %s' % self.offer_listing)

                setattr(self, shipping_price_key, shipping_price)

        return getattr(self, shipping_price_key)

    def get_price(self):
        price_key = 'price'
        if not hasattr(self, price_key):
            shipping_price = self.get_shipping_price()
            if shipping_price is not None:
                listing_price = self.get_listing_price()
                setattr(self, price_key, listing_price['amount'] + shipping_price['amount'])
            else:
                setattr(self, price_key, listing_price['amount'])

        return getattr(self, price_key)

    def get_fba(self):
        fba_key = 'fba'
        if not hasattr(self, fba_key):
            qualifiers = self.offer_listing['Qualifiers']
            if 'FulfillmentChannel' in qualifiers:
                if qualifiers['FulfillmentChannel'].lower().find('amazon') != -1:
                    setattr(self, fba_key, True)
                else:
                    setattr(self, fba_key, False)
            else:
                setattr(self, fba_key, False)

        return getattr(self, fba_key)

    def get_offers(self):
        offers_key = 'offers'
        if not hasattr(self, offers_key):
            setattr(self, offers_key, 0)

        return getattr(self, offers_key)

    def set_offers(self, offer_count):
        offers_key = 'offers'
        setattr(self, offers_key, offer_count)

    def __str__(self):
        rep_dict = dict()
        keys = ['rating', 'feedback_count', 'ships_domestically', 'shipping_time', \
            'condition', 'subcondition', 'landed_price', 'listing_price', 'shipping_price', 'fba']
        for offer_key in keys:
            rep_dict[offer_key] = getattr(self, 'get_%s' % offer_key)()

        return str(rep_dict)

class LowestOfferListing(object):
    def __init__(self, lowest_offer_listing):
        self.lowest_offer_listing = lowest_offer_listing.copy()

    def get_asin(self):
        asin_key = 'asin'
        if not hasattr(self, asin_key):
            setattr(self, asin_key, None)
            product = self.lowest_offer_listing['Product']
            if 'Identifiers' in product:
                if 'MarketplaceASIN' in product['Identifiers']:
                    if 'ASIN' in product['Identifiers']['MarketplaceASIN']:
                        setattr(self, asin_key, product['Identifiers']['MarketplaceASIN']['ASIN'])

        return getattr(self, asin_key)

    def get_marketplace_id(self):
        marketplace_id_key = 'marketplace_id'
        if not hasattr(self, marketplace_id_key):
            setattr(self, marketplace_id_key, None)
            product = self.lowest_offer_listing['Product']
            if 'Identifiers' in product:
                if 'MarketplaceASIN' in product['Identifiers']:
                    if 'MarketplaceId' in product['Identifiers']['MarketplaceASIN']:
                        setattr(
                            self, marketplace_id_key,
                            product['Identifiers']['MarketplaceASIN']['MarketplaceId'])

        return getattr(self, marketplace_id_key)

    def get_offer_listings(self):
        offer_listings_key = 'offer_listings'
        if not hasattr(self, offer_listings_key):
            offer_listings = []
            setattr(self, offer_listings_key, offer_listings)

            product = self.lowest_offer_listing['Product']
            if 'LowestOfferListings' in product and \
                'LowestOfferListing' in product['LowestOfferListings']:
                for offer_listing in product['LowestOfferListings']['LowestOfferListing']:
                    offer_listings.append(OfferListing(offer_listing))

        return getattr(self, offer_listings_key)

    def __str__(self):
        keys = ['rating', 'feedback_count', 'ships_domestically', 'shipping_time', 'condition', \
            'subcondition', 'landed_price', 'listing_price', 'shipping_price', 'fba']
        rep_dict = {'asin': self.get_asin(), 'marketplace_id': self.get_marketplace_id()}
        for offer_listing in self.get_offer_listings():
            for offer_key in keys:
                rep_dict[offer_key] = getattr(offer_listing, 'get_%s' % offer_key)()

        return str(rep_dict)

class OfferFilter(object):
    def __init__(self, **kw):
        self.dict_subcondition = {
            'new': 100,
            'mint': 90,
            'very_good': 81,
            'verygood': 80,
            'good': 70,
            'acceptable': 60,
            'poor': 50,
            'club': 40,
            'oem': 30,
            'warranty': 25,
            'refurbishedwarranty': 20,
            'refurbished_warranty': 21,
            'refurbished': 15,
            'open_box': 10,
            'openbox': 11,
            'Other': 0
        }

        self.filter = {
            'rate': 90,
            'review': 100,
            'domestic': None,
            'shipping_time': 2,
            'subcondition': 70,
            'fba': None
        }

        for key in kw:
            if key in self.filter:
                self.filter[key] = kw[key]

    def get_filtered_offers(self, lowest_offer_listing):
        filtered_offers = []

        offer_listings = lowest_offer_listing.get_offer_listings()
        if len(offer_listings) <= 0:
            mws.log.debug('[NoOfferFound] %s - %s' % (lowest_offer_listing, self.filter))
            return filtered_offers

        for offer_listing in offer_listings:
            fba = offer_listing.get_fba()
            if self.filter['fba'] is not None and fba != self.filter['fba']:
                mws.log.debug('[FBA] %s - %s - %s - %s' % (
                    lowest_offer_listing.get_asin(), lowest_offer_listing.get_marketplace_id(),
                    offer_listing, self.filter))
                continue

            ships_domestically = offer_listing.get_ships_domestically()
            if self.filter['domestic'] is not None and \
                ships_domestically != self.filter['domestic']:
                mws.log.debug('[ShipsDomestically] %s - %s - %s - %s' % (
                    lowest_offer_listing.get_asin(), lowest_offer_listing.get_marketplace_id(),
                    offer_listing, self.filter))
                continue

            shipping_time = offer_listing.get_shipping_time()
            if shipping_time is not None:
                if shipping_time.find('-') == -1:
                    days_str = shipping_time.split(' ')[0].strip()
                else:
                    days_str = shipping_time.split('-')[0].strip()

                try:
                    shipping_days = int(days_str)
                except:
                    shipping_days = 0

                if shipping_days > self.filter['shipping_time']:
                    mws.log.debug('[ShippingTime] %s - %s - %s - %s' % (
                        lowest_offer_listing.get_asin(), lowest_offer_listing.get_marketplace_id(),
                        offer_listing, self.filter))
                    continue

            rating = offer_listing.get_rating()
            if rating['min'] < self.filter['rate']:
                mws.log.debug('[Rating] %s - %s - %s - %s' % (
                    lowest_offer_listing.get_asin(), lowest_offer_listing.get_marketplace_id(),
                    offer_listing, self.filter))
                continue

            feedback_count = offer_listing.get_feedback_count()
            if feedback_count < self.filter['review']:
                mws.log.debug('[Feedback] %s - %s - %s - %s' % (
                    lowest_offer_listing.get_asin(), lowest_offer_listing.get_marketplace_id(),
                    offer_listing, self.filter))
                continue


            subcondition = offer_listing.get_subcondition()
            if self.dict_subcondition[subcondition.lower()] < self.filter['subcondition']:
                mws.log.debug('[Subcondition] %s - %s - %s - %s' % (
                    lowest_offer_listing.get_asin(), lowest_offer_listing.get_marketplace_id(),
                    offer_listing, self.filter))
                continue

            filtered_offers.append(offer_listing)

        if len(filtered_offers) > 0:
            filtered_offers_str = [str(filtered_offer) for filtered_offer in filtered_offers]
            mws.log.debug('[FilteredOffer] %s - %s' % ("\n".join(filtered_offers_str), self.filter))

        return filtered_offers

    def get_lowest_priced_filtered_offer(self, lowest_offer_listing):
        lowest_priced_offer = None

        filtered_offers = self.get_filtered_offers(lowest_offer_listing)
        if len(filtered_offers) <= 0:
            return lowest_priced_offer

        for offer in filtered_offers:
            if lowest_priced_offer is None:
                lowest_priced_offer = offer
                continue

            price1 = self.calc_offer_price(lowest_priced_offer)
            price2 = self.calc_offer_price(offer)
            if price1 > price2:
                lowest_priced_offer = offer

        lowest_priced_offer.set_offers(len(filtered_offers))

        return lowest_priced_offer

    def sort_offer_by_price(self, lowest_offer_listing):
        sorted_offer_listing = []
        offer_listings = lowest_offer_listing.get_offer_listings()
        if len(offer_listings) <= 0:
            return sorted_offer_listing

        offer_listings.sort(key=self.calc_offer_price)

        return offer_listings

    def calc_offer_price(self, offer):
        listing_price = offer.get_listing_price()
        shipping_price = offer.get_shipping_price()

        if 'amount' in listing_price:
            if shipping_price is None:
                price = listing_price['amount']
            else:
                price = listing_price['amount'] + shipping_price['amount']
        else:
            landed_price = offer.get_landed_price()
            price = landed_price['amount']

        return price

class FollowupOfferFilter(OfferFilter):
    def __init__(self, **kw):
        if 'avaliable_sellers' in kw and isinstance(kw['avaliable_sellers'], list):
            self.avaliable_sellers = kw['avaliable_sellers']
        else:
            self.avaliable_sellers = []

        OfferFilter.__init__(self, **kw)

    def get_filtered_offers(self, lowest_offer_listing):
        filtered_offers = []

        offer_listings = lowest_offer_listing.get_offer_listings()
        if len(offer_listings) <= 0:
            mws.log.debug('[NoOfferFound] %s - %s' % (
                lowest_offer_listing, self.avaliable_sellers))
            return filtered_offers

        for offer_listing in offer_listings:
            for seller in self.avaliable_sellers:
                feedback_count = offer_listing.get_feedback_count()
                rating = offer_listing.get_rating()
                if (seller['feedback']['min'] <= feedback_count <= seller['feedback']['max'] and
                        seller['rating']['min'] <= rating['min'] <= seller['rating']['max']):
                    filtered_offers.append(offer_listing)

        if len(filtered_offers) > 0:
            mws.log.debug('[FilteredOffer] %s - %s' % (
                lowest_offer_listing, self.avaliable_sellers))

        return filtered_offers
