class GetProductsServiceStatusParser:
	@classmethod
	def parse(cls, resp):
		result = dict()

		if resp is None or not hasattr(resp, 'GetServiceStatusResult'):
			return result

		status_result = resp.GetServiceStatusResult
		target_attrs = ['Status', 'Messages', 'Timestamp']
		for target_attr in target_attrs:
			if hasattr(status_result, target_attr):
				result[target_attr] = getattr(status_result, target_attr)

		return result

class RequestReportParser:
	@classmethod
	def parse(cls, resp):
		result = dict()

		if resp is None or not hasattr(resp, 'RequestReportResult'):
			return result

		report_request_info = resp.RequestReportResult.ReportRequestInfo
		report_request_info_dict = dict()
		result['ReportRequestInfo'] = report_request_info_dict
		target_attrs = ['ReportRequestId', 'ReportType', 'StartDate', 'EndDate', 'Scheduled', 'SubmittedDate', 'ReportProcessingStatus']
		for target_attr in target_attrs:
			if hasattr(report_request_info, target_attr):
				report_request_info_dict[target_attr] = getattr(report_request_info, target_attr)

		return result

class GetReportRequestListParser:
	@classmethod
	def parse(cls, resp):
		result = dict()

		if resp is None or not hasattr(resp, 'GetReportRequestListResult'):
			return result

		report_request_list_result = resp.GetReportRequestListResult
		if hasattr(report_request_list_result, 'HasNext'):
			result['HasNext'] = getattr(report_request_list_result, 'HasNext')
		if hasattr(report_request_list_result, 'NextToken'):
			result['NextToken'] = getattr(report_request_list_result, 'NextToken')

		if not hasattr(report_request_list_result, 'ReportRequestInfo'):
			return result

		result['ReportRequestInfo'] = []
		report_request_info = report_request_list_result.ReportRequestInfo
		if type(report_request_info) is not list:
			report_request_info = [report_request_info]

		target_attrs = ['ReportRequestId', 'ReportType', 'StartDate', 'EndDate', 'Scheduled', 'SubmittedDate', 'ReportProcessingStatus', 'GeneratedReportId', 'StartedProcessingDate', 'CompletedDate']
		for item in report_request_info:
			item_dict = dict()
			for target_attr in target_attrs:
				if hasattr(item, target_attr):
					item_dict[target_attr] = getattr(item, target_attr)
			result['ReportRequestInfo'].append(dict(item_dict))

		return result

class GetLowestOfferListingsForAsinParser:
	@classmethod
	def parse_product_lowest_offer_listing_qualifiers(cls, product_offer_listing_info):
		product_lowest_offer_listing_qualifiers = dict()
		if not hasattr(product_offer_listing_info, 'Qualifiers'):
			return product_lowest_offer_listing_qualifiers

		qualifiers = product_offer_listing_info.Qualifiers
		if hasattr(qualifiers, 'ItemCondition'):
			product_lowest_offer_listing_qualifiers['ItemCondition'] = qualifiers.ItemCondition

		if hasattr(qualifiers, 'ItemSubcondition'):
			product_lowest_offer_listing_qualifiers['ItemSubcondition'] = qualifiers.ItemSubcondition

		if hasattr(qualifiers, 'FulfillmentChannel'):
			product_lowest_offer_listing_qualifiers['FulfillmentChannel'] = qualifiers.FulfillmentChannel

		if hasattr(qualifiers, 'ShipsDomestically'):
			product_lowest_offer_listing_qualifiers['ShipsDomestically'] = qualifiers.ShipsDomestically

		if hasattr(qualifiers, 'ShippingTime'):
			product_lowest_offer_listing_qualifiers['ShippingTime'] = dict()
			if hasattr(qualifiers.ShippingTime, 'Max'):
				product_lowest_offer_listing_qualifiers['ShippingTime']['Max'] = qualifiers.ShippingTime.Max

		if hasattr(qualifiers, 'SellerPositiveFeedbackRating'):
			product_lowest_offer_listing_qualifiers['SellerPositiveFeedbackRating'] = qualifiers.SellerPositiveFeedbackRating

		return product_lowest_offer_listing_qualifiers

	@classmethod
	def parse_product_lowest_offer_listing_price(cls, product_offer_listing_info):
		product_offer_listing_price = dict()
		if not hasattr(product_offer_listing_info, 'Price'):
			return product_offer_listing_price

		price = product_offer_listing_info.Price
		price_classes = ['LandedPrice', 'ListingPrice', 'Shipping']
		price_elements = ['Amount', 'CurrencyCode']

		for price_class in price_classes:
			if hasattr(price, price_class):
				product_offer_listing_price[price_class] = dict()
				for price_element in price_elements:
					if hasattr(getattr(price, price_class), price_element):
						product_offer_listing_price[price_class][price_element] = getattr(getattr(price, price_class), price_element)

		return product_offer_listing_price

	@classmethod
	def parse_product_lowest_offer_listing(cls, product_offer_listing_info):
		product_lowest_offer_listing = dict()
		if product_offer_listing_info is None:
			return product_lowest_offer_listing

		lowest_offer_listing = product_offer_listing_info
		if hasattr(lowest_offer_listing, 'NumberOfOfferListingsConsidered'):
			product_lowest_offer_listing['NumberOfOfferListingsConsidered'] = lowest_offer_listing.NumberOfOfferListingsConsidered

		if hasattr(lowest_offer_listing, 'MultipleOffersAtLowestPrice'):
			product_lowest_offer_listing['MultipleOffersAtLowestPrice'] = lowest_offer_listing.MultipleOffersAtLowestPrice

		if hasattr(lowest_offer_listing, 'SellerFeedbackCount'):
			product_lowest_offer_listing['SellerFeedbackCount'] = lowest_offer_listing.SellerFeedbackCount

		product_lowest_offer_listing['Qualifiers'] = cls.parse_product_lowest_offer_listing_qualifiers(lowest_offer_listing)
		product_lowest_offer_listing['Price'] = cls.parse_product_lowest_offer_listing_price(lowest_offer_listing)

		return product_lowest_offer_listing


	@classmethod
	def parse_product_lowest_offer_listings(cls, product_info):
		product_lowest_offer_listings = dict()
		if not hasattr(product_info, 'LowestOfferListings'):
			return product_lowest_offer_listings

		product_lowest_offer_listings['LowestOfferListing'] = []
		offer_listings = product_info.LowestOfferListings
		if not hasattr(offer_listings, 'LowestOfferListing'):
			return product_lowest_offer_listings

		for offer_listing in offer_listings.LowestOfferListing:
			product_lowest_offer_listings['LowestOfferListing'].append(cls.parse_product_lowest_offer_listing(offer_listing))

		return product_lowest_offer_listings

	@classmethod
	def parse_product_identifiers(cls, product_info):
		product_identifiers = dict()
		if hasattr(product_info, 'Identifiers'):
			identifiers = product_info.Identifiers
			if hasattr(identifiers, 'MarketplaceASIN'):
				product_identifiers['MarketplaceASIN'] = dict()
				if hasattr(identifiers.MarketplaceASIN, 'ASIN'):
					product_identifiers['MarketplaceASIN']['ASIN'] = identifiers.MarketplaceASIN.ASIN

				if hasattr(identifiers.MarketplaceASIN, 'MarketplaceId'):
					product_identifiers['MarketplaceASIN']['MarketplaceId'] = identifiers.MarketplaceASIN.MarketplaceId

		return product_identifiers

	@classmethod
	def parse_product(cls, product_info):
		product = dict()
		product['Identifiers'] = cls.parse_product_identifiers(product_info)
		product['LowestOfferListings'] = cls.parse_product_lowest_offer_listings(product_info)
		return product

	@classmethod
	def parse(cls, resp):
		result = {
			'GetLowestOfferListingsForASINResult': []
		}

		if resp is None or not hasattr(resp, 'GetLowestOfferListingsForASINResult'):
			return result

		all_offer_listings = resp.GetLowestOfferListingsForASINResult
		for offer_listings in all_offer_listings:
			offer_listings_item = dict()
			offer_listings_item['ASIN'] = offer_listings['ASIN']
			offer_listings_item['status'] = offer_listings['status']

			if hasattr(offer_listings, 'AllOfferListingsConsidered'):
				offer_listings_item['AllOfferListingsConsidered'] = offer_listings.AllOfferListingsConsidered

			if hasattr(offer_listings, 'Product'):
				offer_listings_item['Product'] = cls.parse_product(offer_listings.Product)
			else:
				offer_listings_item['Product'] = dict()

			result['GetLowestOfferListingsForASINResult'].append(offer_listings_item)

		return result

class GetCompetitivePricingForASINResultParser:
	@classmethod
	def parse_product_identifiers(cls, product_info):
		product_identifiers = dict()
		if hasattr(product_info, 'Identifiers'):
			identifiers = product_info.Identifiers
			if hasattr(identifiers, 'MarketplaceASIN'):
				product_identifiers['MarketplaceASIN'] = dict()
				if hasattr(identifiers.MarketplaceASIN, 'ASIN'):
					product_identifiers['MarketplaceASIN']['ASIN'] = identifiers.MarketplaceASIN.ASIN

				if hasattr(identifiers.MarketplaceASIN, 'MarketplaceId'):
					product_identifiers['MarketplaceASIN']['MarketplaceId'] = identifiers.MarketplaceASIN.MarketplaceId

		return product_identifiers

	@classmethod
	def parse_product_competitive_pricing_price(cls, info):
		price_info = dict()
		if not hasattr(info, 'Price'):
			return price_info

		price = info.Price
		price_classes = ['LandedPrice', 'ListingPrice', 'Shipping']
		price_elements = ['Amount', 'CurrencyCode']

		for price_class in price_classes:
			if hasattr(price, price_class):
				price_info[price_class] = dict()
				for price_element in price_elements:
					if hasattr(getattr(price, price_class), price_element):
						price_info[price_class][price_element] = getattr(getattr(price, price_class), price_element)

		return price_info

	@classmethod
	def parse_product_competitive_pricing(cls, product_info):
		product_competitive_pricing = []
		if not hasattr(product_info, 'CompetitivePricing'):
			return product_competitive_pricing

		competitive_pricings = product_info.CompetitivePricing
		for competitive_pricing in competitive_pricings:
			competitive_pricing_item = dict()
			# TODO
			# if hasattr(competitive_pricing, 'NumberOfOfferListings'):
			# 	competitive_pricing_item['NumberOfOfferListings'] = {
			# 		'OfferListingCount': []
			# 	}
			# 	for offer_listing_count in competitive_pricing.NumberOfOfferListings.OfferListingCount:
			# 		competitive_pricing_item['NumberOfOfferListings']['OfferListingCount']['condition'] = offer_listing_count['condition']

			if hasattr(competitive_pricing, 'OfferListingCount'):
				competitive_pricing_item['OfferListingCount'] = competitive_pricing.OfferListingCount

			if hasattr(competitive_pricing, 'TradeInValue'):
				competitive_pricing_item['TradeInValue'] = competitive_pricing.TradeInValue

			competitive_pricing_item['CompetitivePrices'] = dict()
			if hasattr(competitive_pricing, 'CompetitivePrices'):
				competitive_prices = competitive_pricing.CompetitivePrices
				competitive_pricing_item['CompetitivePrices']['CompetitivePrice'] = []
				if hasattr(competitive_prices, 'CompetitivePrice'):
					competitive_price = competitive_prices.CompetitivePrice
					for v_competitive_price in competitive_price:
						competitive_price_item = dict()
						if hasattr(v_competitive_price, 'CompetitivePriceId'):
							competitive_price_item['CompetitivePriceId'] = v_competitive_price.CompetitivePriceId

						competitive_price_item['Price'] = cls.parse_product_competitive_pricing_price(v_competitive_price)
						competitive_price_item['belongsToRequester'] = v_competitive_price['belongsToRequester']
						competitive_price_item['condition'] = v_competitive_price['condition']
						competitive_price_item['subcondition'] = v_competitive_price['subcondition']
						competitive_pricing_item['CompetitivePrices']['CompetitivePrice'].append(competitive_price_item)

			product_competitive_pricing.append(competitive_pricing_item)

		return product_competitive_pricing

	@classmethod
	def parse_product_sales_ranking(cls, product_info):
		sales_ranking = dict()
		if not hasattr(product_info, 'SalesRanking'): 
			return sales_ranking

		sales_ranking['SalesRank'] = []
		if hasattr(product_info['SalesRanking'], 'SalesRank'):
			for sales_rank in product_info['SalesRanking']['SalesRank']:
				sales_ranking['SalesRank'].append({
					'ProductCategoryId': sales_rank.ProductCategoryId,
					'Rank': sales_rank.Rank
				})

		return sales_ranking
	
	@classmethod
	def parse_product(cls, product_info):
		product = dict()
		if product_info is None:
			return product

		product['Identifiers'] = cls.parse_product_identifiers(product_info)
		product['CompetitivePricing'] = cls.parse_product_competitive_pricing(product_info)
		product['SalesRankings'] = cls.parse_product_sales_ranking(product_info)

		return product

	@classmethod
	def parse(cls, resp):
		result = {
			'GetCompetitivePricingForASINResult': []
		}

		if resp is None or not hasattr(resp, 'GetCompetitivePricingForASINResult'):
			return result

		all_compete_pricings = resp.GetCompetitivePricingForASINResult
		for compete_pricings in all_compete_pricings:
			compete_pricings_item = dict()
			compete_pricings_item['ASIN'] = compete_pricings['ASIN']
			compete_pricings_item['status'] = compete_pricings['status']

			if hasattr(compete_pricings, 'Product'):
				compete_pricings_item['Product'] = cls.parse_product(compete_pricings.Product)
			else:
				compete_pricings_item['Product'] = dict()

			result['GetCompetitivePricingForASINResult'].append(compete_pricings_item)

		return result

class SubmitFeedParser:
	@classmethod
	def parse(cls, resp):
		# TODO
		return resp