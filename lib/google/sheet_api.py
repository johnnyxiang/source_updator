import time
import traceback

from gspread import WorksheetNotFound, CellNotFound
from gspread.exceptions import APIError
from oauth2client.client import OAuth2Credentials

from lib.google.service_provider import GoogleServiceProvider
import gspread


class SheetAPI(object):
    TMM_EMAIL = 'olivetrnd153@gmail.com'
    SERVICE_TYPE = 'sheets'
    service = None

    def __init__(self):
        service_provider = GoogleServiceProvider()
        credentials = service_provider.get_creds(self.TMM_EMAIL, self.SERVICE_TYPE)
        credentials = OAuth2Credentials(None, credentials.client_id, credentials.client_secret, credentials.refresh_token, None,
                                        credentials.token_uri, "")
        self.service = gspread.authorize(credentials)

    def get_worksheet(self, spreadsheet_id, sheet_name):

        while True:
            try:
                return self.service.open_by_key(spreadsheet_id).worksheet(sheet_name)
            except APIError as e:
                print e.message
                if "Quota exceeded" in e.message:
                    time.sleep(30)
                raise APIError(e)

    def create_new_sheet_if_not_existed(self, spreadsheet_id, sheet_name, template_name='Template'):
        # check if existed
        gc = self.service.open_by_key(spreadsheet_id)
        try:
            worksheet = gc.worksheet(sheet_name)
            print 'sheet %s already existed' % sheet_name
            return worksheet
        except APIError as e:
            print e.message
            time.sleep(30)
            return self.create_new_sheet_if_not_existed(spreadsheet_id, sheet_name, template_name=template_name)
        except WorksheetNotFound as e:
            print 'sheet %s not existed, will try to create' % sheet_name

        try:
            template_worksheet = gc.worksheet(template_name)
        except WorksheetNotFound as e:
            raise Exception('Template sheet %s not found' % template_name)

        return template_worksheet.duplicate(new_sheet_name=sheet_name)

    @staticmethod
    def row_values(worksheet, row, value_render_option='FORMATTED_VALUE'):
        tried = 0
        while True:
            tried = tried + 1
            try:
                return worksheet.row_values(row, value_render_option=value_render_option)
            except APIError as e:
                if "Quota exceeded" in e.message:
                    time.sleep(30)
                    continue
            except CellNotFound as e:
                return None

            if tried >= 3:
                return None

    @staticmethod
    def col_values(worksheet, col, value_render_option='FORMATTED_VALUE'):
        tried = 0
        while True:
            tried = tried + 1
            try:
                return worksheet.col_values(col, value_render_option=value_render_option)
            except APIError as e:
                if "Quota exceeded" in e.message:
                    time.sleep(30)
                    continue
            except CellNotFound as e:
                return None

            if tried >= 3:
                return None

    @staticmethod
    def find_by_key(worksheet, key):
        tried = 0
        while True:
            tried = tried + 1
            try:
                cell = worksheet.find(key)
                return cell
            except APIError as e:
                if "Quota exceeded" in e.message:
                    time.sleep(30)
                    continue
            except CellNotFound as e:
                return None

            if tried >= 3:
                return None

    @staticmethod
    def update_cell(worksheet, row, col, value):
        while True:
            try:
                worksheet.update_cell(row, col, value)
                return
            except APIError as e:
                print e.message
                if "Quota exceeded" in e.message:
                    time.sleep(30)
                raise APIError(e)

    def update_acell(self, worksheet, label, value):
        while True:
            try:
                worksheet.update_acell(label, value)
                return
            except APIError as e:
                print e.message
                if "Quota exceeded" in e.message:
                    time.sleep(30)
                raise APIError(e)

    # Quota exceeded for quota group
    def append_or_insert_row(self, worksheet, key, values):

        cell = self.find_by_key(worksheet, key)
        if cell is None:
            worksheet.append_row(values)
            return

        col = 0
        for value in values:
            col = col + 1
            if col == 1 or value is None:
                continue
            worksheet.update_cell(cell.row, col, value)


if __name__ == '__main__':
    sheet_api = SheetAPI()
    spreadsheet_id = "1VNL3uMJagogGv69MbSuHf6HSLg1HKO-08ETNSz9oye4"
    # print sheet_api.get_spreadsheet(spreadsheet_id)
    # rint SheetAPI.get_service().open_by_key(spreadsheet_id)
    sheet_api.create_new_sheet_if_not_existed(spreadsheet_id, sheet_name='03/01')
