from lib.google.appscript import reload_order, mark_gray_for_order
from lib.google.sheet_api import SheetAPI
from lib.models import Order


def sheet_name_from_date(sheet_date):
    try:
        return sheet_date.strftime('%m/%d')
    except:
        return sheet_date


class OrderSheetService(SheetAPI):
    def fill_refund_success_info(self, order=None, order_id=None):
        if order is None and order_id is None:
            return

        if order_id is not None:
            order = Order.get(Order.amazon_order_id == order_id)

        order = reload_order(order)

        sheet_name = sheet_name_from_date(order.sheet_date)
        worksheet = self.get_worksheet(order.sheet_id, sheet_name)

        # update status
        status = 'finish'
        row = order.row_no
        self.update_acell(worksheet, label="A%s" % row, value=status)
        print 'order %s at sheet %s row % status updated %s' % (order.amazon_order_id, sheet_name, row, status)
        # update message
        msg = "Fully refunded."
        if order.fulfill_order_id != '':
            msg = order.fulfill_order_id + ". " + msg

        self.update_acell(worksheet, label="AD%s" % row, value=msg)
        print 'order %s at sheet %s row % order info updated %s' % (order.amazon_order_id, msg, row, status)

        # mark gray
        if order.is_fulfilled <= 0:
            mark_gray_for_order(order)

    def fill_cancellation_success_info(self, order=None, order_id=None):
        if order is None and order_id is None:
            return

        if order_id is not None:
            order = Order.get(Order.amazon_order_id == order_id)

        order = reload_order(order)

        sheet_name = sheet_name_from_date(order.sheet_date)
        worksheet = self.get_worksheet(order.sheet_id, sheet_name)

        # update status
        status = 'finish'
        row = order.row_no
        label = "A%s" % row
        self.update_acell(worksheet, label=label, value=status)
        print 'order %s at sheet %s row % status updated %s' % (order.amazon_order_id, sheet_name, row, status)
        # update remark
        if 'cancel' not in order.remark:
            order.remark = "buyer cancelled " + order.remark

        self.update_acell(worksheet, label="S%s" % row, value=order.remark)

        print 'order %s at sheet %s row % remark updated %s' % (order.amazon_order_id, sheet_name, row, order.remark)

        # mark gray
        mark_gray_for_order(order)


if __name__ == '__main__':
    service = OrderSheetService()
    service.fill_cancellation_success_info(order_id='113-5692483-1849828')
