import os

root_dir = (os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), '..')) + '/')

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = root_dir + "/token/google.json"

# Imports the Google Cloud client library
from google.cloud import storage

# Instantiates a client
storage_client = storage.Client()

bucket_name = 'oms-cs'
region = 'us-east'
bucket = storage_client.get_bucket(bucket_name)
