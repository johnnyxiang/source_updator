from lib.models import Order
import urllib, json

APP_SCRIPT_URL = "https://script.google.com/macros/s/AKfycby3oR8IH5Z7uaTi-i_GPUfWeeFV96ejxfyo3dlq8tXZivpW51F0/exec"


def reload_order(order):
    orders = read_orders(order.sheet_id, sheet_name_from_date(order.sheet_date))

    rows = []
    row_nos = []
    colors = []
    index = 0
    for row in orders['orders']:
        if row[1] == order.amazon_order_id:
            rows.append(row)
            colors.append(orders['colors'][index])
            row_nos.append(index + 2)
        index += 1

    if len(rows) == 0:
        return order

    if len(rows) == 1:
        order.row_no = row_nos[0]
        order.remark = row[18]

    return order


def read_orders(spread_id, sheet_name):
    params = {
        's': spread_id,
        'sn': sheet_name,
        'method': 'READ',
        'r': 'A-AO'
    }

    response = get(params)
    return response


def sheet_name_from_date(sheet_date):
    try:
        return sheet_date.strftime('%m/%d')
    except:
        return sheet_date


def http_build_query(parameters):
    return urllib.urlencode(parameters)
    return "&".join("%s=%s" % (key, parameters[key])
                    for key in parameters.keys())


def get(params, max_try=3):
    url = APP_SCRIPT_URL + '?' + http_build_query(params)
    while max_try > 0:
        try:
            response = urllib.urlopen(url)
            text = response.read()
            return json.loads(text)
        except:
            max_try -= 1


# color - Gray("#cccccc");Finished("#ffffff");InvalidByCode("#df7401")
def mark_color(spread_id, sheet_name, row, color, notation=None):
    params = {
        's': spread_id,
        'sn': sheet_name,
        'row': row,
        'method': 'BGCOLOR',
        'code': color
    }

    if notation is not None:
        params['r'] = notation

    response = get(params)
    return response


def mark_color_for_order(order, color):
    if isinstance(order, str):
        order = Order.get(Order.amazon_order_id == order)

    if order.row_no is None:
        order = reload_order(order)
    mark_color(order.sheet_id, sheet_name_from_date(order.sheet_date), order.row_no, color, "A%s:AO%s" % (order.row_no, order.row_no))


def mark_gray_for_order(order):
    mark_color_for_order(order, "#cccccc")


if __name__ == '__main__':
    mark_gray_for_order('114-4238506-4957847')
