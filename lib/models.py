import os
import traceback

from bs4 import BeautifulSoup
from peewee import *
from playhouse.db_url import connect
import json
from config import *
from lib.general import APP_ROOT_DIR
from lib.utils.config_tool import ConfigTool
from lib.utils.helper import Helper
import time

database = MySQLDatabase(None)
accountDB = connect('mysql://' + databaseUser + ':' + databasePW + '@' + databaseHost + ':3306/' + accountsDBName,
                    charset='utf8mb4')
accountDBSave = connect('mysql://' + databaseUser + ':' + databasePW + '@' + databaseHost + ':3306/' + accountsDBName,
                        charset='utf8mb4')
omsDB = connect('mysql://' + omsDatabaseUser + ':' + omsDatabasePW + '@' + omsDatabaseHost + ':3306/' + omsDBName,
                charset='utf8mb4')
dataDB = connect('mysql://' + databaseUser + ':' + databasePW + '@' + databaseHost + ':3306/data', charset='utf8mb4')
dataDBSave = connect('mysql://' + databaseUser + ':' + databasePW + '@' + databaseHost + ':3306/data',
                     charset='utf8mb4')

shopifyDB = connect('mysql://' + databaseUser + ':' + databasePW + '@' + databaseHost + ':3306/shopify', charset='utf8mb4')

reviewDB = connect('mysql://' + databaseUser + ':' + databasePW + '@' + databaseHost + ':3306/amazon_reviews', charset='utf8mb4')


# accountDB.cursor()._defer_warnings = True


#
def get_last_id(type, condition='new', country='us'):
    country = country.lower()
    condition = condition.lower()
    type = type.lower()

    sql = "SELECT last_id FROM source_price_ids_new WHERE index_name = %s AND country = %s AND index_condition = %s  FOR UPDATE"
    cursor = accountDB.execute_sql(sql, [type, country, condition])
    row = cursor.fetchone()
    if row is None:
        sql = "INSERT INTO source_price_ids_new(index_name,country,index_condition) VALUES (%s, %s, %s)"
        accountDB.execute_sql(sql, [type, country, condition])
        return 0
    return row[0]


def save_last_id(type, id, condition='new', country='us'):
    while True:
        try:
            sql = "UPDATE source_price_ids_new SET last_id = %s WHERE  index_name = %s AND country = %s AND index_condition = %s"
            accountDB.execute_sql(sql, [id, type, country, condition])
            break
        except:
            print traceback.format_exc()
            time.sleep(10)
            pass


class BaseModel(Model):
    def __str__(self):
        r = {}
        for k in self.__data__.keys():
            try:
                r[k] = str(getattr(self, k))
            except:
                r[k] = json.dumps(getattr(self, k))
        return str(r)

    class Meta:
        database = database


class Product(BaseModel):
    real_asin = CharField()
    fake_asin = CharField()
    real_new_price = DecimalField()
    real_new_price_to_update = DecimalField()
    real_price_last_checked = DateTimeField()
    sku = CharField()
    status = IntegerField()

    class Meta:
        db_table = 'products'


class RealProduct(BaseModel):
    asin = CharField()
    name = CharField()
    brand = CharField()
    merchant = CharField()
    sellers = IntegerField()
    weight = DecimalField()
    width = IntegerField()
    height = IntegerField()
    length = IntegerField()
    price = DecimalField()
    is_prime = IntegerField()
    binding = CharField()
    info_last_checked = DateTimeField()

    class Meta:
        db_table = 'product_real_asins'


class Blacklist(BaseModel):
    asin = CharField()

    class Meta:
        db_table = 'blacklist'


class AccountEmail(BaseModel):
    uid = CharField()
    AmazonOrderId = CharField()
    subject = CharField()
    fromName = CharField()
    fromAddress = CharField()
    toName = CharField()
    toAddress = CharField()
    messageId = CharField()
    in_reply_to = CharField()
    references = CharField()
    email = TextField()
    created_at = DateTimeField()
    copyright_checked = IntegerField()
    status = CharField()
    type = CharField()

    def email_content(self):
        content = self.email
        if "<head" in content:
            helper = Helper()
            html_parser = 'html.parser'
            soup = BeautifulSoup(content, html_parser)
            content = helper.find_tag(soup, 'body').text.strip()

        return content

    class Meta:
        db_table = 'emails'


class OMSBaseModel(Model):
    def __str__(self):
        r = {}
        for k in self.__data__.keys():
            try:
                r[k] = str(getattr(self, k))
            except:
                r[k] = json.dumps(getattr(self, k))
        return str(r)

    class Meta:
        database = omsDB


class SellerAccount(OMSBaseModel):
    code = CharField()
    name = CharField()
    email = CharField()
    cs_name = CharField()
    gmail_token = CharField()
    email_name = CharField()

    class Meta:
        db_table = 'seller_accounts'


class AmazonOrder(OMSBaseModel):
    account_code = CharField()
    AmazonOrderId = CharField()
    OrderStatus = CharField()
    PurchaseDate = DateTimeField()
    purchase_date = DateField()
    LastUpdateDate = DateTimeField()
    SalesChannel = CharField()
    OrderTotal = DecimalField()
    OrderTotalUSD = DecimalField()
    Currency = CharField()
    BuyerName = CharField()
    BuyerEmail = CharField()
    dataJSON = CharField()
    selfOrder = IntegerField()
    businessOrder = IntegerField()
    invoiceSent = IntegerField()
    creditNoteSent = IntegerField()

    class Meta:
        db_table = 'amazon_orders'


class AmazonOrderItem(OMSBaseModel):
    AmazonOrderId = CharField()
    ASIN = CharField()
    isbn = CharField()
    OrderItemId = CharField()
    SellerSKU = CharField()
    fba = IntegerField()
    Title = CharField()
    QuantityOrdered = IntegerField()
    ItemPrice = DecimalField()
    Currency = CharField()
    ShippingPrice = DecimalField()
    ItemPriceUSD = DecimalField()
    ShippingPriceUSD = DecimalField()
    exchange_rate = DecimalField()
    dataJSON = CharField()
    sku_pattern = CharField()

    class Meta:
        db_table = 'amazon_order_items'


class Email(OMSBaseModel):
    account_code = CharField()
    gmailId = CharField()
    threadId = CharField()
    labelIds = CharField()
    historyId = IntegerField()
    AmazonOrderId = CharField()
    subject = CharField()
    snippet = CharField()
    fromName = CharField()
    fromAddress = CharField()
    toName = CharField()
    toAddress = CharField()
    messageId = CharField()
    in_reply_to = CharField()
    references = CharField()
    email = TextField()
    created_at = DateTimeField()
    status = CharField()
    category = CharField()
    read = IntegerField()
    replied = IntegerField()
    attachments = CharField()

    def email_content(self):
        content = self.email
        if "<head" in content:
            helper = Helper()
            html_parser = 'html.parser'
            soup = BeautifulSoup(content, html_parser)
            content = helper.find_tag(soup, 'body').text.strip()

        return content

    class Meta:
        db_table = 'emails'


class EmailThread(OMSBaseModel):
    account_code = CharField()
    threadId = CharField()
    historyId = IntegerField()
    messageCount = IntegerField()
    snippet = CharField()
    read = IntegerField()
    people = CharField()
    subject = CharField()
    created_at = DateTimeField()
    updated_at = DateTimeField()
    firstMsgId = CharField()
    category = CharField()
    fromName = CharField()
    amazonOrderId = CharField()

    class Meta:
        db_table = 'email_threads'


class EmailKeyword(OMSBaseModel):
    keyword = CharField()
    type = CharField()

    class Meta:
        db_table = 'email_keywords'


class FinanceEvent(OMSBaseModel):
    amazon_order_id = CharField()
    sku = CharField()
    posted_date = DateTimeField()
    type = CharField()
    amount = DecimalField()
    currency = CharField()
    group = CharField()

    class Meta:
        db_table = 'finance_events'


class Feedback(OMSBaseModel):
    amazon_order_id = CharField()
    account_code = CharField()
    country = CharField()
    date = DateField()
    rating = IntegerField()
    comment = CharField()
    removed = IntegerField()

    class Meta:
        db_table = 'feedbacks'


class AccountPerformance(OMSBaseModel):
    account_code = CharField()
    country = CharField()
    SalesChannel = CharField()
    date = DateField()
    odr = DecimalField()
    odr_start = DateField()
    odr_end = DateField()
    orderCount = IntegerField()
    orderWithDefects = IntegerField()
    negativeFeedbacks = IntegerField()
    a_z_claims = IntegerField()
    chargebacks = IntegerField()
    lateShipment = DecimalField()
    preFulfillmentCancellation = DecimalField()
    refunds = DecimalField()
    validTracking = DecimalField()
    onTimeDelivery = DecimalField()
    averageResponseTimeInHours = DecimalField()
    intellectualPropertyData = IntegerField()
    noResponseForContactsOlderThan24Hours = IntegerField()
    responseTimeGreaterThan24Hours = IntegerField()
    feedbacks = TextField()
    totalFeedback = IntegerField()
    positive30 = DecimalField()
    positive90 = DecimalField()
    positive12 = DecimalField()
    positive_lifetime = DecimalField()
    count30 = IntegerField()
    count90 = IntegerField()
    count12 = IntegerField()
    order30 = IntegerField()

    class Meta:
        db_table = 'account_performance'


class WarehouseTracking(OMSBaseModel):
    amazon_order_id = CharField()
    ship_date = DateField()
    carrier = CharField()
    tracking = CharField()
    shipment_status = CharField()
    last_checked = DateTimeField()
    detail_tracking_info = CharField()
    postal_provider = CharField()
    last_tracking_status_updated_at = DateTimeField()
    delivered = IntegerField()

    class Meta:
        db_table = 'warehouse_trackings'


class OrderTracking(OMSBaseModel):
    amazon_order_id = CharField()
    ship_date = DateField()
    carrier = CharField()
    tracking = CharField()
    shipment_status = CharField()
    last_checked = DateTimeField()
    detail_tracking_info = CharField()
    postal_provider = CharField()
    last_tracking_status_updated_at = DateTimeField()
    delivered = IntegerField()

    class Meta:
        db_table = 'order_trackings'


class AtoZ(OMSBaseModel):
    amazon_order_id = CharField()
    created_at = DateTimeField()
    last_updated = DateTimeField()
    az_status = CharField()

    class Meta:
        db_table = 'atozs'


class ChargeBack(OMSBaseModel):
    amazon_order_id = CharField()
    created_at = DateTimeField()
    last_updated = DateTimeField()
    az_status = CharField()

    class Meta:
        db_table = 'chargebacks'


class SupplierRefund(OMSBaseModel):
    fulfill_order_id = CharField()
    created_at = DateTimeField()
    updated_at = DateTimeField()
    refund_status = CharField()
    notes = CharField()
    return_tracking = CharField()
    is_supplier_refunded = IntegerField()

    class Meta:
        db_table = 'supplier_refunds'


class Order(OMSBaseModel):
    amazon_order_id = CharField()
    fulfill_order_id = CharField()
    fulfill_account = CharField()
    account_code = CharField()
    sheet_date = DateField()
    sheet_id = CharField()
    is_gray = IntegerField()
    is_fulfilled = IntegerField()
    remark = CharField()

    row_no = None

    class Meta:
        db_table = 'orders'


class CSEmailReport(OMSBaseModel):
    account = CharField()
    date = DateField()
    cs = CharField()
    emails = IntegerField()

    class Meta:
        db_table = 'cs_email_reports'


class ListingStat(OMSBaseModel):
    account = CharField()
    date = DateField()
    sku = CharField()
    country = CharField()
    total = IntegerField()
    active = IntegerField()
    orders = IntegerField()
    condition = CharField()
    product_type = CharField()

    class Meta:
        db_table = 'listing_stats'


class AccountBaseModel(Model):
    def __str__(self):
        r = {}
        for k in self._data.keys():
            try:
                r[k] = str(getattr(self, k))
            except:
                r[k] = json.dumps(getattr(self, k))
        return str(r)

    class Meta:
        database = accountDB


class AccountModel(AccountBaseModel):
    name = CharField()
    code = CharField()
    book_template_name = CharField()
    product_template_name = CharField()
    cd_template_name = CharField()
    rule_name = CharField()
    book_rule_name = CharField()
    book_tax_code = CharField()
    product_tax_code = CharField()
    cd_tax_code = CharField()
    types = CharField()
    country = CharField()
    mws_access_key = CharField()
    mws_secret_key = CharField()
    mws_auth_token = CharField()
    seller_id = CharField()
    skip_skus = TextField()
    last_order_updated = DateTimeField()
    last_refund_updated = DateTimeField()
    status = CharField()
    email_address = CharField()
    email_password = CharField()
    gmail_token = CharField()
    email_name = CharField()
    store_name = CharField()
    min_profit_rate = DecimalField()
    min_profit = DecimalField()
    max_price = DecimalField()
    min_sellers = IntegerField()
    default_profit_rate = DecimalField()
    book_min_profit_rate = DecimalField()
    book_min_profit = DecimalField()
    shipping = DecimalField()
    last_blackbrand_id = IntegerField()
    leadtime_to_ship = IntegerField()
    upload_qty = IntegerField()

    class Meta:
        db_table = 'accounts'


class Account(object):
    name = None
    code = None
    seller_id = None
    mws_access_key = None
    mws_secret_key = None
    mws_auth_token = None
    email_address = None
    email_password = None
    gmail_token = None
    book_template_name = None
    product_template_name = None
    cd_template_name = None
    rule_name = None
    book_rule_name = None
    book_tax_code = None
    product_tax_code = None
    cd_tax_code = None
    types = None
    country = None
    skip_skus = None
    last_order_updated = None
    last_refund_updated = None
    status = None
    min_profit_rate = None
    min_profit = None
    default_profit_rate = None
    book_min_profit_rate = None
    book_min_profit = None
    shipping = None
    last_blackbrand_id = None
    leadtime_to_ship = None
    id = 0
    account_mode = None
    min_sellers = 1
    max_price = 200.0
    upload_qty = 3
    email_name = None
    store_name = None

    def __init__(self, account_code):
        try:
            try:
                account = AccountModel.get(name=account_code)
            except:
                account = AccountModel.get(seller_id=account_code)

            self.name = account.name
            self.code = account.code
            self.seller_id = account.seller_id
            self.mws_access_key = account.mws_access_key
            self.mws_secret_key = account.mws_secret_key
            self.mws_auth_token = account.mws_auth_token
            self.email_address = account.email_address
            self.email_password = account.email_password
            self.book_template_name = account.book_template_name
            self.product_template_name = account.product_template_name
            self.cd_template_name = account.cd_template_name
            self.rule_name = account.rule_name
            self.book_rule_name = account.book_rule_name
            self.book_tax_code = account.book_tax_code
            self.product_tax_code = account.product_tax_code
            self.cd_tax_code = account.cd_tax_code
            self.types = account.types
            self.country = account.country
            self.skip_skus = account.skip_skus
            self.last_order_updated = account.last_order_updated
            self.last_refund_updated = account.last_refund_updated
            self.status = account.status
            self.min_profit_rate = account.min_profit_rate
            self.min_profit = account.min_profit
            self.max_price = account.max_price
            self.default_profit_rate = account.default_profit_rate
            self.book_min_profit_rate = account.book_min_profit_rate
            self.book_min_profit = account.book_min_profit
            self.shipping = account.shipping
            self.last_blackbrand_id = account.last_blackbrand_id
            self.leadtime_to_ship = account.leadtime_to_ship
            self.min_sellers = account.min_sellers
            self.upload_qty = account.upload_qty
            self.id = account.id
            self.account_mode = account
            self.gmail_token = account.gmail_token
            self.email_name = account.email_name
            self.store_name = account.store_name
        except:
            config_file_path = APP_ROOT_DIR + "/config.ini"
            config = ConfigTool(config_file_path)
            config.set_section('account.%s' % account_code.lower())
            self.seller_id = config.get('seller_id')
            if self.seller_id is None:
                raise Exception('No configuration for %s found' % account_code)

            self.country = config.get('country')
            self.mws_access_key = config.get('mws_access_key')
            self.mws_secret_key = config.get('mws_secret_key')
            self.mws_auth_token = config.get('mws_auth_token')
            self.email_address = config.get('seller_email_address')
            self.email_password = config.get('seller_email_password')
            self.name = account_code
            self.code = config.get('code')

    @classmethod
    def get(cls, name=None, seller_id=None):
        if seller_id is not None:
            return Account(account_code=seller_id)
        return Account(account_code=name)

    def save(self):
        if self.account_mode is not None:
            self.account_mode.last_blackbrand_id = self.last_blackbrand_id
            self.account_mode.last_order_updated = self.last_order_updated
            self.account_mode.last_refund_updated = self.last_refund_updated
            self.account_mode.save()


class AutoRepricingSetting(AccountBaseModel):
    account_code = CharField()
    sku = CharField()
    min_profit = DecimalField()
    min_profit_rate = DecimalField()
    default_profit_rate = DecimalField()
    qty = IntegerField()
    rule_name = CharField()
    leadtime_to_ship = IntegerField()
    min_sellers = IntegerField()
    max_price = DecimalField()

    class Meta:
        db_table = 'auto_repricing_settings'


class BlacklistAsin(AccountBaseModel):
    asin = CharField()
    country = CharField()
    type = CharField()
    source = CharField()

    class Meta:
        db_table = 'blacklist_asins'


class BlacklistAsinLog(AccountBaseModel):
    asin = CharField()
    country = CharField()
    account = CharField()
    type = CharField()
    email_id = IntegerField()

    class Meta:
        db_table = 'blacklist_asin_logs'


class BlacklistBrandAll(AccountBaseModel):
    brand = CharField()
    country = CharField()
    created_at = DateTimeField()
    cleared = IntegerField()
    confirmed = IntegerField()
    source = CharField()

    class Meta:
        db_table = 'blacklist_brands_all'


class BlacklistBrand(AccountBaseModel):
    brand = CharField()
    cleared = IntegerField()

    class Meta:
        db_table = 'blacklist_brands'


class BlacklistBrandUK(AccountBaseModel):
    brand = CharField()
    cleared = IntegerField()

    class Meta:
        db_table = 'blacklist_brands_uk'


class BlacklistBrandCA(AccountBaseModel):
    brand = CharField()
    cleared = IntegerField()

    class Meta:
        db_table = 'blacklist_brands_ca'


class CrawlIsbn(AccountBaseModel):
    isbn = CharField()

    class Meta:
        db_table = 'textbooks_isbns_crawl'


class UkAsin(AccountBaseModel):
    isbn = CharField()

    class Meta:
        db_table = 'uk_asins_crawl'


class DataBaseModel(Model):
    def __str__(self):
        r = {}
        for k in self._data.keys():
            try:
                r[k] = str(getattr(self, k))
            except:
                r[k] = json.dumps(getattr(self, k))
        return str(r)

    class Meta:
        database = dataDB


class USAsin(DataBaseModel):
    isbn = CharField()

    class Meta:
        db_table = 'us_asins_crawl'


class UKAsin(DataBaseModel):
    isbn = CharField()

    class Meta:
        db_table = 'uk_asins_crawl'


class ReviewBaseModel(Model):
    def __str__(self):
        r = {}
        for k in self._data.keys():
            try:
                r[k] = str(getattr(self, k))
            except:
                r[k] = json.dumps(getattr(self, k))
        return str(r)

    class Meta:
        database = reviewDB


class ProductAttr(ReviewBaseModel):
    asin = CharField()
    categories = TextField()

    weight = CharField()
    dimensions = CharField()
    size = CharField()
    images = TextField()
    binding = CharField()

    width = DecimalField()
    height = DecimalField()
    length = DecimalField()

    description = TextField()
    upc = CharField()
    related = CharField()

    model = CharField()
    mpn = CharField()
    color = CharField()
    flavor = CharField()
    parent_asin = CharField()
    features = TextField()
    manufacturer = CharField()
    publisher = CharField()

    class Meta:
        db_table = 'product_attrs'
