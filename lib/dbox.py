import sys
import os
import argparse
import platform
import traceback
import datetime
import requests
from config import *
from lib.models import *
import time

import dropbox
from dropbox.files import WriteMode
from local import *

# Python version must be greater than or equal 2.7
if sys.version_info < (2, 7):
    print("ERROR: Repricing requires Python Version 2.7 or above...exiting.")
    sys.exit(1)
else:
    PY2 = bool(sys.version_info.major == 2)

DROPBOX_ACCESS_TOKEN = "sfs6pcRdmyAAAAAAAAAARAcrxrWW7xofYMV_JABh5qDGtIG1fJ8-l1yAPMH8gCtq"
dbx = dropbox.Dropbox(DROPBOX_ACCESS_TOKEN)
DROPBOX_ROOT_DIR = "/amazon feeds/"

rootDir = os.path.dirname(os.path.realpath(__file__))
feedsFileDir = rootDir + "/feeds"
if os.path.isdir(feedsFileDir) == False:
    os.makedirs(feedsFileDir)


def parse_args():
    parser = argparse.ArgumentParser(description=' ')
    parser.add_argument('-a', '--account', type=str, default="", help='Account SID')
    parser.add_argument('-f', '--file', type=str, help='file path')
    command_args = parser.parse_args()
    return command_args


def uploadToDropbox(localFile, account):
    dropboxPath = DROPBOX_ROOT_DIR + account + "/" + datetime.datetime.utcnow().strftime(
        '%Y-%m-%d') + "/" + os.path.basename(localFile)
    if os.path.isfile(localFile) == False:
        localFile = feedsFileDir + "/" + localFile

    if os.path.isfile(localFile) == False:
        print "file not existed"
        return

    with open(localFile, 'rb') as f:

        print("Uploading " + localFile + " to Dropbox as " + dropboxPath + "...")

        i = 0
        while i < 3:

            try:
                dbx.files_upload(f.read(), dropboxPath, mode=WriteMode('overwrite'))
                break
            except Exception as e:
                print traceback.format_exc()
                print "sleep 30 seconds and try again..."
                time.sleep(30)
            i = i + 1


if __name__ == "__main__":
    args = parse_args()

    uploadToDropbox(args.file, args.account)
