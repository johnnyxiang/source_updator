import datetime
import traceback

from bs4 import BeautifulSoup
import json
from lib.general import get_base_url, get_sales_channel
from lib.models import AccountPerformance, omsDB
from lib.utils.helper import get_page_content_from_url, html_parser, get_browser, Helper


def fetch_feedback_stats(country, account_id, seller_id, browser=None):
    if browser is None:
        browser = get_browser(profile=None, headless=True)

    helper = Helper()
    try:
        url = "%s/sp?seller=%s" % (get_base_url(country), seller_id)
        html = get_page_content_from_url(url, browser=browser)
        soup = BeautifulSoup(html, html_parser)
        trs = helper.find_tags(soup, "#feedback-summary-table tr")
        row_pos = {'positve': 1, 'neutral': 2, 'negative': 3, 'count': 4}
        col_pos = {'30days': 1, '90days': 2, '12mons': 3, 'lifetime': 4}
        print account_id, country
        feedbacks = dict()
        for key, pos in row_pos.iteritems():
            tr = trs[pos]
            tds = helper.find_tags(tr, "td")
            for ckey, cpos in col_pos.iteritems():
                td = tds[cpos]
                print key, ckey, td.text.strip()
                if ckey not in feedbacks:
                    feedbacks[ckey] = dict()
                feedbacks[ckey][key] = td.text.strip()
                if feedbacks[ckey][key] == '-':
                    feedbacks[ckey][key] = '0'
        date = datetime.datetime.now().date()
        try:
            model_data = AccountPerformance.get(AccountPerformance.account_code == account_id,
                                                AccountPerformance.country == country,
                                                AccountPerformance.date == date)
        except:
            model_data = AccountPerformance()
            model_data.account_code = account_id
            model_data.country = country
            model_data.SalesChannel = get_sales_channel(country)
            model_data.date = date

        model_data.feedbacks = json.dumps(feedbacks)
        model_data.totalFeedback = int(feedbacks['lifetime']['count'].replace(',', '').replace('.', ''))
        model_data.positive30 = float(feedbacks['30days']['positve'].replace('%', '').replace('.', '')) / 100
        model_data.positive90 = float(feedbacks['90days']['positve'].replace('%', '').replace('.', '')) / 100
        model_data.positive12 = float(feedbacks['12mons']['positve'].replace('%', '').replace('.', '')) / 100
        model_data.positive_lifetime = int(feedbacks['lifetime']['positve'].replace('%', '').replace('.', '')) / 100
        model_data.count30 = int(feedbacks['30days']['count'].replace(',', '').replace('.', ''))
        model_data.count90 = int(feedbacks['90days']['count'].replace(',', '').replace('.', ''))
        model_data.count12 = int(feedbacks['12mons']['count'].replace(',', '').replace('.', ''))

        sql = "SELECT count(*) FROM amazon_orders WHERE account_code = %s AND SalesChannel = %s AND PurchaseDate BETWEEN %s AND %s"
        yesterday = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%Y-%m-%d 23:59:59")
        days_ago_30 = (datetime.datetime.now() - datetime.timedelta(days=30)).strftime("%Y-%m-%d 00:00:00")
        cursor = omsDB.execute_sql(sql, [account_id, model_data.SalesChannel, days_ago_30, yesterday])
        d = cursor.fetchone()
        try:
            model_data.order30 = d[0]
        except:
            pass
        # print model_data
        model_data.save()
    except:
        print traceback.format_exc()
