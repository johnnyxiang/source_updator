import traceback
from elasticsearch import Elasticsearch, helpers
import requests


class ElasticMapping:
    elastic_actions = []
    es_obj = None
    bulk_length = 20

    def __init__(self, host, port=9200, bulk_length=20, http_auth=None):
        self.es_obj = Elasticsearch(hosts=host, port=port, http_auth=http_auth)
        self.elastic_actions = []
        self.bulk_length = bulk_length

    def get_by_asin(self, asin, region="US"):
        data = self.get_data([asin], region)
        for row in data['hits']['hits']:
            if 'isbn' in row['_source']:
                return row['_source']
        return None

    def get_data(self, asins, offset=0):
        query = {
            "ids": {
                "values": asins
            }
        }

        params = {
            'index': 'listing-mapping',
            'from_': offset,
            'size': len(asins),
            "doc_type": 'doc',
            'body': {
                'query': query
            }
        }

        return self.es_obj.search(**params)

    def get_data_by_asins(self, asins, offset=0):
        map = {}

        data = self.get_data(asins, offset)
        if data['hits']['total'] == 0:
            return map

        for row in data['hits']['hits']:
            asin = row['_source']['asin']
            if 'isbn' in row['_source']:
                map[asin] = row['isbn']

        return map

    def remove_existed(self, asins):
        data = self.get_data(asins)
        for row in data['hits']['hits']:
            if 'isbn' in row['_source']:
                asin = row['_source']['asin']
                if len(asin) > 0:
                    asins.remove(asin)
        return asins

    def bulk_add_data(self, asin, payload):
        act = dict(
            _op_type='index',
            _index="listing-mapping",
            _id=asin,
            _type='doc',
        )

        for i in payload:
            act[i] = payload[i]

        # print act
        self.elastic_actions.append(act)

        if len(self.elastic_actions) >= self.bulk_length:
            self.process_bulk()

    def process_bulk(self):
        try:
            helpers.bulk(self.es_obj, self.elastic_actions)
        except:
            if 'blocked by' in traceback.format_exc():
                ElasticMapping.fix_read_only_settings()
                helpers.bulk(self.es_obj, self.elastic_actions)

            print traceback.format_exc()
        self.elastic_actions = []

    @staticmethod
    def fix_read_only_settings():
        payload = {
            'index': {
                'blocks': {
                    'read_only_allow_delete': "false"
                }
            }
        }
        url = "http://35.199.3.83/listing-mapping/_settings"
        headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
        requests.put(url, json=payload, headers=headers)
        # print r.content


if __name__ == "__main__":
    elastic = ElasticMapping(host="35.199.3.83", port=80)
    # print elastic.get_by_asin("B002KDA532")
    # print elastic.get_data(["B002KDA532", "B01IQ999R6"])
    print elastic.remove_existed(["B002KDA532", "B01IQ999R6"])
