import time

from lib.mws import mws
from lib.general import *


def get_report_by_id(report_id, region, mws_access_key, mws_secret_key, seller_id, auth_token=None, callback=None):
    region = region.upper()
    report_api = mws.Reports(mws_access_key, mws_secret_key, seller_id, auth_token=auth_token, region=region, version="2009-01-01")
    report_result = report_api.get_report(report_id)

    print report_result

    if callback is not None:
        callback(report_result.parsed, region)


def get_report(report_type, region, account_id, mws_access_key, mws_secret_key, seller_id, auth_token=None, start_date=None, end_date=None,
               callback=None):
    region = region.upper()
    market_id = marketplaceIdList[region]
    market_ids = [market_id]

    report_api = mws.Reports(mws_access_key, mws_secret_key, seller_id, auth_token=auth_token, region=region, version="2009-01-01")

    if start_date is not None:
        response = report_api.request_report(report_type, marketplaceids=market_ids, start_date=start_date, end_date=end_date)
    else:
        response = report_api.request_report(report_type, marketplaceids=market_ids)

    while True:
        request_id = response._response_dict["RequestReportResult"]["ReportRequestInfo"]["ReportRequestId"]["value"]
        request_report_list = report_api.get_report_request_list(requestids=[request_id])
        info = request_report_list._response_dict["GetReportRequestListResult"]["ReportRequestInfo"]
        print info

        report_request_id = info["ReportRequestId"]["value"]
        status = info["ReportProcessingStatus"]["value"]
        print 'Submission Id: {}. Current status: {}'.format(report_request_id, status)

        if status in ('_SUBMITTED_', '_IN_PROGRESS_', '_UNCONFIRMED_'):
            print 'Sleeping and check again....'
            time.sleep(60)

        elif status == '_DONE_':
            generated_report_id = info["GeneratedReportId"]["value"]
            report_result = report_api.get_report(generated_report_id)

            # print report_result.parsed
            # print report_result.response.
            data = report_result.parsed if report_result.parsed is not None else report_result.response.content

            # print data

            if callback is not None:
                callback(data, region, account_id)

            break
        else:
            print "Submission processing error. Quit."
            break


def get_inventory_report(region, account_id, aws_access_key, aws_secret_key, seller_id, auth_token=None, callback=None):
    get_report("_GET_FLAT_FILE_OPEN_LISTINGS_DATA_", region, account_id, aws_access_key, aws_secret_key, seller_id, auth_token=auth_token,
               callback=callback)


def get_inactive_inventory_report(region, account_id, aws_access_key, aws_secret_key, seller_id, auth_token=None, callback=None):
    get_report("_GET_MERCHANT_LISTINGS_INACTIVE_DATA_", region, account_id, aws_access_key, aws_secret_key, seller_id,
               auth_token=auth_token,
               callback=callback)


def get_active_listing_report(region, account_id, aws_access_key, aws_secret_key, seller_id, auth_token=None, callback=None):
    get_report("_GET_MERCHANT_LISTINGS_DATA_", region, account_id, aws_access_key, aws_secret_key, seller_id, auth_token=auth_token,
               callback=callback)


def get_all_listing_report(region, account_id, aws_access_key, aws_secret_key, seller_id, auth_token=None, callback=None):
    get_report("_GET_MERCHANT_LISTINGS_ALL_DATA_", region, account_id, aws_access_key, aws_secret_key, seller_id, auth_token=auth_token,
               callback=callback)
