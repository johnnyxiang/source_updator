from lib.mws import mws
from lib.utils.simple_logger import SimpleLogger
from lib.utils.offer_service import OfferService
from lib.utils.offers import OfferFilter
from lib.utils.mws_price_finder import MwsPriceFinder

min_seller_rate = 90
min_seller_feedback = 100
ships_domestically = True
max_shipping_days = 2
productApis = {}

marketplaceIdList = {
    "US": "ATVPDKIKX0DER",
    "CA": "A2EUQ1WTGCTBG2",
    "MX": "A1AM78C64UM0Y8",
    "UK": "A1F83G8C2ARO7P",
    "DE": "A1PA6795UKMFR9",
    "ES": "A1RKKUPIHCS9HS",
    "FR": "A13V1IB3VIYZZH",
    "IT": "APJ6JRA9NG5V4",
    "JP": "A1VC38T7YXB528"
}


def getProductApi(seller_id, access_key, secret_key, region, auth_token=None):
    region = region.upper()
    if region not in productApis:
        productApi = mws.Products(access_key, secret_key, seller_id, region=region, auth_token=auth_token, version="2011-10-01")
        productApis[region] = productApi

    return productApis[region]


def get_matching_product(product_api, asins, region="US"):
    region = region.upper()
    market_id = marketplaceIdList[region]
    return product_api.get_matching_product(marketplaceid=market_id, asins=asins)


def get_matching_product_for_asin(product_api, asins, region="US"):
    region = region.upper()
    market_id = marketplaceIdList[region]
    return product_api.get_matching_product_for_id(marketplaceid=market_id, type="ASIN", ids=asins)


def get_matching_product_for_upc(product_api, asins, region="US"):
    region = region.upper()
    market_id = marketplaceIdList[region]
    return product_api.get_matching_product_for_id(marketplaceid=market_id, type="UPC", ids=asins)


def get_lowest_priced_offers_for_asin(product_api, asin, region="US"):
    region = region.upper()
    market_id = marketplaceIdList[region]
    return product_api.get_lowest_priced_offers_for_asin(marketplaceid=market_id, asin=asin)


def get_competitive_pricing_for_asin(product_api, asins, region="US"):
    region = region.upper()
    market_id = marketplaceIdList[region]
    return product_api.get_competitive_pricing_for_asin(marketplaceid=market_id, asins=asins)


def get_competitive_pricing_for_asins(product_api, asins, country="US", condition="new"):
    response = get_competitive_pricing_for_asin(product_api, asins, region=country)
    parsed_resp = response._response_dict
    # print parsed_resp
    result = parsed_resp['GetCompetitivePricingForASINResult']
    items = result

    if condition.lower() == 'any':
        condition = 'used'

    if 'ASIN' in items:
        items = [items]

    data = {}
    for asin in asins:
        data[asin] = {'count': 0, 'lowest_price': 0, 'asin': asin}
    for item in items:
        asin = item['ASIN']['value']

        count = 0
        lowest_price = -1.0
        product_price = 0.0
        shipping_price = 0.0
        if "Product" not in item:
            data[asin] = {'asin': asin, 'count': count, 'lowest_price': lowest_price, 'product_price': product_price,
                          'shipping_price': shipping_price}
            continue

        product = item['Product']
        try:
            offer_counts = product['CompetitivePricing']['NumberOfOfferListings']['OfferListingCount']
            if 'condition' in offer_counts:
                offer_counts = [offer_counts]

            for offer_count in offer_counts:
                if offer_count['condition']['value'].lower() == condition.lower():
                    count = offer_count['value']
                    break
        except:
            pass

        try:
            prices = product['CompetitivePricing']['CompetitivePrices']['CompetitivePrice']
            if 'Price' in prices:
                prices = [prices]

            for price in prices:
                # print condition, price
                if price['condition']['value'].lower() == condition.lower():
                    lowest_price = price['Price']['LandedPrice']['Amount']['value']
                    product_price = price['Price']['ListingPrice']['Amount']['value']
                    shipping_price = price['Price']['Shipping']['Amount']['value']
                    break
        except:
            pass

        data[asin] = {'asin': asin, 'count': count, 'lowest_price': lowest_price, 'product_price': product_price,
                      'shipping_price': shipping_price}

    return data


def getPriceForAsinsFromMws(product_api, asins, condition="New", region="US", ships_domestically=True, max_shipping_days=5, avg=False,
                            product_type="product"):
    region = region.upper()
    condition = condition.lower()
    if condition == "used":
        condition = "any"

    if product_type.lower() == "product":
        min_seller_rate = 80
        min_seller_feedback = 20
    elif condition.lower() == 'used' or condition.lower() == 'any':
        min_seller_rate = 88
        min_seller_feedback = 50
    else:
        min_seller_rate = 90
        min_seller_feedback = 80

    if product_type.lower() == "product":
        max_shipping_days = 10
    else:
        max_shipping_days = 5

    filter_cond = {
        'rate': min_seller_rate,
        'review': min_seller_feedback,
        'domestic': ships_domestically,
        'shipping_time': max_shipping_days,
        'subcondition': 70
    }

    offer_filter = OfferFilter(**filter_cond)
    # print offer_filter.filter
    mws_price_finder = MwsPriceFinder(product_api, offer_filter)

    marketId = marketplaceIdList[region]
    offers = mws_price_finder.find_offer_for_asins(asins, marketId, condition, avg=avg)

    return offers
    # if condition.lower() == 'all':
    #
    #
    # return offers[condition] if condition in offers else {}
