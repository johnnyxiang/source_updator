from __future__ import print_function

import contextlib
import datetime
import os
import traceback

import dropbox
import six
import sys
import time
import unicodedata

from dropbox.files import WriteMode

DROPBOX_ACCESS_TOKEN = "sfs6pcRdmyAAAAAAAAAARAcrxrWW7xofYMV_JABh5qDGtIG1fJ8-l1yAPMH8gCtq"
dbx = dropbox.Dropbox(DROPBOX_ACCESS_TOKEN)
DROPBOX_ROOT_DIR = "/amazon feeds/"


@contextlib.contextmanager
def stopwatch(message):
    """Context manager to print how long a block of code took."""
    t0 = time.time()
    try:
        yield
    finally:
        t1 = time.time()
        print('Total elapsed time for %s: %.3f' % (message, t1 - t0))


def download(dbx, entry):
    """Download a file.
    Return the bytes of the file, or None if it doesn't exist.
    """
    path = entry.path_display

    while '//' in path:
        path = path.replace('//', '/')

    print(path)

    with stopwatch('download'):
        try:
            md, res = dbx.files_download(path)
        except dropbox.exceptions.HttpError as err:
            print('*** HTTP error', err)
            return None

    data = res.content
    # print(len(data), 'bytes; md:', md)
    return data


def upload_to_dropbox(local_file, account):
    dropbox_path = DROPBOX_ROOT_DIR + account + "/" + datetime.datetime.utcnow().strftime('%Y-%m-%d') + "/" + os.path.basename(
        local_file)
    with open(local_file, 'rb') as f:

        print("Uploading " + local_file + " to Dropbox as " + dropbox_path + "...")

        i = 0
        while i < 3:

            try:
                dbx.files_upload(f.read(), dropbox_path, mode=WriteMode('overwrite'))
                break
            except Exception as e:
                print(traceback.format_exc())
                print("sleep 30 seconds and try again...")
                time.sleep(30)
            i = i + 1
