import time
import json

import requests

cached_rates = {}

default_rates = {
    "CAD": 0.78,
    "EUR": 1.18,
    "GBP": 1.25
}


class ExchangeRate(object):
    @classmethod
    def get_exchange_rate(cls, base_currency, currency, endpoint='latest'):
        payload = {'base': base_currency}

        if endpoint in cached_rates and base_currency in cached_rates[endpoint] and currency in cached_rates[endpoint][base_currency]:
            return cached_rates[endpoint][base_currency][currency]

        # if not isinstance(currencies, list) and isinstance(currencies, str):
        currencies = [currency]

        if len(currencies) > 0:
            payload['symbols'] = ','.join(currencies)

        url = 'https://api.exchangeratesapi.io/' + endpoint
        max_retry = 3
        while True:
            max_retry -= 1

            try:
                resp = requests.get(url, params=payload)
                if resp.status_code >= 500:
                    resp.raise_for_status()
                elif resp.status_code >= 300:
                    return None

                break
            except Exception as e:
                time.sleep(1)

                if max_retry > 0:
                    continue
                else:
                    raise ExchangeRateServerError(e.message)

        rates = json.loads(resp.text)
        if 'error' in rates:
            raise InvalidCurrencyError(rates['error'])

        if endpoint not in cached_rates:
            cached_rates[endpoint] = {}

        if base_currency not in cached_rates[endpoint]:
            cached_rates[endpoint][base_currency] = {}

        cached_rates[endpoint][base_currency][currency] = rates['rates'][currency]

        return rates['rates'][currency]


class ExchangeRateServerError(Exception):
    """
    ExchangeRateServerError shall be raise when http://fixer.io/ server crashes.
    """


class InvalidCurrencyError(ValueError):
    """
    InvalidCurrencyError shall be raised when currency is not supported.
    """


if __name__ == '__main__':
    from currency_mapping import CurrencyMapping

    base_currency = 'USD'
    symbols = CurrencyMapping.get_supported_currencies()
    # symbols.remove(base_currency)

    rates = ExchangeRate.get_exchange_rate(base_currency, symbols)
    print('Base currency: %s' % base_currency)
    print(rates)
