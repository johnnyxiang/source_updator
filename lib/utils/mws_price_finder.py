from datetime import datetime

from lib import mws
from ..mws.utils import list_unique
from .currency_mapping import CurrencyMapping
from .offers import LowestOfferListing
import traceback
import types
import time


class MwsPriceFinder(object):
    def __init__(self, amazon_mws, offer_filter):
        self.amazon_mws = amazon_mws
        self.offer_filter = offer_filter
        self._send_mws_request = True

    def find_offer_for_asins(self, asins, country, condition, excludeme="True", avg=False):
        lowest_offers = {'new': dict(), 'any': dict(), 'verygood': dict()}
        asins = list_unique(asins)
        if len(asins) <= 0:
            return lowest_offers

        lowest_offer_listings = self._get_lowest_offer_listings(asins, country, condition, excludeme)

        for asin, lowest_offer_listing in lowest_offer_listings.items():
            # print condition, asin, lowest_offer_listing
            if condition.lower() == 'all':
                for cond in ['new', 'any']:
                    lowest_offers[cond][asin] = self.filter_lowest_offers(asin, country, lowest_offer_listing, cond, avg)
            else:
                lowest_offers[condition.lower()][asin] = self.filter_lowest_offers(asin, country, lowest_offer_listing, condition, avg)
                if condition.lower() == 'any':
                    lowest_offers['verygood'][asin] = self.filter_lowest_offers(asin, country, lowest_offer_listing, 'verygood', avg)

        # print lowest_offers
        return lowest_offers

    def filter_lowest_offers(self, asin, country, lowest_offer_listing, condition, avg=False):
        offers = self.offer_filter.get_lowest_priced_filtered_offers(lowest_offer_listing, condition)

        # print asin, offers, lowest_offer_listing
        if offers is not None and len(offers) > 0:
            total_offers = len(offers)
            offer_dicts = [self._offer_to_dict(offer) for offer in offers]
            # print offer_dicts
            if len(offer_dicts) == 1 or avg is not True:
                offer_dict = offer_dicts[0]
            else:
                offer_dicts = [self._offer_to_dict(offer) for offer in offers]
                # print offer_dicts
                if len(offer_dicts) > 2:
                    offer_dicts = offer_dicts[:2]

                # print offer_dicts
                avg_price = round(sum(o['price'] for o in offer_dicts) / len(offer_dicts), 2)

                offer_dict = offer_dicts[0]
                offer_dict['shipping_price'] = 0
                offer_dict['product_price'] = avg_price
                offer_dict['price'] = avg_price
                offer_dict['asin'] = asin
                offer_dict['country_code'] = country.lower()
                if 'currency' not in offer_dict:
                    offer_dict['currency'] = CurrencyMapping.get_currency(country)

            # offer_dict['total_offers'] = total_offers

            # print asin, condition, offer_dict
            return offer_dict
            #
        else:
            offer_dict = dict()
            offer_dict['asin'] = asin
            offer_dict['has_offer'] = 'n'
            offer_dict['total_offers'] = 0
            return offer_dict

    def _get_lowest_offer_listings(self, asins, country, condition, excludeme="True"):

        lowest_offers = dict()
        while True:

            try:
                search_condition = condition if condition.lower() != 'all' else None

                response = self.amazon_mws.get_lowest_offer_listings_for_asin(country, asins, condition=search_condition,
                                                                              excludeme=excludeme)
                parsed_resp = response._response_dict

                # print parsed_resp

                if type(parsed_resp["GetLowestOfferListingsForASINResult"]) is types.ListType:
                    results = parsed_resp["GetLowestOfferListingsForASINResult"]
                else:
                    results = [parsed_resp["GetLowestOfferListingsForASINResult"]]

                for offer_listings in results:
                    try:
                        lowest_offer_listing = LowestOfferListing(offer_listings, LowestOfferListing)
                        lowest_offers[lowest_offer_listing.get_asin()] = lowest_offer_listing
                    except:
                        print offer_listings

                return lowest_offers

            except Exception as e:
                # print traceback.format_exc()
                if 'throttled' in traceback.format_exc() or 'QuotaExceeded' in traceback.format_exc():
                    print 'throttled, wait and retry again.'

                    if 'You exceeded your quota of 36000.0' in traceback.format_exc():
                        time.sleep(60)
                    else:
                        time.sleep(10)

                print traceback.format_exc()

                if 'AccessDenied' in traceback.format_exc():
                    raise e

                    # self._send_mws_request = False
                    # finally:

    def _offer_to_dict(self, offer):
        offer_dict = dict()

        offer_dict['rate'] = offer.get_rating()['min']
        offer_dict['feedback'] = offer.get_feedback_count()

        offer_dict['fba'] = offer.get_fba()

        listing_price = offer.get_listing_price()
        landed_price = offer.get_landed_price()
        shipping_price = offer.get_shipping_price()

        if shipping_price is None or 'amount' not in shipping_price:
            offer_dict['shipping_price'] = 0
        else:
            offer_dict['shipping_price'] = shipping_price['amount']

        if 'amount' in listing_price:
            offer_dict['product_price'] = listing_price['amount']
        elif 'amount' in landed_price:
            offer_dict['product_price'] = landed_price['amount'] - offer_dict['shipping_price']
        else:
            offer_dict['product_price'] = 500

        if 'amount' in landed_price:
            offer_dict['price'] = landed_price['amount']
        else:
            offer_dict['price'] = offer_dict['product_price'] + shipping_price['amount']

        if 'currency' in listing_price:
            offer_dict['currency'] = listing_price['currency']
        elif 'currency' in landed_price:
            offer_dict['currency'] = landed_price['currency']
        elif 'currency' in shipping_price:
            offer_dict['currency'] = shipping_price['currency']

        offer_dict['shipping_time'] = offer.get_shipping_time()
        offer_dict['is_ships_domestically'] = offer.get_ships_domestically()

        offer_dict['has_offer'] = 'y'
        offer_dict['item_condition'] = offer.get_condition()
        offer_dict['item_subcondition'] = offer.get_subcondition()
        offer_dict['time'] = datetime.strftime(datetime.utcnow(), '%Y-%m-%dT%H:%M:%S')
        offer_dict['rating_str'] = offer.get_rating_str()
        offer_dict['offers'] = offer.get_offers()
        offer_dict['total_offers'] = offer.get_total_offers()
        return offer_dict
