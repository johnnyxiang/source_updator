class ShippingCost(object):
    shipping_cost_mapping = {
        'us_us': 0,
        'us_uk': 15.00,
        'us_de': 15.00,
        'us_fr': 15.00,
        'us_es': 15.00,
        'us_it': 10.00,
        'us_mx': 15.32,
        'us_ca': 15.00,
        'us_jp': 11.73,
        'us_in': 25,
        'us_au': 10,
        'uk_uk': 0,
        'uk_us': 25.0,
        'uk_de': 10.04,
        'uk_fr': 11.77,
        'uk_es': 13.33,
        'uk_it': 14.84,
        'uk_mx': 15.32,
        'uk_ca': 10.61,
        'uk_jp': 11.73,
        'uk_in': 25
    }

    @classmethod
    def get_shipping_cost(cls, country, forward_country):
        forward_country_lower = forward_country.lower()
        if forward_country_lower not in ['us', 'uk']:
            return 0

        shipping_cost_key = '%s_%s' % (forward_country_lower, country.lower())
        if shipping_cost_key in cls.shipping_cost_mapping:
            shipping_cost = cls.shipping_cost_mapping[shipping_cost_key]
        else:
            shipping_cost = 0

        return shipping_cost


if __name__ == '__main__':
    assert ShippingCost.get_shipping_cost('us', 'us') == 0
    assert ShippingCost.get_shipping_cost('us', 'jp') == 0
    assert ShippingCost.get_shipping_cost('de', 'us') == 10.04
    assert ShippingCost.get_shipping_cost('jp', 'uk') == 11.73
    print('Success!')
