import os
import sys
import csv
from glob import glob
import time
import re

if sys.version_info.major == 3:
    PY2 = False
else:
    PY2 = True


class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)


def get_old_files(directory, days):
    """
    Get old files that last modified time is older than days in the
    specified directory, return them in a list
    """
    old_files = []
    abs_path = os.path.abspath(os.path.expanduser(directory))
    if not os.path.isdir(abs_path):
        return old_files

    files = glob("%s/*" % abs_path)
    for file_path in files:
        if (time.time() - os.path.getmtime(file_path)) > 3600 * 24 * days:
            old_files.append(file_path)

    return old_files


def file_lines_count(file_path):
    """
    Return lines count of a given file.
    """
    if not os.path.isfile(file_path):
        return 0

    with open(file_path, 'r') as f:
        line_count = sum(1 for line in f)

    return line_count


def get_subdirectories_name(directory):
    dirs = glob("%s/*/" % directory)
    return [os.path.basename(os.path.dirname(dir_path)) for dir_path in dirs]


def get_lastest_files(directory, file_pattern="*", days="3"):
    """
    Get lastest files that match the give 'file_pattern'(with glob) and last modified time is
    newer than days specified by 'days', return them in a list
    """
    latest_files = []
    abs_path = os.path.abspath(os.path.expanduser(directory))
    if not os.path.isdir(abs_path):
        return latest_files

    files = glob("%s/%s" % (abs_path, file_pattern))
    for file_path in files:
        if (time.time() - os.path.getmtime(file_path)) < 3600 * 24 * days:
            latest_files.append(file_path)

    return latest_files


def is_file_old_than(file_path, ndays):
    expired = False
    if os.path.isfile(file_path):
        expired = (time.time() - os.path.getmtime(file_path)) > 3600 * 24 * ndays
    else:
        expired = True

    return expired


def is_asin_valid(asin):
    return bool(asin and not asin.isspace() and re.match('[0-9]{10}|[A-Z]{1}[0-9A-Z]{9}', asin))


def merge_latest_inventory_report(directory, file_pattern, days, output_path):
    files_found = get_lastest_files(directory, file_pattern, days)

    headers = ['sku', 'asin', 'price', 'quantity']
    with open(output_path, 'w') as output_fh:
        output_fh.write("\t".join(headers))
        output_fh.write("\n")

    records = dict()
    for file_path in files_found:
        try:
            if PY2:
                file_fh = open(file_path, 'rb')
            else:
                file_fh = open(file_path, newline='')

            file_reader = csv.reader(file_fh, delimiter="\t")
            if PY2:
                header_record = file_reader.next()
            else:
                header_record = next(file_reader)

            record_dict = dict()
            for record in file_reader:
                formatted_record = []

                for idx, header in enumerate(header_record):
                    record_dict[header] = record[idx]

                for header in headers:
                    formatted_record.append(record_dict[header])

                if record_dict['sku'] not in records:
                    records[record_dict['sku']] = "\t".join(formatted_record)
        finally:
            file_fh.close()

    with open(output_path, 'a') as output_fh:
        output_fh.write("\n".join(records.values()))


def is_amazon_order(text):
    if not isinstance(text, str):
        return False
    regex = re.compile('[0-9A-Z]{3}-[0-9]{7}-[0-9]{7}')
    return False if regex.match(text) is None else True


if __name__ == "__main__":
    count = file_lines_count(
        os.path.join(os.path.dirname(__file__), 'data', 'inventory_report.txt'))
    if count == 100:
        print('Success')
    else:
        print('Failed')
