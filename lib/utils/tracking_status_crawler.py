import datetime
import json
import traceback
import time
import urllib2
from bs4 import BeautifulSoup
import requests

from lib.utils.helper import get_page_content_from_url, Helper, get_browser


class URLRejectedException(Exception):
    pass


def get_carrier_link(carrier, tracking_number):
    if carrier == "" and tracking_number is None:
        return

    tracking_number = tracking_number.lower()

    if carrier == "" or carrier is None or carrier.lower() == 'other':
        if tracking_number[0:2] == "gm":
            carrier = "dhlgm"
        elif tracking_number[0:1] == "9" and len(tracking_number) > 15:
            carrier = "usps"
        elif tracking_number.isdigit() and len(tracking_number) == 10:
            carrier = "dhl"
    else:
        carrier = carrier.lower().strip()

    link = None

    if carrier in ['dhlgm', 'dhl global mail', 'dhl ecommerce']:
        link = "http://webtrack.dhlglobalmail.com/?trackingnumber=" + tracking_number
        carrier = 'dhlgm'
    elif carrier[0:3] == 'dhl':
        link = "http://www.dhl.com/shipmentTracking?AWB=" + tracking_number + "&brand=DHL"
        carrier = 'dhl'
    elif carrier == "usps":
        link = "https://tools.usps.com/go/TrackConfirmAction.action?tRef=fullpage&tLc=1&text28777=&tLabels=" + tracking_number
    elif carrier in ["ups", "upsmi", "ups mail innovations"]:
        link = "https://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=" + tracking_number
        carrier = 'ups'
    elif carrier == ["fedex", "fedex smartpost"]:
        link = "https://www.fedex.com/apps/fedextrack/?tracknumbers=" + tracking_number
        carrier = 'fedex'
    elif carrier == "osm":
        link = "http://osmart.osmworldwide.us/tracking/results?trackingNumbers=" + tracking_number
    elif carrier == "ontrac":
        link = "https://www.ontrac.com/tracking.asp?trackingres=submit&tracking_number=" + tracking_number
    elif carrier == "asendia":
        link = "http://tracking.asendiausa.com/get_packageID.aspx?d=1&c=0&p=" + tracking_number

    return link, carrier


class TrackingStatusCrawler(object):
    lastStatus = "None"
    description = "No info yet"
    postal_provider = ""
    last_updated = None
    process_date = None

    tracking_link = None
    tracking = None
    carrier = None
    html_parser = 'html.parser'
    helper = Helper()
    browser = None
    init_browser = None

    def __init__(self, carrier, tracking_number, browser=None, init_browser=None):
        self.tracking = tracking_number
        self.tracking_link, self.carrier = get_carrier_link(carrier, tracking_number=tracking_number)
        if browser is None:
            if init_browser is None:
                self.browser = get_browser()
            else:
                self.browser = init_browser(browser)
        else:
            self.browser = browser

        self.init_browser = init_browser

        self._data = None

    def fetch(self):
        if "dhlgm" == self.carrier:
            self.tracking_status_dhlgm()
        elif "dhl" == self.carrier:
            self.tracking_status_dhl()
        elif "ups" == self.carrier:
            self.tracking_status_ups()
        elif "usps" == self.carrier:
            self.tracking_status_usps()
        elif "fedex" == self.carrier:
            self.tracking_status_fedex()
        elif "osm" == self.carrier:
            self.tracking_status_osm()
        elif "asendia" == self.carrier:
            self.tracking_status_asendia()

    @property
    def delivered(self):
        try:
            return True if 'delivered' in self.lastStatus.lower() else False
        except:
            return False

    @property
    def exception(self):
        try:
            return True if 'exception' in self.lastStatus.lower() or 'return' in self.lastStatus.lower() or 'attempt' in self.lastStatus.lower() or 'alert' in self.lastStatus.lower() else False
        except:
            return False

    def shipping_status(self):
        if 'may be incorrect' in self.lastStatus:
            return 'Invalid Tracking'

        if '\n' in self.lastStatus:
            parts = self.lastStatus.split('\n')
            status = parts[0]
        else:
            status = self.lastStatus

        if status is None or len(status) == 0:
            return 'Not Available'

        return status[0:100]

    def __str__(self):
        return "%s - %s - %s - %s" % (
            self.carrier, self.tracking, self.lastStatus, self.last_updated)

    def __unicode__(self):
        return u"%s - %s - %s - %s" % (
            self.carrier, self.tracking, self.lastStatus, self.last_updated)

    def tracking_status_dhlgm(self):
        html = get_page_content_from_url(self.tracking_link, self.browser)
        if html is not None:
            if 'The requested URL was rejected' in html:
                raise URLRejectedException("The requested URL was rejected")

            try:
                soup = BeautifulSoup(html, self.html_parser)
                sTag = self.helper.find_tag(soup, ".status-info h2")
                if sTag is not None:
                    self.lastStatus = sTag.text.strip()

                dTag = self.helper.find_tag(soup, ".status-info p")

                if dTag != None:
                    last_updated = dTag.decode_contents(formatter="html").split("<br/>")[0].replace(" ET", "").strip()
                    self.last_updated = datetime.datetime.strptime(last_updated, '%a, %B %d, %Y at %I:%M %p')
                    self.description = dTag.text.strip()

                tTag = self.helper.find_tag(soup, ".timeline-event.timeline-last .timeline-description")
                if tTag != None:
                    timelineLast = tTag.text.strip()
                    self.description = self.description + "\n" + timelineLast

                postal_providerdds = self.helper.find_tags(soup, ".container.details .col-md-4 .card.card-info dd")

                if len(postal_providerdds) >= 3:
                    self.postal_provider = postal_providerdds[2].decode_contents(formatter="html")

                try:
                    hasPCO = False
                    for timelineLi in self.helper.find_tags(soup, "ol.timeline li"):
                        if "timeline-date" in timelineLi["class"]:
                            self.process_date = datetime.datetime.strptime(timelineLi.text, '%b %d, %Y')
                        tld = self.helper.find_tag(timelineLi, ".timeline-description")
                        if tld != None and tld.text == "Processing Completed at Origin":
                            hasPCO = True
                            break

                    if hasPCO is False:
                        self.process_date = None

                except:
                    print traceback.format_exc()
                    pass
            except:
                print traceback.format_exc()
                pass

    def tracking_status_dhl(self):
        request = urllib2.Request(self.tracking_link)  # Manual encoding required
        handler = urllib2.urlopen(request)
        data = handler.read()
        d = json.loads(data)
        try:
            self.lastStatus = d["results"][0]["checkpoints"][0]["description"].split("-")[0].strip()
        except:
            self.lastStatus = "Unknown"

        try:
            self.description = d["results"][0]["checkpoints"][0]["description"] + "\n" + \
                               d["results"][0]["checkpoints"][0][
                                   "date"] + " " + d["results"][0]["checkpoints"][0]["time"] + ", " + \
                               d["results"][0]["checkpoints"][0][
                                   "location"]
        except:
            self.description = d['errors'][0]['message']

        try:
            self.last_updated = datetime.datetime.strptime(
                d["results"][0]["checkpoints"][0]["date"].strip() + " " + d["results"][0]["checkpoints"][0][
                    "time"].strip(), '%A, %B %d, %Y %H:%M')
        except:
            pass

    def tracking_status_ups(self):
        html = get_page_content_from_url(self.tracking_link, self.browser)

        if html != None:
            soup = BeautifulSoup(html, self.html_parser)
            try:

                lastStatus = self.helper.find_tag(soup, "#stApp_txtPackageStatus")
                if lastStatus is None:
                    lastStatus = self.helper.find_tag(soup, ".ups-progress_current_row_heading ng-tns-c5-3 strong")

                if lastStatus is not None:
                    self.lastStatus = lastStatus.text.strip()
                else:
                    self.lastStatus = "Unknown"
            except:
                print traceback.format_exc()

            dTag = self.helper.find_tag(soup, "p.criticalMessage")
            if dTag is not None:
                self.description = dTag.text.strip()

            try:
                lastTag = self.helper.find_tag(soup, "tr.ups-progress_current_row")

                if lastTag is not None:
                    tds = self.helper.find_tags(lastTag, "td")

                    last_updated = tds[1].text.strip().replace("\t", "").replace("\n", "")
                    last_updated = last_updated.split(" ")[0]
                    self.last_updated = datetime.datetime.strptime(last_updated, '%m/%d/%Y%I:%M')
            except:
                print traceback.format_exc()

            try:
                if "Postal Service Tracking ID" in html:
                    postal_providerdd = self.helper.find_tag(soup, "dl.outHozFixed1.clearfix dd")

                    if postal_providerdd is not None:
                        self.postal_provider = postal_providerdd.text
            except:
                pass
                print traceback.format_exc()

    def tracking_status_usps(self):
        html = get_page_content_from_url(self.tracking_link, self.browser)

        if html is not None:
            try:
                soup = BeautifulSoup(html, self.html_parser)
                try:
                    self.lastStatus = self.helper.find_tag(soup, ".delivery_status h2").text.strip()
                except:
                    self.lastStatus = "Unknown"

                try:
                    lastTag = self.helper.find_tag(soup, ".delivery_status .status_feed p")

                    if lastTag != None:
                        last_updated = lastTag.text.replace("\t", "").replace("\n", "").replace(" ", "")
                        if 'at' in last_updated:
                            parts = last_updated.split('at')
                            last_updated = parts[0]
                        try:
                            self.last_updated = datetime.datetime.strptime(last_updated, '%B%d,%Y')
                        except:
                            self.last_updated = datetime.datetime.strptime(last_updated, '%B%d,%Yat%I:%M')
                except:
                    pass
                    # print traceback.format_exc()

                dTag = self.helper.find_tag(soup, ".delivery_status .status_feed p.important")
                if dTag is not None:
                    self.description = dTag.text.strip()

                dTag = self.helper.find_tag(soup, "div.expected_delivery")
                if dTag is not None:
                    self.description += "\n" + dTag.text.strip()
            except:
                print traceback.format_exc()

    def tracking_status_fedex(self):

        data_body = '{"TrackPackagesRequest":{"appType":"WTRK","appDeviceType":"DESKTOP","supportHTML":true,"supportCurrentLocation":true,"uniqueKey":"","processingParameters":{},"trackingInfoList":[{"trackNumberInfo":{"trackingNumber":"%s","trackingQualifier":"","trackingCarrier":""}}]}}' % self.tracking
        data = {
            'data': data_body,
            'action': 'trackpackages',
            'locale': 'en_US',
            'version': '1',
            'format': 'json'
        }

        r = requests.post("https://www.fedex.com/trackingCal/track", data=data)
        d = json.loads(r.text)
        response = d["TrackPackagesResponse"]["packageList"][0]

        self.lastStatus = response["keyStatus"]
        self.description = response["statusWithDetails"]

        last_updated = response["displayActDeliveryDateTime"].strip()
        if len(last_updated) > 0:
            self.last_updated = datetime.datetime.strptime(last_updated, '%m/%d/%Y %I:%M %p')
        else:
            try:
                self.last_updated = datetime.datetime.strptime(response['scanEventList'][0]['date'], '%Y-%m-%d');
            except:
                self.last_updated = None

    def tracking_status_osm(self):
        html = get_page_content_from_url(self.tracking_link, self.browser)

        if html is not None:
            try:
                soup = BeautifulSoup(html, self.html_parser)
                try:
                    lastStatuses = self.helper.find_tags(soup, ".trackingLabel")
                    for l in lastStatuses:
                        if "status" in l.text.lower():
                            self.lastStatus = self.helper.find_tag(l, ".trackingText").text.strip()
                            break
                except:
                    self.lastStatus = "Unknown"

                try:
                    lastTag = self.helper.find_tag(soup, ".trackDetails .trackaing-time")

                    if lastTag is not None:
                        last_updated = lastTag.text.strip()
                        self.last_updated = datetime.datetime.strptime(last_updated, '%A, %B %d, %Y at %I:%M %p')
                except:
                    print traceback.format_exc()

                dTag = self.helper.find_tag(soup, ".trackDetails .tracking-status")
                if dTag is not None:
                    self.description = dTag.text.strip()

            except:
                print traceback.format_exc()

    def tracking_status_asendia(self):
        html = get_page_content_from_url(self.tracking_link, self.browser)

        if html is not None:
            try:
                soup = BeautifulSoup(html, self.html_parser)
                trs = self.helper.find_tags(soup, "#gvPostal tr")
                for tr in trs:
                    tds = self.helper.find_tags(tr, "td")
                    if len(tds) > 0:
                        self.lastStatus = tds[2].text.split("-")[0].strip()
                        self.description = tr.text
                        break
            except:
                print traceback.format_exc()
