import time
import sys
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from elasticsearch.exceptions import RequestError
from elasticsearch.exceptions import NotFoundError
from elasticsearch.exceptions import ConnectionTimeout
from elasticsearch.exceptions import ConnectionError
from elasticsearch.exceptions import SSLError
from elasticsearch.exceptions import TransportError
from elasticsearch.exceptions import ElasticsearchException


class OfferService(object):
    us_host = "104.196.114.179"
    us_retail_host = "35.231.9.43"

    def __init__(self, host, port, user, password, max_retry=3):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.max_retry = max_retry

        self.esclient = Elasticsearch(hosts=host, port=port, http_auth=(user, password))

        self.es_retail_us = Elasticsearch(hosts=host, port=port, http_auth=(user, password))
        self.es_retail_uk = Elasticsearch(hosts=host, port=port, http_auth=(user, password))

    def search_offers(self, query, country_code='us', condition='Any', size=500, offset=0, sort=None):
        if condition.lower() != 'new':
            condition = 'Any'

        params = {
            'index': 'repricing_%s' % condition.lower(),
            'from_': offset,
            'size': size,
            'body': {
                'query': query
            }
        }

        if sort != None:
            params["body"]["sort"] = sort

        print params
        resp = None
        retry = self.max_retry
        while retry > 0:
            try:
                resp = self.esclient.search(**params)
                break
            except NotFoundError as e:
                break
            except RequestError as e:
                raise e
            except ConnectionTimeout:
                time.sleep(1)
            except (TransportError, ElasticsearchException) as e:
                print e
                resp = -1
                retry -= 1

                status_code = getattr(e, 'status_code', None)
                if status_code == 'N/A':
                    time.sleep(3)
            except Exception as e:
                print e.message
                resp = -1
                retry -= 1
        # print resp

        if resp is None:
            result = None
        elif resp == -1:
            result = False
        else:
            result = dict()
            for item in resp['hits']['hits']:
                result[item['_id']] = item['_source']
        # print result
        return result

    def get_offer(self, asin_list, country_code='us', condition='Any'):
        """Search available offer on server.
        Result is a dict of asin-offer pair, offer is also a dict, it has the following keys:
        asin - The asin of the offer for.
        feedback - The seller's feedback count.
        rate - The seller's rate range, '95-97%' etc.
        product_price - The price in the request country's currency.
        shipping_price - The shipping price in the request country's currency.
        time - The time when the offer was added to server.
        has_offer - Whether there is an offer.
        offers - Offer count.
        item_condition - The asin condition.
        country_code
        product_code

        Returns:
          None - Not found on elastic search server
          False - Elastic search unavailable or could not connect to elastic search
          dict - Result on elastic search
        """
        if condition.lower() != 'new':
            condition = 'Any'

        params = {
            'index': 'lowest_offer_listings_%s_%s' % (country_code.lower(), condition.lower()),
            'from_': 0,
            'size': 500,
            'body': {
                'query': {'terms': {'_id': asin_list}}
            }
        }

        resp = None
        retry = self.max_retry
        while retry > 0:
            try:
                resp = self.esclient.search(**params)
                break
            except NotFoundError as e:
                break
            except RequestError as e:
                raise e
            except ConnectionTimeout:
                time.sleep(1)
            except (TransportError, ElasticsearchException) as e:
                resp = -1
                retry -= 1

                status_code = getattr(e, 'status_code', None)
                if status_code == 'N/A':
                    time.sleep(3)
            except Exception as e:
                resp = -1
                retry -= 1

        if resp is None:
            result = None
        elif resp == -1:
            result = False
        else:
            result = dict()
            for item in resp['hits']['hits']:
                result[item['_id']] = item['_source']

        return result

    def get_offer_for_asin(self, asin, country_code='us', condition='Any'):
        """Search available offer for one asin on server.
        Result is a dict of asin-offer pair, offer is also a dict, it has the following keys:
        asin - The asin of the offer for.
        feedback - The seller's feedback count.
        rate - The seller's rate range, '95-97%' etc.
        product_price - The price in the request country's currency.
        shipping_price - The shipping price in the request country's currency.
        time - The time when the offer was added to server.
        has_offer - Whether there is an offer.
        offers - Offer count.
        item_condition - The asin condition.
        country_code
        product_code

        Returns:
          None - Not found on elastic search server
          False - Elastic search unavailable or could not connect to elastic search
          dict - Result on elastic search
        """
        retry = self.max_retry
        # resp: None - not found
        # resp: -1 - elastic search unavailable or could not connect to elastic search
        resp = None
        while retry > 0:
            try:
                resp = self.esclient.get(
                    index='repricing_%s' % condition.lower(), id=asin, doc_type=country_code)
                break
            except NotFoundError:
                break
            except RequestError as e:
                raise e
            except (TransportError, ElasticsearchException) as e:
                resp = -1
                retry -= 1

                status_code = getattr(e, 'status_code', None)
                if status_code == 'N/A':
                    time.sleep(3)
            except Exception as e:
                resp = -1
                retry -= 1

        if resp is None or '_source' not in resp or 'asin' not in resp['_source']:
            result = None
        elif resp == -1:
            result = False
        else:
            result = dict()
            result[resp['_source']['asin']] = resp['_source']

        return result

    def save_competitor(self, offers, country_code='us', condition='any'):
        common_args = dict(
            _op_type='index',
            _index="competitor_%s" % condition.lower(),
            _type=country_code.lower(),
        )
        service_offers = []
        # print offers
        for _, offer in offers.items():
            if 'asin' not in offer:
                continue
            service_offer = dict()
            service_offer.update(common_args)
            service_offer['_id'] = offer['asin']
            service_offer['_source'] = dict()
            for key, value in offer.iteritems():
                service_offer['_source'][key] = value

            service_offers.append(service_offer)

        # print service_offers
        retry = self.max_retry
        while retry > 0:
            try:
                helpers.bulk(self.es_retail_us, service_offers, request_timeout=30)
                break
            except (ConnectionTimeout, ConnectionError, SSLError, TransportError):
                retry -= 1
                continue
            except Exception as e:
                raise e

    def save_offer(self, offers, country_code='us', condition='any'):
        """
        Save offer to service.
        offers : dict
            The same as return value of get_offer
        country_code : string
            ALPHA-2 country code
        condition : string
            "new" or "any"
        offers - Offer count.
        """
        service_offers = []
        if condition.lower() == 'verygood':
            condition = 'vg'

        common_args = {
            '_op_type': 'index',
            '_index': 'repricing_%s' % condition.lower(),
            '_type': country_code.lower()
        }
        has_offer_attrs = ['asin', 'item_condition', 'rate', 'feedback', 'product_price',
                           'shipping_price', 'has_offer', 'offers', 'total_offers', 'time', 'country_code', 'product_code']
        out_of_stock_attrs = ['asin', 'item_condition', 'has_offer', 'total_offers', 'offers', 'time',
                              'country_code', 'product_code']

        # print offers
        for _, offer in offers.items():
            if 'asin' not in offer:
                continue
            service_offer = dict()
            service_offer.update(common_args)
            service_offer['_id'] = offer['asin']
            service_offer['_source'] = dict()
            if offer['has_offer'] == 'n':
                attrs = out_of_stock_attrs
            else:
                attrs = has_offer_attrs

            if 'total_offers' not in offer:
                offer['total_offers'] = offer['offers']

            for attr in attrs:
                if attr == 'rate':
                    service_offer['_source']['rate'] = offer['rating_str']
                else:
                    service_offer['_source'][attr] = offer[attr]

            service_offers.append(service_offer)
        # print service_offers
        retry = self.max_retry
        while retry > 0:
            try:
                helpers.bulk(self.esclient, service_offers, request_timeout=30)
                break
            except (ConnectionTimeout, ConnectionError, SSLError, TransportError):
                retry -= 1
                continue
            except Exception as e:
                raise e

    def get_weights(self, asin_list):
        query = {
            "query": {
                "terms": {"_id": asin_list}
            }
        }

        retry = self.max_retry
        resp = None
        while retry > 0:
            try:
                resp = self.esclient.search(
                    index='weight_info', doc_type='weight', body=query)
                break
            except NotFoundError as e:
                break
            except Exception as e:
                print(e)
                retry -= 1
                continue

        result = dict()
        if resp is None or 'hits' not in resp or 'hits' not in resp['hits']:
            return result

        for item in resp['hits']['hits']:
            result[item['_id']] = item['_source']

        return result


if __name__ == '__main__':
    ps = OfferService('35.185.13.71', 8080, 'elasticuser', 'KbersRiseUp153')
    query = {"bool": {
        "filter": [
            {
                "match": {"country_code": "uk"}
            },
            {
                "range": {
                    "offers": {"gte": 5}
                }
            },
            {
                "range": {

                    "time": {"gte": "2018-03-16T00:00:00"}
                }
            }
            ,
            {
                "range": {

                    "product_price": {"lte": 10}
                }
            }

        ]
    }}
    print ps.search_offers(query, size=10)
    sys.exit()

    asin_list = ['0766834727', '0718079183', '1430028602']
    result = ps.get_offer(asin_list)
    separator = '+' * 20
    print("%s get_offer %s" % (separator, separator))
    print(result)

    result = ps.get_offer_for_asin('0766834727')
    print("%s get_offer_by_asin %s" % (separator, separator))
    print(result)

    weight_ps = OfferService('35.185.13.71', 8080, 'elasticuser', 'KbersRiseUp153')
    result = weight_ps.get_weights(asin_list)
    print("%s get_weights %s" % (separator, separator))
    print(result)
