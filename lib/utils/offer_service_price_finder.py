import json
import os
from datetime import datetime

from elasticsearch.exceptions import RequestError

from lib.utils.offers import ESOfferFilter
from .offer_service import OfferService
from .simple_logger import SimpleLogger
from .currency_mapping import CurrencyMapping
from ..mws.utils import list_unique


class OfferServicePriceFinder(object):
    def __init__(self, log_dir=None, offer_alive_hours=48, offer_service=None, offer_filter=None):
        self.offer_alive_hours = offer_alive_hours

        if log_dir is None:
            self.logger = SimpleLogger.get_stream_logger('OfferServicePriceFinder')
        else:
            log_path = os.path.join(log_dir, 'offer_service_price_finder.log')
            self.logger = SimpleLogger.get_rotating_file_logger('OfferServicePriceFinder', log_path)

        if offer_service is not None:
            self.offer_service = offer_service
        else:
            self.offer_service = OfferService(
                '104.196.114.179', 8080, 'elasticuser', 'KbersRiseUp153')

        if offer_filter is not None:
            self.offer_filter = offer_filter
        else:
            filter_cond = {
                'rate': 85,
                'review': 50,
                'domestic': True,
                'shipping_time': 5,
                'subcondition': 70
            }

            self.offer_filter = ESOfferFilter(**filter_cond)

    def search_offers(self, query, country='us', condition='Any', size=500, offset=0, sort=None):
        offers = dict()
        offer_service = self._get_offer_service_for_country(country)
        try:
            offers_info = offer_service.search_offers(query, country, condition, size, offset, sort)

            if offers_info is None:
                offers = None
            elif offers_info == -1:
                offers = False
            else:
                offers = self._parse_service_offers(offers_info)
        except RequestError as e:
            offers_info = False
            self.logger.warn(
                '[ElasticSearchRequestError] message: %s, info: %s', e.error, str(e.info))

        return offers

    def find_offer_for_asins(self, asins, country, condition, excludeme=True):
        condition = condition.lower()
        """
        Returns:
          None - Not found on elastic search server
          False - Elastic search unavailable or could not connect to elastic search
          dict - Result on elastic search
        """
        offers = dict()

        asins = list_unique(asins)
        if len(asins) <= 0:
            return offers

        offer_service = self._get_offer_service_for_country(country)
        try:
            offers_info = offer_service.get_offer(asins, country, condition)
            # print country,condition,asins,offers_info
            if offers_info is None:
                offers = None
            elif offers_info == -1:
                offers = False
            else:
                offers = self._parse_service_offers(offers_info, condition)
                for asin in asins:
                    offers.setdefault(asin, None)
        except RequestError as e:
            self.logger.warn(
                '[ElasticSearchRequestError] message: %s, info: %s', e.error, str(e.info))

        return offers

    def save_offers(self, offers, country, condition):
        offer_service = self._get_offer_service_for_country(country)

        # print offers, country, condition
        try:
            offer_service.save_offer(offers, country, condition)
        except Exception as e:
            raise e
            # self.logger.exception(e)

    def cleanup(self):
        SimpleLogger.close_logger(self.logger)

    def _parse_service_offers(self, offers_info, condition):
        offers = dict()
        if len(offers_info) <= 0:
            return offers
        for key, value in offers_info.items():
            # Set offer to expired when no time field exist or time value is invalid
            if 'time' not in value:
                expired = True
            else:
                try:
                    offer_time = datetime.strptime(value['time'][:19], '%Y-%m-%dT%H:%M:%S')
                    now = datetime.utcnow()
                    diff_seconds = (now - offer_time).total_seconds()
                    expired = diff_seconds > 3600 * self.offer_alive_hours
                except ValueError:
                    expired = True

            lowest_offers = json.loads(value['offers'])
            loffers = self.offer_filter.get_lowest_priced_filtered_offers(lowest_offers, condition)
            if loffers is None:
                continue

            offer = loffers[0]
            offers[key] = offer
            offers[key]['_from'] = 'service'
            offers[key]['currency'] = CurrencyMapping.get_currency(offer['country'])
            offers[key]['expired'] = expired
            offers[key]['time'] = value['time']
            offers[key]['total_offers'] = len(lowest_offers)
            # Set offer to out of stock when value is incomplete
            try:
                offers[key]['product_price'] = float(offer['product_price'])
                offers[key]['shipping_price'] = float(offer['shipping_price'])
                offers[key]['price'] = \
                    offers[key]['product_price'] + offers[key]['shipping_price']
            except Exception as e:
                self.logger.warn('[OfferServerParseError] %s', value)
                self.logger.exception(e)
                offers[key]['has_offer'] = 'n'
                offers[key]['offers'] = 0

        return offers

    def _get_offer_service_for_country(self, country):
        return self.offer_service
