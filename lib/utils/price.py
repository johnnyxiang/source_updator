from lib.utils.closing_fee import ClosingFee
from lib.utils.currency_mapping import CurrencyMapping
from lib.utils.shipping_cost import ShippingCost
from lib.utils.exchange_rate import ExchangeRate


def get_usd_rates():
    base_currency = "USD"
    symbols = CurrencyMapping.get_supported_currencies()
    rates = {}
    for symbol in symbols:
        if symbol.lower() == "usd":
            rate = 1.0
        else:
            rate = ExchangeRate.get_exchange_rate(base_currency, symbol)
        rates[symbol] = rate
    return rates


def get_max_price(min_price):
    return round(max(get_price(min_price) * 1.5, 10), 2)


def get_min_price(source_price, marketplace, min_profit_amount, min_profit_rate, source="us", debug=False, type='product'):
    price = float(source_price)
    min_profit_amount = float(min_profit_amount)
    min_profit_rate = float(min_profit_rate)

    if type != 'product' and type != 'p':
        closing_fee = ClosingFee.get_closing_fee(marketplace)
    else:
        closing_fee = 0

    currency = CurrencyMapping.get_currency(marketplace)
    source_currency = CurrencyMapping.get_currency(source)
    rates = get_usd_rates()
    price_in_usd = float(price / rates[source_currency])
    closing_fee_in_usd = float(closing_fee / rates[source_currency])

    shipping_cost = float(ShippingCost.get_shipping_cost(marketplace, source))
    if price < 10:
        shipping_cost = shipping_cost * 0.8

    sales_price_by_min_amount = (price_in_usd + shipping_cost + min_profit_amount + closing_fee_in_usd) / 0.85
    sales_price_by_min_rate = ((price_in_usd + shipping_cost) * (1 + min_profit_rate) + closing_fee_in_usd) / 0.85

    sales_price_in_usd = max(sales_price_by_min_rate, sales_price_by_min_amount)

    if debug:
        print {'source_price_in_usd': price_in_usd, 'shipping_cost': shipping_cost,
               'total cost': price_in_usd + shipping_cost}
        print {'min_amount': min_profit_amount, 'min_rate': min_profit_rate}
        print {'sales_price_by_min_amount': sales_price_by_min_amount, 'sales_price_by_min_rate': sales_price_by_min_rate}
        print {'sales_price_in_usd': sales_price_in_usd, 'final_price': round(sales_price_in_usd * float(rates[currency]), 2)}
        profit = sales_price_in_usd * 0.85 - price_in_usd - shipping_cost - closing_fee_in_usd
        roi = profit / (price_in_usd + shipping_cost)
        print 'profit: $%s, cost $%s, roi: %s' % (round(profit, 2), price_in_usd + shipping_cost, str(round(100 * roi, 2)) + '%')

    return max(round(sales_price_in_usd * float(rates[currency]), 2), 8.99)


def get_price(min_price):
    price = round(max(min_price + 10, min_price * 1.5), 2)
    if price > 2000 >= min_price:
        price = 2000
    return price


def get_roi(price, shipping, marketplace, source_price, source_marketplace='us'):
    currency = CurrencyMapping.get_currency(marketplace)
    source_currency = CurrencyMapping.get_currency(source_marketplace)
    rates = get_usd_rates()
    price_in_usd = float((price + shipping) / rates[currency])
    source_price_in_usd = float(source_price / rates[source_currency])

    shipping_cost = float(ShippingCost.get_shipping_cost(marketplace, source_marketplace))
    if price < 10:
        shipping_cost = shipping_cost * 0.8

    cost = shipping_cost + source_price_in_usd
    amazon_fee = price_in_usd * 0.15
    profit = price_in_usd * 0.85 - cost
    roi = profit / cost
    print 'profit: $%s, \n sales price %s, \n cost $%s (%s + %s), \n amazon fee %s \n roi: %s' % (
        round(profit, 2), round(price_in_usd, 2), cost, source_price_in_usd, shipping_cost, round(amazon_fee, 2),
        str(round(100 * roi, 2)) + '%')


if __name__ == "__main__":
    # print get_usd_rates()
    # min_price = get_min_price(source_price=41.39, marketplace="ca", min_profit_amount=5.8, min_profit_rate=0.4, source="us", debug=True)
    # print min_price, get_price(min_price)

    # min_price = get_min_price(source_price=31.96, marketplace="us", min_profit_amount=1.2, min_profit_rate=0.2, source="us", debug=True,
    #                           type='c')
    # print min_price, min_price - 3.99, get_price(min_price)

    get_roi(price=37.45, shipping=3.89, marketplace='us', source_price=13.94, source_marketplace='us')
    get_roi(price=41.86, shipping=3, marketplace='de', source_price=10.16, source_marketplace='us')
