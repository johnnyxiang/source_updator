from lib import mws
import types


class OfferListing(object):
    def __init__(self, offer_listing):
        self.offer_listing = offer_listing.copy()

    def get_condition(self):
        condition_key = 'condition'
        if not hasattr(self, condition_key):
            setattr(self, condition_key, None)
            qualifiers = self.offer_listing['Qualifiers']
            if 'ItemCondition' in qualifiers:
                setattr(self, condition_key, qualifiers['ItemCondition']['value'])

        return getattr(self, condition_key)

    def get_subcondition(self):
        subcondition_key = 'subcondition'
        if not hasattr(self, subcondition_key):
            setattr(self, subcondition_key, None)
            qualifiers = self.offer_listing['Qualifiers']
            if 'ItemSubcondition' in qualifiers:
                setattr(self, subcondition_key, qualifiers['ItemSubcondition']['value'])

        return getattr(self, subcondition_key)

    def get_ships_domestically(self):
        ships_domestically_key = 'ships_domestically'
        if not hasattr(self, ships_domestically_key):
            setattr(self, ships_domestically_key, None)
            qualifiers = self.offer_listing['Qualifiers']
            if 'ShipsDomestically' in qualifiers:
                if qualifiers['ShipsDomestically']['value'] == 'False':
                    setattr(self, ships_domestically_key, False)
                else:
                    setattr(self, ships_domestically_key, True)

        return getattr(self, ships_domestically_key)

    def get_shipping_time(self):
        shipping_time_key = 'shipping_time'
        if not hasattr(self, shipping_time_key):
            setattr(self, shipping_time_key, None)
            qualifiers = self.offer_listing['Qualifiers']
            if 'ShippingTime' in qualifiers and 'Max' in qualifiers['ShippingTime']:
                setattr(self, shipping_time_key, qualifiers['ShippingTime']['Max']['value'])

        return getattr(self, shipping_time_key)

    def get_rating(self):
        rating_key = 'rating'
        if not hasattr(self, rating_key):
            setattr(self, rating_key, None)
            qualifiers = self.offer_listing['Qualifiers']
            if 'SellerPositiveFeedbackRating' in qualifiers:
                if qualifiers['SellerPositiveFeedbackRating']['value'] == 'Just Launched':
                    rating = {
                        'min': 0,
                        'max': 69
                    }
                elif qualifiers['SellerPositiveFeedbackRating']['value'] == 'Less than 70%':
                    rating = {
                        'min': 60,
                        'max': 69
                    }
                else:
                    rating_min, rating_max = \
                        qualifiers['SellerPositiveFeedbackRating']['value'][:-1].split('-')
                    rating = {
                        'min': int(rating_min),
                        'max': int(rating_max)
                    }
                setattr(self, rating_key, rating)

        return getattr(self, rating_key)

    def get_rating_str(self):
        rating_str_key = 'rating_str'
        if not hasattr(self, rating_str_key):
            setattr(self, rating_str_key, None)
            qualifiers = self.offer_listing['Qualifiers']
            if 'SellerPositiveFeedbackRating' in qualifiers:
                setattr(self, rating_str_key, qualifiers['SellerPositiveFeedbackRating']['value'])

        return getattr(self, rating_str_key)

    def get_feedback_count(self):
        feedback_count_key = 'feedback_count'
        if not hasattr(self, feedback_count_key):
            setattr(self, feedback_count_key, None)
            if 'SellerFeedbackCount' in self.offer_listing:
                setattr(self, feedback_count_key, int(self.offer_listing['SellerFeedbackCount']['value']))

        return getattr(self, feedback_count_key)

    def get_landed_price(self):
        landed_price_key = 'landed_price'
        if not hasattr(self, landed_price_key):
            setattr(self, landed_price_key, None)
            price = self.offer_listing['Price']
            if 'LandedPrice' in price:
                landed_price = dict()
                if 'CurrencyCode' in price['LandedPrice']:
                    landed_price['currency'] = price['LandedPrice']['CurrencyCode']['value']
                else:
                    mws.log.info('[LandedPriceNoCurrency] %s' % self.offer_listing)

                if 'Amount' in price['LandedPrice']:
                    landed_price['amount'] = float(price['LandedPrice']['Amount']['value'])
                else:
                    mws.log.info('[LandedPriceNoAmount] %s' % self.offer_listing)

                setattr(self, landed_price_key, landed_price)

        return getattr(self, landed_price_key)

    def get_listing_price(self):
        listing_price_key = 'listing_price'
        if not hasattr(self, listing_price_key):
            setattr(self, listing_price_key, None)
            price = self.offer_listing['Price']
            if 'ListingPrice' in price:
                listing_price = dict()
                if 'CurrencyCode' in price['ListingPrice']:
                    listing_price['currency'] = price['ListingPrice']['CurrencyCode']['value']
                else:
                    mws.log.info('[ListingPriceNoCurrency] %s' % self.offer_listing)

                if 'Amount' in price['ListingPrice']:
                    listing_price['amount'] = float(price['ListingPrice']['Amount']['value'])
                else:
                    mws.log.info('[ListingPriceNoAmount] %s' % self.offer_listing)

                setattr(self, listing_price_key, listing_price)

        return getattr(self, listing_price_key)

    def get_shipping_price(self):
        shipping_price_key = 'shipping_price'
        if not hasattr(self, shipping_price_key):
            setattr(self, shipping_price_key, None)
            price = self.offer_listing['Price']
            if 'Shipping' in price:
                shipping_price = dict()
                if 'CurrencyCode' in price['Shipping']:
                    shipping_price['currency'] = price['Shipping']['CurrencyCode']['value']
                else:
                    mws.log.info('[ShippingPriceNoCurrency] %s' % self.offer_listing)

                if 'Amount' in price['Shipping']:
                    shipping_price['amount'] = float(price['Shipping']['Amount']['value'])
                else:
                    mws.log.info('[ShippingPriceNoAmount] %s' % self.offer_listing)

                setattr(self, shipping_price_key, shipping_price)

        return getattr(self, shipping_price_key)

    def get_price(self):
        price_key = 'price'
        if not hasattr(self, price_key):
            shipping_price = self.get_shipping_price()
            if shipping_price is not None:
                listing_price = self.get_listing_price()
                setattr(self, price_key, listing_price['amount'] + shipping_price['amount'])
            else:
                setattr(self, price_key, listing_price['amount'])

        return getattr(self, price_key)

    def get_fba(self):
        fba_key = 'fba'
        if not hasattr(self, fba_key):
            qualifiers = self.offer_listing['Qualifiers']
            if 'FulfillmentChannel' in qualifiers:
                if qualifiers['FulfillmentChannel']['value'].lower().find('amazon') != -1:
                    setattr(self, fba_key, True)
                else:
                    setattr(self, fba_key, False)
            else:
                setattr(self, fba_key, False)

        return getattr(self, fba_key)

    def get_offers(self):
        offers_key = 'offers'
        if not hasattr(self, offers_key):
            setattr(self, offers_key, 0)

        return getattr(self, offers_key)

    def set_offers(self, offer_count):
        offers_key = 'offers'
        setattr(self, offers_key, offer_count)

    def get_total_offers(self):
        offers_key = 'total_offers'
        if not hasattr(self, offers_key):
            setattr(self, offers_key, 0)

        return getattr(self, offers_key)

    def set_total_offers(self, offer_count):
        offers_key = 'total_offers'
        setattr(self, offers_key, offer_count)

    def __str__(self):
        rep_dict = dict()
        keys = ['rating', 'feedback_count', 'ships_domestically', 'shipping_time', \
                'condition', 'subcondition', 'landed_price', 'listing_price', 'shipping_price', 'fba']
        for offer_key in keys:
            rep_dict[offer_key] = getattr(self, 'get_%s' % offer_key)()

        return str(rep_dict)


class LowestOfferListing(object):
    def __init__(self, lowest_offer_listing, condition):
        self.lowest_offer_listing = lowest_offer_listing.copy()
        self.condition = condition

    def get_asin(self):
        asin_key = 'asin'
        if not hasattr(self, asin_key):
            setattr(self, asin_key, None)
            product = self.lowest_offer_listing['Product']
            if 'Identifiers' in product:
                if 'MarketplaceASIN' in product['Identifiers']:
                    if 'ASIN' in product['Identifiers']['MarketplaceASIN']:
                        setattr(self, asin_key, product['Identifiers']['MarketplaceASIN']['ASIN']['value'])

        return getattr(self, asin_key)

    def get_marketplace_id(self):
        marketplace_id_key = 'marketplace_id'
        if not hasattr(self, marketplace_id_key):
            setattr(self, marketplace_id_key, None)
            product = self.lowest_offer_listing['Product']
            if 'Identifiers' in product:
                if 'MarketplaceASIN' in product['Identifiers']:
                    if 'MarketplaceId' in product['Identifiers']['MarketplaceASIN']:
                        setattr(
                            self, marketplace_id_key,
                            product['Identifiers']['MarketplaceASIN']['MarketplaceId']['value'])

        return getattr(self, marketplace_id_key)

    def get_offer_listings(self):
        offer_listings_key = 'offer_listings'
        if not hasattr(self, offer_listings_key):
            offer_listings = []
            setattr(self, offer_listings_key, offer_listings)

            product = self.lowest_offer_listing['Product']
            # print product
            if 'LowestOfferListings' in product and \
                            'LowestOfferListing' in product['LowestOfferListings']:

                if type(product['LowestOfferListings']['LowestOfferListing']) is types.ListType:
                    listings = product['LowestOfferListings']['LowestOfferListing']
                else:
                    listings = [product['LowestOfferListings']['LowestOfferListing']]

                for offer_listing in listings:
                    offer_listings.append(OfferListing(offer_listing))

        return getattr(self, offer_listings_key)

    def get_offer_count(self):
        return len(self.get_offer_listings())

    def __str__(self):
        keys = ['rating', 'feedback_count', 'ships_domestically', 'shipping_time', 'condition', \
                'subcondition', 'landed_price', 'listing_price', 'shipping_price', 'fba']
        rep_dict = {'asin': self.get_asin(), 'marketplace_id': self.get_marketplace_id()}
        for offer_listing in self.get_offer_listings():
            print offer_listing
            for offer_key in keys:
                rep_dict[offer_key] = getattr(offer_listing, 'get_%s' % offer_key)()

        return str(rep_dict)


class OfferFilter(object):
    def __init__(self, **kw):
        self.dict_subcondition = {
            'new': 100,
            'mint': 90,
            'very_good': 81,
            'verygood': 80,
            'good': 70,
            'acceptable': 60,
            'poor': 50,
            'club': 40,
            'oem': 30,
            'warranty': 25,
            'refurbishedwarranty': 20,
            'refurbished_warranty': 21,
            'refurbished': 15,
            'open_box': 10,
            'openbox': 11,
            'Other': 0
        }

        self.filter = {
            'rate': 90,
            'review': 100,
            'domestic': None,
            'shipping_time': 2,
            'subcondition': 70,
            'fba': None
        }

        for key in kw:
            if key in self.filter:
                self.filter[key] = kw[key]

                # with open('forbidden_sellers.csv') as f:
                #     self.forbidden_sellers  = f.readlines()

                # print self.forbidden_sellers

    def get_filtered_offers(self, lowest_offer_listing, condition='any'):
        filtered_offers = []

        offer_listings = lowest_offer_listing.get_offer_listings()
        # print len(offer_listings)
        # print offer_listings

        if len(offer_listings) <= 0:
            mws.log.debug('[NoOfferFound] %s - %s' % (lowest_offer_listing, self.filter))
            return filtered_offers

        for offer_listing in offer_listings:
            fba = offer_listing.get_fba()
            offer_condition = offer_listing.get_condition()
            # print fba, offer_condition, offer_listing

            if condition.lower() == 'new' and offer_condition.lower() != 'new':
                continue

            # forbidden sellers
            if self.filter['fba'] is not None and fba != self.filter['fba']:
                mws.log.debug('[FBA] %s - %s - %s - %s' % (
                    lowest_offer_listing.get_asin(), lowest_offer_listing.get_marketplace_id(),
                    offer_listing, self.filter))
                continue

            ships_domestically = offer_listing.get_ships_domestically()
            if self.filter['domestic'] is not None and ships_domestically != self.filter['domestic']:
                mws.log.debug('[ShipsDomestically] %s - %s - %s - %s' % (
                    lowest_offer_listing.get_asin(), lowest_offer_listing.get_marketplace_id(),
                    offer_listing, self.filter))
                continue

            shipping_time = offer_listing.get_shipping_time()
            if shipping_time is not None and fba is False:
                if shipping_time.find('-') == -1:
                    days_str = shipping_time.split(' ')[0].strip()
                else:
                    days_str = shipping_time.split('-')[0].strip()

                try:
                    shipping_days = int(days_str)
                except:
                    shipping_days = 0

                if shipping_days > self.filter['shipping_time']:
                    mws.log.debug('[ShippingTime] %s - %s - %s - %s' % (
                        lowest_offer_listing.get_asin(), lowest_offer_listing.get_marketplace_id(),
                        offer_listing, self.filter))
                    continue

            rating = offer_listing.get_rating()
            if rating['min'] < self.filter['rate'] and fba is False:
                mws.log.debug('[Rating] %s - %s - %s - %s' % (
                    lowest_offer_listing.get_asin(), lowest_offer_listing.get_marketplace_id(),
                    offer_listing, self.filter))
                continue

            feedback_count = offer_listing.get_feedback_count()
            if feedback_count < self.filter['review'] and fba is False:
                mws.log.debug('[Feedback] %s - %s - %s - %s' % (
                    lowest_offer_listing.get_asin(), lowest_offer_listing.get_marketplace_id(),
                    offer_listing, self.filter))
                continue

            subcondition = offer_listing.get_subcondition()
            subcondition_score = self.dict_subcondition['verygood'] if condition == 'verygood' else self.filter['subcondition']
            if self.dict_subcondition[subcondition.lower()] < subcondition_score:
                mws.log.debug('[Subcondition] %s - %s - %s - %s' % (
                    lowest_offer_listing.get_asin(), lowest_offer_listing.get_marketplace_id(),
                    offer_listing, self.filter))
                continue

            filtered_offers.append(offer_listing)
        # print len(filtered_offers)
        if len(filtered_offers) > 0:
            filtered_offers_str = [str(filtered_offer) for filtered_offer in filtered_offers]
            mws.log.debug('[FilteredOffer] %s - %s' % ("\n".join(filtered_offers_str), self.filter))

        return filtered_offers

    def get_lowest_priced_filtered_offer(self, lowest_offer_listing, condition='any'):
        lowest_priced_offer = None

        filtered_offers = self.get_filtered_offers(lowest_offer_listing, condition)
        if len(filtered_offers) <= 0:
            return lowest_priced_offer

        for offer in filtered_offers:
            if lowest_priced_offer is None:
                lowest_priced_offer = offer
                continue

            price1 = self.calc_offer_price(lowest_priced_offer)
            price2 = self.calc_offer_price(offer)
            if price1 > price2:
                lowest_priced_offer = offer

        lowest_priced_offer.set_offers(len(filtered_offers))
        return lowest_priced_offer

    def get_lowest_priced_filtered_offers(self, lowest_offer_listing, condition='any', count=3):
        lowest_priced_offer = None

        filtered_offers = self.get_filtered_offers(lowest_offer_listing, condition)
        if len(filtered_offers) <= 0:
            return lowest_priced_offer

        filtered_offers.sort(key=lambda o: self.calc_offer_price(o))  # sorts in place
        offer_count = len(filtered_offers)
        total_offers = lowest_offer_listing.get_offer_count()
        if len(filtered_offers) > count:
            filtered_offers = filtered_offers[0:count]

        offers = []
        for offer in filtered_offers:
            offer.set_offers(offer_count)
            offer.set_total_offers(total_offers)
            offers.append(offer)
        return offers

    def sort_offer_by_price(self, lowest_offer_listing):
        sorted_offer_listing = []
        offer_listings = lowest_offer_listing.get_offer_listings()
        if len(offer_listings) <= 0:
            return sorted_offer_listing

        offer_listings.sort(key=self.calc_offer_price)

        return offer_listings

    def calc_offer_price(self, offer):
        listing_price = offer.get_listing_price()
        shipping_price = offer.get_shipping_price()

        if 'amount' in listing_price:
            if shipping_price is None:
                price = listing_price['amount']
            else:
                price = listing_price['amount'] + shipping_price['amount']
        else:
            landed_price = offer.get_landed_price()
            price = landed_price['amount']

        return price


class FollowupOfferFilter(OfferFilter):
    def __init__(self, **kw):
        if 'avaliable_sellers' in kw and isinstance(kw['avaliable_sellers'], list):
            self.avaliable_sellers = kw['avaliable_sellers']
        else:
            self.avaliable_sellers = []

        OfferFilter.__init__(self, **kw)

    def get_filtered_offers(self, lowest_offer_listing, condition='any'):
        filtered_offers = []

        offer_listings = lowest_offer_listing.get_offer_listings()

        if len(offer_listings) <= 0:
            mws.log.debug('[NoOfferFound] %s - %s' % (
                lowest_offer_listing, self.avaliable_sellers))
            return filtered_offers

        for offer_listing in offer_listings:
            for seller in self.avaliable_sellers:
                feedback_count = offer_listing.get_feedback_count()
                rating = offer_listing.get_rating()

                if (seller['feedback']['min'] <= feedback_count <= seller['feedback']['max'] and
                                seller['rating']['min'] <= rating['min'] <= seller['rating']['max']):
                    filtered_offers.append(offer_listing)

        if len(filtered_offers) > 0:
            mws.log.debug('[FilteredOffer] %s - %s' % (
                lowest_offer_listing, self.avaliable_sellers))

        return filtered_offers


class ESOfferFilter(object):
    def __init__(self, **kw):
        self.dict_subcondition = {
            'new': 100,
            'mint': 90,
            'very_good': 81,
            'verygood': 80,
            'good': 70,
            'acceptable': 60,
            'poor': 50,
            'club': 40,
            'oem': 30,
            'warranty': 25,
            'refurbishedwarranty': 20,
            'refurbished_warranty': 21,
            'refurbished': 15,
            'open_box': 10,
            'openbox': 11,
            'Other': 0
        }

        self.filter = {
            'rate': 90,
            'review': 100,
            'domestic': None,
            'shipping_time': 2,
            'subcondition': 70,
            'fba': None
        }

        for key in kw:
            if key in self.filter:
                self.filter[key] = kw[key]

                # with open('forbidden_sellers.csv') as f:
                #     self.forbidden_sellers  = f.readlines()

                # print self.forbidden_sellers

    def get_filtered_offers(self, offer_listings, condition='any'):
        filtered_offers = []

        if len(offer_listings) <= 0:
            mws.log.debug('[NoOfferFound] %s - %s' % (offer_listings, self.filter))
            return filtered_offers

        for offer_listing in offer_listings:
            fba = offer_listing['fba']
            offer_condition = offer_listing['condition']
            # print fba, offer_condition, offer_listing

            if condition.lower() == 'new' and offer_condition.lower() != 'new':
                continue

            # forbidden sellers
            if self.filter['fba'] is not None and fba != self.filter['fba']:
                continue

            ships_domestically = offer_listing['domestic']
            if self.filter['domestic'] is not None and ships_domestically != self.filter['domestic']:
                continue

            shipping_time = offer_listing['shipping_time']
            if shipping_time is not None and fba is False:

                if shipping_time['min'] > self.filter['shipping_time']:
                    continue

            rating = offer_listing['rating']
            if rating['min'] < self.filter['rate'] and fba is False:
                continue

            feedback_count = offer_listing['feedback']
            if feedback_count < self.filter['review'] and fba is False:
                continue

            subcondition = offer_listing['subcondition']
            if self.dict_subcondition[subcondition.lower()] < self.filter['subcondition']:
                continue

            filtered_offers.append(offer_listing)
        # print len(filtered_offers)
        if len(filtered_offers) > 0:
            filtered_offers_str = [str(filtered_offer) for filtered_offer in filtered_offers]
            mws.log.debug('[FilteredOffer] %s - %s' % ("\n".join(filtered_offers_str), self.filter))

        return filtered_offers

    def get_lowest_priced_filtered_offer(self, lowest_offer_listing, condition='any'):
        lowest_priced_offer = None

        filtered_offers = self.get_filtered_offers(lowest_offer_listing, condition)
        if len(filtered_offers) <= 0:
            return lowest_priced_offer

        for offer in filtered_offers:
            if lowest_priced_offer is None:
                lowest_priced_offer = offer
                continue

            price1 = self.calc_offer_price(lowest_priced_offer)
            price2 = self.calc_offer_price(offer)
            if price1 > price2:
                lowest_priced_offer = offer

        lowest_priced_offer.set_offers(len(filtered_offers))
        return lowest_priced_offer

    def get_lowest_priced_filtered_offers(self, lowest_offer_listing, condition='any', count=3):
        lowest_priced_offer = None

        filtered_offers = self.get_filtered_offers(lowest_offer_listing, condition)
        if len(filtered_offers) <= 0:
            return lowest_priced_offer

        filtered_offers.sort(key=lambda o: self.calc_offer_price(o))  # sorts in place
        if len(filtered_offers) > count:
            filtered_offers = filtered_offers[0:count]

        offers = []
        for offer in filtered_offers:
            offers.append(offer)
        return offers

    def sort_offer_by_price(self, lowest_offer_listing):
        sorted_offer_listing = []
        offer_listings = lowest_offer_listing.get_offer_listings()
        if len(offer_listings) <= 0:
            return sorted_offer_listing

        offer_listings.sort(key=self.calc_offer_price)

        return offer_listings

    def calc_offer_price(self, offer):
        return offer['price']
