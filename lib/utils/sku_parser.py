class SkuParser:
    @classmethod
    def parse_sku_pattern(cls, sku):
        return "-".join(sku.split("-")[:-1])

    @classmethod
    def parse_sku(cls, sku, country_code='uk'):
        """
        Return value: A dict with keys:
        'product_code'
        'item_condition': 'New' or 'Any'
        'zd': True or False
        'source': source country code
        'compete': Whether compete or not
        
        Please check parse_source, parse_condition, parse_zd, parse_compete.
        """
        sku = sku.replace("_", "-")

        product_code = sku.lower().split("-")[-1][:1]

        if sku.lower().find("bk") > 0:
            product_code = 'c'

        if sku.lower().find("-a") > 0:
            product_code = 'p'

        if product_code not in ['c', 'g', 'p']:
            product_code = 'c'

        try:
            item_condition = cls.parse_condition(sku)
        except:
            item_condition = 'New'
        zd = cls.parse_zd(sku)
        compete = cls.parse_compete(sku)
        source = cls.parse_source(sku, country_code)

        return {
            'product_code': product_code,
            'item_condition': item_condition,
            'zd': zd,
            'source': source,
            'compete': compete
        }

    @classmethod
    def parse_source(cls, sku, country_code):
        """
        The only supported source countries are 'us', 'uk', 'ca', 'de'.
        If country_code is not one of the source country,
        but sku contains any source country,
        then this item is a foward-item from sku-contained source country to country_code.
        If country_code is one of the source country,
        and sku contains any source country besides the country_code,
        then this item is a foward-item from sku-contained source country to country_code.
        All other situation shall be regard as none-forward, source is equal to country_code.
        """
        source_countries = ['us', 'uk', 'ca']

        source = None
        country_code_lower = country_code.lower()
        sku_lower = sku.lower()
        if country_code_lower in source_countries:
            for source_country in source_countries:
                if country_code_lower == source_country:
                    continue

                if sku_lower.find(source_country) >= 0:
                    source = source_country
                    break
        else:
            for source_country in source_countries:
                if sku_lower.find(source_country) >= 0:
                    source = source_country
                    break

        if source is None:
            source = country_code_lower

        return source

    @classmethod
    def parse_condition(cls, sku):
        """
        If sku contains 'new' or 'xin' or 'mint' or 'brand_new',
        it's condition shall be 'New'; otherwise, 'Any' is used.
        If split sku by '-', and the last part starts with 'P' or 'p',
        then it's condition must be 'New'; otherwise SkuPatternError shall be raised.
        """
        sku_lower = sku.lower()

        codes_new = ['p']
        sku_patterns_new = ['new', 'xin', 'mint', 'brand_new', 'nn']

        item_condition = None
        if sku_lower[:1] == 'n':
            item_condition = 'New'
        else:
            for sku_pattern_new in sku_patterns_new:
                if sku_lower.find(sku_pattern_new) >= 0:
                    item_condition = 'New'
                    break

            if item_condition is None:
                item_condition = 'Any'

        product_code = sku_lower.split("-")[-1][:1]
        if product_code in codes_new:
            if item_condition != 'New':
                raise SkuPatternError('Could not found new flag in product sku %s.' % sku)

        return item_condition

    @classmethod
    def parse_zd(cls, sku):
        """
        If 'zd' is found in sku, then sku's asin source is the same as it's asin.
        """
        if sku.lower().find("-a") > 0:
            return True
        return sku.lower().find('zd') > 0

    @classmethod
    def parse_compete(cls, sku):
        """
        If 'cpte' is found in sku, then sku is flagged as compete.
        """
        return sku.lower().find('cpte') > 0

    @classmethod
    def parse_type(cls, sku):
        """
        Replace sku '_' with '-', then split by '-',
        if the last part starts with 'P' or 'p', then type is 'product';
        if the last part starts with 'b' or 'B', or 'c' or 'C', type is 'book';
        otherwise, type is 'unknown'.
        """
        sku = sku.replace('_', '-')
        code = sku.lower().split("-")[-1][:1]
        if code in ['b', 'c']:
            product_type = 'book'
        elif code in ['g']:
            product_type = 'cd'
        elif code == 'p':
            product_type = 'product'
        else:
            product_type = 'unknown'

        return product_type

    @classmethod
    def parse_follow_up(cls, sku):
        """
        Replace sku '_' with '-', then split by '-',
        if the last part starts with 'b' or 'B', then it's follow-up
        """
        sku = sku.replace('_', '-')
        code = sku.lower().split('-')[-1][:1]

        return code in ['b']


class SkuPatternError(ValueError):
    """
    If sku contains '-P' or '-p',
    then it's condition must be new, if new flag is not found in it,
    then SkuPatternError shall be raised.
    """


if __name__ == '__main__':
    skus = {
        'N2017EYWJPMay10ukhealth-P': {
            'country_code': 'jp',
            'source': 'uk',
            'item_condition': 'New'
        },
        'NEW-20170510-YSLUSukhealth-P': {
            'country_code': 'us',
            'source': 'uk',
            'item_condition': 'New'
        },
        'XINEsqyj_UKKITCHEN050917-P': {
            'country_code': 'es',
            'source': 'uk',
            'item_condition': 'New'
        },
        'Usedusbook-170508-Qy1Us-C': {
            'country_code': 'us',
            'source': 'us',
            'item_condition': 'Any'
        },
        'New115CAUSHOMEZDDOBA20170508-P': {
            'country_code': 'ca',
            'source': 'us',
            'item_condition': 'New'
        },
        'XINFrqyj_UKKITCHEN050117ZD-P': {
            'country_code': 'fr',
            'source': 'uk',
            'item_condition': 'New'
        },
        'newES-ukkitchen0501017-p': {
            'country_code': 'es',
            'source': 'uk',
            'item_condition': 'New'
        },
        'newIT-ukbeauty04292017-p': {
            'country_code': 'it',
            'source': 'uk',
            'item_condition': 'New'
        },
        'NwLQDEUkKITCHEN04292017-P': {
            'country_code': 'de',
            'source': 'uk',
            'item_condition': 'New'
        },
        'N67itusbeauty042717-P': {
            'country_code': 'it',
            'source': 'us',
            'item_condition': 'New'
        },
        'XINDeqyj_UKHEALTH062817-P0000628': {
            'country_code': 'uk',
            'source': 'uk',
            'item_condition': 'New'
        },
        'new-ukkitchen04242019zd-p000011': {
            'country_code': 'us',
            'source': 'uk',
            'item_condition': 'New'
        },
        'nhealth-092419-p28082': {
            'country_code': 'us',
            'source': 'us',
            'item_condition': 'New'
        }

    }

    # print CSVtool.parse_sku("", "uk")
    has_error = False
    for sku, info in skus.items():
        print sku, SkuParser.parse_sku_pattern(sku)
        source = SkuParser.parse_source(sku, info['country_code'])
        if source != info['source']:
            has_error = True
            print(
                '[FAILED] { sku: %s, country_code: %s, source: %s, source_parsed: %s }' % (
                    sku, info['country_code'], info['source'], source))

        condition = SkuParser.parse_condition(sku)
        if condition != info['item_condition']:
            has_error = True
            print('[FAILED] { sku: %s, item_condition: %s, condition_found: %s }' % (sku, info['item_condition'], condition))

        follow_up = SkuParser.parse_follow_up(sku)
        if follow_up:
            has_error = True
            print('[FAILED] { sku: %s, follow_up: %s, follow_up_found: %s }' % (sku, str(False), str(follow_up)))

    if not has_error:
        print('Succeed!')
