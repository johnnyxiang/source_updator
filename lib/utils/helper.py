import re
from random import randint

import os
from selenium import webdriver
import urllib2
import cookielib
import traceback
import platform

import time
import urlparse

from selenium.common.exceptions import SessionNotCreatedException

from lib.general import APP_ROOT_DIR

user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36'
amazon_base = 'http://www.amazon.com'
html_parser = 'html.parser'

aheaders = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0.1) Gecko/2010010' \
                  '1 Firefox/4.0.1',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en-us,en;q=0.5',
    'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.7'}

profile_dir = APP_ROOT_DIR + "/web_profile"

if os.path.isdir(profile_dir) is False:
    os.makedirs(profile_dir)

chromedriver_version = 77
MAX_DRIVER_VERSION = 81


def get_browser(profile='Profile 1', headless=False, disableImage=False, disableJS=False):
    global chromedriver_version
    options = webdriver.ChromeOptions()

    if platform.system() == "Windows":
        browser = webdriver.Chrome(APP_ROOT_DIR + "/driver/chromedriver")
        options.add_argument('--disable-gpu')  # Last I checked this was necessary.
    else:
        if platform.system().lower() == "linux":
            driver_path = APP_ROOT_DIR + "/driver/chromedriver_linux_%s" % chromedriver_version
            os.chmod(driver_path, 0o777)
            options.add_argument("--no-sandbox");
            options.add_argument("--disable-dev-shm-usage");
            # options.binary_location = "/usr/lib/chromium-browser/chromium-browser"
        else:
            driver_path = APP_ROOT_DIR + "/driver/chromedriver_mac"

        if profile is not None:
            options.add_argument("user-data-dir=" + profile_dir)
            options.add_argument('--profile-directory=Profile 1')

        if headless is True:
            options.add_argument('--headless')
            options.headless = True
        prefs = {}
        if disableImage is True:
            prefs.update({"profile.managed_default_content_settings.images": 2})

        if disableJS is True:
            prefs.update({'profile.managed_default_content_settings.javascript': 2})

        if len(prefs) > 0:
            options.add_experimental_option("prefs", prefs)

        print chromedriver_version
        try:
            browser = webdriver.Chrome(chrome_options=options, executable_path=driver_path)
        except SessionNotCreatedException as e:
            # if 'must be between' in e.message:

            if chromedriver_version > MAX_DRIVER_VERSION:
                raise e

            chromedriver_version = chromedriver_version + 1
            return get_browser(profile=profile, headless=headless, disableImage=disableImage, disableJS=disableJS)

    return browser


def get_page_content_from_url(url, browser=None, try_times=3):
    # print url
    for x in range(0, try_times):
        try:
            toClose = False
            if browser is None:
                toClose = True
                browser = get_browser()
            browser.get(url)
            time.sleep(2)
            html_source = browser.page_source

            while True:
                if "robot" in html_source and "captcha" in html_source:
                    print 'robot check, wait 60 secs'
                    time.sleep(60)
                    browser.get(url)
                    html_source = browser.page_source
                else:
                    break

            if toClose is True:
                browser.close()

            return html_source


        except:
            print traceback.format_exc()

            try:
                cj = cookielib.CookieJar()
                opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))

                p = opener.open(url)
                page = p.read()

                if page is not None:
                    return page

            except:
                print traceback.format_exc()
                try:
                    req = urllib2.Request(url, aheaders)
                    con = urllib2.urlopen(req)
                    return con.read()
                except urllib2.HTTPError, e:
                    print e.fp.read()

                continue

    # print 2
    return None


class Helper:
    def __init__(self):
        pass

    @staticmethod
    def find_next_page_from_links(links, fetched_links):
        if len(links) == 0:
            return None
        index1 = randint(0, len(links) - 1)
        link = links[index1]

        while True:
            index2 = randint(link[1], link[2])
            if index2 not in fetched_links[index1]:
                fetched_links[index1].append(index2)
                if len(fetched_links[index1]) == link[2] - link[1]:
                    del links[index1]

                nextLink = link[0] + str(index2)
                return [nextLink, links, fetched_links]

    def find_tags(self, soup, filter_expr):
        attrs = self.get_filter_attrs(filter_expr)
        tags = []

        for item in attrs:
            attr = item["attr"]
            tag = item["tag"]
            select = item["select"]
            text = item["text"]
            if select != "":
                tags += soup.select(select)
            elif tag != "":
                if text != "":
                    tags = tags + soup.findAll(tag, text=text)
                else:
                    tags = tags + soup.findAll(tag, attrs=attr)
            elif len(attr) > 0:
                tags = tags + soup.findAll(attrs=attr)
        return tags

    def find_tag(self, soup, filter_expr):
        tags = self.find_tags(soup, filter_expr)
        if len(tags) > 0:
            return tags[0]
        else:
            return None

    @staticmethod
    def format_url(url, baseUrl=None):
        if baseUrl is None:
            baseUrl = amazon_base
        else:
            baseUrl = urlparse.urljoin(baseUrl, '/')

        # print baseUrl

        if "//" not in url:

            if url[0] != "/":

                urlElements = url.split("/")
                baseUrlElements = baseUrl.split("/")

                baseUrlElements.pop()

                newUrlElt = []
                for elt in urlElements:
                    if elt == "..":
                        baseUrlElements.pop()
                    else:
                        newUrlElt.append(elt)

                baseUrlElements += newUrlElt
                return "/".join(baseUrlElements)
            else:
                url = baseUrl + url

        return url

    def find_next_page(self, soup):
        link = self.find_tag(soup, "#pagnNextLink")

        if link is not None:
            return self.format_url(link["href"])

        return self.find_next_best_seller_page(soup)

    def find_next_best_seller_page(self, soup):
        link = self.find_tag(soup, ".zg_selected a")
        links = self.find_tags(soup, ".zg_page")

        if link is not None and links is not None:
            page = link["page"]

            if int(page) < len(links):
                return int(page) + 1

        return None

    @staticmethod
    def get_filter_attrs(filter_expr):
        attrs = []
        filters = filter_expr.split(";")

        for filter in filters:
            attr = {}
            tag = ""
            select = ""
            text = ""
            for xfilter in filter.split(","):
                x = xfilter.split(":")

                if len(x) == 2 and x[0].strip() == "tag":
                    tag = x[1].strip()
                elif len(x) == 2 and x[0].strip() == "text":
                    text = x[1].strip()
                elif len(x) == 2:
                    if "recompile" in x[1]:
                        attr[x[0].strip()] = re.compile(x[1].replace("recompile-", ""))
                    else:
                        attr[x[0].strip()] = x[1].strip()
                elif len(x) == 1:
                    select = xfilter
            item = {'attr': attr, 'tag': tag, 'select': select, 'text': text}
            attrs.append(item)
            # print attrs
        return attrs
