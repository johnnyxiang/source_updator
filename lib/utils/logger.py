import os
from lib.general import APP_ROOT_DIR
import logging
import sys

formatter = logging.Formatter('%(asctime)s [%(levelname)s]: %(message)s')
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setFormatter(formatter)
logger = logging.getLogger('account_assistant.' + __name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(stream_handler)


def get_logger(name):
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(stream_handler)
    return logger


if __name__ == '__main__':
    logger = get_logger(__name__)
    logging.info("test")
