import logging
from ConfigParser import RawConfigParser, NoOptionError

logger = logging.getLogger("filter")


class ConfigTool(object):
    section = ""
    config = None

    def __init__(self, file_path):
        self.config = RawConfigParser()
        self.config.read(file_path)
        self.file_path = file_path

    def set_section(self, section):
        self.section = section

    def get(self, key):

        if self.section:
            try:
                return self.config.get(self.section, key)
            except NoOptionError:
                message_info = [self.section, str(key), "ConfigTool.error", "no_key"]
                logger.error("|".join(message_info))
                return None

        else:
            return None

    def get_all_dict(self):
        dict = {}
        lists = self.config.items(self.section)
        for l in lists:
            dict[l[0]] = l[1]

        return dict

    def get_all(self):

        return self.config.items(self.section)

    def get_section_key(self, section, key):

        return self.config.get(section, key)

    def get_section_all(self, section):

        return self.config.items(section)

    def set(self, key, value):
        self.config.set(section=self.section, option=key, value=value)

    def save_config(self):
        with open(self.file_path, 'wb') as configfile:
            self.config.write(configfile)
