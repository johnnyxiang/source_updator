import logging
import logging.handlers

class SimpleLogger(object):
    @classmethod
    def get_stream_logger(cls, name, level=logging.DEBUG, format_string=None):
        logger = logging.getLogger(name)

        if format_string is None:
            format_string = "%(asctime)s %(name)s [%(levelname)s]:%(message)s"

        logger.setLevel(level)

        sh = logging.StreamHandler()
        sh.setLevel(level)
        formatter = logging.Formatter(format_string)
        sh.setFormatter(formatter)
        logger.addHandler(sh)

        return logger

    @classmethod
    def get_file_logger(cls, name, log_path, level=logging.INFO, format_string=None):
        logger = logging.getLogger(name)

        stream_level = logging.DEBUG
        file_level = level
        if format_string is None:
            format_string = "%(asctime)s %(name)s [%(levelname)s]:%(message)s"

        logger.setLevel(stream_level)

        fh = logging.StreamHandler()
        fh.setLevel(stream_level)
        formatter = logging.Formatter(format_string)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

        fh = logging.FileHandler(log_path)
        fh.setLevel(file_level)
        formatter = logging.Formatter(format_string)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

        return logger

    @classmethod
    def get_rotating_file_logger(
            cls, name, log_path, max_mega_bytes=20, backup_count=5,
            level=logging.INFO, format_string=None):
        logger = logging.getLogger(name)

        stream_level = logging.DEBUG
        file_level = level
        if format_string is None:
            format_string = "%(asctime)s %(name)s [%(levelname)s]:%(message)s"

        logger.setLevel(stream_level)

        fh = logging.StreamHandler()
        fh.setLevel(stream_level)
        formatter = logging.Formatter(format_string)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

        max_bytes = max_mega_bytes * 1024 ** 2
        fh = logging.handlers.RotatingFileHandler(
            log_path, maxBytes=max_bytes, backupCount=backup_count)
        fh.setLevel(file_level)
        formatter = logging.Formatter(format_string)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

        return logger

    @classmethod
    def close_logger(cls, logger):
        if not hasattr(logger, 'handlers') or not isinstance(logger.handlers, list):
            return

        handlers = logger.handlers[:]
        for handler in handlers:
            if isinstance(handler, logging.FileHandler):
                handler.close()
            logger.removeHandler(handler)

if __name__ == '__main__':
    import os
    import time

    tmp_dir = os.getenv('Temp')
    tmp_log = os.path.join(tmp_dir, 'rotating_file.log')
    if os.path.isfile(tmp_log):
        os.remove(tmp_log)

    rotating_logger = SimpleLogger.get_rotating_file_logger(
        'get_rotating_file_logger_test', tmp_log, 1)
    for i in range(0, 10000):
        rotating_logger.info('RotatingFileLogger Information %d' % i)

    SimpleLogger.close_logger(rotating_logger)

    if os.path.isfile(tmp_log):
        stat_info = os.stat(tmp_log)
        if stat_info.st_size > 0:
            print('Succeed!')
        else:
            print('ERROR: Rotating log %s is empty.' % tmp_log)
        os.remove(tmp_log)
    else:
        print('ERROR: Rotating log %s is not found.' % tmp_log)
