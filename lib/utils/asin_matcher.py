import os
import traceback

import requests
import re
import time
import json

from elasticsearch import Elasticsearch


class AsinMatcher:
    """
    Match the given asin of its source.
    """
    def __init__(self, max_retry=3, cache_dir='cache', host="35.199.3.83", port=80, user='elasticuser',
                 password='KbersRiseUp153'):
        if user is None:
            self.es_client = Elasticsearch(hosts=host, port=port)
        else:
            self.es_client = Elasticsearch(hosts=host, port=port, http_auth=(user, password))
        self.asin_pattern = re.compile("^B\d{2}\w{7}|\d{9}(X|\d)$")
        self.max_retry = max_retry
        self.cache_dir = cache_dir
        if not os.path.isdir(self.cache_dir):
            try:
                os.makedirs(self.cache_dir)
            except Exception as e:
                self.cache_dir = '.'

        self.cache_file = os.path.join(self.cache_dir, 'matched-asins.txt')
        self.cache_not_matched_asins = os.path.join(self.cache_dir, 'no-matched-asins.txt')

    def match_by_dropbox(self, asin):
        pass

    def match_asins_by_api(self, isbns):
        asin_sources = []
        chunks = [isbns[x:x + 100] for x in xrange(0, len(isbns), 100)]
        # print chunks

        for chunk in chunks:
            isbns = ['"%s"' % asin for asin in chunk]
            url = "http://35.199.3.83/listing-mapping/_search?size=200&q=isbn:%s&pretty" % ",".join(isbns)
            #url = "http://elasticuser:KbersRiseUp153@104.196.114.179:8080/listing-mapping/_search?size=200&q=isbn:%s&pretty" % ",".join(isbns)
            # \\print url
            max_retry = self.max_retry

            current_time = time.time()
            while True:
                max_retry -= 1
                try:
                    resp = requests.get(url)
                    if resp.status_code >= 500:
                        resp.raise_for_status()
                    elif resp.status_code >= 300:
                        return None

                    break
                except Exception as e:
                    print e.message
                    time.sleep(1)

                    if max_retry > 0:
                        continue
                    else:
                        raise AsinMatchServerError(e.message)
            # print "get response from sever took",int(time.time()-current_time)," seconds"
            # current_time = time.time()
            resp_json = json.loads(resp.text)
            # print resp_json
            if resp_json["hits"]["total"] > 0:
                for hit in resp_json["hits"]["hits"]:
                    try:
                        asin_source = hit["_source"]["isbn"]
                        asin = hit["_source"]["asin"]
                        asin_sources.append({"asin": asin, "isbn": asin_source})
                    except:
                        pass

                        # print "parse data took",int(time.time()-current_time)," seconds"
                        # self.save_to_cache(asin, asin_source)

        # print asin_sources
        # asins = [asin for asin in asins if asin not in asin_sources]
        # for asin in asins:
        #    try:
        #        asin_source = self.match_by_oldapi(asin)
        #        asin_sources[asin] = asin_source
        #    except Exception as e:
        #        print e.message

        # print asin_sources

        return asin_sources

    def match_asins_by_api_v2(self, isbns, size=500):
        query = {
            "terms": {"isbn": isbns}
        }

        query = {
            "filtered": {
                "filter": {
                    "terms": {
                        "isbn": isbns
                    }
                }
            }
        }
        ps = []
        page = 0
        while True:
            try:
                offset = page * size
                if offset > 10000:
                    break

                params = {
                    'index': 'listing-mapping',
                    'from_': offset,
                    'size': size,
                    'body': {
                        'query': query
                    }
                }

                # print params
                data = self.es_client.search(**params)
                # print data

                if data['hits']['total'] == 0:
                    break

                for row in data['hits']['hits']:
                    asin = row['_source']['asin']
                    isbn = row['_source']['isbn']
                    ps.append({'isbn': isbn, 'asin': asin})

                page = page + 1
                if data['hits']['total'] <= page * size or offset + size > 10000:
                    break
            except:
                print traceback.format_exc()
                break

        return ps

    def match_isbn_by_api(self, asin):
        isbns = self.match_by_api([asin])
        return isbns[asin] if asin in isbns else None

    def match_by_api_v2(self, asins, size=500):
        query = {
            "terms": {"_id": asins}
        }
        ps = []
        page = 0
        while True:
            try:
                offset = page * size
                if offset > 10000:
                    break

                params = {
                    'index': 'listing-mapping',
                    'from_': offset,
                    'size': size,
                    'body': {
                        'query': query
                    }
                }

                # print params
                data = self.es_client.search(**params)
                # print data

                if data['hits']['total'] == 0:
                    break

                for row in data['hits']['hits']:
                    asin = row['_source']['asin']
                    isbn = row['_source']['isbn']
                    ps.append({'isbn': isbn, 'asin': asin})

                page = page + 1
                if data['hits']['total'] <= page * size or offset + size > 10000:
                    break
            except:
                print traceback.format_exc()
                break

        return ps

    def match_by_api(self, asins):
        """
        Give asin and return it's according source.

        Asin is assumed to match pattern '^B\d{2}\w{7}|\d{9}(X|\d)$',
        if not, AsinInvalidError shall be raised.

        If error happens exceed max_retry times when communicate with server,
        AsinMatchServerError shall be raised.
        """
        asins = [asin for asin in asins if self.check_no_match(asin) == False]
        asin_sources = {}
        # for asin in asins:
        #    asin_source = self.find_in_cache(asin)
        #    if asin_source is not None:
        #        asin_sources[asin] = asin_source
        # asins = [asin for asin in asins if asin not in asin_sources]

        chunks = [asins[x:x + 50] for x in xrange(0, len(asins), 50)]
        # print chunks

        for chunk in chunks:
            isbns = ['"%s"' % asin for asin in chunk]
            url = "http://35.199.3.83/listing-mapping/_search?size=50&q=asin:%s&pretty" % ",".join(isbns)

            # print url
            max_retry = self.max_retry

            current_time = time.time()
            while True:
                max_retry -= 1
                try:
                    resp = requests.get(url)
                    if resp.status_code >= 500:
                        resp.raise_for_status()
                    elif resp.status_code >= 300:
                        return None

                    break
                except Exception as e:
                    print e.message
                    time.sleep(1)

                    if max_retry > 0:
                        continue
                    else:
                        raise AsinMatchServerError(e.message)
            # print "get response from sever took",int(time.time()-current_time)," seconds"
            # current_time = time.time()
            resp_json = json.loads(resp.text)
            # print resp_json
            if resp_json["hits"]["total"] > 0:
                for hit in resp_json["hits"]["hits"]:
                    try:
                        asin_source = hit["_source"]["isbn"]
                        asin = hit["_source"]["asin"]
                        # if self.is_valid_asin(asin_source):
                        asin_sources[asin] = asin_source
                    except:
                        print hit["_source"]
                        # print "parse data took",int(time.time()-current_time)," seconds"
                        # self.save_to_cache(asin, asin_source)

        # print asin_sources
        asins = [asin for asin in asins if asin not in asin_sources]
        # for asin in asins:
        #    try:
        #        asin_source = self.match_by_oldapi(asin)
        #        asin_sources[asin] = asin_source
        #    except Exception as e:
        #        print e.message

        # print asin_sources

        return asin_sources

    def match_by_oldapi(self, asin):
        """
        Give asin and return it's according source.

        Asin is assumed to match pattern '^B\d{2}\w{7}|\d{9}(X|\d)$',
        if not, AsinInvalidError shall be raised.

        If error happens exceed max_retry times when communicate with server,
        AsinMatchServerError shall be raised.
        """
        if self.check_no_match(asin):
            return None

        asin_source = self.find_in_cache(asin)
        if asin_source is not None:
            return asin_source

        url = "http://35.166.131.3:8080/listing-mapping/api/mapping/%s" % asin
        payload = {'accessKey': '7c2eeed11dab9a747e7517583e6ee857'}

        asin_org = asin
        asin = self.pad_asin(asin)
        if not self.is_valid_asin(asin):
            raise AsinInvalidError('%s is not a valid asin.' % asin)

        max_retry = self.max_retry
        while True:
            max_retry -= 1

            try:
                resp = requests.get(url, params=payload)
                if resp.status_code >= 500:
                    resp.raise_for_status()
                elif resp.status_code >= 300:
                    return None

                break
            except Exception as e:
                time.sleep(1)

                if max_retry > 0:
                    continue
                else:
                    raise AsinMatchServerError(e.message)

        resp_str = resp.text
        if resp_str.lower().find('error') != -1 or resp_str.find('No source ASIN/ISBN found') != -1:
            self.save_not_matched_asin(asin)
            return None

        asin_source = resp_str.split(',')[0].strip()
        self.save_to_cache(asin, asin_source)

        return asin_source

    def save_not_matched_asin(self, asin):
        with open(self.cache_not_matched_asins, 'a') as not_matched_asins:
            not_matched_asins.write("%s\n" % asin)

    def check_no_match(self, asin):
        result = False
        if not os.path.isfile(self.cache_not_matched_asins):
            return result

        with open(self.cache_not_matched_asins, 'r') as not_matched_asins:
            for line in not_matched_asins:
                if line.find(asin) != -1:
                    result = True
                    break

        return result

    def save_to_cache(self, asin, asin_source):
        with open(self.cache_file, 'a') as cache_file:
            cache_file.write("%s\t%s\n" % (asin, asin_source))

    def find_in_cache(self, asin):
        result = None
        if not os.path.isfile(self.cache_file):
            return result

        with open(self.cache_file, 'r') as cache_file:
            for line in cache_file:
                parts = line.split("\t")
                if parts[0] == asin:
                    result = parts[1].strip()
                    break

        return result

    def pad_asin(self, asin):
        if len(asin) < 10 and not asin.lower().startswith('b'):
            asin = "{0:0>10}".format(asin)

        return asin

    def is_valid_asin(self, asin):
        return self.asin_pattern.match(asin) is not None


class AsinMatchServerError(Exception):
    """
    AsinMatchServerError shall be raised when ConnectionError, TooManyRedirects, HTTPError(5XX) happens.
    """


class AsinInvalidError(ValueError):
    """
    AsinInvalidError shall be raised when asin does not match pattern "^B\d{2}\w{7}|\d{9}(X|\d)$".
    """


import sys

if __name__ == '__main__':
    asin_matcher = AsinMatcher()

    print asin_matcher.match_asins_by_api(['B000000GTT', 'B00000DMF0'])

    sys.exit()
    # asins_for_test = ['B019TLR3GO', 'B01K3NI3QG', 'B01FJ0MPNU']
    # asin_sources = ['0321836510', '0000ACCENT', '1599620588', '0596007124', None]
    #
    # try:
    #     print asin_matcher.match_by_api(asins_for_test)
    # except Exception as e:
    #     print(e)
