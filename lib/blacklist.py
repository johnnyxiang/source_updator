import re

import datetime

import StringIO
import requests

from lib.listing import add_blacklist
from lib.models import *
from lib.utils import dropbox_utils
import argparse
import traceback
import dropbox
import csv
import subprocess
import time

DROPBOX_ACCESS_TOKEN = "ksBvMCycir0AAAAAAAA_zDb8QmQfwAg5KIvadh9dPUAGvRcyCzAGJCmDJHJ8DlYD"

BookCDASINToDelete = "/ZAODIANBIAO_QINGQUAN/BookCDASIN"
ProductASINToDelete = "/ZAODIANBIAO_QINGQUAN/ProductASIN"

rootDir = os.path.dirname(os.path.realpath(__file__))

cacheDir = rootDir + "/cache/"
if os.path.isdir(cacheDir) is False:
    os.makedirs(cacheDir)


def createDBIfNotExisted(account):
    database_name = account
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    db_name = "blacklist"
    sql = "CREATE TABLE " + db_name + " (" \
                                      "`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT," \
                                      "`asin` VARCHAR(10) DEFAULT NULL," \
                                      "PRIMARY KEY (`id`)," \
                                      "UNIQUE KEY `asin` (`asin`)" \
                                      ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"
    try:
        database.execute_sql(sql)
    except:
        if "exists" not in traceback.format_exc():
            print traceback.format_exc()


def ayncQingquan():
    try:
        print "sync from dropbox..."
        current_time = time.time()
        dbx = dropbox.Dropbox(DROPBOX_ACCESS_TOKEN)
        data_source = []
        data_source_full = []
        local_cache = rootDir + "/cache/qinquan.txt"

        logs = []
        try:
            with open(local_cache, 'r+') as fp:
                read_lines = fp.readlines()
                logs = [line.rstrip('\n') for line in read_lines]
        except:
            pass

        with open(local_cache, 'a+') as fp:
            for folder in [BookCDASINToDelete, ProductASINToDelete]:
                for entry in dbx.files_list_folder(folder, recursive=True).entries:
                    if '.txt' in entry.name and entry.name not in logs:
                        data = dropbox_utils.download(dbx, entry)
                        # print data
                        reader = csv.reader(data.split('\n'), delimiter='\t')
                        for row in reader:
                            if row is not None and len(row) > 0:
                                print row
                                data_source.append({'asin': row[0]})
                                data_source_full.append({'asin': row[0], 'country': 'all', 'source': 'dropbox'})

                            if len(data_source) > 500:
                                try:
                                    with database.atomic():
                                        Blacklist.replace_many(data_source).execute()
                                except:
                                    pass

                                try:
                                    with accountDB.atomic():
                                        BlacklistAsin.replace_many(data_source_full).execute()
                                except:
                                    pass

                                data_source = []
                                data_source_full = []

                        if len(data_source) > 0:
                            try:
                                with database.atomic():
                                    Blacklist.replace_many(data_source).execute()
                            except:
                                pass

                            try:
                                with accountDB.atomic():
                                    BlacklistAsin.replace_many(data_source_full).execute()
                            except:
                                pass

                            data_source = []
                            data_source_full = []

                        fp.write(entry.name + '\n')

        print "Done sync from dropbox, took", int(time.time() - current_time), " seconds"
    except:
        print traceback.format_exc()


def asyncFromAppScript():
    print "sync from app script..."
    current_time = time.time()
    r = requests.get(
        'https://script.google.com/macros/s/AKfycbwmiA0NbnIWn_8vs0NN1VSxrGUNd9VRNsMPKGfv61EFUM6mtP6n/exec?flag=XS&method=READASINS')
    # print r.text.split(",")
    asin_pattern = re.compile("^B\d{2}\w{7}|\d{9}(X|\d)$")
    data_source = [{'asin': asin.strip()} for asin in r.text.split(",") if len(asin.strip()) == 10]
    chunks = [data_source[x:x + 1000] for x in xrange(0, len(data_source), 1000)]
    index = 0
    print "%d chunks to sync " % len(chunks)
    for chunk in chunks:
        index = index + 1
        try:
            with database.atomic():
                Blacklist.replace_many(chunk).execute()

                print "Done sync %d chunk" % index

        except:
            print "Failed sync %d chunk" % index
            print traceback.format_exc()
            pass

    data_source = [{'asin': asin.strip(), 'country': 'all', 'type': 'order', 'source': 'order sheet'} for asin in r.text.split(",") if
                   len(asin.strip()) == 10]
    chunks = [data_source[x:x + 1000] for x in xrange(0, len(data_source), 1000)]
    index = 0
    print "%d chunks to sync " % len(chunks)
    for chunk in chunks:
        index = index + 1
        try:
            with accountDB.atomic():
                BlacklistAsin.replace_many(chunk).execute()

                print "Done sync %d chunk" % index

        except:
            print "Failed sync %d chunk" % index
            print traceback.format_exc()
            pass

    print "Done sync from app script, took", int(time.time() - current_time), " seconds"


def asyncBlacklistFromProductInfo():
    return
    sql = "insert ignore into blacklist (asin) " \
          "select `fake_asin` from products p " \
          "join product_real_asins r on p.real_asin = r.asin " \
          "where brand in ( " \
          "select distinct brand from product_real_asins " \
          "    where merchant like  CONCAT(brand, '%') and sellers <= 3 " \
          ")"

    try:
        database.execute_sql(sql)
    except:
        print traceback.format_exc()


def syncBlacklistDbDump(db_table, database_name):
    while True:
        command = "mysqldump --opt --user=" + databaseUser + " --password=" + databasePW + " -h" + databaseHost + " " + \
                  accountsDBName + " " + db_table + " | mysql -u " + databaseUser + " -p" + databasePW + " -h" + databaseHost + " " + database_name

        print command

        # command = shlex.split(sql)
        # subprocess.call(sql, shell=True)

        try:
            output = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
            print output
            if "error" not in output.lower():
                return
        except subprocess.CalledProcessError as e:
            output = e.output.decode()
            print output

        time.sleep(3)


def syncBlacklistDb(region, databaseName):
    region = region.lower()
    db_table = "blacklist_"
    if region in ['uk', 'de', 'fr', 'es', 'it', 'au']:
        db_table = db_table + 'uk'

    else:
        db_table = db_table + region

    remote_db_table = db_table
    if region == 'ca':
        remote_db_table = 'blacklist_us'
    database.init(databaseName, host=databaseHost, user=databaseUser, password=databasePW)

    sql = "CREATE TABLE `" + db_table + "` (" \
                                        "`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT," \
                                        "`asin` VARCHAR(10) DEFAULT NULL," \
                                        "PRIMARY KEY (`id`)," \
                                        "UNIQUE KEY `asin` (`asin`)" \
                                        ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"
    try:
        database.execute_sql(sql)
    except:
        if "exists" not in traceback.format_exc():
            print traceback.format_exc()

    # download local blacklist_db
    local_blacklist_file = APP_ROOT_DIR + "/data/local-%s-%s.txt" % (db_table, time.time())
    start_time = time.time()
    while True:
        command = "mysql --user=" + databaseUser + " --password=" + databasePW + " -h" + databaseHost + " " + \
                  databaseName + " -e 'select asin from " + db_table + "' -B > " + local_blacklist_file
        print command
        try:
            output = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
            print output
            if "error" not in output.lower():
                break
        except subprocess.CalledProcessError as e:
            output = e.output.decode()
            print output

        time.sleep(3)

    print "Finish dump local blacklist table, took %s seconds" % (int(time.time() - start_time))

    start_time = time.time()
    with open(local_blacklist_file) as f:
        reader = csv.reader(f, delimiter='\t')
        local_blacklist_asin = set([l[0] for l in reader])
    print "found %s local asins, took %s seconds" % (len(local_blacklist_asin), int(time.time() - start_time))

    if len(local_blacklist_asin) < 1000:
        syncBlacklistDbDump(db_table, databaseName)
        return

    # download local blacklist_db
    remote_blacklist_file = APP_ROOT_DIR + "/data/remote-%s-%s.txt" % (remote_db_table, time.time())
    start_time = time.time()
    while True:
        command = "mysql --user=" + databaseUser + " --password=" + databasePW + " -h" + databaseHost + " " + \
                  accountsDBName + " -e 'select asin from " + remote_db_table + "' -B > " + remote_blacklist_file
        print command
        try:
            output = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
            print output
            if "error" not in output.lower():
                break
        except subprocess.CalledProcessError as e:
            output = e.output.decode()
            print output

        time.sleep(3)

    print "Finish dump remote blacklist table, took %s seconds" % (int(time.time() - start_time))

    start_time = time.time()
    # to_sync = set()
    with open(remote_blacklist_file) as f:
        reader = csv.reader(f, delimiter='\t')
        # remote_blacklist_asin = set([l[0] for l in reader])
        to_sync = set([l[0] for l in reader if l[0] not in local_blacklist_asin])
    # print "found %s remote asins, took %s seconds" % (len(remote_blacklist_asin), int(time.time() - start_time))
    local_blacklist_asin.clear()
    # to_sync = remote_blacklist_asin - local_blacklist_asin
    print "found %s asins to sync" % len(to_sync)
    if len(to_sync) > 0:
        chunk_size = 100
        asins = list(to_sync)
        chunks = [asins[x:x + chunk_size] for x in xrange(0, len(asins), chunk_size)]
        index = 0
        for chunk in chunks:
            index = index + 1
            with database.atomic() as transaction:
                for asin in chunk:
                    try:
                        sql = "insert ignore into " + db_table + " (asin) value (%s)"
                        database.execute_sql(sql, [asin])
                    except:
                        print traceback.format_exc()

            print "%s/%s done" % (index, len(chunks))

    return

    # select max id
    # sql = "select asin from %s ORDER BY id desc limit 1" % db_table
    # cursor = database.execute_sql(sql)
    # row = cursor.fetchone()
    start_time = time.time()
    local_blacklist_asin = set()

    if not os.path.exists(APP_ROOT_DIR + "/data"):
        os.mkdir(APP_ROOT_DIR + "/data")

    # local_blacklist_file = APP_ROOT_DIR + "/data/%s.txt" % db_table
    # if os.path.isfile(local_blacklist_file):
    #     with open(local_blacklist_file) as f:
    #         reader = csv.reader(f, delimiter=',')
    #         local_blacklist_asin = set([l[0] for l in reader])

    if len(local_blacklist_asin) < 7000000:
        local_blacklist_asin.clear()

        batch_size = 50000
        id = 0
        while True:
            print id, len(local_blacklist_asin)
            sql = "select asin,id from " + db_table + " where id > %s  ORDER BY id asc limit %s"
            cursor = database.execute_sql(sql, [id, batch_size])
            rows = cursor.fetchall()
            if len(rows) == 0:
                break
            local_blacklist_asin.update(set([row[0] for row in rows]))
            id = rows[len(rows) - 1][1]

    print "found %s asins, took %s seconds" % (len(local_blacklist_asin), int(time.time() - start_time))

    start_time = time.time()
    batch_size = 50000
    id = 0
    remote_blacklist_asin = set()
    while True:
        print id, len(remote_blacklist_asin)
        sql = "select asin,id from " + db_table + " where id > %s  ORDER BY id asc limit %s"
        cursor = accountDB.execute_sql(sql, [id, batch_size])
        rows = cursor.fetchall()
        if len(rows) == 0:
            break
        to_sync_batch = set([row[0] for row in rows]) - local_blacklist_asin
        if len(to_sync_batch) > 0:
            remote_blacklist_asin.update(to_sync_batch)
        id = rows[len(rows) - 1][1]

    print "found %s asins, took %s seconds" % (len(remote_blacklist_asin), int(time.time() - start_time))

    to_sync = remote_blacklist_asin - local_blacklist_asin
    print "found %s asins to sync" % len(to_sync)

    start_time = time.time()
    chunk_size = 100
    asins = list(to_sync)
    chunks = [asins[x:x + chunk_size] for x in xrange(0, len(asins), chunk_size)]
    index = 0
    for chunk in chunks:
        index = index + 1
        with database.atomic() as transaction:
            for asin in chunk:
                try:
                    sql = "insert ignore into " + db_table + " (asin) value (%s)"
                    database.execute_sql(sql, [asin])
                except:
                    print traceback.format_exc()

        print "%s/%s done" % (index, len(chunks))

    # write to local cache
    # if len(to_sync) > 0:
    #     local_blacklist_asin.update(to_sync)
    #     with open(local_blacklist_file, 'w') as d_file_writer:
    #         for asin in local_blacklist_asin:
    #             try:
    #                 d_file_writer.write(asin + "\n")
    #             except:
    #                 print asin, "failed"

    print "save %s asins, took %s seconds" % (len(asins), int(time.time() - start_time))

    # row = None
    #
    # if row is None:
    #     while True:
    #         command = "mysqldump --opt --user=" + databaseUser + " --password=" + databasePW + " -h" + databaseHost + " " + \
    #                   accountsDBName + " " + db_table + " | mysql -u " + databaseUser + " -p" + databasePW + " -h" + databaseHost + " " + databaseName
    #
    #         print command
    #
    #         # command = shlex.split(sql)
    #         # subprocess.call(sql, shell=True)
    #
    #         try:
    #             output = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
    #             print output
    #             if "error" not in output.lower():
    #                 break
    #         except subprocess.CalledProcessError as e:
    #             output = e.output.decode()
    #             print output
    #
    #         time.sleep(3)
    #
    #     return
    #
    # last_asin = row[0]
    # sql = "select id from " + db_table + " where asin = %s"
    # cursor = accountDB.execute_sql(sql, [last_asin])
    # row = cursor.fetchone()
    # if row is None:
    #     last_id = 0
    # else:
    #     last_id = row[0]
    #
    # print last_id, last_asin
    # sql = "select asin from " + db_table + " where id > %s"
    # cursor = accountDB.execute_sql(sql, [last_id])
    # rows = cursor.fetchall()
    # print "%s asins found to be async for %s." % (len(rows), region)
    # index = 1
    # chunk_size = 5000
    # unique_asins = [row[0] for row in rows]
    # chunks = [unique_asins[x:x + chunk_size] for x in xrange(0, len(unique_asins), chunk_size)]
    #
    # parsed_file_name = APP_ROOT_DIR + "/listings/" + db_table + ".sql"
    # writer = open(parsed_file_name, "wb")
    # writer.write('SET autocommit=0;\n')
    # writer.write('SET unique_checks=0;\n')
    # writer.write('SET foreign_key_checks=0;\n\n')
    #
    # for asins in chunks:
    #     writer.write('INSERT ignore INTO ' + db_table + ' (asin) VALUES \n')
    #     lines = []
    #     for asin in asins:
    #         line = '("' + asin + '")'
    #         lines.append(line)
    #     value_line = ",\n".join(lines);
    #     value_line = value_line + ";\n"
    #     writer.write(value_line)
    #
    # writer.write('COMMIT;\n')
    # writer.write('SET unique_checks=1;\n')
    # writer.write('SET foreign_key_checks=1;\n')
    # writer.close()
    #
    # try:
    #     sql = "mysql --user=" + databaseUser + " --password=" + databasePW + " -h" + databaseHost + " " + databaseName + " < " + parsed_file_name
    #     print sql
    #     subprocess.call(sql, shell=True)
    # except:
    #     print traceback.format_exc()
    #
    #     # with database.atomic() as transaction:
    #     #     for asin in asins:
    #     #         sql = "insert ignore into " + db_table + " (asin) value (%s)"
    #     #         database.execute_sql(sql, [asin])
    #     #         if index % 1000 == 0:
    #     #             print "%s/%s %s" % (index, len(rows), asin)
    #     #         index += 1
    #     # time.sleep(60)


def syncDeleteASINs(region, databaseName):
    region = region.lower()
    # if region == 'us':
    #     return

    db_table = "deleted_asins"
    if region in ['uk', 'de', 'fr', 'es', 'it', 'ca', 'au']:
        db_table = db_table + '_uk'
    elif region != 'us':
        db_table = db_table + "_" + region

    sql = "CREATE TABLE `" + db_table + "` (" \
                                        "`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT," \
                                        "`asin` VARCHAR(10) DEFAULT NULL," \
                                        "PRIMARY KEY (`id`)," \
                                        "UNIQUE KEY `asin` (`asin`)" \
                                        ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"
    try:
        database.execute_sql(sql)
    except:
        if "exists" not in traceback.format_exc():
            print traceback.format_exc()

    sql = "mysqldump --opt --user=" + databaseUser + " --password=" + databasePW + " -h" + databaseHost + " " + \
          accountsDBName + " " + db_table + " | mysql -C -u " + databaseUser + " -p" + databasePW + " -h" + databaseHost + " " + databaseName
    # subprocess.Popen(sql, shell=True)
    subprocess.call(sql, shell=True)
    # time.sleep(60)
    print sql


def sync_from_dsi(from_date=None, days_back=3):
    if from_date is None:
        from_date = datetime.datetime.utcnow().strftime('%Y-%m-%d')
    base = datetime.datetime.strptime(from_date, '%Y-%m-%d')
    date_list = [base - datetime.timedelta(days=x) for x in range(0, days_back)]
    for date in date_list:
        tracking_date = date.strftime('%Y-%m-%d')
        url = "http://dsi.dovertmt.club/index/badasin?date=%s" % tracking_date
        r = requests.get(url)

        f = StringIO.StringIO(r.content)
        reader = csv.reader(f, delimiter=",")

        row_no = 0
        for row in reader:
            # ASIN,Store Name
            if len(row) == 0 or row[0].lower() == 'asin':
                continue
            row_no = row_no + 1
            try:
                asin = row[0]

                account = row[1].lower() if len(row) > 1 else ""
                if len(account) == 0:
                    region = 'us'
                elif 'us' in account:
                    region = 'us'
                else:
                    region = 'uk'

                # add_blacklist(asin, region)
                add_blacklist(asin, region, account=row[1], type='cs', source='dsi')
                print tracking_date, row_no, region, row
            except:
                print row_no


def all_listings_table_existed(all_listing_table, db_connection):
    sql = "select count(*) from " + all_listing_table + " limit 1"
    try:
        db_connection.execute_sql(sql)
        return True
    except:
        pass
    return False


def asyncBlacklist(account, country, sync_db=1):
    createDBIfNotExisted(account)

    ayncQingquan()
    asyncBlacklistFromProductInfo()
    if sync_db > 0:
        syncBlacklistDb(country, account)
    # syncDeleteASINs(country, account)
    try:
        asyncFromAppScript()
    except:
        print traceback.format_exc()

    try:
        sync_from_dsi()
    except:
        print traceback.format_exc()
    all_listing_table = "all_listings"
    inv_db_table = "inventory"
    pro_db_table = 'products'
    if country.upper() != "US":
        inv_db_table = inv_db_table + "_" + country.lower()
        pro_db_table = pro_db_table + "_" + country.lower()
        all_listing_table = all_listing_table + "_" + country.lower()

    sql = "update " + pro_db_table + " p join " + all_listing_table + " l on l.sku = p.sku set p.status = -5 where p.fake_asin <> l.asin"
    try:
        print sql
        database.execute_sql(sql)
    except:
        print traceback.format_exc()

    sql = "update " + all_listing_table + " l join " + pro_db_table + "  p on l.sku = p.sku set l.status = -5 " \
                                                                      "where (p.fake_asin <> l.asin  or p.status <=-5 ) and p.fake_asin is not null"
    try:
        print sql
        database.execute_sql(sql)
    except:
        print traceback.format_exc()

    # select count(*) from all_listings
    # where  product_type = 'product' and status >-5
    # and asin not in (
    #     select fake_asin from products where status >-5 and real_new_price_to_update > 0
    # )
    if country.lower() == 'us':
        try:
            sql = "update " + all_listing_table + " l join " + pro_db_table + " p on p.sku = l.sku set l.status = 10 where l.status>-5"
            database.execute_sql(sql)

            print sql
            sql = "update " + all_listing_table + " set status =-5 where product_type = 'product' and status <>10 and status >-5"
            database.execute_sql(sql)

            print sql

        except:
            print traceback.format_exc()
            # try:
            #     sql = 'select asin from ' + all_listing_table + ' where product_type = "product" and status >-5'
            #     cursor = database.execute_sql(sql)
            #     rows = cursor.fetchall()
            #     existed_asins = set([row[0] for row in rows])
            #     print "found %s product asins " % len(existed_asins)
            #     if len(existed_asins) > 0:
            #         sql = 'select fake_asin from ' + pro_db_table + ' where status >-5'
            #         cursor = database.execute_sql(sql)
            #         rows = cursor.fetchall()
            #         c_asins = set([row[0] for row in rows])
            #         print "found %s product asins in db" % len(c_asins)
            #         to_remove = existed_asins - c_asins
            #         print "found %s product to be removed" % len(to_remove)
            #         if len(to_remove) > 0:
            #             chunk_size = 500
            #             to_remove = list(to_remove)
            #             chunks = [to_remove[x:x + chunk_size] for x in xrange(0, len(to_remove), chunk_size)]
            #             for chunk in chunks:
            #                 with database.atomic() as transaction:
            #                     for asin in chunk:
            #                         try:
            #                             sql = "update " + all_listing_table + " set status =-5 where asin = %s"
            #                             database.execute_sql(sql, [asin])
            #                         except:
            #                             print traceback.format_exc()
            # except:
            #     print traceback.format_exc()

    sql = "update books p join " + all_listing_table + " l on l.sku = p.sku set p.status = -5 where p.fake_asin <> l.asin"
    try:
        print sql
        database.execute_sql(sql)
    except:
        print traceback.format_exc()

    sql = "update " + all_listing_table + " l join books  p on l.sku = p.sku set l.status = -5 " \
                                          "where p.fake_asin <> l.asin and p.fake_asin is not null"
    try:
        print sql
        database.execute_sql(sql)
    except:
        print traceback.format_exc()

    # sql = "update " + inv_db_table + " set status = -5 where status > -5 and asin in (select asin from blacklist)"
    sql = "update " + inv_db_table + " i join blacklist b on b.asin = i.asin " \
                                     "set status = -5 where status > -5 and b.asin is not null"
    try:
        print sql
        database.execute_sql(sql)
    except:
        print traceback.format_exc()

    sql = "update " + all_listing_table + " i join blacklist b on b.asin = i.asin " \
                                          "set status = -5 where status > -5 and b.asin is not null"
    try:
        print sql
        database.execute_sql(sql)
    except:
        print traceback.format_exc()

    blacllist_db_name = "blacklist_"
    if country.lower() in ['uk', 'de', 'fr', 'es', 'it', 'au']:
        blacllist_db_name = blacllist_db_name + 'uk'
    else:
        blacllist_db_name = blacllist_db_name + country.lower()

    sql = "update " + pro_db_table + " set status = -5 where status > -5 and (fake_asin in (select asin from " + blacllist_db_name + ") or  real_asin in (select asin from " + blacllist_db_name + "))"
    try:
        print sql
        database.execute_sql(sql)
    except:
        pass
        print traceback.format_exc()

    sql = "update " + pro_db_table + " set status = -5 where status > -5 and (fake_asin in (select asin from blacklist) or  real_asin in (select asin from blacklist))"
    try:
        print sql
        database.execute_sql(sql)
    except:
        pass
        print traceback.format_exc()

    sql = "update " + inv_db_table + " i join " + blacllist_db_name + " b on b.asin = i.asin " \
                                                                      "set status = -5 where status > -5 and b.asin is not null"
    try:
        print sql
        database.execute_sql(sql)
    except:
        print traceback.format_exc()

    sql = "update " + all_listing_table + " i join " + blacllist_db_name + " b on b.asin = i.asin " \
                                                                           "set status = -5 where status > -5 and b.asin is not null"
    try:
        print sql
        database.execute_sql(sql)
    except:
        print traceback.format_exc()


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', default='702us', help='Account SID')
    parser.add_argument('-c', '--country', default="us", help='Country')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()

    asyncBlacklist(args.account, args.account)
    # asyncFromAppScript()
    # ayncQingquan()
