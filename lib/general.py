import os
import socket
import sys
import time

marketplaceIdList = {
    "US": "ATVPDKIKX0DER",
    "CA": "A2EUQ1WTGCTBG2",
    "MX": "A1AM78C64UM0Y8",
    "UK": "A1F83G8C2ARO7P",
    "DE": "A1PA6795UKMFR9",
    "ES": "A1RKKUPIHCS9HS",
    "FR": "A13V1IB3VIYZZH",
    "IT": "APJ6JRA9NG5V4",
    "JP": "A1VC38T7YXB528",
    "AU": "A39IBJ37TRP1C6",
    "CN": "AAHKV2X7AFYLW"
}
salesChannels = {
    "US": "Amazon.com",
    "CA": "Amazon.ca",
    "MX": "Amazon.com.au",
    "UK": "Amazon.co.uk",
    "DE": "Amazon.de",
    "ES": "Amazon.es",
    "FR": "Amazon.fr",
    "IT": "Amazon.it",
    "JP": "Amazon.com.jp",
    "AU": "Amazon.com.au",
    "CN": "Amazon.cn"
}
regions = {
    'US': ['US', 'CA', 'MX'],
    'EU': ['UK', 'DE', 'FR', 'ES', 'IT'],
    'JP': ['JP'],
    'AU': ['AU'],
    'CN': ['CN']
}


def get_sales_channel(region):
    region = region.upper()
    return salesChannels[region] if region in salesChannels else None

def get_country_code(sales_channel):
    counties = {v.lower(): k for k, v in salesChannels.iteritems()}
    sales_channel = sales_channel.lower()
    return counties[sales_channel] if sales_channel in counties else None

def get_region(country):
    country = country.upper()
    for region,countries in regions.iteritems():
        if country in countries:
            return region
    return country


def get_base_url(region):
    region = region.upper()
    if region in salesChannels:
        return "https://www.%s" % salesChannels[region].lower()

    return None


def get_marketplace_ids(region):
    region = region.upper()
    if region in regions['EU']:
        region = 'EU'
    if region in regions['US']:
        region = 'US'
    return [marketplaceIdList[country] for country in regions[region]]


APP_ROOT_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

currencies = {
    "US": "USD",
    "UK": "GBP",
    "DE": "EUR",
    "ES": "EUR",
    "IT": "EUR",
    "FR": "EUR",
    "JP": "JPY"
}


def get_lock(process_name):
    if sys.platform == "win32":
        return

    global lock_socket  # Without this our lock gets garbage collected
    lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    try:
        lock_socket.bind('\0' + process_name)
        print 'I got the lock for %s' % (process_name)
    except socket.error:
        print 'lock exists'
        sys.exit()


def clean_files(hours=12):
    cutoff = time.time() - hours * 3600

    folders = [APP_ROOT_DIR + "/listings", APP_ROOT_DIR + "/feeds", APP_ROOT_DIR + "/feedbacks", APP_ROOT_DIR + "/data"]
    for folder in folders:
        if not os.path.exists(folder):
            continue
        files = os.listdir(folder)
        for xfile in files:
            file_path = folder + "/" + xfile
            if os.path.isfile(file_path) and os.stat(file_path).st_ctime <= cutoff:
                try:
                    os.remove(file_path)
                    print "deleted file", folder + "/" + xfile
                except:
                    pass

    cutoff = time.time() - 3 * 24 * 3600

    folders = ["/home/sola/projects/repricing/temp/update",
               "/home/sola/projects/repricing/temp/reports",
               "/home/sola/projects/repricing/temp",
               "/home/sola/projects/repricing/logs",
               "/home/sola/projects/repricing/temp/orders"]
    for folder in folders:
        if not os.path.exists(folder):
            continue
        files = os.listdir(folder)
        for xfile in files:
            file_path = folder + "/" + xfile
            if os.path.isfile(file_path) and os.stat(file_path).st_ctime <= cutoff:
                try:
                    os.remove(file_path)
                    print "deleted file", folder + "/" + xfile
                except:
                    pass


if __name__ == "__main__":
    print get_country_code('Amazon.co.uk')
