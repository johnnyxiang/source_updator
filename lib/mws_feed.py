from mws import mws
import traceback
from general import *


def submit_cancellation_feed(region, file_path, mws_access_key, mws_secret_key, seller_id, auth_token=None, callback=None):
    feed_type = '_POST_FLAT_FILE_ORDER_ACKNOWLEDGEMENT_DATA_'
    return submit_flat_file_to_amazon(region, file_path, feed_type, mws_access_key, mws_secret_key,
                                      seller_id, auth_token, callback=callback)


def submit_refund_feed(region, file_path, mws_access_key, mws_secret_key, seller_id, auth_token=None, callback=None):
    feed_type = '_POST_FLAT_FILE_PAYMENT_ADJUSTMENT_DATA_'
    return submit_flat_file_to_amazon(region, file_path, feed_type, mws_access_key, mws_secret_key,
                                      seller_id, auth_token, callback=callback)


def get_feed_api(region, mws_access_key, mws_secret_key, seller_id, auth_token=None):
    # region = get_region(region)
    return mws.Feeds(mws_access_key, mws_secret_key, seller_id, auth_token=auth_token, region=region.upper(), version="2009-01-01")


def submit_invoice(region, credential, file_path, feed_options):
    feed_api = get_feed_api(region, credential.mws_access_key, credential.mws_secret_key, credential.seller_id,
                            auth_token=credential.mws_auth_token)

    return submit_flat_feed_to_amazon(region, feed_api, file_path, feed_type='_UPLOAD_VAT_INVOICE_',
                                      content_type='application/pdf',
                                      feed_options=feed_options)


def submit_flat_feed_to_amazon(region, feed_api, file_path, feed_type, content_type=None, callback=None, feed_options=None):
    if content_type is None:
        content_type = "text/tab-separated-values; charset=iso-8859-1"

    mktids = [marketplaceIdList[region.upper()]]

    with open(file_path, 'rb') as myfile:
        if content_type == 'application/octet-stream':
            feed = myfile
        else:
            feed = myfile.read()

        response = submit_to_amazon(region, feed_api, feed, feed_type, mktids, content_type, feed_options)
        print 'Submitted product feed: ', response._response_dict
        feed_submission_id = response._response_dict["SubmitFeedResult"]["FeedSubmissionInfo"]["FeedSubmissionId"]["value"]

        return get_feed_submission_result(feed_api, feed_submission_id, callback)


def submit_flat_file_to_amazon(region, file_path, type, mws_access_key, mws_secret_key, seller_id, auth_token=None, callback=None,
                               content_type="text/tab-separated-values; charset=iso-8859-1", feed_options=None):
    feed_api = get_feed_api(region, mws_access_key, mws_secret_key, seller_id, auth_token=auth_token)

    return submit_flat_feed_to_amazon(region, feed_api, file_path, type, content_type=content_type, callback=callback,
                                      feed_options=feed_options)


def submit_to_amazon(region, feed_api, feed, feed_type, mktids, content_type, feed_options):
    while True:
        try:
            return feed_api.submit_feed(feed, feed_type, marketplaceids=mktids,
                                        content_type=content_type, purge='false', feed_options=feed_options)

        except Exception as e:
            if "QuotaExceeded" in e.message:
                print "Quota exceeded, sleep 60 seconds"
                time.sleep(60)
            elif "RequestThrottled" in e.message:
                print "Request is throttled, sleep 120 seconds"
                time.sleep(120)
            elif 'rejected' in e.message and region in regions['EU']:
                current_index = regions['EU'].index(region)
                print current_index, region
                if current_index == (len(regions['EU']) - 1):
                    raise e
                region = regions['EU'][current_index + 1]
                mktids = [marketplaceIdList[region]]
            else:
                raise e


def get_feed_submission_result(feed_api, feed_submission_id, callback=None):
    wait = 1
    while wait < 10:
        try:
            submission_list = feed_api.get_feed_submission_list(feedids=[feed_submission_id])
            info = submission_list._response_dict["GetFeedSubmissionListResult"]["FeedSubmissionInfo"]
            print info

            id = info["FeedSubmissionId"]["value"]
            status = info["FeedProcessingStatus"]["value"]
            print 'Submission Id: {}. Current status: {}'.format(id, status)

            if status in ('_SUBMITTED_', '_IN_PROGRESS_', '_UNCONFIRMED_'):
                print 'Sleeping and check again....'
                time.sleep(wait * 30)
                wait = wait + 1
            elif status == '_DONE_':
                feedResult = feed_api.get_feed_submission_result(id)
                print feedResult.parsed

                if callback is not None:
                    callback(feedResult.parsed, feed_api.region, feed_api.account_id)
                else:
                    return feedResult.parsed
            else:
                print "Submission processing error. Quit."
                return "Submission processing error. Quit."

        except:
            print traceback.format_exc()
            time.sleep(2 * 60)
            return traceback.format_exc()
