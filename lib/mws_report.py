from lib.mws import mws
import time
import datetime
import os
import csv
import StringIO

marketplaceIdList = {
    "US": "ATVPDKIKX0DER",
    "CA": "A2EUQ1WTGCTBG2",
    "MX": "A1AM78C64UM0Y8",
    "UK": "A1F83G8C2ARO7P",
    "DE": "A1PA6795UKMFR9",
    "ES": "A1RKKUPIHCS9HS",
    "FR": "A13V1IB3VIYZZH",
    "IT": "APJ6JRA9NG5V4",
    "JP": "A1VC38T7YXB528"
}


def getReport(type, region, seller_id, mws_access_key, mws_secret_key, callback=None, fromdate=None, todate=None, country=None):
    if country == None:
        country = region

    mktids = [marketplaceIdList[country.upper()]]

    reportApi = mws.Reports(mws_access_key, mws_secret_key, seller_id, region=region, version="2009-01-01")

    response = reportApi.request_report(type, marketplaceids=mktids, start_date=fromdate, end_date=todate)

    repeat_submit = 0

    while True:
        request_report_list = reportApi.get_report_request_list(
            requestids=[response._response_dict["RequestReportResult"]["ReportRequestInfo"]["ReportRequestId"]["value"]])

        info = request_report_list._response_dict["GetReportRequestListResult"]["ReportRequestInfo"]
        print info

        id = info["ReportRequestId"]["value"]

        status = info["ReportProcessingStatus"]["value"]
        print 'Submission Id: {}. Current status: {}'.format(id, status)

        if status in ('_SUBMITTED_', '_IN_PROGRESS_', '_UNCONFIRMED_'):
            if status == '_SUBMITTED_':
                repeat_submit += 1

            # quit if submitted status repeat 5 times
            if repeat_submit >= 7:
                print "Submission repeat error. Quit."
                break

            print 'Sleeping and check again....'
            time.sleep(60)

        elif status == '_DONE_':
            generatedReportId = info["GeneratedReportId"]["value"]
            reportResult = reportApi.get_report(generatedReportId)

            # print reportResult

            parseReportData(reportResult.parsed, type, country, seller_id, callback)

            break
        else:
            print "Submission processing error. Quit."
            break


def parseReportData(data, type, country, seller_id, callback):
    rootDir = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

    fileName = rootDir + "/listings/" + type + "-" + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M") + ".csv"
    print fileName
    w = open(fileName, "wb")
    writer = csv.writer(w, delimiter=',')

    f = StringIO.StringIO(data)
    reader = csv.reader(f, delimiter="\t")

    index = 0
    for row in reader:
        index = index + 1

        if callback in ['None', 'print']:
            print row
        writer.writerow(row)

    w.close()

    if callback is not None and callback not in ['None', 'print']:
        callback(fileName, seller_id, country)


def get_report_request_list(report_type, region, seller_id, mws_access_key, mws_secret_key, auth_token=None, fromdate=None, todate=None,
                            callback=None):
    report_api = mws.Reports(mws_access_key, mws_secret_key, seller_id, auth_token=auth_token, region=region, version="2009-01-01")

    mktids = [marketplaceIdList[region.upper()]]

    request_report_list = report_api.get_report_request_list(types=[report_type], processingstatuses=['_DONE_'], fromdate=fromdate,
                                                             todate=todate, marketplaceids=mktids, max_count='100')

    parsed_response = request_report_list.parsed

    print seller_id, region, report_type, mktids

    info = parsed_response["ReportRequestInfo"]

    for report in info:
        generated_report_id = report["GeneratedReportId"]["value"]
        reportResult = report_api.get_report(generated_report_id)

        # print reportResult.parsed
        if callback is not None:
            callback(reportResult.parsed, region, seller_id)

        time.sleep(10)
    nextToken = info['NextToken'] if 'NextToken' in info else None
    while nextToken is not None:
        request_report_list = report_api.get_report_request_list_by_next_token(nextToken)
        parsed_response = request_report_list.parsed

        print seller_id, region, report_type, mktids

        info = parsed_response["ReportRequestInfo"]

        for report in info:
            generated_report_id = report["GeneratedReportId"]["value"]
            reportResult = report_api.get_report(generated_report_id)

            # print reportResult.parsed
            if callback is not None:
                callback(reportResult.parsed, region, seller_id)

            time.sleep(10)

        nextToken = info['NextToken'] if 'NextToken' in info else None
        print nextToken
