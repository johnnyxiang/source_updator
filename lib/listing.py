import re
import string

import unicodecsv as csv
import traceback

from lib.elastic_product import ElasticProduct
from lib.models import *
from lib.utils.asin_matcher import AsinMatcher
from lib.utils.sku_parser import SkuParser

elastic = ElasticProduct(host="35.199.3.83", port=80,http_auth=('elasticuser', 'KbersRiseUp153'))
asin_matcher = AsinMatcher()

eu = ['uk', 'de', 'fr', 'es', 'it', 'au']


def add_blacklist(asin, region, account=None, type=None, source='email', email_id=None):
    try:
        BlacklistAsin.create(asin=asin, type=type, country=region, source=source)
    except:
        traceback.format_exc()

    try:
        BlacklistAsinLog.create(asin=asin, type=type, country=region, account=account, email_id=email_id)
    except:
        traceback.format_exc()

    try:
        Blacklist.create(asin=asin)
    except:
        traceback.format_exc()

    try:
        if region.lower in eu:
            db_table = "blacklist_uk"
        else:
            db_table = "blacklist_%s" % region.lower()

        sql = "insert  into " + db_table + " (asin) values ('" + asin + "')"
        # print sql
        accountDB.execute_sql(sql)
    except:
        pass


def clear_by_ingredients(region):
    database_name = "restricted_ingredients_"
    if region in eu or region == 'ca':
        database_name = database_name + 'eu'
    else:
        database_name = database_name + region
    sql = "select name from " + database_name
    cursor = accountDB.execute_sql(sql)
    rows = cursor.fetchall()
    for row in rows:
        restricted_ingredient = row[0]
        ps = elastic.get_data_by_keyword(restricted_ingredient)

        asins = [p['asin'] for p in ps]
        print restricted_ingredient, len(asins), asins
        for asin in asins:
            # clean_asin(asin, region)
            add_blacklist(asin, region, type='restricted', account=restricted_ingredient, source='restricted ingredient')


def clear_db_by_ingredients(region, database_connection):
    region = region.lower()
    database_name = "restricted_ingredients_"
    if region in eu:
        database_name = database_name + 'eu'
    else:
        database_name = database_name + region
    sql = "select name from " + database_name
    cursor = accountDB.execute_sql(sql)
    rows = cursor.fetchall()

    product_table_name = 'product_data'
    if region in ['']:
        product_table_name = 'product_data_uk'
    elif region != 'us':
        product_table_name = 'product_data_%s' % region

    for row in rows:
        restricted_ingredient = row[0]
        clear_db_by_ingredient(region, restricted_ingredient, product_table_name, database_connection)


def remove_punctuation(s):
    # Awesome Case Cover/galaxy S4 Defender Case Cover(the Other Woman)
    s = s.replace(',', ' ').replace('-', ' ').replace('/', ' ').replace('(', ' ').replace(')', ' ')
    regex = re.compile('[%s]' % re.escape(string.punctuation))
    return regex.sub('', s)


def clear_file_by_ingredients(region, file_path, test=False):
    region = region.lower()
    database_name = "restricted_ingredients_"
    if region in eu:
        database_name = database_name + 'eu'
    else:
        database_name = database_name + region

    sql = "select name,type from " + database_name
    cursor = accountDB.execute_sql(sql)
    rows = cursor.fetchall()
    restricted_ingredients = [[remove_punctuation(row[0].lower()), row[1]] for row in rows if row[0] is not None and len(row[0]) > 0]

    print 'clear %s against %s, %d keywords found' % (file_path, database_name, len(restricted_ingredients))

    blacklist_skus = set()
    with open(file_path) as f:
        reader = csv.reader(f, delimiter=",")
        line_no = 0
        asin_col = 16
        title_col = 0
        sku_col = 3
        for line in reader:

            if line_no == 0:
                col_no = 0
                for t in line:
                    if t == 'product-id':
                        asin_col = col_no
                    if t == 'item-name':
                        title_col = col_no
                    if t == 'product-id':
                        asin_col = col_no
                    if t == 'seller-sku':
                        sku_col = col_no
                    col_no += 1

            try:
                line_no = line_no + 1
                asin = line[asin_col]
                sku = line[sku_col]
                title = line[title_col]

                sku = clear_row(region, restricted_ingredients, asin=asin, sku=sku, title=title, test=test)

                if sku is not None:
                    blacklist_skus.add(sku)
            except:
                print traceback.format_exc()
                pass

            if line_no % 1000 == 0:
                print line_no

    print "%s blacklist skus found." % len(blacklist_skus)

    return blacklist_skus


def clear_row(region, restricted_ingredients, asin, sku, title, test=False):
    sku_info = SkuParser.parse_sku(sku, region)
    # if sku_info['product_code'] != 'p':
    #     return None
    check = contain_blacklist_ingre(title, restricted_ingredients, sku_info['product_code'])
    if check is not None:
        print check, asin, sku, title
        if test is False:
            # clean_asin(asin, region)
            add_blacklist(asin, region, type='restricted', account=check, source='restricted ingredient')
        return sku

    return None


def contain_blacklist_ingre(title, restricted_ingredients, product_code='p'):
    #
    title = remove_punctuation(title.lower())
    title_words = set(title.split())
    for restricted_ingredient in restricted_ingredients:
        restricted_ingredient_word = restricted_ingredient[0].lower()
        restricted_ingredient_type = restricted_ingredient[1].lower()
        if restricted_ingredient_type != 'a' and restricted_ingredient_type != product_code:
            continue
        ingredient_words = set(restricted_ingredient_word.split())
        # print ingredient_words,title_words
        if ingredient_words.issubset(title_words) is True:
            return restricted_ingredient

    return None


def clear_db_by_brands(region, database_connection):
    region = region.lower()
    database_name = "blacklist_brands"
    if region in eu:
        database_name = database_name + '_uk'
    elif region != 'us':
        database_name = database_name + region
    sql = "select brand from " + database_name
    cursor = accountDB.execute_sql(sql)
    rows = cursor.fetchall()

    product_table_name = 'product_data'
    if region != 'us':
        product_table_name = 'product_data_%s' % region

    for row in rows:
        restricted_ingredient = row[0]
        clear_db_by_brand(region, restricted_ingredient, product_table_name, database_connection)

    if region != 'us':
        sql = "SELECT brand FROM blacklist_brands"
        cursor = accountDB.execute_sql(sql)
        rows = cursor.fetchall()
        for row in rows:
            restricted_ingredient = row[0]
            clear_db_by_brand(region, restricted_ingredient, product_table_name, database_connection)


def clear_db_by_brand(region, brand, product_table_name, database_connection):
    return
    if brand is None:
        return

    brand = brand.strip()
    sql = "select asin,name from " + product_table_name + " where name like %s or name like %s or name like %s"

    cursor = database_connection.execute_sql(sql, ['% ' + brand + ' %s', '% ' + brand, brand + ' %s'])
    asins = cursor.fetchall()
    # asins = [p['asin'] for p in ps]
    print brand, len(asins)
    for asin in asins:
        try:
            if 'paperback' in asin[1].lower() or 'hardcover' in asin[1].lower() or \
                            'audio' in asin[1].lower() or ' cd' in asin[1].lower():
                continue
            print brand, ':', asin[0], asin[1]
            # clean_asinasin(asin[0], region)
            add_blacklist(asin[0], region, type='brand', account=brand)
        except:
            pass


def clear_db_by_ingredient(region, restricted_ingredient, product_table_name, database_connection):
    if len(restricted_ingredient) <= 3:
        return

    sql = "select asin,name from " + product_table_name + " where MATCH (name) AGAINST (%s IN BOOLEAN MODE)"
    restricted_ingredient = restricted_ingredient.replace("-", " ").replace("(", " ").replace(")", " ")
    restricted_ingredient = " ".join(
        ['+' + p.replace("'", '').replace('"', '') for p in restricted_ingredient.split(" ") if len(p) > 0])

    print restricted_ingredient
    cursor = database_connection.execute_sql(sql, [restricted_ingredient])
    asins = cursor.fetchall()
    # asins = [p['asin'] for p in ps]
    print restricted_ingredient, len(asins)
    for asin in asins:
        try:
            if 'paperback' in asin[1].lower() or 'hardcover' in asin[1].lower() or \
                            'audio' in asin[1].lower() or ' cd' in asin[1].lower():
                continue
            print restricted_ingredient, ':', asin[0], asin[1]
            # clean_asin(asin[0], region)
            add_blacklist(asin[0], region, type='restricted', account=restricted_ingredient, source='restricted ingredient')
        except:
            pass


def clear_by_brand(account_info, region, elastic_server=None):
    last_blackbrand_id = account_info.last_blackbrand_id
    if account_info.country.lower() in ["uk", "ca", "cn", "jp"]:
        brands = BlacklistBrandUK.select().where(BlacklistBrandUK.id > last_blackbrand_id)
    # elif account_info.country == 'ca':
    #     brands = BlacklistBrandCA.select().where(BlacklistBrandCA.id > last_blackbrand_id)
    else:
        brands = BlacklistBrand.select().where(BlacklistBrand.id > last_blackbrand_id)

    if elastic_server is None:
        elastic_server = elastic
    if len(brands) > 0:
        for brandObj in brands:

            # reload
            if account_info.country.lower() in ["uk", "ca", "cn", "jp"]:
                brandObj = BlacklistBrandUK.get(BlacklistBrandUK.id == brandObj.id)
            else:
                brandObj = BlacklistBrand.get(BlacklistBrand.id == brandObj.id)

            if brandObj.cleared < 2:
                clear_brand(brandObj.brand, region, elastic_server)
                if elastic_server != elastic:
                    clear_brand(brandObj.brand, region, elastic)

                brandObj.cleared = brandObj.cleared + 1
                brandObj.save()

            last_blackbrand_id = brandObj.id

            if last_blackbrand_id != account_info.last_blackbrand_id:
                account_info.last_blackbrand_id = last_blackbrand_id
                account_info.save()


def clear_all_brand(region, elastic_server=None):
    last_blackbrand_id = 0
    if region in ["uk", "ca", "cn", "jp"]:
        brands = BlacklistBrandAll.select().where(BlacklistBrandAll.id > last_blackbrand_id,
                                                  BlacklistBrandAll.confirmed >= 0,
                                                  BlacklistBrandAll.cleared < 1)
    else:
        brands = BlacklistBrandAll.select().where(BlacklistBrandAll.id > last_blackbrand_id,
                                                  BlacklistBrandAll.confirmed >= 0,
                                                  BlacklistBrandAll.cleared < 1,
                                                  BlacklistBrandAll.country == 'us')

    total = len(brands)
    print "total %s brands to clean" % total

    if elastic_server is None:
        elastic_server = elastic
    if len(brands) > 0:
        index = 0
        for brandObj in brands:
            index = index + 1
            print "%s/%s - %s" % (index, total, brandObj.brand)
            brandObj.cleared = brandObj.cleared + 1
            brandObj.save()
            clear_brand(brandObj.brand, region, elastic_server)
            if elastic_server != elastic:
                clear_brand(brandObj.brand, region, elastic)


def get_brand_from_asin(asin):
    p = elastic.get_by_asin(asin)
    if p is None:
        return None

    if 'brand' in p and len(p['brand']) > 0:
        return p['brand'].replace("Brand:", "").strip()

    return None


def clean_brand_from_asin(brand, region, created_at=None, source=None, account=None, email_id=None):
    if brand.startswith("B0") is True:
        return

    try:
        BlacklistBrandAll.create(brand=brand, country=region, created_at=created_at, source=source)
        # sql = "INSERT INTO blacklist_brands_all (brand,country) VALUES (%s,%s)"
        # print '"%s" goes to blacklist db' % brand
        # accountDB.execute_sql(sql, [brand, region])
    except:
        pass

    if account is not None:
        try:
            sql = "INSERT INTO blacklist_brand_logs(brand,country,account,email_id) VALUES  (%s,%s,%s,%s)"
            accountDB.execute_sql(sql, [brand, region, account, email_id])
        except:
            pass

    try:
        sql = "INSERT INTO blacklist_brands (brand) VALUES (%s)"
        print '"%s" goes to blacklist db' % brand
        accountDB.execute_sql(sql, brand)
    except:
        pass

    if region in eu:
        try:
            sql = "INSERT INTO blacklist_brands_uk (brand) VALUES (%s)"
            print sql
            accountDB.execute_sql(sql, brand)
        except:
            pass

    if region.lower() == 'ca':
        try:
            sql = "INSERT INTO blacklist_brands_ca (brand) VALUES (%s)"
            print sql
            accountDB.execute_sql(sql, brand)
        except:
            pass


def clear_brand(brand, region, elastic_server=None):
    if elastic_server is None:
        elastic_server = elastic

    try:
        ps = elastic_server.get_data_by_brand(brand)
        if len(ps) == 0:
            return
            # fetch asin mappings
        asins = [item['asin'] for item in ps]
        duplicated_asins = asin_matcher.match_asins_by_api(asins)
        total = len(ps) + len(duplicated_asins)
        print brand, total
        index = 1
        with accountDBSave.atomic() as transaction:
            for item in ps:
                asin = item['asin']
                if 'brand' not in item:
                    item['brand'] = ''
                print "%s/%s" % (index, total), brand, asin, item['title'], item['brand']
                index = index + 1
                # clean_asin(asin, region)
                add_blacklist(asin, region, type='brand', account=brand, source='brand blacklist')

        with accountDBSave.atomic() as transaction:
            for item in duplicated_asins:
                print "duplicated %s/%s" % (index, total), brand, item
                index = index + 1
                # clean_asin(item['asin'], region)
                add_blacklist(item['asin'], region, type='brand', account=brand, source='brand blacklist')
    except:
        print traceback.format_exc()


def white_clear_brand(brand, region, elastic_server=None):
    if elastic_server is None:
        elastic_server = elastic

    try:
        ps = elastic_server.get_data_by_brand(brand)

        # fetch asin mappings
        asins = [item['asin'] for item in ps]
        duplicated_asins = asin_matcher.match_asins_by_api(asins)
        total = len(ps) + len(duplicated_asins)
        print brand, total
        index = 1
        with accountDB.atomic() as transaction:
            for item in ps:
                asin = item['asin']
                if 'brand' not in item:
                    item['brand'] = ''
                print "%s/%s" % (index, len(ps)), brand, asin, item['title'], item['brand']
                index = index + 1
                white_asin(asin, region)
            for item in duplicated_asins:
                print "duplicated %s/%s" % (index, total), brand, item
                index = index + 1
                white_asin(item['asin'], region)
    except:
        print traceback.format_exc()


def clean_asin(asin, region, db=None):
    if db is None:
        db_connection = database
    else:
        db_connection = db
    region_key = region.lower() if region.lower() not in eu else 'uk'
    # try:
    #     db_table = "deleted_asins_%s" % region_key
    #     sql = "INSERT INTO " + db_table + " (asin) value ('" + asin + "')"
    #     accountDB.execute_sql(sql)
    # except:
    #     pass

    try:
        db_table = "blacklist_%s" % region_key
        sql = "insert  into " + db_table + " (asin) values ('" + asin + "')"
        accountDBSave.execute_sql(sql)
    except:
        pass


def white_asin(asin, region, db=None):
    if db is None:
        db_connection = database
    else:
        db_connection = db
    region_key = region.lower() if region.lower() not in eu else 'uk'
    # try:
    #     db_table = "deleted_asins_%s" % region_key
    #     sql = "INSERT INTO " + db_table + " (asin) value ('" + asin + "')"
    #     accountDB.execute_sql(sql)
    # except:
    #     pass

    try:
        db_table = "blacklist_%s" % region_key
        sql = "delete from " + db_table + "  where asin = %s"
        accountDB.execute_sql(sql, asin)

        db_table = "black2white_%s" % region_key
        sql = "insert into " + db_table + " (asin) values(%s)"
        accountDB.execute_sql(sql, asin)
    except:
        pass


if __name__ == "__main__":
    # database.init("718eu", host=databaseHost, user=databaseUser, password=databasePW)
    # clear_db_by_ingredients('uk', database)
    # sql = "SELECT brand FROM b2w_brands"
    # cursor = accountDB.execute_sql(sql)
    # rows = cursor.fetchall()
    # for row in rows:
    #     print row[0]
    #     if len(row[0]) > 0:
    #         print white_clear_brand(row[0], 'us')
    # brands = BlacklistBrandUK.select().where(BlacklistBrandUK.id > 100)
    # index = 0
    # for brand in brands:
    #     index = index + 1
    #     print "%d/%d" % (index, len(brands))
    #     if " " not in brand.brand:
    #         continue
    # clear_brand('Liberty Imports', region='us')
    # clear_all_brand('us')
    # clear_by_brand(accountInfo, country)
    str = '1 X Lidan Paishi - 120 tablets,(Solstice)'
    restricted_ingredients = [['Solstice', 'p']]
    # restricted_ingredients = set([remove_punctuation(ingre.lower()) for ingre in ingres])

    title = remove_punctuation(str.lower())
    title_words = set(title.split())
    # print title, title_words,restricted_ingredients
    print contain_blacklist_ingre(title=str, restricted_ingredients=restricted_ingredients)
    # print str.translate(None, string.punctuation)
    pass
