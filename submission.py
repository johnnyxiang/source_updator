import sys
import argparse

from lib.mws import mws
from lib.models import *
from local import sids
import time


def count_submissions(access_key, secret_key, account_id, region='US'):
    feedApi = mws.Feeds(access_key, secret_key, account_id, region=region, version="2009-01-01")
    result = feedApi.get_feed_submission_count(processingstatuses=['_IN_PROGRESS_', '_SUBMITTED_'])
    print result._response_dict['GetFeedSubmissionCountResult']['Count']['value']


def list_submissions(access_key, secret_key, account_id, region='US'):
    feedApi = mws.Feeds(access_key, secret_key, account_id, region=region, version="2009-01-01")
    feed_types = ['_POST_FLAT_FILE_INVLOADER_DATA_']
    lists = feedApi.get_feed_submission_list(processingstatuses=['_IN_PROGRESS_', '_SUBMITTED_'])
    feeds = lists._response_dict['GetFeedSubmissionListResult']['FeedSubmissionInfo']
    if 'FeedSubmissionId' in feeds:
        feeds = [feeds]
    for feed in feeds:
        for k, v in feed.iteritems():
            if len(k) == 0:
                continue
            try:
                print k, ': ', v['value']
            except:
                continue
                print k, ': ', v

        print '\t'

    if 'NextToken' in lists._response_dict['GetFeedSubmissionListResult']:
        time.sleep(5)
        next_token = lists._response_dict['GetFeedSubmissionListResult']['NextToken']['value']
        while next_token is not None and next_token != 'none':
            print next_token
            try:
                lists = feedApi.get_submission_list_by_next_token(next_token)
                data = lists._response_dict['GetFeedSubmissionListByNextTokenResult']
                feeds = data['FeedSubmissionInfo']
                if 'FeedSubmissionId' in feeds:
                    feeds = [feeds]
                for feed in feeds:
                    for k, v in feed.iteritems():
                        if len(k) == 0:
                            continue
                        try:
                            print k, ': ', v['value']
                        except:
                            continue
                            print k, ': ', v

                    print '\t'
                if 'NextToken' in data:
                    next_token = data['NextToken']['value']
                    time.sleep(5)
                else:
                    next_token = None

            except:
                next_token = None
                print traceback.format_exc()


def cancel_submission(access_key, secret_key, account_id, feed_id, region='US'):
    feedApi = mws.Feeds(access_key, secret_key, account_id, region=region, version="2009-01-01")
    res = feedApi.cancel_feed_submissions(feedids=[feed_id])
    print res._response_dict


def cancel_submissions(access_key, secret_key, account_id, region='US'):
    feedApi = mws.Feeds(access_key, secret_key, account_id, region=region, version="2009-01-01")
    res = feedApi.cancel_feed_submissions()
    print res._response_dict


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-t', '--type', type=str, default="list", help='list,cancel, cancel_all')
    parser.add_argument('-f', '--feed_id', type=str, default="", help='Feed ID')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()
    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    print args

    database_name = args.account
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)
    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        print "No account info for " + args.account + " found"
        sys.exit()

    country = args.country.upper()
    if args.type == 'count':
        count_submissions(accountInfo.mws_access_key,
                          accountInfo.mws_secret_key, accountInfo.seller_id, country)
    elif args.type == 'cancel' and len(args.feed_id) > 0:
        cancel_submission(accountInfo.mws_access_key,
                          accountInfo.mws_secret_key, accountInfo.seller_id, args.feed_id, country)
    elif args.type == 'cancel_all' and len(args.feed_id) > 0:
        cancel_submissions(accountInfo.mws_access_key,
                           accountInfo.mws_secret_key, accountInfo.seller_id, country)
    else:
        list_submissions(accountInfo.mws_access_key,
                         accountInfo.mws_secret_key, accountInfo.seller_id, country)
