import sys
import argparse
import traceback
import datetime
import time
from lib.utils.simple_logger import SimpleLogger
from lib import mws_product
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
from lib.models import *
from local import sids

logger = SimpleLogger.get_stream_logger('source_price_updator')
offer_service_price_finder = OfferServicePriceFinder()


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-ta', '--target_account', default="", help='Target Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-tc', '--target_country', default="us", help='Target country')
    parser.add_argument('-d', '--condition', type=str, default="new", help='Source ASIN condition')
    parser.add_argument('-t', '--type', type=str, default="product", help='Product Type: product, book or cd')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-o', '--offset', type=int, default=0, help='offset')
    parser.add_argument('-s', '--size', type=int, default=20, help='page size')
    parser.add_argument('-m', '--mode', type=str, default="production", help='running mode')
    parser.add_argument('-f', '--min_id', type=int, default=0, help='min id')
    parser.add_argument('-mh', '--max_hour', type=int, default=6, help='max hour')
    command_args = parser.parse_args()
    return command_args


def check_by_es(ItemIds, db_table, earliest_date):
    # check es server
    try:
        update_sql = " insert into " + db_table + "  (sku,real_new_price_to_update,real_price_last_checked) values "
        values = []
        found = []
        offers_info = offer_service_price_finder.find_offer_for_asins([p['asin'] for p in ItemIds], "us", condition)
        if offers_info is not None:
            for p in ItemIds:
                try:
                    asin = p['asin']
                    sku = p['sku']

                    if asin not in offers_info or offers_info[asin] is None or 'product_price' not in offers_info[asin]:
                        continue

                    offer = offers_info[asin]
                    # sometimes, event query with new condition, used condition offers returned
                    if condition.lower() == "new" and offer['item_condition'] != 'new':
                        continue

                    if 'product_price' in offer:
                        offer_time_str = offer['time'][:19]
                        offer_time = datetime.datetime.strptime(offer_time_str, '%Y-%m-%dT%H:%M:%S')
                        if offer_time > earliest_date:
                            values.append("('" + sku + "'," + str(
                                round(offer['product_price'] + offer['shipping_price'], 2)) + ",'" + offer_time_str + "')")
                            found.append(asin)

                            print asin, round(offer['product_price'] + offer['shipping_price'], 2), offer_time_str
                    else:
                        pass
                except Exception as e:
                    print traceback.format_exc()

            if len(values) > 0:
                update_sql = update_sql + ",".join(values)
                update_sql = update_sql + " ON DUPLICATE KEY UPDATE real_new_price_to_update=VALUES(real_new_price_to_update),real_price_last_checked=VALUES(real_price_last_checked);"
                # print sql
                database.execute_sql(update_sql)

            ItemIds = [item for item in ItemIds if item['asin'] not in found]

    except Exception as e:
        print traceback.format_exc()

    return ItemIds


def process(country, productApi, target_account, target_country, ptype, condition, minId, size, offset, limit, max_hour=6):
    database_name = target_account
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    if ptype == "product":
        db_table = "products"
    elif ptype == "cd":
        db_table = "cds"
    else:
        db_table = "books"

    if target_country.upper() != "US":
        db_table = db_table + "_" + target_country.lower()
    condition = condition.lower()
    found = 0
    pindex = 0

    while True:
        with database.atomic() as transaction:  # Opens new transaction.
            try:
                earliest_date = (datetime.datetime.utcnow() - datetime.timedelta(hours=max_hour))
                last_date = earliest_date.strftime('%Y-%m-%d %H:%M:%S')
                sql = "select real_asin,sku,real_new_price,id,`condition`,real_price_last_checked  from " + db_table + " where status >=0 and real_asin <> 'None' and real_asin is not null "

                if ptype != "book":
                    sql = sql + " and id > %d" % minId
                    sql = sql + " and real_new_price_to_update = 0 "
                else:
                    sql = sql + " and real_price_last_checked <= '%s'" % last_date

                if condition.lower() != "all":
                    sql = sql + " and `condition` = '%s' " % condition
                if ptype != "book":
                    sql = sql + " order by id asc"
                else:
                    sql = sql + " order by real_price_last_checked asc"

                sql = sql + " limit %d" % size

                if offset > 0:
                    sql = sql + " offset %d" % offset

                print sql

                cursor = database.execute_sql(sql)
                rows = cursor.fetchall()

                if len(rows) == 0:
                    break

                ItemIds = []
                min_date = ''
                for row in rows:
                    asin = str(row[0]).zfill(10)
                    ItemIds.append(
                        {'id': row[3], 'asin': asin, 'sku': row[1], 'current_source_price': row[2], 'condition': row[4]})
                    pindex = pindex + 1
                    minId = row[3]
                    min_date = row[5]

                print target_account, "loading ", minId, min_date, len(ItemIds), '; total found ', found

                if len(ItemIds) > 0:
                    ItemIds = check_by_es(ItemIds, db_table, earliest_date)
                    print len(ItemIds)

                if len(ItemIds) > 0:
                    try:
                        update_sql = " insert into " + db_table + " (sku,real_new_price_to_update,real_price_last_checked) values "
                        values = []

                        chunks = [ItemIds[x:x + 20] for x in xrange(0, len(ItemIds), 20)]
                        offers_info = {}
                        offers_to_save = {}
                        for chunk in chunks:
                            asins = [item["asin"] for item in chunk]
                            chunk_offer_info = mws_product.getPriceForAsinsFromMws(productApi, asins, condition, country,
                                                                                   product_type=ptype)
                            offers_info.update(chunk_offer_info[condition])
                        # print offers_info

                        for p in ItemIds:
                            now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
                            try:
                                asin = p['asin']
                                pid = p['id']
                                sku = p['sku']

                                if asin not in offers_info or 'product_price' not in offers_info[asin]:
                                    print pid, asin, 'no offer info'
                                    values.append("('" + sku + "',0,'" + now + "')")
                                    continue

                                offer = offers_info[asin]

                                # sometimes, event query with new condition, used condition offers returned
                                if condition.lower() == "new" and offer['item_condition'].lower() != 'new':
                                    print pid, asin, 'no offer info'
                                    values.append("('" + sku + "',0,'" + now + "')")
                                    continue

                                if 'product_price' in offer:
                                    # print pid, asin,offer['product_price'],offer['shipping_price'],offer['time']
                                    values.append("('" + sku + "'," + str(
                                        round(offer['product_price'] + offer['shipping_price'], 2)) + ",'" + now + "')")
                                    offer["product_code"] = "p" if ptype == "product" else "c"
                                    offer["country_code"] = country
                                    offer['time'] = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
                                    offer['asin'] = asin
                                    offer['item_condition'] = condition

                                    offers_to_save[asin] = offer
                                    found = found + 1

                                    print pid, asin, round(offer['product_price'] + offer['shipping_price'], 2)
                                else:
                                    print pid, asin, 'no offer info'
                                    values.append("('" + sku + "',0,'" + now + "')")
                            except Exception as e:
                                print pid, asin, 'no offer info'
                                values.append("('" + sku + "',0,'" + now + "')")

                        update_sql = update_sql + ",".join(values)
                        update_sql = update_sql + " ON DUPLICATE KEY UPDATE real_new_price_to_update=VALUES(real_new_price_to_update),real_price_last_checked=VALUES(real_price_last_checked);"
                        # print update_sql
                        database.execute_sql(update_sql)
                        if len(offers_to_save) > 0:
                            try:
                                offer_service_price_finder.save_offers(offers_to_save, country, condition)
                            except:
                                print traceback.format_exc()
                                pass

                        time.sleep(1)

                    except Exception as e:
                        print traceback.format_exc()

                if limit != 0 and pindex >= limit:
                    break
            except:
                print traceback.format_exc()


def merge_two_dicts(x, y):
    z = x.copy()  # start with x's keys and values
    z.update(y)  # modifies z with y's keys and values & returns None
    return z


if __name__ == "__main__":
    args = parse_args()

    print args
    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    condition = args.condition
    country = args.country.upper()
    offset = args.offset
    limit = args.limit
    ptype = args.type
    max_hour = args.max_hour

    if args.mode == "d" or args.mode == "debug":
        debug = True
    else:
        debug = False

    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        logger.error("No account info for " + args.account + " found")
        sys.exit()

    productApi = mws_product.getProductApi(accountInfo.seller_id, accountInfo.mws_access_key,
                                           accountInfo.mws_secret_key, country)

    minId = args.min_id
    size = args.size

    targets_eu = {"401eu": "uk", "711eu": "uk", "713eu": "uk", "714eu": "uk", "719eu": "uk"}
    targets_us = {"702us": "us", "707us": "us", "709us": "us", "710us": "us", "713us": "us", "720us": "us",
                  "722us": "us", "724us": "us"}
    if len(args.target_country) > 0:
        if args.target_account.lower() == "all":
            if args.target_country.lower() == "all":
                targets = merge_two_dicts(targets_eu, targets_us)
            elif args.target_country == "us":
                targets = targets_us
            else:
                targets = targets_eu
        else:
            targets = {args.target_account: args.target_country}
    else:
        targets = {args.account: country}
    print targets
    for ta, tc in targets.iteritems():
        process(country, productApi, ta, tc, ptype, condition, minId, size, offset, limit, max_hour)
