import argparse
import csv

import datetime

import StringIO
import requests

from lib.listing import add_blacklist


def parse_args():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-d', '--date', type=str, default=None, help='Date')
    parser.add_argument('-b', '--days_back', type=int, default=10, help='days back')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":

    args = parse_args()

    if args.date is None:
        tracking_date = datetime.datetime.utcnow().strftime('%Y-%m-%d')
    else:
        tracking_date = args.date

    base = datetime.datetime.strptime(tracking_date, '%Y-%m-%d')
    date_list = [base - datetime.timedelta(days=x) for x in range(0, args.days_back)]
    for date in date_list:
        tracking_date = date.strftime('%Y-%m-%d')
        url = "http://dsi.dovertmt.club/index/badasin?date=%s" % tracking_date
        r = requests.get(url)

        f = StringIO.StringIO(r.content)
        reader = csv.reader(f, delimiter=",")

        row_no = 0
        for row in reader:
            # ASIN,Store Name
            if len(row) == 0 or row[0].lower() == 'asin':
                continue
            row_no = row_no + 1
            try:
                asin = row[0]

                account = row[1].lower() if len(row) > 1 else ""
                if len(account) == 0:
                    region = 'us'
                elif 'us' in account:
                    region = 'us'
                else:
                    region = 'uk'

                # add_blacklist(asin, region)
                add_blacklist(asin, region, account=row[1], type='cs', source='dsi')
                print tracking_date, row_no, region, row
            except:
                print row_no
