#!/usr/local/bin/python
# coding: utf-8
import sys
import argparse
import datetime
import email
import email.header
from _ssl import SSLEOFError
from email.header import decode_header
from lib.elastic_product import ElasticProduct
from lib.general import get_lock
from lib.listing import add_blacklist, clean_brand_from_asin, get_brand_from_asin
from lib.models import *
import re
import poplib

from lib.utils.asin_matcher import AsinMatcher

reload(sys)
sys.setdefaultencoding("utf-8")

dir = os.path.dirname(os.path.dirname(__file__))
region = "US"
database_name = None

elastic = ElasticProduct(host="35.199.3.83", port=80,http_auth=('elasticuser', 'KbersRiseUp153'))
asin_matcher = AsinMatcher()


def init_db():
    try:
        sql = "SELECT 1 FROM emails LIMIT 1"
        database.execute_sql(sql)
    except:
        sql = "CREATE TABLE emails (" \
              "id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT," \
              "type VARCHAR(20) DEFAULT NULL," \
              "AmazonOrderId VARCHAR(20) DEFAULT NULL," \
              "subject VARCHAR(255) DEFAULT NULL," \
              "fromName VARCHAR(255) DEFAULT NULL," \
              "fromAddress VARCHAR(255) DEFAULT NULL," \
              "messageId VARCHAR(255) DEFAULT NULL," \
              "email TEXT," \
              "created_at timestamp NULL DEFAULT NULL," \
              "updated_at DATETIME DEFAULT NULL," \
              "uid INT(11) DEFAULT '0'," \
              "parsed TINYINT(1) DEFAULT '0'," \
              "copyright_checked TINYINT(1) DEFAULT '0'," \
              "toName VARCHAR(255) DEFAULT NULL," \
              "toAddress VARCHAR(255) DEFAULT NULL," \
              "in_reply_to VARCHAR(250) DEFAULT NULL," \
              "`references` VARCHAR(250) DEFAULT NULL," \
              "status VARCHAR(20) DEFAULT 'New'," \
              "PRIMARY KEY (`id`)," \
              "KEY `type` (`type`)," \
              "KEY AmazonOrderId (`AmazonOrderId`)," \
              "KEY created_at (`created_at`)," \
              "KEY messageId (`messageId`)," \
              "FULLTEXT KEY email (`email`)" \
              ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;"
        # print sql
        try:
            database.execute_sql(sql)
        except:
            if "exists" not in traceback.format_exc():
                print traceback.format_exc()
                sys.exit(0)


email_keywords = {}
for keyword in EmailKeyword.select():
    if keyword.type not in email_keywords:
        email_keywords[keyword.type] = []
    email_keywords[keyword.type].append(keyword.keyword)

restrictedProductSubjects = email_keywords['restricted_product_subject']
productRemovalSubjects = email_keywords['product_removal_subject']
brandWarningKeywords = email_keywords['ip_warning']
returnSubjects = email_keywords['return_subject']
cancellationSubjects = email_keywords['cancellation_subject']
deliveryInquirySubjects = email_keywords['delivery_inquiry_subject']
changeAddressSubjects = email_keywords['change_address_subject']
fasterSubjects = email_keywords['faster_subject']
atozSubjects = email_keywords['atoz_subject']
invoiceSubjects = email_keywords['invoice_subject']
returnPolicySubjects = email_keywords['return_policy_subject']
policyWarningSubjects = email_keywords['policy_warning_subject']
yourEmailToSubjects = email_keywords['your_email_subject']
receivedWrongItemSubjects = email_keywords['wrong_item_subject']
receivedDamagedItemSubjects = email_keywords['damaged_item_subject']
subjectTypes = {
    "return": returnSubjects,
    "cancellation": cancellationSubjects,
    "delivery": deliveryInquirySubjects,
    "change_address": changeAddressSubjects,
    "faster": fasterSubjects,

    "invoice": invoiceSubjects,
    "return_policy": returnPolicySubjects,
    "pl_warning": policyWarningSubjects,
    "new_order": ['sold', 'Artikel verkauft'],
    "message": ['your message'],

    "wrong_item": receivedWrongItemSubjects,
    "damaged_item": receivedDamagedItemSubjects,
    "restricted": restrictedProductSubjects,
    "a-to-z": atozSubjects,
    "useless": email_keywords['useless']
}


def parse_asins(line):
    return re.findall("B0[0-9A-Z]{8}", line) + re.findall("[0-9]{1}[0-9A-Z]{9}", line)


def read_restricted_products(email_obj):
    subject, content = email_obj.subject, email_obj.email_content()
    restricted_removal = False
    for restrictedProductSubject in restrictedProductSubjects:
        if restrictedProductSubject in subject:
            restricted_removal = True
            break
    if restricted_removal is False:
        return

    # if email_obj.type == "" or email_obj.type is None:
    email_obj.type = "restricted"
    email_obj.save()

    lines = content.split('\n')

    for line in lines:
        if 'http' in line:
            continue
        # print line
        asins = parse_asins(line)
        for asin in asins:
            add_blacklist(asin, region, account=accountInfo.name, type='restricted', email_id=email_obj.id)


def read_product_removal(email_obj):
    subject, content = email_obj.subject, email_obj.email_content()
    if subject not in productRemovalSubjects:
        return

    if email_obj.type == "" or email_obj.type is None:
        email_obj.type = "restricted"
        email_obj.save()

    lines = content.split('\n')

    flags = [
        "Affected Products:",
        "Productos Afectados:",
        "Produits concerns :",
        "Betroffene Artikel:",
        "Prodotti interessati:"
    ]

    begin = False
    for line in lines:
        if begin is True:
            try:
                pName = line.strip()
                print pName
            except:
                print traceback.format_exc()
        else:
            for flag in flags:
                if flag in line:
                    begin = True
    return


denied = ['denied', 'rifiutato il reclamo', 'Nous avons refus la rclamation', 'negado', 'negó el reclamo',
          'Wir haben den A-bis-z Garantieantrag in Hhe von']
withdrawn = ['have withdrawn the claim', 'Ich habe den Anspruch zurckgezogen', 'reclamo ritirato', 'withdrew']
closed = ['geschlossen', 'We have closed an A-to-z Guarantee']
granted = ['accorde', 'effettuato', 'granted', 'You are responsible for the claim', 'Es responsable de esta reclamacin',
           'have granted the customers', 'Garantieanspruch', 'Hemos aprobado una reclamacin bajo la Garanta',
           'wurde stattgegeben', 'Abbiamo concesso un rimborso con Garanzia',
           'Nous avons accept', 'Hemos aprobado una reclamacin',
           'Why was my appeal denied', 'Por qu se ha denegado mi recurso', 'ricorso stato negato',
           'abbiamo accolto il reclamo', 'Wir haben eine Erstattung', 'abbiamo emesso un rimborso',
           'Nous vous tenons responsable de ces rclamations lorsque',
           'vous tenons responsable de ces rclamations lorsque', 'Wir haben dem A-bis-z Garantieantrag']


# credit card issuer
def parse_chargeback(email_obj):
    content = email_obj.email_content().lower().encode("utf-8")
    if 'credit card issuer' not in content:
        return

    email_obj.type = 'chargeback'
    email_obj.save()

    try:
        chargeback = ChargeBack.get(ChargeBack.amazon_order_id == email_obj.AmazonOrderId)
    except:
        chargeback = ChargeBack()
        chargeback.amazon_order_id = email_obj.AmazonOrderId
        chargeback.created_at = email_obj.created_at

    # if 'you are not responsible' in content:
    #     chargeback.az_status = 'Closed'

    chargeback.last_updated = email_obj.created_at
    chargeback.save()


def parse_a2z(email_obj):
    content = email_obj.email_content().lower().encode("utf-8")

    if email_obj.type != 'a-to-z':
        is_withdrawn = None
        for key in withdrawn:
            if key.lower() in content:
                is_withdrawn = True
                break

        if is_withdrawn is True:
            try:
                a2z = AtoZ.get(AtoZ.amazon_order_id == email_obj.AmazonOrderId)
                a2z.az_status = 'Withdrawn'
                a2z.save()
            except:
                pass
        return

    try:
        a2z = AtoZ.get(AtoZ.amazon_order_id == email_obj.AmazonOrderId)
    except:
        a2z = AtoZ()
        a2z.amazon_order_id = email_obj.AmazonOrderId
        a2z.created_at = email_obj.created_at

    a2z.last_updated = email_obj.created_at

    for key in granted:
        if key.lower() in content:
            a2z.az_status = 'Granted'
            a2z.save()
            return

    for key in withdrawn:
        if key.lower() in content:
            a2z.az_status = 'Withdrawn'
            a2z.save()
            return

    for key in closed:
        if key.lower() in content:
            a2z.az_status = 'Closed'
            a2z.save()
            return

    for key in denied:
        if key.lower() in content:
            a2z.az_status = 'Denied'
            a2z.save()
            return
    a2z.save()


# ASIN:B00W9GPKWQ
# B00W9GMKWE
# B00W9G8U3W
# Infringement type: Copyright
# Complaint ID: 5239237781
def parse_complaint(email_obj):
    if "complaint id" not in email_obj.email.lower():
        return
    # if "ebsafetyipro12@gmail.com" == email_obj.toAddress or "ebsafetyipro12@gmail.com" == email_obj.fromAddress:
    #     return

    lines = email_obj.email_content().split('\n')
    complaint_id = None
    type = ""
    asins = []
    emails = []
    for line in lines:
        line = str(line)
        if line.startswith(">"):
            continue
        if "Complaint ID:" in line:
            c_id = line.replace("Complaint ID:", "").strip()
            if len(c_id) == 10 and c_id.isdigit():
                complaint_id = c_id
                continue

        if "Infringement type:" in line:
            type = line.replace("Infringement type:", "").strip()
            continue

        line_asins = re.findall("B0[0-9A-Z]{8}", line)
        if len(line_asins) > 0:
            asins = asins + line_asins
            continue

        match = re.search(r'[\w\.-]+@[\w\.-]+', line)
        if match:
            email = match.group(0)
            if "@amazon" not in email:
                emails.append(email.replace("--", ""))
                continue

    asins = list(set(asins))
    emails = list(set(emails))
    skus = []

    if 'eu' in database_name.lower():
        product_db = 'products_uk'
        listing_db = 'all_listings_uk'
    elif 'ca' in database_name.lower():
        product_db = 'products_ca'
        listing_db = 'all_listings_ca'
    elif 'jp' in database_name.lower():
        product_db = 'products_jp'
        listing_db = 'all_listings_jp'
    elif 'au' in database_name.lower():
        product_db = 'products_au'
        listing_db = 'all_listings_au'
    else:
        product_db = 'products'
        listing_db = 'all_listings'

    try:
        for asin in asins:
            sql = 'SELECT sku FROM ' + product_db + ' WHERE fake_asin = %s'
            cursor = database.execute_sql(sql, [asin])
            row = cursor.fetchone()
            if row is not None:
                skus.append(row[0])
    except:
        print traceback.format_exc()
        pass

    try:
        for asin in asins:
            sql = 'SELECT sku FROM ' + listing_db + ' WHERE asin = %s'
            cursor = database.execute_sql(sql, [asin])
            row = cursor.fetchone()
            if row is not None:
                skus.append(row[0])
    except:
        print traceback.format_exc()
        pass

    if complaint_id is not None:
        sql = "insert ignore into cr_complaints (account_code,asin,type,complaint_id,email,date_created) value(%s,%s,%s,%s,%s,%s)"
        accountDB.execute_sql(sql, [database_name, ",".join(asins), type, complaint_id, ",".join(emails),
                                    email_obj.created_at])

        sql = 'UPDATE cr_complaints SET sku=%s WHERE complaint_id=%s'
        accountDB.execute_sql(sql, [",".join(skus), complaint_id])
        # email_obj.uid = complaint_id
        # email_obj.save()

    print 1, complaint_id, type, asins, skus, emails


cleared_brand = []


def parse_copyright_warning(emailObj):
    if emailObj.subject in ["restricted"]:
        return
    if len(emailObj.AmazonOrderId) > 0:
        return
    content = emailObj.email_content()
    if emailObj.type == "pl_warning":
        try:
            emailObj.type = ""
            emailObj.save()
        except:
            pass

    if emailObj.fromAddress is None:
        return

    if emailObj.fromAddress in ['auto-communication@amazon.com', 'listing-error-feedback@amazon.com',
                                'amazon-selling-coach@amazon.com',
                                'seller-info@amazon.com', 'noreply@amazon.com', accountInfo.email_address.lower()]:
        return
    if emailObj.AmazonOrderId is not None and len(emailObj.AmazonOrderId) > 0:
        return

    if "ebsafetyipro12@gmail.com" == emailObj.toAddress:
        return

    for keyword in brandWarningKeywords:
        if keyword.replace(" ", "").lower() in content.replace(" ", "").lower() or \
                        keyword.replace(" ", "").lower() in emailObj.subject.replace(" ", "").lower():

            lines = content.split('\n')

            for line in lines:
                if 'http' in line or 'Complaint ID' in line:
                    continue
                if 'implementing approval requirements for' in line:
                    brands = re.findall("implementing approval requirements for (.*) products", line)
                    if len(brands) > 0:
                        for brand in brands:
                            clean_brand_from_asin(brand, region, created_at=emailObj.created_at, account=accountInfo.name,
                                                  email_id=emailObj.id, source='email')
                            cleared_brand.append(brand)

                # print line
                asins = parse_asins(line)
                # print asins
                if len(asins) == 0:
                    continue

                if emailObj.type != "pl_warning":
                    try:
                        emailObj.type = "pl_warning"
                        emailObj.save()
                    except:
                        pass

                isbns = asin_matcher.match_by_api(asins)
                if len(isbns) > 0:
                    asins = asins + isbns.keys() + isbns.values()
                    asins = list(set(asins))

                for asin in asins:
                    # add_blacklist(asin, region)
                    add_blacklist(asin, region, account=accountInfo.name, type='ip', email_id=emailObj.id)

                    if 'potential misuse' not in content:
                        try:
                            brand = get_brand_from_asin(asin)
                            print "\n", asin, emailObj.id, brand, "\n"
                            if brand is not None and brand not in cleared_brand:
                                clean_brand_from_asin(brand, region, created_at=emailObj.created_at, account=accountInfo.name,
                                                      email_id=emailObj.id, source='email')
                                cleared_brand.append(brand)
                        except:
                            traceback.format_exc()
            break


def parse_order_id(email_address, emailObj, save=True):
    subject = emailObj.subject.replace("\n", " ")
    orderIds = re.findall(r"[0-9]{3}-[0-9]{7}-[0-9]{7}", subject)

    if orderIds is None or len(orderIds) == 0:
        content = emailObj.email
        lines = content.split('\n')

        for line in lines:
            orderIds = re.findall(r"[0-9]{3}-[0-9]{7}-[0-9]{7}", line)
            if orderIds is not None and len(orderIds) > 0:
                break

    # print orderIds
    if orderIds is not None and len(orderIds) > 0:
        emailObj.AmazonOrderId = orderIds[0]

        if save is True:
            emailObj.save()
        else:
            return orderIds[0]

    return ""


def parse_emails(email_address):
    for emailO in AccountEmail.select().where(AccountEmail.fromAddress != email_address,
                                              (AccountEmail.AmazonOrderId == "") | (
                                                          AccountEmail.AmazonOrderId == None)):
        print emailO.subject
        # emailO.type = parseEmailTypeBySubject(emailO.subject)
        # emailO.save()
        parse_copyright_warning(emailO)

    for emailO in AccountEmail.select().where(AccountEmail.subject << restrictedProductSubjects):
        read_restricted_products(emailO.subject, emailO.email)


# 01,A-Z Claim
# Mailman Reports
# 2,Order cancellation request
# 01,违禁品
# Follow Up Refund
# 1,Order shipping and delivery inquiry
# 01,Product details inquiry
# 1,Re: Order information
# 01, Return Requests
# 3,Received damaged or defective item
# 03,Received wrong item
# Feedback Removal Request Result

def parse_email_type_by_subject(email_object, labelIds=[]):
    for label in labelIds:
        if 'zOthers' in label:
            return 'Other'
        if 'Pricing Error' in label:
            return 'Pricing Error'
        if 'Mailman' in label:
            return 'Mailman Report'
        if 'SENT' in label:
            return 'Sent'
        if 'SPAM' in label:
            return 'Spam'

    subject = email_object.subject
    from_name = email_object.fromName.lower()
    from_email = email_object.fromAddress.lower()
    subject = subject.replace('\n', '')
    subject = subject.replace('\t', '').lower()

    if '[important]' in subject:
        return 'sg'

    for key, subjectKeywords in subjectTypes.iteritems():
        for keyword in subjectKeywords:
            if keyword.lower() in subject or keyword.lower() in from_name or keyword.lower() in from_email:
                return key
    if 'Product details'.lower() in subject:
        return 'product inquery'

    if 'Inquiry'.lower() in subject and email_object.AmazonOrderId is not None:
        return 'delivery'

    if 'CATEGORY_UPDATES' in labelIds:
        return 'Order Update'

    if email_object.AmazonOrderId is not None:
        return 'delivery'


def get_last_uid(folder):
    lastMessages = AccountEmail.select().where(AccountEmail.uid.startswith(folder)).order_by(
        AccountEmail.created_at.desc()).limit(1)

    if lastMessages is not None and len(lastMessages) > 0:
        return int(lastMessages[0].uid.replace(folder, ""))

    return 0


def parse_mail(email_obj):
    type = parse_email_type_by_subject(email_obj)
    if type != email_obj.type:
        email_obj.type = type
        email_obj.save()

    try:
        read_restricted_products(email_obj)
    except:
        pass

    try:
        read_product_removal(email_obj)
    except:
        pass

    try:
        parse_copyright_warning(email_obj)
    except:
        pass

    try:
        parse_complaint(email_obj)
    except:
        print traceback.format_exc()

    try:
        parse_a2z(email_obj)
    except:
        print traceback.format_exc()

    try:
        parse_chargeback(email_obj)
    except:
        print traceback.format_exc()
    if email_obj.status == "New" or email_obj.status is None:
        try:
            keywords = ["tracking", "expect to receive", "haven't received", "have not received", "Package didn't arrive",
                        "Order cancellation request", "havent received", "Never received my order", "Where is my stuff", "local post",
                        "I ordered", "refund", "USPS", "my order", "my package", "has not arrived", "delivered",
                        "never received", "not arrive", "Automatic reply", "it shipped", "when to expect", "to arrive",
                        "Where is this item", "Where is the item", "replacement", "recieved", "delivery", "out of stock",
                        "arrivato", "delivered", "eingetroffen", "ricevuto il pacco", "consegna", "noch nichts erhalten", "Bestellung",
                        "arriver", "reembolso", "Rechnungsanforderung", "geleverd", "Paket nicht angekommen", "Bestellstornierungs",
                        "Demande de facturation", "Solicitud de factura del cliente", "Invoice request", u"Article endommagé ou défectueux",
                        "Richiesta fattura daparte del"]

            for keyword in keywords:
                if keyword.lower() in email_obj.subject.lower() or keyword.lower() in email_obj.email.lower():
                    if email_obj.type not in ['restricted', 'pl_warning']:
                        email_obj.status = "Done"
                        email_obj.save()
                        break
        except:
            print traceback.format_exc()
    print email_obj.id, email_obj.created_at, email_obj.subject


def reparse_mails(email_id=0, email_type='all', days=100):
    if email_id != 0:
        email_obj = AccountEmail.get(AccountEmail.id == email_id)
        parse_mail(email_obj)
        return

    id = 0
    earliest_date = datetime.datetime.now() - datetime.timedelta(days=days)
    while True:
        if email_type.lower() == 'all':
            emails = AccountEmail.select().where(AccountEmail.id > id, AccountEmail.created_at > earliest_date,
                                                 AccountEmail.AmazonOrderId == "").order_by(
                AccountEmail.id.asc()).limit(100)
        elif email_type.lower() == 'chargeback':
            emails = AccountEmail.select().where(AccountEmail.id > id, AccountEmail.created_at > earliest_date,
                                                 AccountEmail.email.contains('credit card issuer')).order_by(
                AccountEmail.id.asc()).limit(100)
        else:
            # emails = AccountEmail.select().where(AccountEmail.id > id, AccountEmail.created_at > earliest_date,
            #                                      (AccountEmail.type == email_type) | (AccountEmail.type == '') | (
            #                                          AccountEmail.type == None)).order_by(
            #     AccountEmail.id.asc()).limit(100)
            emails = AccountEmail.select().where(AccountEmail.id > id, AccountEmail.created_at > earliest_date,
                                                 AccountEmail.type == email_type).order_by(
                AccountEmail.id.asc()).limit(100)

        if len(emails) == 0:
            break
        for email_obj in emails:
            parse_mail(email_obj)
            id = email_obj.id
            print id, email_obj.subject


def save_message(email_address, msg):
    message_id = msg["Message-ID"]
    try:
        email_obj = AccountEmail.get(AccountEmail.messageId == message_id)
        print 'existed', email_obj.created_at, email_obj.subject
        # print eemailObj,2
    except:
        # print traceback.format_exc()
        email_obj = AccountEmail()

        try:

            date_tuple = email.utils.parsedate_tz(msg['Date'])
            if date_tuple:
                email_obj.created_at = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
            earliest_date = datetime.datetime.now() - datetime.timedelta(days=365)
            if email_obj.created_at < earliest_date:
                return

            from_list = email.utils.parseaddr(msg['From'])
            to_list = email.utils.parseaddr(msg['To'])
            # emailObj.uid = messageId

            if "do-not-reply" in from_list[1] or "non-rispondere" in from_list[1]:
                return

            if "In-Reply-To" in msg:
                email_obj.in_reply_to = msg['In-Reply-To']

            if "References" in msg:
                email_obj.references = msg['References']
                email_obj.references = email_obj.references[0:250]

            bytes, encoding = decode_header(msg['Subject'])[0]

            try:
                email_obj.subject = bytes.decode(encoding)
            except:
                email_obj.subject = bytes

            email_obj.subject = email_obj.subject.replace('\n', '')
            email_obj.subject = email_obj.subject.replace('\t', '')
            email_obj.subject = email_obj.subject[0:500]

            if "pricing error" in email_obj.subject:
                return

            email_obj.email = get_email_body(msg)[0:100000]

            email_obj.AmazonOrderId = parse_order_id(email_address, email_obj, False)

            if email_obj.AmazonOrderId is None or email_obj.AmazonOrderId == "":
                if email_obj.in_reply_to is not None:
                    try:
                        last_email = AccountEmail.get(AccountEmail.messageId == email_obj.in_reply_to)
                        email_obj.AmazonOrderId = last_email.AmazonOrderId
                    except:
                        pass

            email_obj.fromName = from_list[0]
            email_obj.fromAddress = from_list[1]
            email_obj.toName = to_list[0]
            email_obj.toAddress = to_list[1]
            email_obj.messageId = msg["Message-ID"]
            email_obj.type = parse_email_type_by_subject(email_obj)

            # print emailObj.uid
            email_obj.save()

            parse_mail(email_obj)

            # print emailObj.email
        except:
            print traceback.format_exc()


def get_email_subject(msg):
    bytes, encoding = decode_header(msg['Subject'])[0]

    try:
        subject = bytes.decode(encoding)
    except:
        subject = bytes

    subject = subject.replace('\n', '')
    subject = subject.replace('\t', '')
    subject = subject[0:500]

    return subject


def get_email_date(mime_msg):
    date_tuple = email.utils.parsedate_tz(mime_msg['Date'])
    if date_tuple:
        return datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))

    return None


def get_email_body(msg):
    attachments = search_message_bodies(msg)

    if 'text/html' in attachments:
        part = attachments['text/html']
        if part.get_content_charset() is None:
            # We cannot know the character set, so return decoded "something"
            text = part.get_payload(decode=True)
            if text is not None:
                return text

        charset = part.get_content_charset()

        html = unicode(part.get_payload(decode=True), str(charset), "ignore")

        return html.strip()

    for type, attach in attachments.iteritems():

        try:
            if type == 'text/plain':
                # print first 3 lines
                part = attachments['text/plain']
                charset = part.get_content_charset()

                text = unicode(part.get_payload(decode=True), str(charset), "ignore")
                return text.strip()

        except:
            print traceback.format_exc()
            # return attach
            pass




def get_decoded_email_body(msg):
    """ Decode email body.
    Detect character set if the header is not set.
    We try to get text/plain, but if there is not one then fallback to text/html.
    :param message_body: Raw 7-bit message body input e.g. from imaplib. Double encoded in quoted-printable and latin-1
    :return: Message body as unicode string
    """
    text = ""
    if msg.is_multipart():
        html = None
        for part in msg.get_payload():

            print "%s, %s" % (part.get_content_type(), part.get_content_charset())

            if part.get_content_charset() is None:
                # We cannot know the character set, so return decoded "something"
                text = part.get_payload(decode=True)
                continue

            charset = part.get_content_charset()

            if part.get_content_type() == 'text/plain':
                text = unicode(part.get_payload(decode=True), str(charset), "ignore").encode('utf8', 'replace')

            if part.get_content_type() == 'text/html':
                html = unicode(part.get_payload(decode=True), str(charset), "ignore").encode('utf8', 'replace')

        if text is not None:
            return text.strip()
        else:
            return html
    else:
        text = unicode(msg.get_payload(decode=True), msg.get_content_charset(), 'ignore').encode('utf8', 'replace')
        return text.strip()


def _search_message_bodies(bodies, part):
    """recursive search of the multiple version of the 'message' inside 
    the the message structure of the email, used by search_message_bodies()"""

    type = part.get_content_type()
    if type.startswith('multipart/'):
        # explore only True 'multipart/*' 
        # because 'messages/rfc822' are also python 'multipart' 
        if type == 'multipart/related':
            # the first part or the one pointed by start 
            start = part.get_param('start', None)
            related_type = part.get_param('type', None)
            for i, subpart in enumerate(part.get_payload()):
                if (not start and i == 0) or (start and start == subpart.get('Content-Id')):
                    _search_message_bodies(bodies, subpart)
                    return
        elif type == 'multipart/alternative':
            # all parts are candidates and latest is best
            for subpart in part.get_payload():
                _search_message_bodies(bodies, subpart)
        elif type in ('multipart/report', 'multipart/signed'):
            # only the first part is candidate
            try:
                subpart = part.get_payload()[0]
            except IndexError:
                return
            else:
                _search_message_bodies(bodies, subpart)
                return

        elif type == 'multipart/signed':
            # cannot handle this
            return

        else:
            # unknown types must be handled as 'multipart/mixed'
            # This is the peace of code could probably be improved, I use a heuristic : 
            # - if not already found, use first valid non 'attachment' parts found
            for subpart in part.get_payload():
                tmp_bodies = dict()
                _search_message_bodies(tmp_bodies, subpart)
                for k, v in tmp_bodies.iteritems():
                    if not subpart.get_param('attachment', None, 'content-disposition') == '':
                        # if not an attachment, initiate value if not already found
                        bodies.setdefault(k, v)
            return
    else:
        bodies[part.get_content_type().lower()] = part
        return

    return


def search_message_bodies(mail):
    """search message content into a mail"""
    bodies = dict()
    _search_message_bodies(bodies, mail)
    return bodies


def decode_text(payload, charset, default_charset):
    if charset:
        try:
            return payload.decode(charset), charset
        except UnicodeError:
            pass

    if default_charset and default_charset != 'auto':
        try:
            return payload.decode(default_charset), default_charset
        except UnicodeError:
            pass

    for chset in ['ascii', 'utf-8', 'utf-16', 'windows-1252', 'cp850']:
        try:
            return payload.decode(chset), chset
        except UnicodeError:
            pass

    return payload, None


def get_last_date():
    try:
        created_after = []
        with open(dir + "/LastEmailRequest.flag") as f:
            created_after = f.readlines()
        created_after = created_after[0].replace("Z", "").strip()
        # print created_after
    except:
        dt = datetime.datetime.utcnow() - datetime.timedelta(minutes=1440)
        created_after = dt.strftime('%d-%b-%Y')

    return created_after


def set_last_date():
    try:
        file = open(dir + "/LastEmailRequest.flag", "w")
        file.write(datetime.date.today().strftime("%d-%b-%Y"))
    except:
        print traceback.format_exc()


def read_mails(email_address, email_password, recent_mode=False):
    while True:
        try:
            if recent_mode is True:
                email_address = 'recent:' + email_address

            m_server = poplib.POP3_SSL('pop.gmail.com')
            m_server.user(email_address)
            m_server.pass_(email_password)

            email_information = m_server.stat()
            print "Number of new emails: %s (%s bytes)" % email_information

            # Get the number of mail messages
            num_messages = len(m_server.list()[1])
            if num_messages == 0:
                print "no more new emails."
                break

            # List the subject line of each message
            for mList in range(num_messages):
                try:
                    if recent_mode is True:
                        id = num_messages - mList
                    else:
                        id = mList + 1
                    for msg in m_server.retr(id):
                        try:
                            if type(msg) == list:
                                num_elements = len(msg)
                                out_string = ""
                                for k in range(num_elements):
                                    out_string += msg[k]
                                    out_string += '\n'

                                message = email.message_from_string(out_string)

                                if recent_mode is True:
                                    date_tuple = email.utils.parsedate_tz(message['Date'])
                                    if date_tuple:
                                        created_at = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
                                        if datetime.datetime.utcnow() - created_at > datetime.timedelta(days=7):
                                            sys.exit(0)

                                save_message(email_address, message)
                                # return
                        except:
                            pass
                except:
                    pass
            try:
                m_server.quit()
            except:
                pass

        except SSLEOFError as e:
            print traceback.format_exc()
            break
        except:
            print email_address, traceback.format_exc()
            if "AUTH" in traceback.format_exc():
                break

    sys.exit(0)


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-t', '--type', type=str, default="new", help='')
    parser.add_argument('-id', '--id', type=int, default=0, help='')
    parser.add_argument('-d', '--days', type=int, default=100, help='')
    parser.add_argument('-et', '--email_type', type=str, default="all", help='')
    parser.add_argument('-r', '--recent_mode', type=int, default=0, help='recent mode?')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":

    args = parse_args()

    print args

    get_lock("account_email_%s_%s" % (args.account.lower(), args.type.lower()))

    database_name = args.account
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    if args.type != 'new':
        reparse_mails(args.id, args.email_type, days=args.days)
        sys.exit(0)

    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        print "No account info for " + args.account + " found"
        sys.exit()

    init_db()

    recent_mode = True if args.recent_mode > 0 else False

    region = accountInfo.country
    if args.type == 'new':
        read_mails(accountInfo.email_address, accountInfo.email_password, recent_mode=recent_mode)
    else:
        reparse_mails(args.id, args.email_type, days=args.days)
