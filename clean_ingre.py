import argparse
import glob
import unicodecsv as csv
import sys
import StringIO

import datetime

from clean_listing import amazon_generated_sku
from lib.listing import clear_db_by_brands, clear_file_by_ingredients
from lib.listing_report import get_all_listing_report
from lib.mws_feed import submit_flat_file_to_amazon
from local import *
from lib.models import *

csv.field_size_limit(sys.maxsize)


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-t', '--type', type=str, default="ingre", help='')
    parser.add_argument('-i', '--ingre', type=str, default=None, help='')
    parser.add_argument('-d', '--download_inventory', type=int, default=1, help='')
    command_args = parser.parse_args()
    return command_args


def clear_file_by_term(region, term, file_path):
    asins = set()
    term_words = set(term.split())
    with open(file_path) as f:
        reader = csv.reader(f, delimiter=",")
        line_no = 0
        asin_col = 16
        title_col = 0
        for line in reader:
            if line_no == 0:
                col_no = 0
                for t in line:
                    if t == 'product-id':
                        asin_col = col_no
                    if t == 'item-name':
                        title_col = col_no
                    col_no += 1
            try:
                line_no = line_no + 1
                title = line[title_col].lower().replace('(', ' ').replace(')', ' ')
                title_words = set(title.split())

                if term_words.issubset(title_words):
                    asins.add(line[asin_col])
                    print line[asin_col], title
            except:
                pass

            if line_no % 1000 == 0:
                print line_no

    print "found %s asins to be cleared" % len(asins)
    if len(asins) > 0:
        all_listing_table = "all_listings"
        pro_db_table = 'products'
        if region.upper() != "US":
            # pro_db_table = pro_db_table + "_" + country.lower()
            all_listing_table = all_listing_table + "_" + country.lower()

        with database.atomic() as transaction:
            for asin in asins:
                sql = "update " + all_listing_table + " set status = -5 where asin = %s"
                database.execute_sql(sql, [asin])
                # sql = "update " + pro_db_table + " set status = -5 where asin = %s"
                # database.execute_sql(sql, [asin])


def parse_and_clean_listing_report(data, region, account_id):
    file_name = APP_ROOT_DIR + "/listings/all-listings-%s-%s.csv" % (region, datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"))

    w = open(file_name, "wb")
    writer = csv.writer(w, delimiter=',', errors='ignore')

    f = StringIO.StringIO(data)
    reader = csv.reader(f, delimiter="\t", errors='ignore', quoting=csv.QUOTE_NONE)

    for row in reader:
        writer.writerow(row)

    w.close()

    blacklist_skus = clear_file_by_ingredients(country, file_name)

    clear_blacklist_skus(blacklist_skus, account_id, region)
    return


feedsFileDir = APP_ROOT_DIR + "/feeds"


def clear_blacklist_skus(blacklist_skus, account_id, country):
    if len(blacklist_skus) == 0:
        return

    feedType = "_POST_FLAT_FILE_INVLOADER_DATA_"
    accountInfo = Account.get(name=account_id)

    list_file = feedsFileDir + '/remove-listings-' + datetime.datetime.utcnow().strftime('%m%d-%H%M') + ".txt"
    with open(list_file, 'wb') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter="\t", quotechar='|', quoting=csv.QUOTE_MINIMAL)
        csvwriter.writerow(["sku", "add-delete"])

        for sku in blacklist_skus:
            if "fba" in sku or amazon_generated_sku(sku):
                continue
            csvwriter.writerow([sku, "x"])

    try:

        result = submit_flat_file_to_amazon(country, list_file, feedType, accountInfo.mws_access_key,
                                            accountInfo.mws_secret_key, accountInfo.seller_id,
                                            auth_token=accountInfo.mws_auth_token)

        print result

    except:
        print traceback.format_exc()


if __name__ == "__main__":
    args = parse_args()
    print args

    region = args.country.lower()

    accountId = args.account
    if accountId is None or len(accountId) == 0:
        accountId = sids[0]

    if accountId not in sids:
        print "account not supported"
        sys.exit()

    database_name = accountId
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    accountInfo = Account.get(name=accountId)

    if accountInfo is None:
        print "No account info for " + accountId + " found"
        sys.exit()

    countries = region.split(",")
    print countries
    for country in countries:
        if args.type == "brand":
            clear_db_by_brands(country, database)
        if args.ingre is not None:
            last_listing_file = max(glob.iglob(APP_ROOT_DIR + "/listings/all-listings-%s*" % country.upper()), key=os.path.getmtime)
            clear_file_by_term(country, args.ingre, last_listing_file)
        else:
            if args.download_inventory > 0:
                get_all_listing_report(country, accountInfo.name, accountInfo.mws_access_key, accountInfo.mws_secret_key,
                                       accountInfo.seller_id, auth_token=accountInfo.mws_auth_token,
                                       callback=parse_and_clean_listing_report)
            else:
                last_listing_file = max(glob.iglob(APP_ROOT_DIR + "/listings/all-listings-%s*" % country.upper()), key=os.path.getmtime)
                blacklist_skus = clear_file_by_ingredients(country, last_listing_file)
                clear_blacklist_skus(blacklist_skus, accountId, country)
