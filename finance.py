# encoding=utf8
# import sys  
# reload(sys)  
# sys.setdefaultencoding('utf8')
import sys
import argparse

from lib.general import get_lock
from lib.models import *
import time
import traceback
import os
from lib.mws import mws
from datetime import datetime
from datetime import timedelta
from dateutil import parser

from local import *

rootDir = os.path.dirname(os.path.realpath(__file__))
dir = os.path.dirname(os.path.realpath(__file__))

update_promotion = False
update_deals = False


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days) + 1):
        yield end_date - timedelta(n)


def getFinanceApi(seller_account):
    orderApi = mws.Finances(seller_account.mws_access_key, seller_account.mws_secret_key, seller_account.seller_id,
                            auth_token=seller_account.mws_auth_token,
                            region=seller_account.country.upper(), version="2015-05-01")
    return orderApi


def updateOrderItems(account_code, posted_after, posted_before):
    # update fba fee for order item
    sql = 'update orders o join finance_events e on o.amazon_order_id = e.amazon_order_id and o.sku = e.sku set '
    tax = "o.tax = (select COALESCE(sum(amount), 0) from finance_events  where (`type` = 'Tax' or `type` = 'GiftWrapTax' or `type` = 'ShippingTax' or `type` = 'SalesTaxCollectionFee') and `amazon_order_id` = e.amazon_order_id and o.`sku` = e.sku)"
    # fba_fee = ", o.fba_fee = (select COALESCE(sum(amount), 0) from finance_events  where `type` like 'FBA%%' and `group` = 'Charge' and `amazon_order_id` = e.amazon_order_id and `sku` = e.sku)"
    commission = ", o.commission = (select COALESCE(sum(amount), 0) from finance_events  where `type` = 'Commission' and `amazon_order_id` = e.amazon_order_id and o.`sku` = e.sku)"
    chargeback = ", o.chargeback = (select COALESCE(sum(amount), 0) from finance_events  where `type` like '%%chargeback' and `group` = 'Charge' and `amazon_order_id` = e.amazon_order_id and o.`sku` = e.sku)"
    total_refund = ", o.Refund = (select COALESCE(sum(amount), 0) from finance_events  where `group` = 'Refund' and `amazon_order_id` = e.amazon_order_id and o.`sku` = e.sku)"
    total_promo = ", o.total_promo = (select COALESCE(sum(amount), 0) from finance_events  where `type` = 'PromotionMetaDataDef' and `amazon_order_id` = e.amazon_order_id and o.`sku` = e.sku)"

    sql += tax + commission + chargeback + total_refund + total_promo + " where o.account_code = '" + account_code + "' and e.posted_date between '" + posted_after + "' and '" + posted_before + "'"
    omsDB.execute_sql(sql)

    sql = "UPDATE orders SET is_refunded = 1 WHERE refund < 0"
    omsDB.execute_sql(sql)


def list_finance_events_by_seller(seller_account, posted_after=None, posted_before=None):
    if isinstance(seller_account, str):
        seller_account = Account.get(Account.seller_id == seller_account)

    ignore_last_refund_updated = False
    if posted_after is not None and posted_before is not None:
        ignore_last_refund_updated = True

    if posted_after is None and seller_account.last_refund_updated is not None:
        posted_after = seller_account.last_refund_updated - timedelta(minutes=60 * 12)

    # if not date set, default to last 30 days
    if posted_after is None:
        dt = datetime.utcnow() - timedelta(minutes=60 * 24 * 30)
        posted_after = dt.strftime('%Y-%m-%dT%H:%M:%S')

    if isinstance(posted_after, str) is False:
        posted_after = posted_after.strftime('%Y-%m-%dT%H:%M:%S')

    if posted_before is None:
        posted_before = (datetime.utcnow() - timedelta(minutes=3)).strftime('%Y-%m-%dT%H:%M:%S')

    print seller_account.name, posted_after, posted_before

    try:
        financeApi = getFinanceApi(seller_account)

        start_date = datetime.strptime(posted_after, '%Y-%m-%dT%H:%M:%S')
        end_date = datetime.strptime(posted_before, '%Y-%m-%dT%H:%M:%S')

        if start_date.date() != end_date.date():
            for single_date in daterange(start_date.date(), end_date.date()):
                if single_date == start_date.date():
                    after = start_date.strftime("%Y-%m-%dT%H:%M:%SZ")
                else:
                    after = single_date.strftime("%Y-%m-%dT00:00:00Z")

                if single_date == end_date.date():
                    before = end_date.strftime("%Y-%m-%dT%H:%M:%SZ")
                else:
                    before = single_date.strftime("%Y-%m-%dT23:59:59Z")

                # print after, before, single_date
                while True:
                    try:
                        events = financeApi.list_financial_events(posted_after=after, posted_before=before)
                        saveEvents(events, seller_account, financeApi)
                        break
                    except:
                        print traceback.format_exc()
                        if "RequestThrottled" in traceback.format_exc():
                            print "wait for 60 seconds..."
                            time.sleep(60)
                        else:
                            break

        else:
            while True:
                try:
                    events = financeApi.list_financial_events(posted_after=posted_after, posted_before=posted_before)
                    saveEvents(events, seller_account, financeApi)
                    break
                except:
                    print traceback.format_exc()
                    if "throttled" in traceback.format_exc().lower():
                        time.sleep(60)
                        continue
                    break

        if ignore_last_refund_updated is False:
            seller_account.last_refund_updated = end_date.strftime("%Y-%m-%d %H:%M:%S")
            seller_account.save()

        updateOrderItems(seller_account.name, start_date.strftime("%Y-%m-%d %H:%M:%S"), end_date.strftime("%Y-%m-%d %H:%M:%S"))

    except:
        print traceback.format_exc()
        pass


def saveEventsByGroup(account_code, seller_id, event_group, events_by_group):
    print event_group

    if event_group == 'ShipmentEventList':
        if 'ShipmentEvent' in events_by_group:
            if "PostedDate" in events_by_group['ShipmentEvent']:
                events = [events_by_group['ShipmentEvent']]
            else:
                events = events_by_group['ShipmentEvent']

            saveShipmentEvents(account_code, events)

        return

    elif event_group == 'RefundEventList':
        if 'ShipmentEvent' in events_by_group:
            if "PostedDate" in events_by_group['ShipmentEvent']:
                events = [events_by_group['ShipmentEvent']]
            else:
                events = events_by_group['ShipmentEvent']

            saveRefundEvents(account_code, events)
        return


def saveShipmentEvents(account_code, events):
    for event in events:
        try:
            amazon_order_id = event['AmazonOrderId']['value']
            posted_date = parser.parse(event['PostedDate']['value']).strftime('%Y-%m-%d %H:%M:%S')

            if 'ShipmentItemList' in event:

                shipmentItemAdjustmentList = event['ShipmentItemList']['ShipmentItem']

                if 'SellerSKU' in shipmentItemAdjustmentList:
                    shipmentItemAdjustmentList = [shipmentItemAdjustmentList]

                for shipment_item in shipmentItemAdjustmentList:
                    SellerSKU = shipment_item['SellerSKU']['value']

                    if 'ItemChargeList' in shipment_item:

                        print amazon_order_id, posted_date, SellerSKU, 'ItemChargeList'

                        if 'ChargeType' in shipment_item['ItemChargeList']['ChargeComponent']:
                            items = [shipment_item['ItemChargeList']['ChargeComponent']]
                        else:
                            items = shipment_item['ItemChargeList']['ChargeComponent']

                        # print items
                        for item in items:

                            chargeType = item['ChargeType'].value
                            amount = item['ChargeAmount']['CurrencyAmount']['value']
                            currency = item['ChargeAmount']['CurrencyCode']['value']

                            print [amazon_order_id, SellerSKU, posted_date, chargeType, amount, currency, 'Charge']

                            if float(amount) != 0:
                                try:
                                    sql = 'REPLACE INTO finance_events (account_code,amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)'
                                    omsDB.execute_sql(sql,
                                                      [account_code, amazon_order_id, SellerSKU, posted_date, chargeType, amount, currency,
                                                       'Charge'])
                                except:
                                    print traceback.format_exc()
                                    pass

                    if 'ItemFeeList' in shipment_item:
                        print amazon_order_id, posted_date, SellerSKU, 'ItemFeeList'

                        if 'FeeType' in shipment_item['ItemFeeList']['FeeComponent']:
                            items = [shipment_item['ItemFeeList']['FeeComponent']]
                        else:
                            items = shipment_item['ItemFeeList']['FeeComponent']

                        # print items
                        for item in items:

                            chargeType = item['FeeType'].value
                            amount = item['FeeAmount']['CurrencyAmount']['value']
                            currency = item['FeeAmount']['CurrencyCode']['value']

                            print [amazon_order_id, SellerSKU, posted_date, chargeType, amount, currency, 'Charge']

                            if float(amount) != 0:
                                try:
                                    sql = 'REPLACE INTO finance_events (account_code,amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)'

                                    omsDB.execute_sql(sql,
                                                      [account_code, amazon_order_id, SellerSKU, posted_date, chargeType, amount, currency,
                                                       'Charge'])
                                except:
                                    print traceback.format_exc()
                                    pass

                    if 'PromotionList' in shipment_item:
                        print amazon_order_id, posted_date, SellerSKU, 'PromotionList'

                        if 'PromotionType' in shipment_item['PromotionList']['Promotion']:
                            items = [shipment_item['PromotionList']['Promotion']]
                        else:
                            items = shipment_item['PromotionList']['Promotion']

                        promotion_amount = 0
                        # print items
                        for item in items:
                            chargeType = item['PromotionType'].value
                            amount = float(item['PromotionAmount']['CurrencyAmount']['value'])
                            currency = item['PromotionAmount']['CurrencyCode']['value']

                            promotion_amount += amount

                        chargeType = 'PromotionMetaDataDef'

                        print [amazon_order_id, SellerSKU, posted_date, chargeType, promotion_amount, currency, 'Charge']

                        if promotion_amount != 0:
                            try:
                                sql = 'REPLACE INTO finance_events (account_code,amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)'
                                omsDB.execute_sql(sql, [account_code, amazon_order_id, SellerSKU, posted_date, chargeType, promotion_amount,
                                                        currency, 'Charge'])
                            except:
                                pass

            if 'OrderChargeList' in event:
                orderChargeAdjustmentList = event['OrderChargeList']

                print amazon_order_id, posted_date, 'OrderChargeList'

                if 'ChargeComponent' in orderChargeAdjustmentList:
                    if 'ChargeType' in orderChargeAdjustmentList['ChargeComponent']:
                        items = [orderChargeAdjustmentList['ChargeComponent']]
                    else:
                        items = orderChargeAdjustmentList['ChargeComponent']

                    # print items
                    for item in items:
                        chargeType = item['ChargeType'].value
                        amount = item['ChargeAmount']['CurrencyAmount']['value']
                        currency = item['ChargeAmount']['CurrencyCode']['value']

                        print [amazon_order_id, posted_date, chargeType, amount, currency, 'OrderCharge']

                        if float(amount) != 0:
                            try:
                                sql = 'REPLACE INTO finance_events (amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s)'
                                sql = 'REPLACE INTO finance_events (account_code,amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)'
                                omsDB.execute_sql(sql, [account_code, amazon_order_id, '', posted_date, chargeType, amount, currency,
                                                        'OrderCharge'])
                            except:
                                pass

            if 'ShipmentFeeList' in event:
                shipmentFeeAdjustmentList = event['ShipmentFeeList']

                print amazon_order_id, posted_date, 'ShipmentFeeList'

                if 'FeeComponent' in shipmentFeeAdjustmentList:
                    if 'FeeType' in shipmentFeeAdjustmentList['FeeComponent']:
                        items = [shipmentFeeAdjustmentList['FeeComponent']]
                    else:
                        items = shipmentFeeAdjustmentList['FeeComponent']

                    # print items
                    for item in items:
                        chargeType = item['FeeType'].value
                        amount = item['FeeAmount']['CurrencyAmount']['value']
                        currency = item['FeeAmount']['CurrencyCode']['value']

                        print [amazon_order_id, posted_date, chargeType, amount, currency, 'ShipmentFee']

                        if float(amount) != 0:
                            try:
                                sql = 'REPLACE INTO finance_events (amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s)'
                                sql = 'REPLACE INTO finance_events (account_code,amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)'
                                omsDB.execute_sql(sql, [account_code, amazon_order_id, '', posted_date, chargeType, amount, currency,
                                                        'ShipmentFee'])
                            except:
                                pass

            if 'OrderFeeList' in event:
                orderFeeAdjustmentList = event['OrderFeeList']

                print amazon_order_id, posted_date, 'OrderFeeList'

                if 'FeeComponent' in orderFeeAdjustmentList:
                    if 'FeeType' in orderFeeAdjustmentList['FeeComponent']:
                        items = [orderFeeAdjustmentList['FeeComponent']]
                    else:
                        items = orderFeeAdjustmentList['FeeComponent']

                    # print items
                    for item in items:
                        chargeType = item['FeeType'].value
                        amount = item['FeeAmount']['CurrencyAmount']['value']
                        currency = item['FeeAmount']['CurrencyCode']['value']

                        print [amazon_order_id, posted_date, chargeType, amount, currency, 'OrderFee']

                        if float(amount) != 0:
                            try:
                                sql = 'REPLACE INTO finance_events (amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s)'
                                sql = 'REPLACE INTO finance_events (account_code,amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)'
                                omsDB.execute_sql(sql, [account_code, amazon_order_id, '', posted_date, chargeType, amount, currency,
                                                        'OrderFee'])
                            except:
                                pass

        except:
            pass


def saveRefundEvents(account_code, events):
    for event in events:
        try:
            amazon_order_id = event['AmazonOrderId']['value']
            posted_date = parser.parse(event['PostedDate']['value']).strftime('%Y-%m-%d %H:%M:%S')

            if 'ShipmentItemAdjustmentList' in event:
                shipmentItemAdjustmentList = event['ShipmentItemAdjustmentList']['ShipmentItem']

                if 'SellerSKU' in shipmentItemAdjustmentList:
                    shipmentItemAdjustmentList = [shipmentItemAdjustmentList]

                for shipment_item in shipmentItemAdjustmentList:
                    SellerSKU = shipment_item['SellerSKU']['value']

                    print amazon_order_id, posted_date, SellerSKU, 'ShipmentItemAdjustmentList'

                    if 'ItemChargeAdjustmentList' in shipment_item:
                        if 'ChargeType' in shipment_item['ItemChargeAdjustmentList']['ChargeComponent']:
                            items = [shipment_item['ItemChargeAdjustmentList']['ChargeComponent']]
                        else:
                            items = shipment_item['ItemChargeAdjustmentList']['ChargeComponent']

                        # print items
                        for item in items:

                            chargeType = item['ChargeType'].value
                            amount = item['ChargeAmount']['CurrencyAmount']['value']
                            currency = item['ChargeAmount']['CurrencyCode']['value']

                            print [amazon_order_id, SellerSKU, posted_date, chargeType, amount, currency, 'Refund']

                            if float(amount) != 0:
                                try:
                                    sql = 'REPLACE INTO finance_events (amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s)'
                                    sql = 'REPLACE INTO finance_events (account_code, amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)'
                                    omsDB.execute_sql(sql,
                                                      [account_code, amazon_order_id, SellerSKU, posted_date, chargeType, amount, currency,
                                                       'Refund'])
                                except:
                                    pass

                    if 'ItemFeeAdjustmentList' in shipment_item:
                        if 'ChargeType' in shipment_item['ItemFeeAdjustmentList']['FeeComponent']:
                            items = [shipment_item['ItemFeeAdjustmentList']['FeeComponent']]
                        else:
                            items = shipment_item['ItemFeeAdjustmentList']['FeeComponent']

                        # print items
                        for item in items:

                            chargeType = item['FeeType'].value
                            amount = item['FeeAmount']['CurrencyAmount']['value']
                            currency = item['FeeAmount']['CurrencyCode']['value']

                            print [amazon_order_id, SellerSKU, posted_date, chargeType, amount, currency, 'Refund']

                            if float(amount) != 0:
                                try:
                                    sql = 'REPLACE INTO finance_events (amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s)'
                                    sql = 'REPLACE INTO finance_events (account_code, amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)'
                                    omsDB.execute_sql(sql,
                                                      [account_code, amazon_order_id, SellerSKU, posted_date, chargeType, amount, currency,
                                                       'Refund'])
                                except:
                                    pass

                    if 'PromotionAdjustmentList' in shipment_item:
                        if 'PromotionType' in shipment_item['PromotionAdjustmentList']['Promotion']:
                            items = [shipment_item['PromotionAdjustmentList']['Promotion']]
                        else:
                            items = shipment_item['PromotionAdjustmentList']['Promotion']

                        # print items
                        for item in items:

                            chargeType = item['PromotionType'].value
                            amount = item['PromotionAmount']['CurrencyAmount']['value']
                            currency = item['PromotionAmount']['CurrencyCode']['value']

                            print [amazon_order_id, SellerSKU, posted_date, chargeType, amount, currency, 'PromotionRefund']

                            chargeType = "PromotionRefund"
                            if float(amount) != 0:
                                try:
                                    sql = 'REPLACE INTO finance_events (amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s)'
                                    sql = 'REPLACE INTO finance_events (account_code, amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)'
                                    omsDB.execute_sql(sql,
                                                      [account_code, amazon_order_id, SellerSKU, posted_date, chargeType, amount, currency,
                                                       'Refund'])
                                except:
                                    pass

            if 'OrderChargeAdjustmentList' in event:
                orderChargeAdjustmentList = event['OrderChargeAdjustmentList']

                print amazon_order_id, posted_date, 'OrderChargeAdjustmentList'

                if 'ChargeComponent' in orderChargeAdjustmentList:
                    if 'ChargeType' in orderChargeAdjustmentList['ChargeComponent']:
                        items = [orderChargeAdjustmentList['ChargeComponent']]
                    else:
                        items = orderChargeAdjustmentList['ChargeComponent']

                    # print items
                    for item in items:
                        chargeType = item['ChargeType'].value
                        amount = item['ChargeAmount']['CurrencyAmount']['value']
                        currency = item['ChargeAmount']['CurrencyCode']['value']

                        print [amazon_order_id, posted_date, chargeType, amount, currency, 'OrderCharge']

                        if float(amount) != 0:
                            try:
                                sql = 'REPLACE INTO finance_events (amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s)'
                                sql = 'REPLACE INTO finance_events (account_code, amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)'
                                omsDB.execute_sql(sql, [account_code, amazon_order_id, '', posted_date, chargeType, amount, currency,
                                                        'OrderChargeRefund'])
                            except:
                                pass

                if 'OrderChargeAdjustment' in orderChargeAdjustmentList:
                    if 'ChargeType' in orderChargeAdjustmentList['OrderChargeAdjustment']:
                        items = [orderChargeAdjustmentList['OrderChargeAdjustment']]
                    else:
                        items = orderChargeAdjustmentList['OrderChargeAdjustment']

                    # print items
                    for item in items:
                        chargeType = item['ChargeType'].value
                        amount = item['ChargeAmount']['CurrencyAmount']['value']
                        currency = item['ChargeAmount']['CurrencyCode']['value']

                        print [amazon_order_id, posted_date, chargeType, amount, currency, 'OrderCharge']

                        if float(amount) != 0:
                            try:
                                sql = 'REPLACE INTO finance_events (amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s)'
                                sql = 'REPLACE INTO finance_events (account_code, amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)'
                                omsDB.execute_sql(sql, [account_code, amazon_order_id, '', posted_date, chargeType, amount, currency,
                                                        'OrderChargeRefund'])
                            except:
                                pass

            if 'ShipmentFeeAdjustmentList' in event:
                shipmentFeeAdjustmentList = event['ShipmentFeeAdjustmentList']

                print amazon_order_id, posted_date, ShipmentFeeAdjustmentList

                if 'FeeComponent' in shipmentFeeAdjustmentList:
                    if 'FeeType' in shipmentFeeAdjustmentList['FeeComponent']:
                        items = [shipmentFeeAdjustmentList['FeeComponent']]
                    else:
                        items = shipmentFeeAdjustmentList['FeeComponent']

                    # print items
                    for item in items:
                        chargeType = item['FeeType'].value
                        amount = item['FeeAmount']['CurrencyAmount']['value']
                        currency = item['FeeAmount']['CurrencyCode']['value']

                        print [amazon_order_id, posted_date, chargeType, amount, currency, 'ShipmentFee']

                        if float(amount) != 0:
                            try:
                                sql = 'REPLACE INTO finance_events (amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s)'
                                sql = 'REPLACE INTO finance_events (account_code, amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)'
                                omsDB.execute_sql(sql, [account_code, amazon_order_id, '', posted_date, chargeType, amount, currency,
                                                        'ShipmentFeeRefund'])
                            except:
                                pass

            if 'OrderFeeAdjustmentList' in event:
                orderFeeAdjustmentList = event['OrderFeeAdjustmentList']

                print amazon_order_id, posted_date, orderFeeAdjustmentList

                if 'FeeComponent' in orderFeeAdjustmentList:
                    if 'FeeType' in orderFeeAdjustmentList['FeeComponent']:
                        items = [orderFeeAdjustmentList['FeeComponent']]
                    else:
                        items = orderFeeAdjustmentList['FeeComponent']

                    # print items
                    for item in items:
                        chargeType = item['FeeType'].value
                        amount = item['FeeAmount']['CurrencyAmount']['value']
                        currency = item['FeeAmount']['CurrencyCode']['value']

                        print [amazon_order_id, posted_date, chargeType, amount, currency, 'OrderFee']

                        if float(amount) != 0:
                            try:
                                sql = 'REPLACE INTO finance_events (account_code, amazon_order_id,sku,posted_date,`type`,amount,currency,`group`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)'
                                omsDB.execute_sql(sql, [account_code, amazon_order_id, '', posted_date, chargeType, amount, currency,
                                                        'OrderFeeRefund'])
                            except:
                                pass

        except:
            print traceback.format_exc()
            pass


def saveEvents(events, seller_account, financeApi):
    eventsData = events.parsed

    for event_group, events_by_group in eventsData['FinancialEvents'].iteritems():
        saveEventsByGroup(seller_account.name, seller_account.seller_id, event_group, events_by_group)

    # load by nextToken
    while True:
        try:
            nextToken = eventsData['NextToken']['value']

            # print nextToken

            events = financeApi.list_financial_events_by_next_token(nextToken)

            eventsData = events.parsed

            saveEvents(events, seller_account, financeApi)

        except:
            break


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="US", help='Source country')
    parser.add_argument('-after', '--posted_after', type=str, default=None, help='posted_after')
    parser.add_argument('-before', '--posted_before', type=str, default=None, help='posted_after')
    command_args = parser.parse_args()
    return command_args


def update_db_records(account):
    sql = "UPDATE orders SET refund = -(SELECT sum(amount) FROM finance_events " \
          "WHERE account_code = '%s' AND `group` = 'Refund' AND (`type` = 'Principal'  OR `type` = 'shippingcharge')  " \
          "AND finance_events.amazon_order_id =  orders.amazon_order_id ) WHERE account_code = '%s' " % (account, account)

    print sql
    omsDB.execute_sql(sql)

    sql = "update orders set refund_usd = 0 where account_code = '%s' and refund_usd is null" % account
    print sql
    omsDB.execute_sql(sql)

    sql = "UPDATE orders SET refund = 0 WHERE account_code = '%s' AND   refund IS NULL" % account
    print sql
    omsDB.execute_sql(sql)

    sql = "UPDATE orders SET is_refunded = 0  WHERE account_code = '%s'" % account
    print sql
    omsDB.execute_sql(sql)

    sql = "UPDATE orders SET is_refunded = 1  WHERE account_code = '%s' AND  (refund_usd <> 0 OR refund <> 0)" % account
    print sql
    omsDB.execute_sql(sql)


def main(args):
    print args
    if args.account not in sids:
        print "account not supported"
        return
    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        print "No account info for " + args.account + " found"
        sys.exit()

    get_lock("finance_%s" % args.account)

    list_finance_events_by_seller(accountInfo, args.posted_after, args.posted_before)

    update_db_records(args.account)


if __name__ == "__main__":
    args = parse_args()
    main(args)
