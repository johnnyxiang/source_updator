import urllib
from lib.blacklist import *
from lib.mail import *
from lib.mws_feed import *
from lib.utils.dropbox_utils import upload_to_dropbox
from lib.utils.price import *
from lib.utils.simple_logger import SimpleLogger
from lib.utils.sku_parser import SkuParser
from local import *

logger = SimpleLogger.get_stream_logger('source_price_updator')

rootDir = os.path.dirname(os.path.realpath(__file__))
feedsFileDir = rootDir + "/feeds"
if not os.path.isdir(feedsFileDir):
    os.makedirs(feedsFileDir)


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', type=str, default="", help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='ASIN country')
    parser.add_argument('-d', '--condition', type=str, default="all", help='Source ASIN condition')
    parser.add_argument('-t', '--type', type=str, default="product", help='Product Type: product, book or cd')
    parser.add_argument('-l', '--limit', type=int, default=15000, help='limit')
    parser.add_argument('-f', '--feed_type', type=str, default="update", help='feed type')
    parser.add_argument('-sku', '--sku', type=str, default=None, help='sku pattern')
    parser.add_argument('-q', '--qty', type=int, default=3, help='upload qty')
    parser.add_argument('-m', '--mode', type=str, default="production", help='running mode')
    parser.add_argument('-min', '--min_price', type=float, default=2.9, help='min_price')
    parser.add_argument('-mc', '--min_change', type=float, default=4, help='min price to trigger auto repricing change')
    parser.add_argument('-sb', '--sync_blacklist', type=int, default=1, help='sync blacklist')
    parser.add_argument('-sp', '--sync_pro', type=int, default=0, help='sync pro table with listing table')

    command_args = parser.parse_args()
    return command_args


def sku_skipped(sku, to_skip):
    if to_skip is None or len(to_skip) == 0:
        return False

    for s in to_skip.split("\n"):
        if s.lower() in sku.lower():
            return True
    return False


def get_recent_gray_asins(account_code, days=14):
    url = "http://35.224.158.228/api/orders/gray/%s?days=%s" % (account_code, days)
    i = 0
    gray_asins = set()
    while True:
        print url
        response = urllib.urlopen(url)
        data = json.loads(response.read())
        if len(data['data']) == 0:
            break
        for row in data['data']:
            if row['asin'] not in gray_asins:
                i = i + 1
                print i, row['asin']
                gray_asins.add(row['asin'])

        links = data['meta']['pagination']['links']
        if 'next' in links and len(links['next']) > 0:
            url = links['next']
        else:
            break

    print "%s gray asins found" % len(gray_asins)
    return gray_asins


def get_recent_ordered_items(account_code, days=5, limit=1000):
    url = "http://35.224.158.228/api/orders/recent/%s?days=%d&limit=%d" % (account_code, days, limit)
    print url

    asins = set()
    i = 0
    while True:
        print url
        response = urllib.urlopen(url)
        data = json.loads(response.read())
        if len(data['data']) == 0:
            break
        for row in data['data']:
            asins.add(row['ASIN'])

            i = i + 1
            print i, row['ASIN']

        links = data['meta']['pagination']['links']
        if 'next' in links and len(links['next']) > 0:
            url = links['next']
        else:
            break

    return asins


def main(args):
    print args

    account_id = args.account
    if account_id is None or len(account_id) == 0:
        account_id = sids[0]

    if account_id not in sids:
        print "account not supported"
        return

    database_name = account_id
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    limit = args.limit
    pro_type = args.type
    region = args.country
    try:
        min_change = args.min_change
    except:
        min_change = 5

    if pro_type == "product":
        db_table = "products"
    elif pro_type == "cd":
        db_table = "cds"
    else:
        db_table = "books"

    if region.upper() != "US":
        db_table = db_table + "_" + args.country.lower()

    if args.mode == "d" or args.mode == "debug":
        debug = True
    else:
        debug = False

    account_info = Account.get(name=account_id)

    if account_info is None:
        logger.error("No account info for " + account_id + " found")
        sys.exit()

    # if region.upper() == "US":
    try:
        sku_pattern = args.sku
    except:
        sku_pattern = None

    # reset
    all_listings_db = 'all_listings'

    if region != 'us':
        all_listings_db = all_listings_db + '_' + region.lower()
        blacklist_table = 'blacklist_uk'
    else:
        blacklist_table = 'blacklist_us'

    # sql = 'UPDATE ' + db_table + ' p JOIN ' + all_listings_db + ' l ON l.sku=p.sku SET real_new_price = 0 WHERE real_new_price > 0 AND  l.status>-5 AND l.status <= 0 '
    # database.execute_sql(sql)


    try:
        sync_pro = True if args.sync_pro > 0 else False
    except:
        sync_pro = False

    if pro_type != 'book' and sync_pro is True:
        sql = "select count(*) from " + all_listings_db
        cursor = database.execute_sql(sql)
        row = cursor.fetchone()
        if row[0] > 10000:
            sql = "UPDATE " + db_table + " p " \
                                         "SET real_new_price = 0 " \
                                         "WHERE real_new_price >0 AND status >-5 AND " \
                                         "fake_asin NOT IN (SELECT asin FROM " + all_listings_db + " WHERE product_type ='" + pro_type + "' AND status >0)"
            database.execute_sql(sql)

    # sql = 'update ' + db_table + ' p join ' + all_listings_db + ' l on p.sku = l.sku set p.real_new_price = l.price'
    sql = 'update ' + db_table + ' p join ' + all_listings_db + ' l on p.sku = l.sku set p.real_new_price = 0 where l.status = -3'
    database.execute_sql(sql)

    recent_ordered_asins = get_recent_ordered_items(account_id)
    with database.atomic() as transaction:
        for asin in recent_ordered_asins:
            sql = "UPDATE " + db_table + " set real_new_price = 0 where fake_asin = %s"
            database.execute_sql(sql, [asin])

    repricing_settings = {}
    for setting in AutoRepricingSetting.select().where(AutoRepricingSetting.account_code == account_info.name):
        repricing_settings[setting.sku.lower()] = setting

    gray_asins = get_recent_gray_asins(account_code=account_id)

    asyncBlacklist(account_id, args.country.lower(), args.sync_blacklist)

    if args.feed_type != "pricing":

        if sku_pattern is not None:
            sku_filter = "and sku like %s"
        else:
            sku_filter = "AND sku <> ''"

        if args.feed_type == "update":
            sql = "SELECT sku,fake_asin,real_new_price_to_update,real_new_price,`condition`,id " \
                  "FROM " + db_table + \
                  " WHERE status >-5 " + sku_filter + \
                  "  AND ((real_new_price <>0 AND real_new_price_to_update = 0)  OR " \
                  " (real_new_price =0 AND real_new_price_to_update <> 0) " \
                  " OR abs(real_new_price_to_update-real_new_price) >= " + str(min_change) + \
                  ") ORDER BY id ASC"
            # " OR real_new_price - real_new_price_to_update  >=  " + str(min_change / 2) + \
        elif args.feed_type == "new":
            sql = "select sku,fake_asin,real_new_price_to_update,real_new_price,`condition`,id  from " + db_table + " where status >-5  " + sku_filter + " and real_new_price_to_update > 0"
        elif args.feed_type == "remove":
            sql = "select sku,fake_asin,real_new_price_to_update,real_new_price,`condition`,id  from " + db_table + " where status >-5 " + sku_filter + " and real_new_price_to_update = 0"
        else:
            inactive_listings_table = 'inactive_listings'
            if region.upper() != "US":
                inactive_listings_table = inactive_listings_table + "_" + args.country.lower()
            sql = "SELECT sku FROM " + inactive_listings_table + " WHERE  mapped >=0 and qty > 0"

        if limit != 0:
            sql = sql + " limit " + str(limit)

        print sql

        if sku_pattern is not None:
            cursor = database.execute_sql(sql, ['%' + args.sku + '%'])
        else:
            cursor = database.execute_sql(sql)
        rows = cursor.fetchall()
        print len(rows)
        #
        list_file = feedsFileDir + '/' + pro_type + '-listings-' + datetime.datetime.utcnow().strftime('%m%d-%H%M')

        if pro_type == "cd":
            shipping_group = account_info.cd_template_name
            tax_code = account_info.cd_tax_code
            shipping = account_info.shipping
            default_min_profit_rate = account_info.min_profit_rate
            default_min_profit = account_info.min_profit
        elif pro_type == "book":
            shipping_group = account_info.book_template_name
            tax_code = account_info.book_tax_code
            shipping = 3.99
            default_min_profit_rate = account_info.book_min_profit_rate
            default_min_profit = account_info.book_min_profit
        else:
            shipping_group = account_info.product_template_name
            tax_code = account_info.product_tax_code
            shipping = account_info.shipping
            default_min_profit_rate = account_info.min_profit_rate
            default_min_profit = account_info.min_profit

        default_profit_rate = account_info.default_profit_rate
        default_upload_qty = account_info.upload_qty

        if args.feed_type == "qty":
            rows = [row for row in rows if not sku_skipped(row[0], account_info.skip_skus)]
            max_feed_size = 1000000
        elif args.feed_type == "remove":
            max_feed_size = 1000000
        else:
            max_feed_size = 200000

        chunks = [rows[x:x + max_feed_size] for x in xrange(0, len(rows), max_feed_size)]
        list_file_num = 1
        for chunk in chunks:
            list_file_name = list_file + "-" + str(list_file_num) + ".txt"
            list_file_num = list_file_num + 1
            logger.info(list_file_name)
            with open(list_file_name, 'wb') as csv_file:
                csv_writer = csv.writer(csv_file, delimiter="\t", quotechar='|', quoting=csv.QUOTE_MINIMAL)
                if args.feed_type == "qty":
                    csv_writer.writerow(["sku", "minimum-seller-allowed-price", "maximum-seller-allowed-price", "quantity"])
                else:
                    csv_writer.writerow(
                        ["sku", "product-id", "product-id-type", "price", "minimum-seller-allowed-price",
                         "maximum-seller-allowed-price",
                         "item-condition", "quantity", "add-delete", "product_tax_code", "merchant_shipping_group_name",
                         "leadtime-to-ship"])

                for row in chunk:
                    sku = row[0]
                    if args.feed_type == "qty":
                        if region.upper() in ["DE", "IT", 'FR', 'ES']:
                            csv_writer.writerow([sku, '9,99', '1999,00', 0])
                        else:
                            csv_writer.writerow([sku, args.min_price, 1999.00, 0])
                    else:
                        asin = row[1]
                        sku_info = SkuParser.parse_sku(sku, region)
                        source_country = sku_info['source']
                        sku_pattern = SkuParser.parse_sku_pattern(sku).lower()
                        if sku_pattern in repricing_settings:
                            min_profit = repricing_settings[sku_pattern].min_profit
                            min_profit_rate = repricing_settings[sku_pattern].min_profit_rate
                            upload_qty = repricing_settings[sku_pattern].qty
                            leadtime_to_ship = repricing_settings[sku_pattern].leadtime_to_ship
                            profit_rate = repricing_settings[sku_pattern].default_profit_rate
                        else:
                            min_profit_rate = default_min_profit_rate
                            min_profit = default_min_profit
                            leadtime_to_ship = account_info.leadtime_to_ship
                            upload_qty = default_upload_qty
                            profit_rate = default_profit_rate

                        min_price = get_min_price(row[2], marketplace=region, min_profit_amount=min_profit,
                                                  min_profit_rate=min_profit_rate,
                                                  source=source_country, type=sku_info['product_code'])

                        # deduct shipping fee
                        min_price = min_price - float(shipping)

                        if sku_info['product_code'] == 'p':
                            price = get_min_price(row[2], marketplace=region, min_profit_amount=min_profit,
                                                  min_profit_rate=profit_rate,
                                                  source=source_country, type=sku_info['product_code'])
                        else:
                            price = get_price(min_price)

                        if row[4].lower() == "new":
                            condition_num = 11
                        else:
                            condition_num = 3

                        if row[2] == 0 or price > 2000 or asin in gray_asins:
                            qty = 0
                            action = 'a'
                        else:
                            qty = upload_qty
                            action = 'a'

                        max_price = get_max_price(min_price)
                        csv_writer.writerow(
                            [row[0], row[1], "ASIN", price, round(min_price, 2), max_price,
                             condition_num, qty, action, tax_code, shipping_group, leadtime_to_ship])

            if args.feed_type != "qty":
                upload_to_dropbox(list_file_name, database_name)

            # print debug,sids,account_id,account_info.mws_access_key
            if debug is False and account_id in sids \
                    and account_info.mws_access_key is not None and len(account_info.mws_access_key) > 0:
                try:
                    result = submit_flat_file_to_amazon(region, list_file_name, "_POST_FLAT_FILE_INVLOADER_DATA_",
                                                        account_info.mws_access_key,
                                                        account_info.mws_secret_key, account_info.seller_id,
                                                        auth_token=account_info.mws_auth_token)
                    sendMail(database_name + region + " uploaded listing file", result)
                except:
                    print traceback.format_exc()
                    sendMail(database_name + region + " fail to upload listing file", traceback.format_exc())
            else:
                print list_file_name

        if args.feed_type == "qty":
            return

        if args.feed_type == "remove":
            sql = "update " + db_table + " set real_new_price = 0 where sku <> '' and real_new_price_to_update = 0"
            database.execute_sql(sql)
            return

    reset_db = True if args.qty > 0 else False
    generate_pricing_file(account_info, pro_type=pro_type, region=region, database_name=database_name,
                          feed_type=args.feed_type, min_change=min_change, reset_db=reset_db, gray_asins=gray_asins)


def generate_pricing_file(account_info, pro_type, region, database_name, feed_type, min_change=5, limit=15000,
                          reset_db=True, gray_asins=set()):
    price_file = feedsFileDir + '/' + pro_type + '-pricing-' + datetime.datetime.utcnow().strftime('%m%d-%H%M')
    max_price_feed_size = 5000

    if pro_type == "product":
        db_table = "products"
        shipping = float(account_info.shipping)
        default_min_profit_rate = account_info.min_profit_rate
        default_min_profit = account_info.min_profit
        default_rule_name = account_info.rule_name
    elif pro_type == "cd":
        db_table = "cds"
        shipping = float(account_info.shipping)
        default_min_profit_rate = account_info.min_profit_rate
        default_min_profit = account_info.min_profit
        default_rule_name = account_info.rule_name
    else:
        db_table = "books"
        shipping = 3.99
        default_min_profit_rate = account_info.book_min_profit_rate
        default_min_profit = account_info.book_min_profit
        default_rule_name = account_info.book_rule_name

    if region.upper() != "US":
        db_table = db_table + "_" + region.lower()

    # repricing settings by sku pattern
    repricing_settings = {}
    for setting in AutoRepricingSetting.select().where(AutoRepricingSetting.account_code == account_info.name):
        repricing_settings[setting.sku.lower()] = setting

    # limit to 3 price files per day
    sql = "select sku,fake_asin,real_new_price_to_update,real_new_price,`condition`,id " \
          "from " + db_table + \
          " where sku <> '' and ((real_new_price <>0 and real_new_price_to_update = 0) " \
          " or (real_new_price =0 and real_new_price_to_update <> 0) " \
          " or abs(real_new_price_to_update-real_new_price) >= " + str(min_change) + \
          ") order by id asc limit " + str(limit)
    #          " OR real_new_price - real_new_price_to_update  >=  " + str(min_change / 2) + \
    print sql
    cursor = database.execute_sql(sql)
    rows = cursor.fetchall()

    price_file_num = 1

    action_name = "START"

    rows = [row for row in rows if row[2] > 0 and not row[1] in gray_asins]

    if len(rows) > max_price_feed_size:
        chunks = [rows[x:x + max_price_feed_size] for x in xrange(0, len(rows), max_price_feed_size)]
    else:
        chunks = [rows]

    currency_code = CurrencyMapping.get_currency(region)
    for chunk in chunks:
        price_file_name = price_file + "-" + str(price_file_num) + ".txt"
        price_file_num = price_file_num + 1
        logger.info(price_file_name)
        with open(price_file_name, 'wb') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter="\t", quotechar='|', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerow(["AutomatePricing-1.0",
                                 "The top 3 rows are for Amazon use only, Do not modify or delete the top 3 rows"])
            csv_writer.writerow(
                ["SKU", "Minimum Seller Allowed Price", "Maximum Seller Allowed Price", "Country Code", "Currency Code", "Rule Name",
                 "Rule Action"])
            csv_writer.writerow(
                ["sku", "minimum-seller-allowed-price", "maximum-seller-allowed-price", "country-code", "currency-code", "rule-name",
                 "rule-action"])

            for row in chunk:
                current_price = row[3]
                new_price = row[2]
                # if current_price > 0 or new_price > 1000:
                #     continue
                asin = row[1]
                # if asin in gray_asins:
                #     continue
                sku = row[0]
                sku_pattern = SkuParser.parse_sku_pattern(sku).lower()
                sku_info = SkuParser.parse_sku(sku, region)
                source_country = sku_info['source']
                if sku_pattern in repricing_settings:
                    min_profit = repricing_settings[sku_pattern].min_profit
                    min_profit_rate = repricing_settings[sku_pattern].min_profit_rate
                    rule_name = repricing_settings[sku_pattern].rule_name

                    if rule_name is None or len(rule_name) == 0:
                        rule_name = default_rule_name
                else:
                    min_profit_rate = default_min_profit_rate
                    min_profit = default_min_profit
                    rule_name = default_rule_name

                min_price = get_min_price(row[2], marketplace=region, min_profit_amount=min_profit,
                                          min_profit_rate=min_profit_rate,
                                          source=source_country, type=sku_info['product_code'])

                # deduct shipping fee
                min_price = min_price - shipping
                max_price = get_max_price(min_price)
                csv_writer.writerow([row[0], round(min_price, 2), max_price, region.upper(), currency_code, rule_name, action_name])

        upload_to_dropbox(price_file_name, database_name)

    if reset_db is True:
        if feed_type == "update":
            sql = "update " + db_table + \
                  " set real_new_price = real_new_price_to_update where sku <> '' and (real_new_price_to_update = 0 " \
                  " or real_new_price = 0" \
                  " or abs(real_new_price_to_update-real_new_price) >= " + str(min_change) + \
                  ")"
            # " OR real_new_price - real_new_price_to_update  >=  " + str(min_change / 2) + \
        if len(rows) > 0:
            last_id = rows[len(rows) - 1][5]
            sql = sql + " and id <= " + str(last_id)
        else:
            sql = "update " + db_table + " set real_new_price = real_new_price_to_update"

        logger.info(sql)
        database.execute_sql(sql)

        sql = "update " + db_table + " set real_new_price = real_new_price_to_update where real_new_price=0"
        print sql
        database.execute_sql(sql)
        # try:
        #     remove_out_stocks(account_info, region=region, database_name=database_name, database=database)
        # except:
        #     print traceback.format_exc()


if __name__ == "__main__":
    args = parse_args()
    main(args)
