import argparse
import csv
import re
import traceback

import requests

from lib.general import *
from lib.google.order_sheet_service import OrderSheetService
from lib.models import Account, omsDB
from lib.mws_feed import submit_refund_feed
from lib.utils.logger import get_logger
from local import *
import urllib
import json

feedsFileDir = APP_ROOT_DIR + "/feeds"
if not os.path.isdir(feedsFileDir):
    os.makedirs(feedsFileDir)

logger = get_logger(__name__)


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-c', '--country', type=str, default=None, help='country of the inventory')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-d', '--date_from', default='', help='Date from')
    parser.add_argument('-o', '--order_id', default='', help='Date from')
    command_args = parser.parse_args()
    return command_args


def is_amazon_order(text):
    regex = re.compile('[0-9A-Z]{3}-[0-9]{7}-[0-9]{7}')
    return False if regex.match(text) is None else True


def get_data_from_api(account):
    url = 'https://oms.tmwarriors.com/api/refunds/requests/%s' % account

    response = urllib.urlopen(url)
    return json.loads(response.read())


def update_status(order_id, status, note=None):
    tried = 0
    while tried < 3:
        try:
            endpoint = "https://oms.tmwarriors.com/api/refunds/request/update/%s" % order_id
            data = {'status': status, 'note': note}
            r = requests.post(endpoint, json=data)
            logger.debug("%s" % r.text)
            return
        except Exception as e:
            tried += 1
            logger.exception(e)
            time.sleep(10)


def filter_data(data):
    rows = []
    if 'data' in data and len(data['data']) > 0:
        for row in data['data']:
            if 'status' in row and row['status'] == 'Canceled':
                print 'order %s has been canceled' % row['amazonOrderId']

                update_status(row['amazonOrderId'], 'Order Canceled')
                try:
                    order_sheet_service.fill_cancellation_success_info(order_id=row['amazonOrderId'])
                except:
                    pass

                continue

            rows.append(row)

    return rows


def create_feed_file(rows):
    feed_file_path = feedsFileDir + '/adjustment-' + str(int(round(time.time() * 1000))) + '.txt'

    logger.info("%s refund request(s) found" % len(rows))
    # print(data['data'])
    if len(rows) > 0:
        with open(feed_file_path, 'wb') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter="\t", quotechar='|', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerow(
                ["order-id", "order-item-id", "adjustment-reason-code", "currency", "item-price-adj", "shipping-price-adj"])

            i = 0
            for row in rows:
                i += 1
                line = [row['amazonOrderId'], row['orderItemId'], row['refundReason'], row['currencyCode'],
                        row['productAmount'],
                        row['shippingAmount']]
                logger.info(i, line)
                csv_writer.writerow(line)
    return feed_file_path


def handle_result(response, account_code, feed_file_path, data):
    failed = []
    for line in response.split('\n'):
        line_data = line.split('\t')
        if len(line_data) < 2:
            continue
        amazon_order_id = line_data[1]
        if not is_amazon_order(amazon_order_id):
            continue
        # print line_data

        if 'refund amount for the item price is too high' not in line:
            failed.append(amazon_order_id)
            update_status(amazon_order_id, 'Failed', line_data[5])

    # sendMail("%s refund report, %s failed" % (account_code, len(failed)), response)
    logger.info("%s refund report, %s failed" % (account_code, len(failed)), response)
    records = dict()
    for row in rows:
        records[row['amazonOrderId']] = row['type']

    with open(feed_file_path, 'r') as f:
        reader = csv.reader(f, delimiter="\t")
        for row in reader:
            if row[0] in failed:
                continue

            update_status(row[0], 'Refunded')

            try:
                if row[0] in records and records[row[0]] == 'FullRefund':
                    order_sheet_service.fill_refund_success_info(order_id=row[0])
            except:
                print traceback.format_exc()
                pass

            try:
                sql = "UPDATE orders SET is_refunfed = 1 WHERE  amazonOrderId = %s"
                omsDB.execute_sql(sql, [row[0]])
            except:
                pass


if __name__ == "__main__":
    args = parse_args()

    if args.account not in sids:
        logger.error("account not supported")
        sys.exit(0)

    account_info = Account.get(name=args.account)
    if account_info is None:
        logger.error("No account info for " + args.account + " found")
        sys.exit()

    region = args.country
    if region is None:
        region = account_info.country

    region = region.upper()

    data = get_data_from_api(args.account)
    order_sheet_service = OrderSheetService()
    rows = filter_data(data)
    if len(rows) > 0:
        file_path = create_feed_file(rows)

        result = submit_refund_feed(region, file_path, account_info.mws_access_key, account_info.mws_secret_key,
                                    account_info.seller_id, auth_token=account_info.mws_auth_token)

        handle_result(result, account_code=account_info.code, feed_file_path=file_path, data=rows)
