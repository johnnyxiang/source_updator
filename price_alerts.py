from lib.general import get_lock
from lib.listing_report import get_inactive_inventory_report
from lib.models import *
import file_generator
import argparse

from lib.utils.utils import Struct
from listing import parse_inactive_inventory_report


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', type=str, default="all", help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='Country')
    parser.add_argument('-d', '--download', type=int, default=1, help='download inventory')
    parser.add_argument('-m', '--mode', type=str, default="production", help='running mode')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-min', '--min_price', type=float, default=2.9, help='min_price')
    parser.add_argument('-sb', '--sync_blacklist', type=int, default=1, help='sync blacklist')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()
    print args

    get_lock("price_alert_%s" % (args.account.lower()))

    download_inventory = True if args.download > 0 else False
    account = Account.get(name=args.account)

    region = args.country if len(args.country) > 0 else account.country

    countries = region.split(",")
    print countries

    database.init(account.name, host=databaseHost, user=databaseUser, password=databasePW)
    mode = args.mode
    limit = args.limit
    min_price = args.min_price
    for country in countries:
        print 'process %s' % country
        if download_inventory is True:
            print 'downloading inactive inventory report for %s' % country
            get_inactive_inventory_report(country, account.name, account.mws_access_key, account.mws_secret_key, account.seller_id,
                                          callback=parse_inactive_inventory_report)

        args = {"account": account.name, "country": country, "feed_type": "qty", 'limit': limit, 'type': 'product', "mode": mode,
                'min_price': min_price, 'sync_blacklist': args.sync_blacklist}
        file_generator.main(Struct(**args))
