import argparse
from lib.utils.simple_logger import SimpleLogger
from lib.models import *
from lib.mws_feed import *
from local import *
from lib.mail import *

logger = SimpleLogger.get_stream_logger('source_price_updator')

rootDir = os.path.dirname(os.path.realpath(__file__))
feedsFileDir = rootDir + "/feeds"
if os.path.isdir(feedsFileDir) is False:
    os.makedirs(feedsFileDir)


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-t', '--type', type=str, default="inventory", help='Feed Type: inventory or autpricing')
    parser.add_argument('-f', '--file', type=str, help='file path')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()
    logger.info(args)
    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    feed_type = args.type
    region = args.country

    if feed_type == "pricing":
        feedType = "MARS_AUTOMATE_PRICING_FEED"
    else:
        feedType = "_POST_FLAT_FILE_INVLOADER_DATA_"

    list_file_name = args.file
    if os.path.exists(list_file_name) is False:
        list_file_name = feedsFileDir + "/" + list_file_name
        if os.path.exists(list_file_name) is False:
            logger.error("file not exists")
            sys.exit()

    database_name = args.account
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    accountInfo = Account.get(name=args.account)
    if accountInfo is not None:
        logger.error("No account info for " + args.account + " found")
        sys.exit()

    if args.account in sids and accountInfo.mws_access_key is not None and len(accountInfo.mws_access_key) > 0:
        try:
            submit_flat_file_to_amazon(region, list_file_name, feedType, accountInfo.mws_access_key, accountInfo.mws_secret_key,
                                       accountInfo.seller_id, auth_token=accountInfo.mws_auth_token)
            sendMail(database_name + " uploaded " + feed_type + " file", list_file_name)
            logger.info(database_name + " uploaded " + feed_type + " file")
        except:
            print traceback.format_exc()
            sendMail(database_name + " fail to upload " + feed_type + " file", traceback.format_exc())
