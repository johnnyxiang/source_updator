import argparse
import csv
import traceback
import requests

from lib.general import *
from lib.google.order_sheet_service import OrderSheetService
from lib.models import Account, omsDB
from lib.mws_feed import submit_cancellation_feed
from lib.utils.logger import get_logger
from lib.utils.utils import is_amazon_order
from local import *
import urllib
import json

feedsFileDir = APP_ROOT_DIR + "/feeds"
if not os.path.isdir(feedsFileDir):
    os.makedirs(feedsFileDir)

logger = get_logger(__name__)


def update_status(order_id, status, note=None):
    endpoint = "https://oms.tmwarriors.com/api/cancellation/request/update/%s" % order_id
    data = {'status': status, 'note': note}
    r = requests.post(endpoint, data=data)
    logger.debug("%s" % r.text)


def fetch_data_for_order(order_id):
    sql = 'SELECT OrderItemId FROM amazon_order_items WHERE AmazonOrderId=%s'
    cursor = omsDB.execute_sql(sql, [order_id])
    data = {'data': []}
    for row in cursor.fetchall():
        data['data'].append({
            'status': 'New',
            'amazonOrderId': args.order_id,
            'orderItemId': row[0],
            'reason': args.reason
        })
    return data


def fetch_data_from_api(account):
    url = 'https://oms.tmwarriors.com/api/cancellation/requests/%s' % account
    response = urllib.urlopen(url)
    return json.loads(response.read())


def fetch_data(account=None, order_id=None):
    if order_id is not None and len(order_id) > 0:
        return fetch_data_for_order(order_id)
    else:
        return fetch_data_from_api(account)


def filter_data(data):
    rows = []
    if 'data' in data and len(data['data']) > 0:

        for row in data['data']:
            if 'cancellationStatus' in row and row['cancellationStatus'] == 'Canceled':
                print 'order %s has been canceled' % row['amazonOrderId']
                try:
                    update_status(row['amazonOrderId'], 'Canceled')
                except:
                    print traceback.format_exc()
                continue

            rows.append(row)

    return rows


def create_feed_file(data_rows):
    file_path = feedsFileDir + '/cancellation-' + str(int(round(time.time() * 1000))) + '.txt'
    print "%s cancellation request(s) found" % len(data_rows)

    with open(file_path, 'wb') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter="\t", quotechar='|', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(
            ["TemplateType=OrderCancellation", "Version=1.0/1.0.3", "This row for Amazon.com use only.  Do not modify or delete."])
        csv_writer.writerow(
            ["order-id", "cancellation-reason-code", "amazon-order-item-code"])

        i = 0
        for row in data_rows:
            i += 1
            line = [row['amazonOrderId'], row['reason'], row['orderItemId']]
            print i, line
            csv_writer.writerow(line)

    return file_path


def handle_result(response, account_code, feed_file_path):
    failed = []
    for line in response.split('\n'):
        line_data = line.split('\t')
        if len(line_data) < 2:
            continue

        amazon_order_id = line_data[1]
        if not is_amazon_order(amazon_order_id):
            continue

        failed.append(amazon_order_id)
        try:
            update_status(amazon_order_id, 'Failed', line_data[5])
        except:
            print traceback.format_exc()
            pass

    logger.info("%s cancellation report, %s failed" % (account_code, len(failed)), response)

    with open(feed_file_path, 'r') as f:
        reader = csv.reader(f, delimiter="\t")
        for row in reader:
            if row[0] in failed:
                continue
            try:
                update_status(row[0], 'Canceled')
                order_sheet_service.fill_cancellation_success_info(order_id=row[0])
            except:
                pass


def parse_args():
    parser = argparse.ArgumentParser(description='Cancel order feed.')
    parser.add_argument('-c', '--country', type=str, default=None, help='country of the inventory')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-d', '--date_from', default='', help='Date from')
    parser.add_argument('-o', '--order_id', default='', help='Date from')
    parser.add_argument('-r', '--reason', default='ShippingAddressUndeliverable', help='cancellation reason')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()

    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    account_info = Account.get(name=args.account)
    #
    if account_info is None:
        print "No account info for " + args.account + " found"
        sys.exit()

    get_lock("cancellation_%s" % args.account)

    region = args.country
    if region is None:
        region = account_info.country

    region = region.upper()

    data = fetch_data(args.account, args.order_id)
    rows = filter_data(data)

    if len(rows) > 0:
        order_sheet_service = OrderSheetService()
        file_path = create_feed_file(rows)
        result = submit_cancellation_feed(region, file_path, account_info.mws_access_key, account_info.mws_secret_key,
                                          account_info.seller_id, auth_token=account_info.mws_auth_token)

        handle_result(result, account_code=account_info.code, feed_file_path=file_path)
