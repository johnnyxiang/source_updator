import sys

if sys.version_info < (2, 7):
    print("ERROR: Utils requires Python Version 2.7 or above...exiting.")
    sys.exit(1)

if sys.version_info.major == 3:
    PY2 = False
else:
    PY2 = True

__version__ = '0.1.0'
