import argparse
import datetime
import random
import traceback
import time

import sys

from lib.feedback import fetch_feedback_stats
from lib.general import regions, get_base_url, get_sales_channel
from lib.models import *
from config import *
from lib.utils.helper import get_browser, get_page_content_from_url, html_parser
from local import sids


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon feedback report')

    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default=None, help='marketplace')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    # print datetime.datetime.strptime('07/01/19', date_formats['it'])
    args = parse_args()

    rand_time = random.randint(0, 10)
    print 'sleep %s seconds' % rand_time
    time.sleep(rand_time)
    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    accountInfo = Account.get(name=args.account)
    databaseName = args.account
    database_name = args.account
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    country = args.country if args.country is not None and len(args.country) > 0 else accountInfo.country
    countries = country.split(",")
    if country.lower() == 'uk' and args.country is None:
        countries = regions['EU']
    print countries

    browser = get_browser(profile=None, headless=True)

    try:
        helper = Helper()
        account_id = accountInfo.name
        for country in countries:
            fetch_feedback_stats(country, account_id, accountInfo.seller_id, browser=browser)
    except:
        print traceback.format_exc()
    finally:
        browser.close()
