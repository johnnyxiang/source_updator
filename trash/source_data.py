import sys
import argparse
from lib.utils.simple_logger import SimpleLogger
from lib.utils.asin_matcher import AsinMatcher
from lib.models import *
import time

# Python version must be greater than or equal 2.7
from local import sids

if sys.version_info < (2, 7):
    print("ERROR: Repricing requires Python Version 2.7 or above...exiting.")
    sys.exit(1)
else:
    PY2 = bool(sys.version_info.major == 2)

logger = SimpleLogger.get_stream_logger('source_price_updator')

asin_matcher = AsinMatcher()


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-t', '--type', type=str, default="product", help='Product Type: product, book or cd')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()

    logger.info(args)

    limit = args.limit
    ptype = args.type
    p_index = 0

    if ptype == "product":
        db_table = "products"
    elif ptype == "cd":
        db_table = "cds"
    else:
        db_table = "books"

    if args.country.upper() != "US":
        db_table = db_table + "_" + args.country.lower()



    p_index = 0
    while True:
        current_time = time.time()
        sql = "select id,fake_asin from " + db_table + " where real_asin is null order by id asc limit 100"
        cursor = dataDB.execute_sql(sql)
        rows = cursor.fetchall()

        if len(rows) == 0:
            break

        ids = {}
        for row in rows:
            ids[row[1]] = row[0]

        asins = [row[1] for row in rows]

        source_asins = asin_matcher.match_by_api(asins)

        sql = "INSERT INTO " + db_table + " (id,real_asin) VALUES "

        values = []
        for asin in asins:
            if asin not in source_asins:
                source_asin = "None"
            else:
                source_asin = source_asins[asin]
            if source_asin is None:
                source_asin = "None"
            id = ids[asin]
            values.append(" (" + str(id) + ",'" + source_asin + "')")

        sql = sql + ",".join(values)
        sql = sql + " ON DUPLICATE KEY UPDATE real_asin=VALUES(real_asin);"
        # print sql
        dataDB.execute_sql(sql)

        p_index = p_index + len(rows)

        print rows[0][0], " took", int(time.time() - current_time), " seconds"
        if limit != 0 and p_index >= limit:
            break
