import argparse
import traceback

import sys

from lib.elastic_product import ElasticProduct
from lib.models import *
from local import sids


def init_db():
    try:
        sql = "SELECT 1 FROM product_data LIMIT 1"
        database.execute_sql(sql)
    except:
        sql = "CREATE TABLE `product_data` (" \
              "`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT," \
              "`asin` VARCHAR(10) DEFAULT NULL," \
              "`title` VARCHAR(525) DEFAULT NULL," \
              "`brand` VARCHAR(100) DEFAULT NULL," \
              "`ranking` INT(11) DEFAULT '0'," \
              "PRIMARY KEY (`id`)," \
              "UNIQUE KEY `asin` (`asin`)" \
              ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"

        try:
            database.execute_sql(sql)
        except:
            if "exists" not in traceback.format_exc():
                print traceback.format_exc()


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-ta', '--target_account', default="", help='Target Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-tc', '--target_country', default="us", help='Target country')
    parser.add_argument('-t', '--type', type=str, default="product", help='Product Type: product, book or cd')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-o', '--offset', type=int, default=0, help='offset')
    parser.add_argument('-s', '--size', type=int, default=50, help='page size')
    parser.add_argument('-m', '--mode', type=str, default="production", help='running mode')
    parser.add_argument('-f', '--min_id', type=int, default=0, help='min id')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()

    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    elastic = ElasticProduct(host="35.199.3.83", port=80,http_auth=('elasticuser', 'KbersRiseUp153'))

    if len(args.target_account) > 0:
        database_name = args.target_account

    else:
        database_name = args.account

    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    init_db()

    country = args.country.upper()
    target_country = args.target_country.upper()
    offset = args.offset
    limit = args.limit
    ptype = args.type
    pindex = 0

    if args.mode == "d" or args.mode == "debug":
        debug = True
    else:
        debug = False

    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        print "No account info for %s found" % args.account
        sys.exit()

    if ptype == "product":
        db_table = "products"
    elif ptype == "cd":
        db_table = "cds"
    else:
        db_table = "books"

    if target_country.upper() != "US":
        db_table = db_table + "_" + target_country.lower()

    minId = args.min_id

    while True:
        sql = "select real_asin,fake_asin,id from " + db_table + " where status > -5 and id > " + str(
            minId) + " and real_asin <> 'None' and real_asin is not null "
        sql = sql + " order by id asc limit " + str(args.size)

        # print  sql
        cursor = database.execute_sql(sql)
        rows = cursor.fetchall()

        if len(rows) == 0:
            break

        ItemIds = []

        for row in rows:
            asin = str(row[0]).zfill(10)
            fake_asin = str(row[1]).zfill(10)
            minId = row[2]
            ItemIds.append({'id': minId, 'asin': asin, 'fake_asin': fake_asin})
            pindex = pindex + 1

        print "loading ", minId

        if len(ItemIds) > 0:
            try:
                asins = [item["asin"] for item in ItemIds]
                asins = asins + [item["fake_asin"] for item in ItemIds]
                asins = list(set(asins))

                data = elastic.get_data(asins)

                update_sql = " INSERT INTO product_data  (asin,title,brand,ranking) VALUES "
                values = []

                for row in data['hits']['hits']:
                    asin = row['_source']['asin']
                    if 'title' not in row['_source'] or row['_source']['title'] is None:
                        continue

                    title = row['_source']['title']

                    brand = row['_source']['brand'] if 'brand' in row['_source'] and row['_source']['brand'] is not None else ""
                    ranking = row['_source']['ranking'] if 'ranking' in row['_source'] else "0"
                    title = title.replace('%', '').replace('"', "")[0:500]
                    brand = brand.replace('%', '').replace('"', "")[0:100]
                    values.append("(\"%s\",\"%s\",\"%s\",\"%s\")" % (asin, title, brand, ranking))

                if len(values) > 0:
                    update_sql = update_sql + ",".join(values)
                    update_sql = update_sql + " ON DUPLICATE KEY UPDATE title=VALUES(title),brand=VALUES(brand),ranking=VALUEs(ranking);"
                    # print update_sql
                    database.execute_sql(update_sql)

            except:
                print traceback.format_exc()
                break
