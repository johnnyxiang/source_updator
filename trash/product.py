import sys
import argparse
import traceback

from lib.elastic_product import ElasticProduct
from lib.utils.simple_logger import SimpleLogger
from lib import mws_product
from lib.models import *
from local import sids

logger = SimpleLogger.get_stream_logger('source_price_updator')


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-ta', '--target_account', default="", help='Target Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-tc', '--target_country', default="us", help='Target country')
    parser.add_argument('-t', '--type', type=str, default="product", help='Product Type: product, book or cd')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-o', '--offset', type=int, default=0, help='offset')
    parser.add_argument('-s', '--size', type=int, default=100, help='page size')
    parser.add_argument('-m', '--mode', type=str, default="asin", help='running mode')
    parser.add_argument('-f', '--min_id', type=int, default=0, help='min id')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":

    args = parse_args()
    if args.account not in sids:
        print "account not supported"
        sys.exit(0)
    print args

    elastic = ElasticProduct(host="35.199.3.83", port=80,http_auth=('elasticuser', 'KbersRiseUp153'))

    if len(args.target_account) > 0:
        database_name = args.target_account
    else:
        database_name = args.account

    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    country = args.country.upper()
    target_country = args.target_country.upper()
    offset = args.offset
    limit = args.limit
    ptype = args.type
    pindex = 0

    if args.mode == "d" or args.mode == "debug":
        debug = True
    else:
        debug = False

    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        logger.error("No account info for " + args.account + " found")
        sys.exit()

    if ptype == "product":
        db_table = "products"
    elif ptype == "cd":
        db_table = "cds"
    else:
        db_table = "books"

    if target_country.upper() != "US":
        db_table = db_table + "_" + target_country.lower()

    minId = args.min_id

    productApi = mws_product.getProductApi(accountInfo.seller_id, accountInfo.mws_access_key,
                                           accountInfo.mws_secret_key, country)

    found = 0
    while True:
        sql = "select real_asin,fake_asin,id from " + db_table + " where status > -2 and id > " + str(
            minId) + " and real_asin <> 'None' and real_asin is not null "

        # sql = sql + " and real_new_price_to_update = 0 "
        sql = sql + " order by id asc limit " + str(args.size)

        print  sql
        cursor = database.execute_sql(sql)
        rows = cursor.fetchall()

        if len(rows) == 0:
            break

        ItemIds = []

        for row in rows:
            asin = str(row[0]).zfill(10)
            fake_asin = str(row[1]).zfill(10)
            minId = row[2]
            ItemIds.append(
                {'id': minId, 'asin': asin, 'fake_asin': fake_asin})
            pindex = pindex + 1

        print "loading ", minId, '; total found ', found

        if len(ItemIds) > 0:
            try:

                if args.mode == 'both':
                    asins = [item["asin"] for item in ItemIds]
                    asins = asins + [item["fake_asin"] for item in ItemIds]
                elif args.mode == 'fake':
                    asins = [item["fake_asin"] for item in ItemIds]
                else:
                    asins = [item["asin"] for item in ItemIds]
                asins = list(set(asins))
                asins = elastic.remove_existed(asins)
                if (len(asins)) == 0:
                    continue

                chunks = [asins[x:x + 10] for x in xrange(0, len(asins), 10)]
                # print chunks
                for chunk in chunks:
                    response = mws_product.get_matching_product(productApi, chunk)
                    parsed_resp = response._response_dict
                    returned_asins = []
                    # print parsed_resp
                    # parsed_resp['GetMatchingProductResult']
                    if "ASIN" in parsed_resp['GetMatchingProductResult']:
                        results = [parsed_resp['GetMatchingProductResult']]
                    else:
                        results = parsed_resp['GetMatchingProductResult']

                    for product_dict in results:
                        if "Error" in product_dict:
                            print product_dict['Error']['Message']['value']
                            continue

                        asin = product_dict['ASIN']['value']
                        try:
                            product = product_dict['Product']
                            try:
                                title = product['AttributeSets']['ItemAttributes']['Title']['value']
                            except:
                                title = ""
                            try:
                                brand = product['AttributeSets']['ItemAttributes']['Brand']['value']
                            except:
                                brand = ""

                            try:
                                sales_ranking = product['SalesRankings']['SalesRank'][0]['Rank']['value']
                            except:
                                sales_ranking = 0
                            # print asin, title, brand, sales_ranking

                            payload = {
                                'asin': asin,
                                'brand': brand,
                                'title': title,
                                'ranking': sales_ranking
                            }
                            returned_asins.append(asin)
                            try:
                                elastic.bulk_add_data(asin, payload=payload)
                                # ElasticProduct.add_product_index(asin, payload=payload)
                            except:
                                print traceback.format_exc()
                        except:
                            print traceback.format_exc()
                            # print parsed_resp

                    not_returned = [asin for asin in chunk if asin not in returned_asins]
                    if len(not_returned) > 0:
                        param = (', '.join('"' + item + '"' for item in not_returned))
                        sql = "update " + db_table + " set status = -2 where real_asin in (" + param + ") or  fake_asin in (" + param + ")"

                        print sql
                        database.execute_sql(sql)
                    time.sleep(1)

            except Exception as e:
                print traceback.format_exc()

        offset = offset + len(ItemIds)

        if limit != 0 and pindex >= limit:
            break
