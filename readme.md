READ ME
=======

### Purpose
+ All sorts of small tools for Amazon data mining and analysis 

### How to install 
**_only tested and recommanded on linux environment_** 

clone git repo
cd source_updator && sudo pip install -r requirements.txt
sudo apt-get update
sudo apt-get upgrade

####add swap
sudo fallocate -l 4G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab

#### install requirement
sudo pip install -r requirements.txt  && sudo sh install.sh

####config
cp config.ini.default  config.ini
nano config.ini

nano local.py
enter sids =  ['xxx'], sids should contain all account codes supported

#### init run
python cronjob.py -a xxx // xxx is the account code configed in config.ini file

### Fetch account performance data 
feedback.py 
it downloads seller performance data ([_GET_V1_SELLER_PERFORMANCE_REPORT_](https://docs.developer.amazonservices.com/en_US/reports/Reports_ReportType.html#ReportTypeCategories__PerformanceReports)) and/or negative feedback reports([_GET_SELLER_FEEDBACK_DATA_](https://docs.developer.amazonservices.com/en_US/reports/Reports_ReportType.html#ReportTypeCategories__PerformanceReports))

### Fetch account feedbacks stats
feedbacks.py 
read feedback stats from seller storefront profile page

### Fetch orders
order.py

### Fetch account email
account_email.py

