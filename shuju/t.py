#!/usr/local/bin/python
# coding: utf-8
import os
import urllib2
import json

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
if __name__ == "__main__":
    file_path = lib_dir + "data/check-orders.txt"
    errors = 0
    with open(file_path) as f:
        for line in f:
            order_id = line.strip()
            try:
                url = "http://35.224.158.228/api/orders/search?q=%s" % order_id
                # print url
                response = urllib2.urlopen(url)
                data = json.load(response)
                if 'data' not in data or len(data['data']) == 0:
                    errors = errors + 1
                    print errors, order_id

            except:
                errors = errors + 1
                print errors, order_id
