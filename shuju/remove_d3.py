#!/usr/local/bin/python
# coding: utf-8
import traceback
import os
import sys
import csv
import glob

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

if __name__ == "__main__":
    uk_files = [f for f in glob.glob(lib_dir + "data/office/uk/*.csv")]
    us_files = [f for f in glob.glob(lib_dir + "data/office/us/*.csv")]

    uk_asins = set()
    us_asins = set()
    for csv_file in uk_files:
        f = open(csv_file)
        reader = csv.reader(f, delimiter=',')
        asins = [l[0] for l in reader if len(l) > 0]
        uk_asins.update(set(list(asins)))

    for csv_file in us_files:
        f = open(csv_file)
        reader = csv.reader(f, delimiter=',')
        asins = [l[0] for l in reader if len(l) > 0]
        us_asins.update(set(list(asins)))


    asins = us_asins - uk_asins
    d_file = lib_dir + "data/office/d.csv"
    if len(asins) > 0:
        with open(d_file, 'ab') as d_file_writer:
            for asin in asins:
                d_file_writer.write(asin + "\n")

