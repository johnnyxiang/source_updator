import os
import sys

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

if __name__ == "__main__":
    total = 0
    isbns = []
    for isbn in range(8173660000, 8173669996):
        total += 1
        print total, isbn
        isbns.append(isbn)
    for isbn in range(8184040008, 8184049994):
        total += 1
        print total, isbn
        isbns.append(isbn)
    for isbn in range(9350230003, 9350230003):
        total += 1
        print total, isbn
        isbns.append(isbn)

    for isbn in range(9351100006, 9351109992):
        total += 1
        print total, isbn
        isbns.append(isbn)

    for isbn in range(9352130006, 9352139992):
        total += 1
        print total, isbn
        isbns.append(isbn)

    d_file = lib_dir + "data/isbns.txt"
    with open(d_file, 'w') as d_file_writer:
        for asin in isbns:
            d_file_writer.write(str(asin) + "\n")
