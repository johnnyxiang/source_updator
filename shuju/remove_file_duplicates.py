#!/usr/local/bin/python
# coding: utf-8
import traceback
import os
import sys
import csv
import glob

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

if __name__ == "__main__":

    c_file_path = lib_dir + "data/caprods/702ca-prods.csv"
    f = open(c_file_path)
    reader = csv.reader(f, delimiter=',')
    asins = set([l[1] for l in reader])
    print "%d asins found" % len(asins)

    all_asins = set()


    all_files = [lib_dir + "data/caprods/718ca-prods.csv"]
    for file_path in all_files:
        if os.path.basename(file_path) == os.path.basename(c_file_path):
            continue

        f = open(file_path)
        reader = csv.reader(f, delimiter=',')
        all_asins.update(set([l[1] for l in reader]))
        print "%d all asins found " % len(all_asins)

    duplicated = asins.intersection(all_asins)
    print "%d duplicates found" % len(duplicated)
    #
    file_path = lib_dir + "data/caprods/d.csv"
    if len(duplicated) > 0:
        with open(file_path, 'w') as d_file_writer:
            for asin in duplicated:
                d_file_writer.write(asin + "\n")
