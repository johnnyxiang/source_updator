import argparse
import traceback
import datetime

import sys
import os

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
from lib.utils.simple_logger import SimpleLogger
from lib.models import *

logger = SimpleLogger.get_stream_logger('source_price_updator')
offer_service_price_finder = OfferServicePriceFinder()


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-o', '--offset', type=int, default=0, help='offset')
    parser.add_argument('-s', '--size', type=int, default=100, help='page size')
    parser.add_argument('-m', '--mode', type=str, default="asin", help='running mode')
    parser.add_argument('-f', '--min_id', type=int, default=0, help='min id')

    command_args = parser.parse_args()
    return command_args


def get_prices(isbns, country='us', condition='new'):
    # check es server
    earliest_date = (datetime.datetime.utcnow() - datetime.timedelta(hours=24))
    try:
        values = []
        rows = []
        found = []
        offers_info = offer_service_price_finder.find_offer_for_asins(isbns, country, condition)

        if offers_info is not None:

            for asin in isbns:
                try:

                    if asin not in offers_info or offers_info[asin] is None or 'product_price' not in offers_info[asin]:
                        continue

                    offer = offers_info[asin]
                    # sometimes, event query with new condition, used condition offers returned
                    if condition.lower() == "new" and offer['item_condition'] != 'new':
                        continue

                    if 'product_price' in offer:
                        offer_time_str = offer['time'][:19]
                        offer_time = datetime.datetime.strptime(offer_time_str, '%Y-%m-%dT%H:%M:%S')
                        if offer_time > earliest_date:
                            found.append(asin)
                            rows.append(
                                [asin, round(offer['product_price'] + offer['shipping_price'], 2), offer['offers']])
                            print asin, round(offer['product_price'] + offer['shipping_price'], 2)
                    else:
                        pass
                except Exception as e:
                    print traceback.format_exc()

            if len(rows) > 0:
                with dataDB.atomic() as transaction:
                    for row in rows:
                        if country.lower() == "us":
                            update_sql = " UPDATE fba_candidates SET us_lowest_price = %s, us_sellers=%s WHERE asin = %s"
                        else:
                            update_sql = " UPDATE fba_candidates SET uk_lowest_price = %s, uk_sellers=%s WHERE asin = %s"
                        dataDB.execute_sql(update_sql, [row[1], row[2], row[0]])
                        # update_sql = update_sql + ",".join(values)
                        # update_sql = update_sql + " ON DUPLICATE KEY UPDATE price=VALUES(price),price_last_checked=VALUES(price_last_checked);"
                        # print sql
                        # accountDB.execute_sql(update_sql)

            isbns = [item for item in isbns if item not in found]

    except Exception as e:
        print traceback.format_exc()

    return isbns


if __name__ == "__main__":

    args = parse_args()
    print args
    offset = args.offset
    limit = args.limit
    pindex = 0
    country = args.country.upper()

    minId = args.min_id

    found = 0
    while True:
        sql = "SELECT asin,id FROM fba_candidates WHERE id > " + str(minId)

        if country.lower() == "uk":
            sql = sql + " and uk_lowest_price = 0"
        else:
            sql = sql + " and us_lowest_price = 0"

        sql = sql + " order by id asc limit " + str(
            args.size if args.limit == 0 or args.size < args.limit else args.limit)

        print sql

        cursor = dataDB.execute_sql(sql)
        rows = cursor.fetchall()

        if len(rows) == 0:
            break
        minId = rows[len(rows) - 1][1]
        asins = [str(row[0]).zfill(10) for row in rows]
        print "loading ", minId, '; total found ', found

        try:
            asins = list(set(asins))
            get_prices(asins, country)
        except Exception as e:
            print traceback.format_exc()

        offset = offset + len(asins)

        if limit != 0 and pindex >= limit:
            break
