import argparse
import traceback
import time
import sys
import os

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
from lib.utils.simple_logger import SimpleLogger
from lib import mws_product
from lib.models import *

logger = SimpleLogger.get_stream_logger('source_price_updator')
offer_service_price_finder = OfferServicePriceFinder()


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-ta', '--target_account', default="", help='Target Account SID')
    parser.add_argument('-c', '--country', type=str, default=None, help='Source ASIN country')
    parser.add_argument('-tc', '--target_country', default="us", help='Target country')
    parser.add_argument('-t', '--type', type=str, default="product", help='product,price')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-o', '--offset', type=int, default=0, help='offset')
    parser.add_argument('-s', '--size', type=int, default=100, help='page size')
    parser.add_argument('-m', '--mode', type=str, default="asin", help='running mode')
    parser.add_argument('-f', '--min_id', type=int, default=0, help='min id')

    command_args = parser.parse_args()
    return command_args


def get_price_info(asin, mws_product, region):
    response = mws_product.get_lowest_priced_offers_for_asin(productApi, asin, region=region)
    parsed_resp = response._response_dict
    result = parsed_resp['GetLowestPricedOffersForASINResult']
    summary = result['Summary']
    # print summary

    data = {
        'total_offers': int(summary['TotalOfferCount']['value']),
        'fba_offers': 0,
        'buybox_price': 0,
        'lowest_price': 0
    }
    if 'NumberOfOffers' in summary:
        offer_numbers = summary['NumberOfOffers']['OfferCount']
        total_offers = 0
        if 'condition' in offer_numbers:
            offer_numbers = [offer_numbers]
        for offer in offer_numbers:
            if 'condition' in offer:
                print offer
                condition = offer['condition']['value']
                fulfillment_channel = offer['fulfillmentChannel']['value']
                total = int(offer['value'])
                if condition == "new":
                    total_offers = total_offers + total
                    if fulfillment_channel == "Amazon":
                        data['fba_offers'] = total

        data['total_offers'] = total_offers
        try:
            data['buybox_price'] = summary['BuyBoxPrices']['BuyBoxPrice']['LandedPrice']['Amount']['value']
        except:
            pass

        try:
            lowest_prices = summary['LowestPrices']['LowestPrice']
            if 'LandedPrice' in lowest_prices:
                lowest_price = lowest_prices['LandedPrice']['Amount']['value']
            else:
                lowest_price = 0
                for p in lowest_prices:
                    if p['condition']['value'] == 'new':
                        price = p['LandedPrice']['Amount']['value']
                        if lowest_price == 0 or lowest_price > price:
                            lowest_price = price

            data['lowest_price'] = lowest_price
        except:
            pass

    return data


def get_prices(isbns, country, mws_product, condition="new"):
    try:
        rows = []
        chunks = [isbns[x:x + 20] for x in xrange(0, len(isbns), 20)]
        offers_info = {}
        for chunk in chunks:
            if country.lower() == 'us':
                chunk_offer_info = mws_product.getPriceForAsinsFromMws(productApi, chunk, condition, country, avg=False)
            else:
                chunk_offer_info = mws_product.getPriceForAsinsFromMws(productApi, chunk, condition, country, ships_domestically=None,
                                                                       max_shipping_days=100, avg=False)
            offers_info.update(chunk_offer_info)
        # print offers_info

        for asin in isbns:
            try:
                if asin not in offers_info or 'product_price' not in offers_info[asin]:
                    print asin, 'no offer info'
                    rows.append([asin, 0, 0])
                    continue

                offer = offers_info[asin]

                # sometimes, event query with new condition, used condition offers returned
                if condition.lower == "new" and offer['item_condition'] != 'new':
                    print asin, 'no offer info'
                    rows.append([asin, 0, 0])
                    continue

                if 'product_price' in offer:
                    # print pid, asin,offer['product_price'],offer['shipping_price'],offer['time']
                    print asin, round(offer['product_price'] + offer['shipping_price'], 2)
                    rows.append([asin, round(offer['product_price'] + offer['shipping_price'], 2), offer['offers']])
                else:
                    print asin, 'no offer info'
                    rows.append([asin, 0, 0])
            except Exception as e:
                print asin, 'no offer info'
                rows.append([asin, 0, 0])

        with dataDB.atomic():
            for row in rows:
                if country.lower() == "us":
                    update_sql = " UPDATE fba_candidates SET us_lowest_price =%s, us_sellers=%s WHERE asin = %s"
                else:
                    update_sql = " UPDATE fba_candidates SET uk_lowest_price =%s, uk_sellers=%s WHERE asin = %s"

                dataDB.execute_sql(update_sql, [row[1], row[2], row[0]])
    except:
        print traceback.format_exc()


def get_product_info(asins, mws_product, region):
    chunks = [asins[x:x + 10] for x in xrange(0, len(asins), 10)]
    # print chunks
    for chunk in chunks:
        response = mws_product.get_matching_product(productApi, chunk, region=region)
        parsed_resp = response._response_dict
        returned_asins = []
        # print parsed_resp
        # parsed_resp['GetMatchingProductResult']
        if "ASIN" in parsed_resp['GetMatchingProductResult']:
            results = [parsed_resp['GetMatchingProductResult']]
        else:
            results = parsed_resp['GetMatchingProductResult']

        for product_dict in results:
            if "Error" in product_dict:
                print product_dict['Error']['Message']['value']
                continue

            asin = product_dict['ASIN']['value']
            try:
                product = product_dict['Product']
                try:
                    title = product['AttributeSets']['ItemAttributes']['Title']['value']
                except:
                    title = ""
                try:
                    brand = product['AttributeSets']['ItemAttributes']['Brand']['value']
                except:
                    brand = ""

                try:
                    sales_ranking = product['SalesRankings']['SalesRank'][0]['Rank']['value']
                except:
                    sales_ranking = 0

                try:
                    weight = product['AttributeSets']['ItemAttributes']['PackageDimensions']['Weight'][
                        'value']
                except:
                    weight = 0

                try:
                    height = product['AttributeSets']['ItemAttributes']['PackageDimensions']['Height'][
                        'value']
                except:
                    height = 0

                try:
                    length = product['AttributeSets']['ItemAttributes']['PackageDimensions']['Length'][
                        'value']
                except:
                    length = 0

                try:
                    width = product['AttributeSets']['ItemAttributes']['PackageDimensions']['Width'][
                        'value']
                except:
                    width = 0

                dimensions = "%s x %s x %s" % (round(float(height), 1), round(float(length), 1), round(float(width), 1))
                # <ns2:PackageDimensions>
                #     <ns2:Height Units="inches">2.80</ns2:Height>
                #     <ns2:Length Units="inches">9.75</ns2:Length>
                #     <ns2:Width Units="inches">8.40</ns2:Width>
                #     <ns2:Weight Units="pounds">0.40</ns2:Weight>
                # </ns2:PackageDimensions>

                print asin, title, brand, sales_ranking

                if accountInfo.country == "us":
                    sql = "UPDATE fba_candidates SET brand = %s,title=%s,us_rank=%s, weight = %s, dimensions = %s WHERE asin = %s"
                    dataDB.execute_sql(sql, [brand, title, sales_ranking, weight, dimensions, asin])
                else:
                    sql = "UPDATE fba_candidates SET uk_rank =%s WHERE asin = %s"
                    dataDB.execute_sql(sql, [sales_ranking, asin])

            except:
                print traceback.format_exc()
                # print parsed_resp

        time.sleep(1)


if __name__ == "__main__":

    args = parse_args()

    print args

    if len(args.target_account) > 0:
        database_name = args.target_account
    else:
        database_name = args.account

    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    target_country = args.target_country.upper()
    offset = args.offset
    limit = args.limit
    pindex = 0

    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        logger.error("No account info for " + args.account + " found")
        sys.exit()

    if args.country is None or len(args.country) == 0:
        country = accountInfo.country.upper()
    else:
        country = args.country.upper()

    minId = args.min_id

    productApi = mws_product.getProductApi(accountInfo.seller_id, accountInfo.mws_access_key,
                                           accountInfo.mws_secret_key, country)

    found = 0
    while True:
        sql = "SELECT asin,id FROM fba_candidates WHERE id > " + str(minId)

        if args.type == 'price':
            if country.lower() == "uk":
                sql = sql + " and uk_lowest_price = 0"
            else:
                sql = sql + " and uk_lowest_price = 0"
        else:
            if country.lower() == "uk":
                sql = sql + " and uk_rank = 0"
            else:
                sql = sql + " and title is null"

        sql = sql + " order by id asc limit " + str(
            args.size if args.limit == 0 or args.size < args.limit else args.limit)

        print sql

        cursor = dataDB.execute_sql(sql)
        rows = cursor.fetchall()

        if len(rows) == 0:
            break

        ItemIds = []
        for row in rows:
            asin = str(row[0]).zfill(10)
            minId = row[1]
            ItemIds.append({'id': minId, 'asin': asin})
            pindex = pindex + 1

        print "loading ", minId, '; total found ', found

        if len(ItemIds) > 0:
            try:
                asins = [item["asin"] for item in ItemIds]
                asins = list(set(asins))

                if args.type == 'product':
                    get_product_info(asins, mws_product, country)
                else:
                    get_prices(asins, country, mws_product)
                    # for asin in asins:
                    #     try:
                    #         data = get_price_info(asin, mws_product, country)
                    #         print data
                    #         if country.lower() == "uk":
                    #             sql = "UPDATE fba_candidates SET uk_lowest_price=%s,uk_buybox_price=%s,uk_sellers=%s,uk_fba_sellers=%s WHERE asin = %s"
                    #             dataDB.execute_sql(sql,
                    #                                [data['lowest_price'], data['buybox_price'], data['total_offers'],
                    #                                 data['fba_offers'], asin])
                    #         else:
                    #             # sql = "UPDATE fba_candidates SET uk_lowest_price=%s,uk_buybox_price=%s,uk_sellers=%s,uk_fba_sellers=%s WHERE asin = %s"
                    #             sql = "UPDATE fba_candidates SET us_lowest_price=%s,us_buybox_price=%s,us_sellers=%s,us_fba_sellers=%s WHERE asin = %s"
                    #             dataDB.execute_sql(sql,
                    #                                [data['lowest_price'], data['buybox_price'], data['total_offers'],
                    #                                 data['fba_offers'],
                    #                                 asin])
                    #     except:
                    #         print traceback.format_exc()
                    #
                    #     time.sleep(1)

            except Exception as e:
                print traceback.format_exc()

        offset = offset + len(ItemIds)

        if limit != 0 and pindex >= limit:
            break
