#!/usr/local/bin/python
# coding: utf-8
import traceback
import os
import sys
import csv
import glob

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

if __name__ == "__main__":
    folder = '704us'
    used_files = [f for f in glob.glob(lib_dir + "data/%s/*used.csv" % folder)]
    new_files = [f for f in glob.glob(lib_dir + "data/%s/*new.csv" % folder)]
    used_asins = set()
    new_asins = set()
    index = 0
    for csv_file in used_files:
        index = index + 1
        f = open(csv_file)
        reader = csv.reader(f, delimiter=',')
        asins = [l[0] for l in reader if len(l) > 0]
        used_asins.update(set(list(asins)))
        print "found %s used asins in %s used files" % (len(used_asins), index)

    index = 0
    for csv_file in new_files:
        index = index + 1
        f = open(csv_file)
        reader = csv.reader(f, delimiter=',')
        asins = [l[0] for l in reader if len(l) > 0]
        new_asins.update(set(list(asins)))
        print "found %s new asins in %s new files" % (len(new_asins), index)

    new_asins.update(used_asins)
    existed_asins = new_asins
    print "%s unique existed asins " % len(existed_asins)

    new_files = [f for f in glob.glob(lib_dir + "data/%s/new/*.csv" % folder)]
    index = 0
    c_asins = set()
    for csv_file in new_files:
        index = index + 1
        f = open(csv_file)
        reader = csv.reader(f, delimiter=',')
        asins = [l[0] for l in reader if len(l) > 0]
        c_asins.update(set(list(asins)))
        print "found %s new asins in %s new files" % (len(c_asins), index)

    asins = c_asins - existed_asins

    print "%s unique new asins " % len(asins)

    if len(asins) > 0:
        asins = list(asins)
        chunk_size = 400000
        chunks = [asins[x:x + chunk_size] for x in xrange(0, len(asins), chunk_size)]
        index = 0
        for chunk in chunks:
            index = index + 1
            d_file = lib_dir + "data/%s/new_%d.csv" % (folder, index)
            with open(d_file, 'ab') as d_file_writer:
                for asin in chunk:
                    d_file_writer.write(asin + "\n")
