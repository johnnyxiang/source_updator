import csv
import glob
import sys
import os
import time
import traceback

from lib.utils.asin_matcher import AsinMatcher

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib.elastic_product import ElasticProduct

elastic = ElasticProduct(host="35.199.3.83", port=80,http_auth=('elasticuser', 'KbersRiseUp153'))

asin_matcher = AsinMatcher()


def write_to_file(file_name, data):
    with open(file_name, 'ab') as csvfile:
        for row in data:
            try:
                csvfile.write(row + "\n")
            except:
                print traceback.format_exc()
                pass


if __name__ == "__main__":
    cat_name = 'book0301'

    file_isbn = lib_dir + "/data/data/existed/%s.txt" % cat_name
    file_asin = lib_dir + "/data/data/asin/%s.csv" % cat_name
    # mapping_file = lib_dir + "/data/mapping.txt"
    # mapping = {}
    # with open(mapping_file) as f:
    #     reader = csv.reader(f, delimiter=',')
    #     for row in reader:
    #         try:
    #             mapping[row[1]].append(row[0])
    #         except:
    #             mapping[row[1]] = [row[0]]
    #
    #         if row[1] not in mapping:
    #             mapping[row[1]] = []
    #
    # print "%d mapping" % len(mapping)

    asins = set()
    isbns = []
    with open(file_isbn) as f:
        reader = csv.reader(f, delimiter=',')
        index = 0
        for row in reader:
            index = index + 1
            isbns.append(row[0])
            if len(isbns) > 20:
                try:
                    mappings = asin_matcher.match_asins_by_api(isbns)
                    for mapping in mappings:
                        try:
                            asins.add(mapping['asin'])
                        except:
                            pass
                    print 'found %d' % len(asins)
                except:
                    pass

                isbns = []

        if len(isbns) > 20:
            try:
                mappings = asin_matcher.match_asins_by_api(isbns)
                for mapping in mappings:
                    try:
                        asins.add(mapping['asin'])
                    except:
                        pass
            except:
                pass
    write_to_file(file_asin, asins)
    print "Done, total %s asins found" % len(asins)
