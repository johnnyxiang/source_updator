#!/usr/local/bin/python
# coding: utf-8
import traceback
import os
import sys
import csv
import glob

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


if __name__ == "__main__":
    asin_files = [f for f in glob.glob(lib_dir + "data/orders/asins/*.txt")]
    asin_set = set()
    index = 0
    for csv_file in asin_files:
        index = index + 1
        f = open(csv_file)
        reader = csv.reader(f, delimiter=',')
        asins = [l[0] for l in reader if len(l) > 0]
        asin_set.update(set(list(asins)))
        print "found %s asins in %s files" % (len(asin_set), index)

    for region in ['all', 'dvds', 'sf', 'na', 'rs']:
        order_file = lib_dir + "data/orders/%s-book-orders.csv" % region
        with open(order_file) as f:
            reader = csv.reader(f, delimiter=',')
            rows = [row for row in reader]
            order_asins = [l[0] for l in rows if len(l) > 0]
            total_profit = sum([float(l[1]) for l in rows if len(l) > 0 and is_number(l[1])])
            in_set = [asin for asin in order_asins if asin in asin_set]
            profit_in_set = sum([float(l[1]) for l in rows if len(l) > 0 and is_number(l[1]) and l[0] in asin_set])
            # print "Region %s book: total %s/$%s, %s/$%s in set,  %s/%s" % (
            #     region, len(order_asins), str(total_profit), len(in_set), str(profit_in_set),
            #     str(round(float(len(in_set) * 100) / float(len(order_asins)), 2)) + "%",
            #     str(round(float(profit_in_set * 100) / total_profit, 2)) + "%"
            # )

            print "Region %s book: total %s, %s in set,  %s, est. profit %s" % (
                region, len(order_asins), len(in_set),
                str(round(float(len(in_set) * 100) / float(len(order_asins)), 2)) + "%",
                str(round(float(profit_in_set * 100) / total_profit, 2)) + "%"
            )
