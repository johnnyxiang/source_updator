#!/usr/local/bin/python
# coding: utf-8
import traceback
import os
import sys
import csv
import glob

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

if __name__ == "__main__":
    account = '724us'
    excludes = []
    files = [f for f in glob.glob(lib_dir + "data/usbk1/high/*.csv") if account not in f]
    files_copy = list(files)
    unique_asins = set()
    for csv_file in files:
        excluded = False
        for exclude in excludes:
            if exclude in csv_file:
                excluded = True
                break
        if excluded:
            print 'skip', csv_file
            continue

        f = open(csv_file)
        reader = csv.reader(f, delimiter=',')
        c = set([l[0] for l in reader])
        unique_asins.update(c)
        print "find %d unique asins " % len(unique_asins)

    print "\n"

    asins_to_check = set()
    files = [f for f in glob.glob(lib_dir + "data/usprods/all/*.csv") if account in f]
    for csv_file in files:
        f = open(csv_file)
        reader = csv.reader(f, delimiter=',')
        c = set([l[0] for l in reader])
        asins_to_check.update(c)
        print "find %d asins to check " % len(asins_to_check)

    duplicated = asins_to_check.intersection(unique_asins)
    print "find %d duplicated asins" % len(duplicated)
    if len(duplicated) > 0:
        d_file = lib_dir + "data/usprods/d-%s.csv" % account
        with open(d_file, 'ab') as d_file_writer:
            for asin in duplicated:
                d_file_writer.write(asin + "\n")
