#!/usr/local/bin/python
# coding: utf-8
import traceback
import os
import sys
import csv
import glob

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

if __name__ == "__main__":

    files = [f for f in glob.glob(lib_dir + "data/us/uploaded/*.csv")]
    files_copy = list(files)
    asin_map = {}
    index = 0
    unique_asins = set()
    total = 0
    for csv_file in files:
        filename = os.path.basename(csv_file)
        try:
            f = open(csv_file)
            reader = csv.reader(f, delimiter=',', dialect=csv.excel_tab)

            asins = [l[0] for l in reader if len(l[0]) > 0]
            total += len(asins)
            unique_asins.update(set(asins))

            print "%d unique asins found, total %d " % (len(unique_asins), total)
        except:
            print filename
            print traceback.format_exc()

    file = lib_dir + 'data/us/724us-problem-mapping1.txt'
    with open(file) as f:
        reader = csv.reader(f, delimiter=',', dialect=csv.excel_tab)
        asins = [l[0] for l in reader if len(l[0]) > 0]
        total += len(asins)
        unique_asins.update(set(asins))

        print "%d unique asins found, total %d " % (len(unique_asins), total)


    file = lib_dir + 'data/us/724us-problem.csv'
    with open(file) as f:
        reader = csv.reader(f, delimiter=',', dialect=csv.excel_tab)
        problem_sku = []
        for line in reader:
            if line[1] in unique_asins:
                problem_sku.append(line)
        print "%d problem asins found, total %d" % (len(problem_sku),len(asins))

        if len(problem_sku) > 0:
            d_file = lib_dir + "data/us/proble724.csv"
            with open(d_file, 'w') as d_file_writer:
                for asin in problem_sku:
                    d_file_writer.write(','.join(asin) + "\n")

    sys.exit(1)

    merged_files = [f for f in glob.glob(lib_dir + "data/usbk/merged/*.csv")]
    merged_asins = set()
    for csv_file in merged_files:
        filename = os.path.basename(csv_file)
        try:
            f = open(csv_file)
            reader = csv.reader(f, delimiter=',', dialect=csv.excel_tab)

            asins = [l[0] for l in reader if len(l[0]) > 0]
            merged_asins.update(set(asins))

            print "%d merged asins found " % len(merged_asins)
        except:
            print filename
            print traceback.format_exc()

    files = [f for f in glob.glob(lib_dir + "data/usbk1/ca/*.csv")]
    other_unique_asins = set()
    for csv_file in files:
        f = open(csv_file)
        reader = csv.reader(f, delimiter=',')
        filename = os.path.basename(csv_file)

        asins = [l[0] for l in reader]
        other_unique_asins.update(set(asins))

        print "%d uploaded asins found " % len(other_unique_asins)


    # files = [f for f in glob.glob(lib_dir + "data/usbk1/blacklist/*.csv")]
    # blacklist_asins = set()
    # for csv_file in files:
    #     f = open(csv_file)
    #     reader = csv.reader(f, delimiter=',')
    #     filename = os.path.basename(csv_file)
    #
    #     asins = [l[0] for l in reader]
    #     blacklist_asins.update(set(asins))
    #
    #     print "%d blacklist asins found " % len(blacklist_asins)

    unique_asins = unique_asins - other_unique_asins
    print "%d asins found " % len(unique_asins)

    chunk_size = 300000
    unique_asins = list(unique_asins)
    chunks = [unique_asins[x:x + chunk_size] for x in xrange(0, len(unique_asins), chunk_size)]
    index = 0
    for chunk in chunks:
        index = index + 1

        d_file = lib_dir + "data/usbk1/new-ca-%d.csv" % index
        with open(d_file, 'w') as d_file_writer:
            for asin in chunk:
                d_file_writer.write(asin + "\n")

                # for csv_file in files:
                #     f = open(csv_file)
                #     reader = csv.reader(f, delimiter=',')
                #     filename = os.path.basename(csv_file)
                #
                #     for line in reader:
                #         sku = line[0]
                #         asin = line[1]
                #         unit = {'sku': sku, 'account': filename}
                #         if asin not in asin_map:
                #             asin_map[asin] = [unit]
                #         else:
                #             asin_map[asin].append(unit)
                #
                #     index = index + 1
                #     print "No %d/%d, %s, found %d unique asins" % (index, len(files), filename, len(asin_map))
                #
                # duplicates = {}
                # index = 0
                # total = len(asin_map)
                # for asin in asin_map.keys():
                #     units = asin_map[asin]
                #     if len(units) >= 2:
                #         for unit in units:
                #             if unit['account'] not in duplicates:
                #                 duplicates[unit['account']] = [[asin, unit['sku']]]
                #             else:
                #                 duplicates[unit['account']].append([asin, unit['sku']])
                #     index = index + 1
                #     del (asin_map[asin])
                #     if index % 10000 == 0:
                #         print "Finished %d/%d" % (index, total)
                # asin_map = {}
                # for account in duplicates.keys():
                #     units = duplicates[account]
                #     d_file = lib_dir + "data/usbk-uploaded/new/d/%s.csv" % account
                #     print "Account %s found %d duplicated asins" % (account, len(units))
                #     with open(d_file, 'w') as d_file_writer:
                #         for unit in units:
                #             d_file_writer.write(','.join(unit) + "\n")
                #     del (duplicates[account])
