import sys
import os
import traceback
import csv

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib.utils.simple_logger import SimpleLogger
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
from lib.utils.asin_matcher import AsinMatcher

logger = SimpleLogger.get_stream_logger('source_price_updator')

offer_service_price_finder = OfferServicePriceFinder()

asin_matcher = AsinMatcher()


def write_to_file(file_name, data):
    list_file = lib_dir + "data/%s.txt" % file_name
    with open(list_file, 'ab') as csvfile:
        for asin in data:
            csvfile.write(asin + "\n")


if __name__ == "__main__":
    last_id = 0
    size = 100
    found = 0

    filename = "all-asins-5452060"
    file_path = lib_dir + "data/%s.txt" % filename
    f = open(file_path)
    reader = csv.reader(f, delimiter=',')
    all_asins = set([l[0] for l in reader])
    print "%d all asins" % len(all_asins)

    existed_file_path = lib_dir + "data/all-books.csv"
    f = open(existed_file_path)
    reader = csv.reader(f, delimiter=',')
    existed_asins = set([l[0] for l in reader])
    print "%d existed asins" % len(existed_asins)

    asins = all_asins - existed_asins

    print "%d asins found good to use" % len(asins)

    if len(asins) > 0:
        write_to_file('good-asins', asins)
