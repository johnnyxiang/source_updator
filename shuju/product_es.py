import sys
import argparse
import traceback
import os
import time

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib import mws_product
from lib.models import *
from local import sids
from lib.elastic_product import ElasticProduct
from shuju.utils import get_product_info_mws


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-s', '--size', type=int, default=100, help='page size')
    parser.add_argument('-db', '--db_table', type=str, default='prod_check_ken', help='db table name')
    command_args = parser.parse_args()
    return command_args


elastic = ElasticProduct(host="35.199.3.83", port=80, http_auth=('elasticuser', 'KbersRiseUp153'))

if __name__ == "__main__":

    args = parse_args()
    print args

    db_table = args.db_table
    country = args.country
    index_type = '%s_%s' % ('product_group_info', db_table)

    limit = args.limit
    pindex = 0
    while True:
        with accountDB.atomic() as transaction:
            last_id = get_last_id(index_type, country=country)
            # sql = "SELECT isbn,id FROM " + db_table + " WHERE weight =0 and id > " + str(last_id)
            sql = "SELECT isbn,id FROM " + db_table + " WHERE `group` is null and id > " + str(last_id)
            page_size = args.size if args.limit == 0 or args.size < args.limit else args.limit
            sql = sql + " order by id asc limit %s" % page_size

            print sql
            cursor = dataDB.execute_sql(sql)
            rows = cursor.fetchall()

            if len(rows) == 0:
                break

            asins = [str(row[0]).zfill(10) for row in rows]
            min_id = rows[len(rows) - 1][1]
            try:
                save_last_id(index_type, min_id, country=country)
            except:
                break

            pindex = len(rows) + 1
            print "loading ", min_id, ';  '

        try:
            existed = elastic.get_data_by_asins(asins, country)
            existed_dict = {k: v for k, v in existed.items()}
            if len(existed_dict) > 0:
                with dataDBSave.atomic() as transaction:
                    for asin, payload in existed_dict.iteritems():
                        # sql = 'update ' + db_table + ' set `weight` = %s,`width` = %s,`height` = %s,`length` = %s where isbn = %s'
                        # dataDBSave.execute_sql(sql, [payload['weight'], payload['width'], payload['height'], payload['length'], asin])
                        sql = 'update ' + db_table + ' set `group` = %s where isbn = %s'
                        dataDBSave.execute_sql(sql, [payload['group'], asin])


        except:
            if "QuotaExceeded" in traceback.format_exc() or 'RequestThrottled' in traceback.format_exc():
                time.sleep(60)
            else:
                minId = rows[len(rows) - 1][1]

            print traceback.format_exc()

        if limit != 0 and pindex >= limit:
            break
