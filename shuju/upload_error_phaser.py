#!/usr/local/bin/python
# coding: utf-8
import traceback
import os
import sys
import csv

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)


def write_to_file(file_name, data):
    list_file = lib_dir + "data/errors/parsed/%s.csv" % file_name
    with open(list_file, 'ab') as csvfile:

        if isinstance(data, dict):
            data = [[k, v] for k, v in data.iteritems()]

        for asin in data:
            if isinstance(asin, basestring):
                csvfile.write(asin + "\n")
            else:
                csvfile.write(",".join(asin) + "\n")


if __name__ == "__main__":
    file_name = "742us-01.txt"
    file_dir = lib_dir + "/data/errors/raw/"
    file_path = file_dir + file_name
    errors = []
    error_codes = {}
    with open(file_path) as f:
        reader = csv.reader(f, delimiter='\t')
        # original-record-number	sku	error-code	error-type	error-message
        for line in reader:
            if len(line) < 5 or line[0] == 'original-record-number':
                continue

            errors.append([line[1], line[2]])
            # try:
            #     errors[line[2]].append(line[1])
            # except:
            #     errors[line[2]] = [line[1]]

            if line[2] not in error_codes:
                error_codes[line[2]] = line[4]

                # print line

    # for k, v in errors.iteritems():
    print "found %s errors " % len(errors)
    dist_file = os.path.basename(file_name)
    write_to_file(dist_file, errors)

    dist_file = os.path.basename(file_name) + '-error-codes'
    write_to_file(dist_file, error_codes)
