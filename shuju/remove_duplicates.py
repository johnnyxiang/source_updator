#!/usr/local/bin/python
# coding: utf-8
import traceback
import os
import sys
import csv
import glob

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

if __name__ == "__main__":
    file_name = "711uk-prods"
    file_path = lib_dir + "data/eu/p/%s.csv" % file_name
    d_file_path = lib_dir + "data/eu/p/%s-duplicates.csv" % file_name
    cleared = dict()
    duplicates = list()

    total = 0
    with open(file_path) as f:
        reader = csv.reader(f, delimiter=',')
        for line in reader:
            sku = line[0]
            asin = line[1]
            if asin in cleared:
                duplicates.append(line)
            cleared[asin] = line
            total = total + 1

    print "%d isbns found, %d after clearance, %d duplicates" % (total, len(cleared), len(duplicates))

    if len(duplicates) > 0:
        with open(d_file_path, 'w') as d_file_writer:
            for row in duplicates:
                d_file_writer.write(",".join(row) + "\n")
