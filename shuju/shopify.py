import sys
import argparse
import traceback
import os

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib import mws_product
from lib.models import *
from local import sids


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default=None, help='Source ASIN country')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-s', '--size', type=int, default=100, help='page size')
    parser.add_argument('-it', '--id_type', type=str, default='ASIN', help='ID Type')
    parser.add_argument('-db', '--db_table', type=str, default='products', help='db table name')

    command_args = parser.parse_args()
    return command_args


def get_product_info(dbModel, db_table, asins, product_api, region, id_type='ASIN'):
    if id_type.lower() == 'upc':
        chunks = [asins[x:x + 5] for x in xrange(0, len(asins), 5)]
    else:
        chunks = [asins[x:x + 10] for x in xrange(0, len(asins), 10)]
    # print chunks
    for chunk in chunks:
        if id_type.lower() == 'upc':
            response = mws_product.get_matching_product_for_upc(product_api, chunk, region=region)
            parsed_resp = response._response_dict

            if "Id" in parsed_resp['GetMatchingProductForIdResult']:
                results = [parsed_resp['GetMatchingProductForIdResult']]
            else:
                results = parsed_resp['GetMatchingProductForIdResult']
        else:
            response = mws_product.get_matching_product(product_api, chunk, region=region)
            parsed_resp = response._response_dict

            if "ASIN" in parsed_resp['GetMatchingProductResult']:
                results = [parsed_resp['GetMatchingProductResult']]
            else:
                results = parsed_resp['GetMatchingProductResult']

        with dbModel.atomic():
            for product_dict in results:
                if "Error" in product_dict:
                    print product_dict['Error']['Message']['value']

                    try:
                        if id_type.lower() == 'upc':
                            asin = product_dict['Id']['value']
                            sql = "UPDATE " + db_table + " SET asin=%s, binding=%s WHERE upc = %s"
                            dbModel.execute_sql(sql, ["None", asin])
                        else:
                            asin = product_dict['ASIN']['value']
                            sql = "UPDATE " + db_table + " SET brand = %s,title=%s, binding=%s WHERE asin = %s"
                            dbModel.execute_sql(sql, ["None", "None", "None", asin])
                    except:
                        pass

                    continue

                # print product_dict
                # asin = product_dict['ASIN']['value']
                upc = None
                if id_type.lower() == 'upc':
                    upc = product_dict['Id']['value']

                try:
                    if 'Product' in product_dict:
                        product = product_dict['Product']
                    elif "AttributeSets" in product_dict['Products']['Product']:
                        product = product_dict['Products']['Product']
                    else:
                        product = product_dict['Products']['Product'][0]

                    asin = product['Identifiers']['MarketplaceASIN']['ASIN']['value']

                    try:
                        title = product['AttributeSets']['ItemAttributes']['Title']['value']
                    except:
                        title = ""

                    if len(title) > 255:
                        title = title[:255]
                    try:
                        brand = product['AttributeSets']['ItemAttributes']['Brand']['value']
                    except:
                        brand = ""

                    if len(brand) > 100:
                        brand = brand[:100]

                    try:
                        binding = product['AttributeSets']['ItemAttributes']['Binding']['value']
                    except:
                        binding = "None"

                    try:
                        group = product['AttributeSets']['ItemAttributes']['ProductGroup']['value']
                    except:
                        group = ""

                    try:
                        sales_ranking = product['SalesRankings']['SalesRank'][0]['Rank']['value']
                    except:
                        sales_ranking = 0

                    try:
                        weight = round(float(product['AttributeSets']['ItemAttributes']['PackageDimensions']['Weight']['value']), 2)
                    except:
                        weight = 0

                    try:
                        height = round(float(product['AttributeSets']['ItemAttributes']['PackageDimensions']['Height']['value']), 2)
                    except:
                        height = 0

                    try:
                        length = round(float(product['AttributeSets']['ItemAttributes']['PackageDimensions']['Length']['value']), 2)
                    except:
                        length = 0

                    try:
                        width = round(float(product['AttributeSets']['ItemAttributes']['PackageDimensions']['Width']['value']), 2)
                    except:
                        width = 0

                    try:
                        mpn = product['AttributeSets']['ItemAttributes']['PartNumber']['value']
                    except:
                        mpn = ""

                    # SmallImage
                    try:
                        image = product['AttributeSets']['ItemAttributes']['SmallImage']['URL']['value']
                    except:
                        image = ""

                    if upc is not None:
                        mpn = upc
                    print asin, title, brand, sales_ranking, mpn

                    if group == '':
                        group = binding

                    if id_type.lower() == 'upc':
                        sql = "UPDATE " + db_table + " SET brand = %s,title=%s,weight= %s,height= %s ,length= %s ,width= %s,binding=%s,asin=%s WHERE upc = %s"
                        dbModel.execute_sql(sql, [brand, title, weight, height, length, width, group, asin, mpn])
                    else:
                        sql = "UPDATE " + db_table + " SET  brand = %s,title=%s,weight= %s,height= %s ,length= %s ,width= %s,binding=%s,mpn=%s,image=%s WHERE asin = %s"
                        dbModel.execute_sql(sql, [brand, title, weight, height, length, width, group, mpn, image, asin])

                except:
                    print traceback.format_exc()

        time.sleep(1)


if __name__ == "__main__":

    args = parse_args()
    print args

    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        print "No account info for " + args.account + " found"
        sys.exit()

    if args.country is None or len(args.country) == 0:
        country = accountInfo.country.upper()
    else:
        country = args.country.upper()

    db_table = args.db_table

    productApi = mws_product.getProductApi(accountInfo.seller_id, accountInfo.mws_access_key,
                                           accountInfo.mws_secret_key, country, auth_token=accountInfo.mws_auth_token)

    index_type = '%s_%s' % ('product_info', db_table)

    limit = args.limit
    pindex = 0
    id_type = args.id_type
    page_size = args.size if args.limit == 0 or args.size < args.limit else args.limit
    while True:
        with accountDB.atomic() as transaction:
            last_id = get_last_id(index_type, country=country)
            sql = "SELECT %s,id FROM %s WHERE (binding ='' OR binding IS NULL) AND id > %s order by id asc limit %s" % (
                id_type, db_table, last_id, page_size)
            print sql
            cursor = shopifyDB.execute_sql(sql)
            rows = cursor.fetchall()

            if len(rows) == 0:
                break

            if id_type.lower() == 'upc':
                asins = [str(row[0]).zfill(12) for row in rows]
            else:
                asins = [str(row[0]).zfill(10) for row in rows]

            min_id = rows[len(rows) - 1][1]
            try:
                save_last_id(index_type, min_id, country=country)
            except:
                break

            pindex = len(rows) + 1
            print "loading ", min_id, ';  '

        try:

            get_product_info(shopifyDB, db_table, asins, productApi, country, id_type)

            minId = rows[len(rows) - 1][1]


        except:
            if "QuotaExceeded" in traceback.format_exc() or 'RequestThrottled' in traceback.format_exc():
                time.sleep(60)
            else:
                minId = rows[len(rows) - 1][1]

            print traceback.format_exc()

        if limit != 0 and pindex >= limit:
            break
