import csv
import sys
import os
import time
import traceback

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)


def write_to_file(file_name, data):
    with open(file_name, 'ab') as csvfile:
        for row in data:
            try:
                csvfile.write(row + "\n")
            except:
                pass


if __name__ == "__main__":
    mapping_file = lib_dir + "/data/mapping.txt"
    asin_set = set()
    isbn_set = set()
    dist_file = lib_dir + "/data/prods.txt"
    dist_file_isbn = lib_dir + "/data/prods_isbn.txt"
    index = 0
    with open(mapping_file) as f:
        reader = csv.reader(f, delimiter=',')
        for line in reader:
            if line[1].startswith("B"):
                # asin_set.add(line[0])
                isbn_set.add(line[1])
            index = index + 1
            if index % 1000 == 0:
                print index, len(asin_set), len(isbn_set)

    print "%d asins found" % len(asin_set)
    print "%d isbn found" % len(isbn_set)
    # write_to_file(dist_file, asin_set)
    write_to_file(dist_file_isbn, isbn_set)
