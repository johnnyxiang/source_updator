import csv
import glob
import sys
import argparse
import traceback
import os
import time

from elasticsearch5 import NotFoundError, RequestError, ConnectionTimeout, TransportError, ElasticsearchException, Elasticsearch

from lib.models import accountDB

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), '..')) + '/')
sys.path.insert(0, lib_dir)


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-c', '--country', type=str, default='us', help='Source ASIN country')
    parser.add_argument('-g', '--group', type=str, default='kitchen', help='Source ASIN country')
    parser.add_argument('-l', '--limit', type=str, default='1m', help='limit')
    parser.add_argument('-s', '--size', type=int, default=200, help='page size')

    command_args = parser.parse_args()
    return command_args


elastic = Elasticsearch(hosts='35.199.3.83', port=80)


def write_to_file(file_name, data):
    list_file = lib_dir + "data/new_fake/raw/%s.csv" % file_name
    with open(list_file, 'ab') as csvfile:
        for asin in data:
            try:
                if isinstance(asin, basestring):
                    csvfile.write(asin + "\n")
                elif isinstance(asin, dict):
                    line = asin.values()
                    csvfile.write("\t".join(line) + "\n")
                else:
                    csvfile.write("\t".join(asin) + "\n")
            except:
                pass


def search(product_group, scroll_size='100k', page_size=500, country='us'):
    query = {
        "bool": {
            "must": {"match": {
                "ProductGroup": {"query": product_group,
                                 "operator": "and"
                                 }
            }},
            "filter": {"range": {
                "sales_rank.raw": {
                    "gt": 1000
                }
            }}
        }
    }

    params = {
        'scroll': scroll_size,
        'size': page_size,
        'index': 'product',
        'doc_type': country.lower(),
        'body': {
            'query': query,
            'sort': [
                {
                    'sales_rank.raw': {
                        "order": "asc"
                    }
                }
            ]
        }
    }

    print params
    resp = None
    retry = 3
    while retry > 0:
        try:
            resp = elastic.search(**params)
            break
        except NotFoundError as e:
            break
        except RequestError as e:
            raise e
        except ConnectionTimeout:
            time.sleep(1)
        except (TransportError, ElasticsearchException) as e:
            print e
            resp = -1
            retry -= 1

            status_code = getattr(e, 'status_code', None)
            if status_code == 'N/A':
                time.sleep(3)
        except Exception as e:
            print e.message
            resp = -1
            retry -= 1
    if resp is None:
        return None

    if resp == -1:
        return False

    return resp


if __name__ == "__main__":
    args = parse_args()
    print args

    files = [f for f in glob.glob(lib_dir + "data/new_fake/used/*.csv")]

    used_asin = set()
    for csv_file in files:
        f = open(csv_file)
        reader = csv.reader(f, delimiter=',')
        used_asin.update(set([l[0] for l in reader]))
        print "%d used asins found" % len(used_asin)

    sql = "SELECT brand FROM blacklist_brands"
    cursor = accountDB.execute_sql(sql)
    rows = cursor.fetchall()
    blacklist_brands = set([row[0].lower() for row in rows])

    size = args.size
    limit = args.limit

    data = search(product_group=args.group, scroll_size=limit, page_size=size, country=args.country)

    sid = data['_scroll_id']
    scroll_size = data['hits']['total']
    total_size = scroll_size
    total_pages = int(total_size / size)
    page_no = 0
    ps = []
    total = 0

    dist_file = args.group.lower()
    dist_file = dist_file.replace(' ', '-')
    # Start scrolling
    while scroll_size > 0:
        print "Scrolling..."
        data = elastic.scroll(scroll_id=sid, scroll=limit)
        # Update the scroll ID
        sid = data['_scroll_id']
        # Get the number of results that we returned in the last scroll
        scroll_size = len(data['hits']['hits'])
        # print "scroll size: " + str(scroll_size)

        if data['hits']['total'] == 0:
            break

        ps = []
        for row in data['hits']['hits']:
            asin = row['_source']['asin']
            brand = row['_source']['brand']
            if asin in used_asin:
                continue

            if brand.lower() in blacklist_brands:
                continue

            if 'title' in row['_source']:
                try:
                    title = row['_source']['title']
                    brand = row['_source']['brand']
                    if brand == '':
                        continue
                    image = row['_source']['attributes']['SmallImage']['URL']['value'].strip()
                    if image == '':
                        continue
                    image = image.replace('._SL75_', '._UX679_')

                    ps.append({'brand': row['_source']['brand'], 'asin': asin, 'title': title, 'image': image})
                except:
                    print traceback.format_exc()
                total = total + 1

        if len(ps) > 0:
            write_to_file(dist_file, ps)

        page_no = page_no + 1
        print 'page %s/%s, total %s/%s' % (page_no, total_pages, total, total_size)
