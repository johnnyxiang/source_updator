import csv
import sys
import os

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), '..')) + '/')
sys.path.insert(0, lib_dir)


if __name__ == "__main__":
    file_name = 'kitchen'
    list_file = lib_dir + "data/new_fake/raw/%s.csv" % file_name
    with open(list_file) as f:
        reader = csv.reader(f, delimiter='\t')
        for line in reader:
            asin = line[0]
            brand = line[1]
            image = line[2]
            title = line[3]
            print asin, brand, title, image
            break
