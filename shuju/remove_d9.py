#!/usr/local/bin/python
# coding: utf-8
import traceback
import os
import sys
import csv
import glob

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

if __name__ == "__main__":

    mapping_file = lib_dir + "data/usbk-uploaded/new/mapping.txt"
    pro_asins = set()
    f = open(mapping_file)
    reader = csv.reader(f, delimiter=',', dialect=csv.excel_tab)
    pro_asins.update(set([l[0] for l in reader]))
    print "total %d prod asins found" % len(pro_asins)

    files = [f for f in glob.glob(lib_dir + "data/usbk-uploaded/new/uploaded/*.txt")]
    for csv_file in files:
        filename = os.path.basename(csv_file)
        rows = list()
        try:
            # f = open(csv_file)
            # reader = csv.reader(f, delimiter='\t', dialect=csv.excel_tab)
            for line in open(csv_file, 'rU'):
                cols = line.split('\t')
                if cols[1] in pro_asins:
                    rows.append([cols[0], cols[1]])
        except:
            print filename
            print traceback.format_exc()

        print "%d prods found in %s" % (len(rows), filename)
        if len(rows) > 0:
            d_file = lib_dir + "data/usbk-uploaded/new/d/d-%s" % filename
            with open(d_file, 'w') as d_file_writer:
                #
                d_file_writer.write('\t'.join(['sku', 'product-id', 'product-id-type', 'add-delete']) + "\n")
                for row in rows:
                    row.append('ASIN')
                    row.append('x')
                    d_file_writer.write('\t'.join(row) + "\n")
