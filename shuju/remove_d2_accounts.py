#!/usr/local/bin/python
# coding: utf-8
import traceback
import os
import sys
import csv
import glob
from os.path import basename

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

if __name__ == "__main__":
    account = '705'
    max_appearance = 3
    # excludes = ['709', '713', '724', '720', '710','704']
    excludes = ['719', '721']
    all_files = [f for f in glob.glob(lib_dir + "data/usbk1/low/*.csv")]
    asin_dict = {}
    total_files = len(all_files)
    index = 0
    for csv_file in all_files:
        index = index + 1
        excluded = False
        for k in excludes:
            if k in os.path.basename(csv_file):
                excluded = True
                break
        if excluded:
            print os.path.basename(csv_file), 'excluded'
            continue

        f = open(csv_file)
        reader = csv.reader(f, delimiter=',')
        asins = set([l[0] for l in reader])
        for asin in asins:
            asin_dict[asin] = 1 if asin not in asin_dict else asin_dict[asin] + 1

        print "%d/%d - %d asin found " % (index, total_files, len(asin_dict))

    for k, v in asin_dict.items():
        if v < max_appearance:
            del asin_dict[k]

    print "\n %d duplicated asin found " % len(asin_dict)

    check_files = [f for f in glob.glob(lib_dir + "data/usbk1/low/%s*.csv" % account)]
    duplicates = set()
    for csv_file in check_files:
        f = open(csv_file)
        reader = csv.reader(f, delimiter=',')
        file_duplicates = [l[0] for l in reader if l[0] in asin_dict]
        duplicates.update(set(file_duplicates))

        print "%d duplicates found in %s, total duplicates %d" % (
            len(file_duplicates), os.path.basename(csv_file), len(duplicates))

    if len(duplicates) > 0:
        d_file = lib_dir + "data/usbk1/d/d-%s.csv" % account
        with open(d_file, 'w') as d_file_writer:
            for asin in duplicates:
                d_file_writer.write(asin + "\n")
