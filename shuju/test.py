#!/usr/local/bin/python
# coding: utf-8
import traceback

import gzip

import os

from lib.elastic_product import ElasticProduct

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')


def parse(path):
    g = gzip.open(path, 'r')
    for l in g:
        yield eval(l)


if __name__ == "__main__":
    elastic = ElasticProduct(host="35.199.3.83", port=80, http_auth=('elasticuser', 'KbersRiseUp153'))
    i = 0
    payloads = {}
    for l in parse(lib_dir + "/data/meta_Books.json.gz"):
        try:
            i = i + 1
            asin = l["asin"].replace("'", "")

            # if asin.startswith("B") is False:
            #     continue
            # if 'categories' in l and "CDs" in (" ".join(str(x) for x in l['categories'])):
            #     continue
            if 'title' not in l:
                continue

            title = l['title']
            if len(title) == 0:
                continue
            brand = l['brand'] if 'brand' in l else ''

            payload = {
                'asin': asin,
                'title': title
            }

            if len(brand) > 0:
                payload['brand'] = brand

            if 'salesRank' in l:
                payload['sales_ranks'] = []
                for cat, rank in l['salesRank'].items():
                    payload['sales_ranks'].append({'category': cat, 'rank': rank})
                    if 'sales_rank' not in payload:
                        payload['sales_rank'] = rank

            if 'imUrl' in l:
                payload['imUrl'] = l['imUrl']

            if 'categories' in l:
                payload['categories'] = l['categories']

            payloads[asin] = payload
            if i % 100 == 0:
                print i, asin
        except:
            print traceback.format_exc()
            print l
            pass

        if len(payloads) >= 100:
            asins = payloads.keys()
            asins = elastic.remove_existed(asins)
            for asin in asins:
                try:
                    elastic.bulk_add_data(asin, payload=payloads[asin])
                except:
                    print traceback.format_exc()
            payloads = {}
