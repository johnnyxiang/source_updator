#!/usr/local/bin/python
# coding: utf-8
import traceback
import os
import sys
import csv
import glob

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

if __name__ == "__main__":
    uk_files = [f for f in glob.glob(lib_dir + "data/caprod/u/*.csv")]
    uk_asins = set()
    index = 0
    by_cat_map = {'na': []}

    cats = ['toy', 'kitchen', 'office', 'beauty', 'health', 'home', 'sport', 'outdoor', 'baby', 'tool', 'pet', 'pro']
    for cat in cats:
        by_cat_map[cat] = []

    for csv_file in uk_files:
        index = index + 1
        f = open(csv_file)
        reader = csv.reader(f, delimiter=',')
        rows = [l for l in reader if len(l) > 0]
        for row in rows:
            asin = row[0]
            sku = row[1]
            row_cat = 'na'
            for cat in cats:
                if cat in sku.lower():
                    row_cat = cat
                    break
            by_cat_map[row_cat].append(row)

            # print "found %s asins in %s files" % (len(uk_asins), index)

    for cat, rows in by_cat_map.iteritems():
        if len(rows) > 0:
            d_file = lib_dir + "data/caprod/cat_%s.csv" % cat
            with open(d_file, 'ab') as d_file_writer:
                for row in rows:
                    d_file_writer.write(','.join(row) + "\n")
