import sys
import argparse
from lib.utils.simple_logger import SimpleLogger
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
from lib.models import *

# Python version must be greater than or equal 2.7
if sys.version_info < (2, 7):
    print("ERROR: Repricing requires Python Version 2.7 or above...exiting.")
    sys.exit(1)
else:
    PY2 = bool(sys.version_info.major == 2)

logger = SimpleLogger.get_stream_logger('source_price_updator')

offer_service_price_finder = OfferServicePriceFinder()


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')

    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-d', '--condition', type=str, default="any", help='Source ASIN condition')
    parser.add_argument('-t', '--type', type=str, default="book", help='Product Type: product, book')
    parser.add_argument('-o', '--offset', type=int, default=0, help='Start offset')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-s', '--size', type=int, default=500, help='page size')

    command_args = parser.parse_args()
    return command_args


def build_query(country, min_date):
    query = {"bool": {
        "filter": [{
            "match": {"country_code": country}
        },
            {
                "match": {"product_code": "c"}
            },
            {
                "range": {
                    "time": {"gte": min_date}
                }
            }, {
                "range": {
                    "product_price": {"lte": 100}
                }
            }
        ]
    }}
    return query


if __name__ == "__main__":
    args = parse_args()
    print args
    if args.condition == "new":
        db_table = "source_prices_new"
    else:
        db_table = "source_prices_used"
    limit = args.limit

    sort = {"time": {"order": "asc"}}

    offset = args.offset
    min_date = "2018-06-24T09:04:07"
    last_min_date = None
    total = 0
    while True:
        try:
            if min_date == last_min_date:
                offset = offset + args.size
            else:
                offset = 0
            last_min_date = min_date

            query = build_query(args.country, min_date)

            offers = offer_service_price_finder.search_offers(query, condition=args.condition, size=args.size,
                                                              offset=offset, sort=sort)

            if len(offers) == 0:
                break

            update_sql = " insert into " + db_table + "  (asin,price,offers,offer_time) values "
            values = []
            for asin, offer in offers.iteritems():
                price = offer["price"]
                total_offers = offer["offers"]
                values.append("('" + asin + "'," + str(price) + "," + str(total_offers) + ",'" + offer["time"] + "')")
                min_date = offer['time']

            update_sql = update_sql + ",".join(values)
            update_sql = update_sql + " ON DUPLICATE KEY UPDATE price=VALUES(price),offers=VALUES(offers),offer_time=VALUES(offer_time);"
            # print update_sql
            try:
                accountDB.execute_sql(update_sql)
            except Exception as e:
                print e.message

            total = total + len(offers)

        except Exception as e:
            offset = offset + 1
            print e.message

        if limit > 0 and total >= limit:
            break
