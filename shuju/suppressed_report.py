import os
import sys
import csv
import glob

from lib.utils.sku_parser import SkuParser

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

if __name__ == "__main__":
    file_name = "724us"
    file_path = "%s/data/suppressed/%s.txt" % (lib_dir, file_name)
    with open(file_path) as f:
        reader = csv.reader(f, delimiter='\t')
        line_no = 0
        for line in reader:
            line_no += 1
            if line_no == 1:
                print  line
                continue

            sku = line[0]
            sku_info = SkuParser.parse_sku(sku)

            if sku.endswith('-s') or sku_info['product_code'] != 'p':
                continue
            status = line[8]

            if status == 'Active':
                continue
            print line
            # break
