import csv
import glob
import sys
import os
import time
import traceback

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib.elastic_product import ElasticProduct

elastic = ElasticProduct(host="35.199.3.83", port=80,http_auth=('elasticuser', 'KbersRiseUp153'))


def write_to_file(file_name, data):
    with open(file_name, 'ab') as csvfile:
        for row in data:
            try:
                csvfile.write(row + "\n")
            except:
                pass


if __name__ == "__main__":
    file_isbn = lib_dir + "/data/data/existed/done.existed-prod-isbns.csv"

    line_no = 0
    found = 0
    p = 0
    exclude_groups = []
    isbns_by_group = {}
    existed = []
    no_group = []

    existed_files = [f for f in glob.glob(lib_dir + "data/data/test-existed/*.*")]
    for existed_file in existed_files:
        with open(existed_file) as f:
            reader = csv.reader(f, delimiter=',')
            existed.extend([f[0] for f in reader])
            print "%d existed asins found " % len(existed)

    with open(file_isbn) as f:
        isbns = []
        reader = csv.reader(f, delimiter=',')
        for line in reader:
            line_no = line_no + 1
            isbn = line[0].strip()
            if line_no % 1000 == 0:
                print line_no

            if isbn in existed:
                continue

            isbns.append(isbn)
            if len(isbns) >= 500:
                print "PROCESS %s" % p
                product_data = elastic.get_data_by_asins(isbns)
                p = p + 1
                if product_data is not None:
                    for isbn, data in product_data.iteritems():
                        if data['group'] == '' or data['group'] is None or data['group'] in exclude_groups:
                            no_group.append(isbn)
                            continue

                        if data['group'] not in isbns_by_group:
                            isbns_by_group[data['group']] = []

                        isbns_by_group[data['group']].append(isbn)

                        found = found + 1

                for group, isbns in isbns_by_group.iteritems():
                    print group, len(isbns)
                    file_name = group.lower().replace(' ', '-')
                    file_name = lib_dir + "/data/data/test/%s.txt" % file_name
                    write_to_file(file_name, isbns)

                if len(no_group) > 100:
                    file_name = lib_dir + "/data/data/no_group1.txt"
                    write_to_file(file_name, no_group)
                    no_group = []
                isbns_by_group = {}
                isbns = []
                print found, line_no
