import sys
import argparse
import traceback
import os

import time

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib import mws_product
from lib.models import *
from local import sids
from shuju.utils import get_product_info, get_prices, get_prices_form_es, clear_db_by_ingredients, clear_db_by_brands, \
    clear_blacklist, get_price_info, get_product_info_from_es


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default=None, help='Source ASIN country')
    parser.add_argument('-t', '--type', type=str, default="product", help='')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-o', '--order', type=str, default='asc', help='order')
    parser.add_argument('-s', '--size', type=int, default=100, help='page size')
    parser.add_argument('-m', '--mode', type=str, default="asin", help='running mode')
    parser.add_argument('-f', '--fwd', type=str, default='us', help='us,uk')
    parser.add_argument('-db', '--db_table', type=str, default='', help='db table name')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":

    args = parse_args()

    print args

    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    database_name = args.account
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    limit = args.limit
    pindex = 0

    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        print "No account info for " + args.account + " found"
        sys.exit()

    if args.country is None or len(args.country) == 0:
        country = accountInfo.country.upper()
    else:
        country = args.country.upper()
    if args.order == 'asc':
        minId = 0
    else:
        minId = 1000000000

    if args.type in ['price', 'price_info', 'product']:
        productApi = mws_product.getProductApi(accountInfo.seller_id, accountInfo.mws_access_key,
                                               accountInfo.mws_secret_key, country, auth_token=accountInfo.mws_auth_token)
    else:
        productApi = None

    if len(args.db_table) > 0:
        db_table = args.db_table
    elif args.fwd == 'us':
        db_table = 'us_asins_crawl'
    else:
        db_table = 'uk_asins_crawl'

    dbModel = dataDB

    found = 0

    if args.type == "clear":
        clear_db_by_brands(country, dbModel, db_table)
        clear_db_by_ingredients(country, dbModel, db_table)
        clear_blacklist(country, dbModel, db_table)
        sys.exit()
    if args.type == "clear_brand":
        clear_db_by_brands(country, dbModel, db_table)
        sys.exit()
    if args.type == "clear_ingre":
        clear_db_by_ingredients(country, dbModel, db_table)
        sys.exit()
    if args.type == "clear_black":
        clear_blacklist(country, dbModel, db_table)
        sys.exit()

    if args.type == "price":
        sql = "update " + db_table + " set status = -5 where status > 0 and (us_title = 'None' or uk_title = 'None' or us_lwh>50 or us_weight>10  or uk_lwh>50 or uk_weight>10)"
        print sql
        dbModel.execute_sql(sql)

    index_type = '%s_%s' % (args.type, db_table)
    while True:
        with accountDB.atomic() as transaction:
            last_id = get_last_id(index_type, country=country)
            sql = "SELECT isbn,id FROM " + db_table + " WHERE id > " + str(last_id)

            if country.lower() == "uk":
                price_field = 'uk_price'
                lowest_price_field = 'uk_price_lowest'
                offer_field = 'uk_offers'
                title_field = 'uk_title'
            else:
                price_field = 'us_price'
                offer_field = 'us_offers'
                title_field = 'us_title'
                lowest_price_field = 'us_price_lowest'

            if args.type == "price" or args.type == 'es_price':
                sql = sql + "  and status >0 and  " + price_field + "= 0"
            elif args.type == "price_info":
                sql = sql + "  and status >0 and  " + lowest_price_field + "= 0"
            else:
                sql = sql + " and status >=0 and  " + title_field + " is null"

            page_size = args.size if args.limit == 0 or args.size < args.limit else args.limit
            sql = sql + " order by id %s limit %s" % (args.order, page_size)

            print sql

            cursor = dbModel.execute_sql(sql)
            rows = cursor.fetchall()

            if len(rows) == 0:
                break

            asins = [str(row[0]).zfill(10) for row in rows]
            min_id = rows[len(rows) - 1][1]
            try:
                save_last_id(index_type, min_id, country=country)
            except:
                break

            pindex = len(rows) + 1
            print "loading ", min_id, '; total found ', found

        try:
            if args.type == 'product':
                get_product_info(dataDBSave, db_table, asins, productApi, country)
            elif args.type == 'product_es':
                get_product_info_from_es(dataDBSave, db_table, asins, country)
            elif args.type == 'es_price':
                get_prices_form_es(dataDBSave, db_table, asins, country=country)
            elif args.type == 'price_info':
                get_price_info(dataDBSave, db_table, asins, country, productApi)
            else:
                get_prices(dataDBSave, db_table, asins, country, productApi)

            minId = rows[len(rows) - 1][1]
        except:
            if "QuotaExceeded" in traceback.format_exc() or 'RequestThrottled' in traceback.format_exc():
                time.sleep(60)
            else:
                minId = rows[len(rows) - 1][1]

            print traceback.format_exc()

        if limit != 0 and pindex >= limit:
            break
