import glob
import sys
import os
import traceback

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib.utils.simple_logger import SimpleLogger
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
import csv
from lib.utils.asin_matcher import AsinMatcher

logger = SimpleLogger.get_stream_logger('source_price_updator')
offer_service_price_finder = OfferServicePriceFinder()
asin_matcher = AsinMatcher()


def write_to_file(file_name, data):
    list_file = lib_dir + "data/ukprods/%s.txt" % file_name
    with open(list_file, 'ab') as csvfile:
        for asin in data:
            if isinstance(asin, basestring):
                csvfile.write(asin + "\n")
            else:
                csvfile.write(",".join(asin) + "\n")


if __name__ == "__main__":

    last_id = 0
    size = 100
    found = 0

    files = ['u']
    asin_files = [f for f in glob.glob(lib_dir + "data/ukprods/asin/*.csv")]
    asin_set = set()
    for file_path in asin_files:
        f = open(file_path)
        reader = csv.reader(f, delimiter=',')
        asins = set([l[1] for l in reader])
        asin_set.update(asins)
    print "%s asins found" % len(asin_set)

    isbn_files = [f for f in glob.glob(lib_dir + "data/ukprods/isbn/*.csv")]
    isbn_set = set()
    for file_path in isbn_files:
        f = open(file_path)
        reader = csv.reader(f, delimiter=',')
        isbns = set([l[0] for l in reader])
        isbn_set.update(isbns)
    print "%s isbns found" % len(isbn_set)

    uploaded_files = [f for f in glob.glob(lib_dir + "data/ukprods/uploaded/*.*")]
    uploaded_set = set()
    for file_path in uploaded_files:
        f = open(file_path)
        reader = csv.reader(f, delimiter=',')
        asins = set([l[0] for l in reader])
        uploaded_set.update(asins)
    print "%s uploaded asins found" % len(uploaded_set)

    # f = open(lib_dir + "data/ukprods/all-asins.txt")
    # reader = csv.reader(f, delimiter=',')
    # all_asins = set([l[0] for l in reader])
    # print "%s asins found" % len(all_asins)

    dist_filename = "new-asins"
    # # asins = all_asins.intersection(asin_set)
    # asins = all_asins
    # asins = asins - uploaded_set
    # print "%s asins found" % len(asins)
    # if len(asins) > 0:
    #     write_to_file(dist_filename, asins)

    if len(isbn_set) > 0:
        asins = set()
        isbns = list(isbn_set)
        chunks = [isbns[x:x + 100] for x in xrange(0, len(isbns), 100)]
        index = 0
        for chunk in chunks:
            try:
                mappings = asin_matcher.match_asins_by_api(chunk)
                rows = set()
                for mapping in mappings:
                    try:
                        rows.add(mapping['asin'])
                    except:
                        pass

                        # asins = [mapping["asin"] for mapping in mappings]
                        # write_to_file(dist_filename, rows)
                asins.update(rows)
                index = index + 1
                found = len(asins)
                print index, found
            except:
                print traceback.format_exc()
        write_to_file('all-asins', asins)
        asins = asins.intersection(asin_set)
        asins = asins - uploaded_set
        print "%s asins found" % len(asins)
        if len(asins) > 0:
            write_to_file(dist_filename, asins)
