# coding=utf-8
import sys
import argparse
import traceback
import random
import time
import os
from multiprocessing import Process, Queue, current_process, Pool

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)
from lib.utils.helper import get_browser, Helper, get_page_content_from_url, html_parser
from lib.models import *
from lib.general import get_base_url

browsers = []


def write_to_file(list_file, data):
    with open(list_file, 'ab') as csvfile:
        for asin in data:
            try:
                if isinstance(asin, basestring):
                    csvfile.write(asin + "\n")
                elif isinstance(asin, dict):
                    row = u','.join([str(asin[k]) for k in asin]).encode('utf-8').strip()
                    csvfile.write(row + "\n")
                else:
                    row = u','.join(asin).encode('utf-8').strip()
                    csvfile.write(row + "\n")
            except:
                pass


def worker(work_queue, done_queue, browser=None, db_table='us_products', update_db=False, country='US'):
    try:
        actual_worker(work_queue, done_queue, browser, db_table, update_db, country=country)
    except:
        print traceback.format_exc()


def actual_worker(work_queue, done_queue, browser=None, db_table='us_products', update_db=False, country='US'):
    try:
        while True:
            if work_queue.empty():
                print "Done"
                break
            asin = work_queue.get()
            if asin == 'STOP':
                break

            try:
                download_offer_by_asin(asin, browser, db_table=db_table, update_db=update_db, country=country)
            except:
                print traceback.format_exc()

                # done_queue.put("%s - %s" % (current_process().name, asin))
    except Exception, e:
        print traceback.format_exc()
        # done_queue.put("%s failed with: %s" % (current_process().name, e.message))

    browser.close()
    return True


def download_offer_by_asin(row, browser=None, db_table='us_products', update_db=False, country='US'):
    if browser is None:
        browser = get_browser(profile=None)

    helper = Helper()

    asin = row['asin']
    id = row['id']
    offer_link = "%s/gp/offer-listing/%s/ref=dp_olp_new_mbc?ie=UTF8&condition=new" % (get_base_url(country), asin)
    html = get_page_content_from_url(offer_link, browser)
    soup = BeautifulSoup(html, html_parser)
    offers = helper.find_tags(soup, "div.a-row.a-spacing-mini.olpOffer")
    sellers = []
    seller = None
    for offer in offers:
        prime = helper.find_tags(offer, "span.supersaver > i.a-icon.a-icon-premium,"
                                       "div.olpBadgeContainer > div.olpBadge > span.a-declarative > a.olpFbaPopoverTrigger,"
                                       "span.supersaver > i.a-icon.a-icon-prime")
        is_prime = 1 if len(prime) > 0 else 0
        seller_name = helper.find_tag(offer, ".olpSellerName").text.strip()
        seller_link = helper.find_tag(offer, ".olpSellerName a")
        if seller_link is not None:
            if seller_name == "":
                seller_name = 'Amazon Warehouse'
        elif seller_name == "":
            seller_name = "Amazon"

        price = helper.find_tag(offer, "span.a-size-large.a-color-price.olpOfferPrice.a-text-bold")
        if price is not None:
            price = float(price.text.replace('$', '').replace(u'£', '').replace(',', '').strip())
        else:
            price = 0

        shipping = helper.find_tag(offer, "span.olpShippingPrice")
        if shipping is not None:
            shipping = float(shipping.text.strip('$').replace(u'£', ''))
        else:
            shipping = 0

        # print seller_name, is_prime
        sellers.append({'name': seller_name, 'prime': is_prime, 'price': price, 'shipping': shipping})
        if is_prime == 1 and seller is None:
            seller = seller_name

    if seller is None and len(sellers) > 0:
        seller = sellers[0]['name']

    print id, asin, seller, current_process().name

    sql = 'UPDATE ' + db_table + ' SET checked = 1, offers = %s, seller = %s WHERE asin = %s'
    dataDBSave.execute_sql(sql, [len(sellers), seller, asin])
    #
    if len(sellers) > 0:
        if update_db is True:
            with dataDBSave.atomic() as transaction:
                for seller in sellers:
                    try:
                        sql = "INSERT INTO us_product_sellers (asin,seller_name,is_prime,shipping,price) VALUES (%s,%s,%s,%s,%s)"
                        dataDBSave.execute_sql(sql, [asin, seller['name'], seller['prime'], seller['shipping'], seller['price']])
                    except:
                        pass
        else:
            if os.path.exists(lib_dir + "data") is False:
                os.mkdir(lib_dir + "data")
            write_to_file(lib_dir + "data/asin-sellers.csv", sellers)
    time.sleep(random.randrange(0, 1))


def check_by_multiple_process(asins, max_worker=3, db_table='us_products', update_db=False, country='US'):
    work_queue = Queue()
    done_queue = Queue()
    processes = []

    for asin in asins:
        work_queue.put(asin)

    work_queue.put('STOP')
    workers = min(max_worker, len(asins))

    for w in xrange(workers):
        # , headless=True
        # browser = browsers[w]
        browser = get_browser(profile=None, headless=True)
        p = Process(target=worker, args=(work_queue, done_queue, browser, db_table, update_db, country))
        p.start()
        processes.append(p)

    for p in processes:
        p.join()
        p.terminate()


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-w', '--worker', type=int, default=5, help='no of worker')
    parser.add_argument('-s', '--size', type=int, default=100, help='page size')
    parser.add_argument('-db', '--db_table', type=str, default='us_products', help='db table name')
    parser.add_argument('-u', '--update_db', type=int, default=0, help='update db table')
    parser.add_argument('-c', '--country', type=str, default='US', help='Source ASIN country')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()
    print args

    db_table = args.db_table
    index_type = '%s_%s' % ('offers', db_table)
    country = args.country.upper()
    # download_offer_by_asin('B0151GSC52')
    count = 0
    worker_no = args.worker
    limit = worker_no * 10

    mid = 0
    update_db = True if args.update_db > 0 else False
    asins = []
    while True:
        with accountDB.atomic() as transaction:
            last_id = get_last_id(index_type, country=country)

            sql = "SELECT asin,id FROM " + db_table + " WHERE id > " + str(last_id)
            page_size = args.size if args.limit == 0 or args.size < args.limit else args.limit
            sql = sql + " order by id asc limit %s" % page_size
            print sql
            cursor = dataDB.execute_sql(sql)
            rows = cursor.fetchall()

            if len(rows) == 0:
                print 'No more'
                break

            min_id = rows[len(rows) - 1][1]
            try:
                save_last_id(index_type, min_id, country=country)
            except:
                break

            pindex = len(rows) + 1
            print "loading ", min_id, ';  '

        asins.extend([{'asin': row[0], 'id': row[1]} for row in rows])
        mid = rows[len(rows) - 1][1]
        count = count + len(rows)

        if len(asins) > worker_no * 10 or 0 < args.limit < count:
            check_by_multiple_process(asins, worker_no, db_table, update_db, country=country)
            asins = []

        if 0 < args.limit < count:
            break

        if args.update_db > 0:
            pass


            # for w in xrange(worker_no):
            #     #, disableImage=True, disableJS=True
            #     browser = getBrowser(profile=None)
            #     browsers.append(browser)
            #     print "browser %s inited" % w
