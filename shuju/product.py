import sys
import argparse
import traceback
import os
import time

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib import mws_product
from lib.models import *
from local import sids
from lib.elastic_product import ElasticProduct
from shuju.utils import get_product_info_mws


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default=None, help='Source ASIN country')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-s', '--size', type=int, default=100, help='page size')
    parser.add_argument('-db', '--db_table', type=str, default='', help='db table name')
    parser.add_argument('-f', '--field', type=str, default='isbn', help='db isbn field')
    parser.add_argument('-u', '--update_db', type=int, default=0, help='update db table')
    parser.add_argument('-ck', '--check_es', type=int, default=1, help='check es server first')
    command_args = parser.parse_args()
    return command_args


elastic = ElasticProduct(host="35.199.3.83", port=80,http_auth=('elasticuser', 'KbersRiseUp153'))

if __name__ == "__main__":
    args = parse_args()
    print args

    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        print "No account info for " + args.account + " found"
        sys.exit()

    if args.country is None or len(args.country) == 0:
        country = accountInfo.country.upper()
    else:
        country = args.country.upper()

    db_table = args.db_table

    productApi = mws_product.getProductApi(accountInfo.seller_id, accountInfo.mws_access_key,
                                           accountInfo.mws_secret_key, country, auth_token=accountInfo.mws_auth_token)
    field = args.field
    index_type = '%s_%s_%s' % ('product_info', db_table, field)

    limit = args.limit
    pindex = 0

    while True:
        with accountDB.atomic() as transaction:
            last_id = get_last_id(index_type, country=country)
            if db_table == 'us_prods':
                if field == 'asin':
                    sql = "SELECT " + field + ",id FROM " + db_table + " WHERE (brand ='' or brand is null) and id > " + str(last_id)
                else:
                    sql = "SELECT " + field + ",id FROM " + db_table + " WHERE (isbn_brand ='' or isbn_brand is null) and id > " + str(
                        last_id)
            else:
                sql = "SELECT " + field + ",id FROM " + db_table + " WHERE `group` is null and id > " + str(last_id)
            page_size = args.size if args.limit == 0 or args.size < args.limit else args.limit
            sql = sql + " order by id asc limit %s" % page_size
            print sql
            cursor = dataDB.execute_sql(sql)
            rows = cursor.fetchall()

            if len(rows) == 0:
                break

            asins = [str(row[0]).zfill(10) for row in rows]
            min_id = rows[len(rows) - 1][1]
            try:
                save_last_id(index_type, min_id, country=country)
            except:
                break

            pindex = len(rows) + 1
            print "loading ", min_id, ';  '
        try:
            if args.check_es > 0:
                existed = elastic.get_data_by_asins(asins, country)
                existed_dict = {k: v for k, v in existed.items() if v['group'] is not None or v['binding'] is not None}
                if args.update_db > 0 and len(existed_dict) > 0:
                    with dataDB.atomic() as transaction:
                        for asin, payload in existed_dict.iteritems():
                            if db_table == 'us_prods':
                                if field == 'asin':
                                    sql = 'update ' + db_table + ' set `group` = %s,`brand` = %s where asin = %s'
                                else:
                                    sql = 'update ' + db_table + ' set `isbn_group` = %s,`isbn_brand` = %s where isbn = %s'
                                group = None

                                if 'group' in payload:
                                    group = payload['group']
                                elif 'binding' in payload:
                                    group = payload['binding']

                                brand = payload['brand'] if 'brand' in payload else None
                                dataDB.execute_sql(sql, [group, brand, asin])
                            else:
                                sql = 'update ' + db_table + ' set `group` = %s where ' + field + ' = %s'
                                group = None

                                if 'group' in payload:
                                    group = payload['group']
                                elif 'binding' in payload:
                                    group = payload['binding']

                                dataDB.execute_sql(sql, [group, asin])
                for asin in existed_dict.keys():
                    asins.remove(asin)

            if len(asins) == 0:
                continue

            product_dicts = get_product_info_mws(asins, productApi, country)
            minId = rows[len(rows) - 1][1]
            for asin, payload in product_dicts.iteritems():
                elastic.bulk_add_data(asin, payload=payload, region="us")

            if args.update_db > 0:
                with dataDB.atomic() as transaction:
                    for asin, payload in product_dicts.iteritems():
                        group = None
                        if 'group' in payload:
                            group = payload['group']
                        elif 'binding' in payload:
                            group = payload['binding']

                        if db_table == 'us_prods':
                            if field == 'asin':
                                sql = 'update ' + db_table + ' set `group` = %s,`brand` = %s where asin = %s'
                            else:
                                sql = 'update ' + db_table + ' set `isbn_group` = %s,`isbn_brand` = %s where isbn = %s'

                            brand = payload['brand'] if 'brand' in payload else None
                            dataDB.execute_sql(sql, [group, brand, asin])
                        elif 'cd' in db_table:
                            sql = 'update ' + db_table + ' set `group` = %s,`format` = %s   where ' + field + ' = %s'
                            dataDB.execute_sql(sql, [group, payload['format'], asin])
                        else:
                            sql = 'update ' + db_table + ' set `group` = %s  where ' + field + ' = %s'
                            dataDB.execute_sql(sql, [group, asin])
        except:
            if "QuotaExceeded" in traceback.format_exc() or 'RequestThrottled' in traceback.format_exc():
                time.sleep(60)
            else:
                minId = rows[len(rows) - 1][1]

            print traceback.format_exc()

        if limit != 0 and pindex >= limit:
            break

    elastic.process_bulk()