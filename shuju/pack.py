import sys
import argparse
import traceback
import os

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)
from lib.elastic_product import ElasticProduct
from lib.models import *


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-c', '--country', type=str, default=None, help='Source ASIN country')

    command_args = parser.parse_args()
    return command_args


elastic = ElasticProduct(host="35.199.3.83", port=80,http_auth=('elasticuser', 'KbersRiseUp153'))


def write_to_file(list_file, data):
    with open(list_file, 'ab') as csvfile:
        for asin in data:
            if isinstance(asin, basestring):
                csvfile.write(asin + "\n")
            else:
                row = u','.join(asin).encode('utf-8').strip()
                csvfile.write(row + "\n")


if __name__ == "__main__":
    args = parse_args()
    print args
    list_file = lib_dir + "data/usprods/us-titles.csv"
    min_id = 0
    while True:
        sql = "SELECT asin,id FROM us_prod_data WHERE id > %s AND isbn_title IS NULL ORDER BY id ASC LIMIT 100"
        cursor = dataDB.execute_sql(sql, [min_id])
        rows = cursor.fetchall()
        if len(rows) == 0:
            break
        asins = [row[0] for row in rows]
        products = elastic.get_data_by_asins(asins, region='us')
        # with dataDB.atomic() as transaction:
        data_rows = []
        for asin in asins:
            if products is not None and asin in products:
                product = products[asin]
                title = product['title']
                brand = product['brand']
                if len(title) > 500:
                    title = title[:500]
                if brand is None:
                    brand = 'None'
                if len(brand) > 100:
                    brand = brand[:100]
            else:
                title = 'None'
                brand = 'None'

            data_rows.append([asin, title, brand])
            # sql = 'UPDATE us_prod_data SET title = %s, brand = %s WHERE asin = %s'
            # dataDB.execute_sql(sql, [title, brand, asin])
            # products = elastic.get_data_by_keyword('pack', country='us', limit=100, size=100)
            # print products
            # break
        write_to_file(list_file, data_rows)
        min_id = rows[len(rows) - 1][1]
        print min_id
