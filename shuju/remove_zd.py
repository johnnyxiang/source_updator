#!/usr/local/bin/python
# coding: utf-8
import traceback
import os
import sys
import csv
import glob

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

if __name__ == "__main__":
    all_asin_files = [f for f in glob.glob(lib_dir + "data/uszd/all/*.csv")]
    uploaded_files = [f for f in glob.glob(lib_dir + "data/uszd/uploaded/*.csv")]
    index = 0
    all_asins = set()
    for csv_file in all_asin_files:
        filename = os.path.basename(csv_file)
        try:
            f = open(csv_file)
            reader = csv.reader(f, delimiter=',', dialect=csv.excel_tab)

            asins = [l[0] for l in reader if len(l[0]) > 0 and not l[0].startswith('B')]
            asin_set = set(asins)

            # intersection = hp_asins.intersection(asin_set)
            # print "%d intersection found " % len(intersection)
            #
            # if intersection is not None:
            #     hp_asins = hp_asins - intersection

            all_asins.update(asin_set)
            print "%d asins found " % len(all_asins)
        except:
            print filename
            print traceback.format_exc()

    uploaded_asins = set()
    for csv_file in uploaded_files:
        f = open(csv_file)
        reader = csv.reader(f, delimiter=',')
        filename = os.path.basename(csv_file)
        asins = [l[0] for l in reader if len(l[0]) > 0 and not l[0].startswith('B')]
        uploaded_asins.update(set(asins))

        print "%d uploaded asins found " % len(uploaded_asins)

    not_upload_asins = all_asins - uploaded_asins
    print "%d not uploaded asins found " % len(not_upload_asins)

    chunk_size = 200000
    unique_asins = list(not_upload_asins)
    chunks = [unique_asins[x:x + chunk_size] for x in xrange(0, len(unique_asins), chunk_size)]
    index = 0
    for chunk in chunks:
        index = index + 1

        d_file = lib_dir + "data/uszd/new-%d.csv" % index
        with open(d_file, 'w') as d_file_writer:
            for asin in chunk:
                d_file_writer.write(asin + "\n")
