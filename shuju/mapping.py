import sys
import os
import traceback

import math

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib.utils.simple_logger import SimpleLogger
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
import csv
from lib.utils.asin_matcher import AsinMatcher

logger = SimpleLogger.get_stream_logger('source_price_updator')
offer_service_price_finder = OfferServicePriceFinder()
asin_matcher = AsinMatcher()


def write_to_file(file_name, data):
    list_file = lib_dir + "data/crawl/asin/%s.txt" % file_name
    with open(list_file, 'ab') as csvfile:
        for asin in data:
            csvfile.write(",".join(asin) + "\n")


if __name__ == "__main__":

    last_id = 0
    size = 100
    found = 0

    files = ['uk-beauty-prime']
    for filename in files:

        file_path = lib_dir + "data/crawl/isbn/%s.csv" % filename
        f = open(file_path)
        reader = csv.reader(f, delimiter=',')

        # sku_map = dict()
        # for l in reader:
        #     sku_map[l[0]] = l[1]

        # isbns = sku_map.keys()
        isbns = [l[0] for l in reader]
        found = 0
        index = 0
        if len(isbns) > 0:
            chunk_size = 1000000.0
            chunk_index = 1
            chunks = [isbns[x:x + 20] for x in xrange(0, len(isbns), 20)]

            for chunk in chunks:
                try:
                    mappings = asin_matcher.match_asins_by_api(chunk)
                    # print asins
                    rows = []
                    for mapping in mappings:
                        try:
                            rows.append([mapping['asin'], mapping['isbn']])
                        except:
                            pass

                    dist_filename = filename + "_asins_%s" % chunk_index
                    write_to_file(dist_filename, rows)
                    index = index + 1
                    found = len(rows) + found

                    chunk_index = int(math.ceil(found / chunk_size))
                    print chunk_index, index, found
                except:
                    print traceback.format_exc()
                    pass
