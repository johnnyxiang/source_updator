import sys
import argparse
import os

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib import mws_product
from lib.models import *
from local import sids


def get_product_info(db_table, asins, product_api, region):
    chunks = [asins[x:x + 10] for x in xrange(0, len(asins), 10)]
    # print chunks
    for chunk in chunks:
        response = mws_product.get_matching_product(product_api, chunk, region=region)
        parsed_resp = response._response_dict

        if "ASIN" in parsed_resp['GetMatchingProductResult']:
            results = [parsed_resp['GetMatchingProductResult']]
        else:
            results = parsed_resp['GetMatchingProductResult']

        with reviewDB.atomic():
            for product_dict in results:

                try:
                    asin = product_dict['ASIN']['value']
                    sql = "UPDATE " + db_table + " SET attr_fetched=%s WHERE asin = %s"
                    reviewDB.execute_sql(sql, [1, asin])
                except:
                    pass

                if "Error" in product_dict:
                    print product_dict['Error']['Message']['value']
                    continue

                try:

                    try:
                        productAttr = ProductAttr.get(ProductAttr.asin == asin)
                    except:
                        productAttr = ProductAttr()
                        productAttr.asin = asin

                    product = product_dict['Product']
                    try:
                        title = product['AttributeSets']['ItemAttributes']['Title']['value']
                    except:
                        title = ""

                    if len(title) > 255:
                        title = title[:255]
                    try:
                        brand = product['AttributeSets']['ItemAttributes']['Brand']['value']
                    except:
                        brand = ""

                    if len(brand) > 100:
                        brand = brand[:100]

                    try:
                        sales_ranking = product['SalesRankings']['SalesRank'][0]['Rank']['value']
                    except:
                        sales_ranking = 0

                    try:
                        productAttr.weight = round(
                            float(product['AttributeSets']['ItemAttributes']['PackageDimensions']['Weight']['value']), 1)
                    except:
                        pass

                    dimension = []
                    try:
                        productAttr.width = round(float(product['AttributeSets']['ItemAttributes']['PackageDimensions']['Width']['value']),
                                                  1)
                        dimension.append(productAttr.width)
                    except:
                        pass
                    try:
                        productAttr.height = round(
                            float(product['AttributeSets']['ItemAttributes']['PackageDimensions']['Height']['value']), 1)
                        dimension.append(productAttr.height)
                    except:
                        pass

                    try:
                        productAttr.length = round(
                            float(product['AttributeSets']['ItemAttributes']['PackageDimensions']['Length']['value']), 1)
                        dimension.append(productAttr.length)
                    except:
                        pass

                    print asin, title, brand, sales_ranking

                    if len(dimension) > 0:
                        dimension = " x ".join([str(d) for d in dimension]) + " inches;"
                    else:
                        dimension = ""

                    if productAttr.weight is not None:
                        dimension = dimension + " " + str(productAttr.weight) + " pounds"

                    productAttr.dimensions = dimension

                    try:
                        productAttr.size = product['AttributeSets']['ItemAttributes']['Size']['value']
                    except:
                        pass

                    try:
                        productAttr.binding = product['AttributeSets']['ItemAttributes']['Binding']['value']
                    except:
                        pass

                    try:
                        productAttr.color = product['AttributeSets']['ItemAttributes']['Color']['value']
                    except:
                        pass

                    try:
                        productAttr.flavor = product['AttributeSets']['ItemAttributes']['Flavor']['value']
                    except:
                        pass

                    try:
                        productAttr.mpn = product['AttributeSets']['ItemAttributes']['PartNumber']['value']
                    except:
                        pass
                    try:
                        productAttr.manufacturer = product['AttributeSets']['ItemAttributes']['Manufacturer']['value']
                    except:
                        pass
                    try:
                        productAttr.model = product['AttributeSets']['ItemAttributes']['Model']['value']
                    except:
                        pass
                    try:
                        productAttr.publisher = product['AttributeSets']['ItemAttributes']['Publisher']['value']
                    except:
                        pass

                    productAttr.save()

                except:
                    print traceback.format_exc()

        time.sleep(1)


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default=None, help='Source ASIN country')
    parser.add_argument('-t', '--type', type=str, default="product", help='')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-o', '--order', type=str, default='asc', help='order')
    parser.add_argument('-s', '--size', type=int, default=100, help='page size')
    parser.add_argument('-m', '--mode', type=str, default="asin", help='running mode')
    parser.add_argument('-f', '--fwd', type=str, default='us', help='us,uk')
    parser.add_argument('-db', '--db_table', type=str, default='', help='db table name')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":

    args = parse_args()

    print args

    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    limit = args.limit
    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        print "No account info for " + args.account + " found"
        sys.exit()

    if args.country is None or len(args.country) == 0:
        country = accountInfo.country.upper()
    else:
        country = args.country.upper()

    minId = 0
    productApi = mws_product.getProductApi(accountInfo.seller_id, accountInfo.mws_access_key,
                                           accountInfo.mws_secret_key, country, auth_token=accountInfo.mws_auth_token)

    if len(args.db_table) > 0:
        db_table = args.db_table
    else:
        db_table = 'products'

    found = 0
    page_size = args.size if args.limit == 0 or args.size < args.limit else args.limit
    index_type = '%s_%s' % (args.type, db_table)
    while True:
        with accountDB.atomic() as transaction:
            last_id = get_last_id(index_type, country=country)
            sql = "SELECT asin,id FROM " + db_table + " WHERE id > %s and attr_fetched = 0 limit %s" % (last_id, page_size)
            print sql
            cursor = reviewDB.execute_sql(sql)
            rows = cursor.fetchall()

            if len(rows) == 0:
                break
            asins = [str(row[0]).zfill(10) for row in rows]
            min_id = rows[len(rows) - 1][1]
            try:
                save_last_id(index_type, min_id, country=country)
            except:
                break

            pindex = len(rows) + 1
            print "loading ", min_id, '; total found ', found

        try:
            get_product_info(db_table, asins, productApi, country)

            minId = rows[len(rows) - 1][1]
        except:
            if "QuotaExceeded" in traceback.format_exc() or 'RequestThrottled' in traceback.format_exc():
                time.sleep(60)
            else:
                minId = rows[len(rows) - 1][1]

            print traceback.format_exc()

        if limit != 0 and pindex >= limit:
            break
