#!/usr/local/bin/python
# coding: utf-8
import argparse
import os

import sys

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib.elastic_product import ElasticProduct
from lib.models import dataDB, reviewDB


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-o', '--offset', type=int, default=0, help='offset')
    parser.add_argument('-s', '--size', type=int, default=100, help='page size')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    elastic = ElasticProduct(host="35.199.3.83", port=80, http_auth=('elasticuser', 'KbersRiseUp153'))
    args = parse_args()

    print args

    country = args.country.upper()

    minId = 0

    while True:
        sql = "SELECT asin,id,name,brand,sales_rank,binding FROM products WHERE name is not null and name <> '' and id > " + str(
                minId)

        sql = sql + " order by id asc limit " + str(
            args.size if args.limit == 0 or args.size < args.limit else args.limit)

        print minId, sql

        cursor = reviewDB.execute_sql(sql)
        rows = cursor.fetchall()
        if len(rows) == 0:
            break

        for row in rows:
            try:
                asin = row[0]
                title = row[2]
                if row[3] is not None:
                    title = title + " by " + row[3]
                payload = {
                    'asin': asin,
                    'title': title,
                    'brand': row[3],
                    'rank': row[4],
                    'binding': row[5]
                }
                minId = row[1]
                elastic.bulk_add_data(asin, payload=payload, region=args.country)
            except:
                print row
                exit()
