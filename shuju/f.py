#!/usr/local/bin/python
# coding: utf-8
import traceback

import gzip

from lib.elastic_product import ElasticProduct
import csv

from lib.models import omsDB


def parse(path):
    g = gzip.open(path, 'r')
    for l in g:
        yield eval(l)


if __name__ == "__main__":

    with open("data/shipping_fee.csv") as f:
        reader = csv.reader(f, delimiter=',')
        i = 0
        for line in reader:
            try:
                i = i + 1
                region = line[0]
                amazon_order_id = line[1]
                price = float(line[2])
                if len(amazon_order_id) == 0:
                    continue
                sql = "INSERT INTO export_shipping_costs(region,amazon_order_id,price) VALUES (%s,%s,%s) " \
                      "ON DUPLICATE KEY UPDATE price=VALUES(price);"
                omsDB.execute_sql(sql, [region, amazon_order_id, price])
                print i, amazon_order_id, price
            except:
                print line
                print traceback.format_exc()
                break
