import os
import sys
import traceback
import time
import datetime
import subprocess

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib import mws_product
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
from lib.listing import eu
from lib.models import accountDB, dataDB
from lib.elastic_product import ElasticProduct
from config import *

offer_service_price_finder = OfferServicePriceFinder()
elastic = ElasticProduct(host="35.199.3.83", port=80, http_auth=('elasticuser', 'KbersRiseUp153'))


def get_prices(dbModel, db_table, isbns, country, product_api, condition="new", product_type="product"):
    rows = []
    chunks = [isbns[x:x + 20] for x in xrange(0, len(isbns), 20)]
    offers_info = {}
    for chunk in chunks:
        if country.lower() == 'us':
            chunk_offer_info = mws_product.getPriceForAsinsFromMws(product_api, chunk, condition, country,
                                                                   avg=False, product_type=product_type)
        else:
            chunk_offer_info = mws_product.getPriceForAsinsFromMws(product_api, chunk, condition, country,
                                                                   ships_domestically=None,
                                                                   max_shipping_days=100, avg=False, product_type=product_type)
        offers_info.update(chunk_offer_info[condition])

    for asin in isbns:
        try:
            if asin not in offers_info:
                print asin, 'no offer info'
                rows.append([asin, -1, 0])
                continue

            offer = offers_info[asin]
            total_offer = offer['total_offers'] if 'total_offers' in offer else 0
            if 'product_price' not in offers_info[asin]:
                print asin, 'no offer info'
                rows.append([asin, -1, total_offer])
                continue

            # sometimes, event query with new condition, used condition offers returned
            if condition.lower == "new" and offer['item_condition'] != 'new':
                print asin, 'no offer info'
                rows.append([asin, -1, total_offer])
                continue

            if 'product_price' in offer:
                print asin, round(offer['product_price'] + offer['shipping_price'], 2)
                rows.append([asin, round(offer['product_price'] + offer['shipping_price'], 2), total_offer])
            else:
                print asin, 'no offer info'
                rows.append([asin, -1, total_offer])
        except:
            print asin, 'no offer info'
            rows.append([asin, -1, 0])

    with dbModel.atomic():
        for row in rows:
            if country.lower() == "us":
                update_sql = " UPDATE " + db_table + " SET us_price =%s, us_offers=%s WHERE isbn = %s"
            else:
                update_sql = " UPDATE  " + db_table + "  SET uk_price =%s, uk_offers=%s WHERE isbn = %s"

            dbModel.execute_sql(update_sql, [row[1], row[2], row[0]])


def get_price_info(dbModel, db_table, asins, country, product_api, condition="new"):
    rows = []
    chunks = [asins[x:x + 20] for x in xrange(0, len(asins), 20)]
    offers_info = {}
    for chunk in chunks:
        chunk_price_info = mws_product.get_competitive_pricing_for_asins(product_api, chunk, country=country, condition=condition)
        offers_info.update(chunk_price_info)

    for asin in asins:
        try:
            if asin not in offers_info:
                print asin, 'no price info'
                rows.append([asin, -1, 0])
                continue
            offer = offers_info[asin]
            count = offer['count']
            lowest = offer['lowest_price']
            rows.append([asin, lowest, count])
            print [asin, lowest, count]
        except:
            rows.append([asin, -1, 0])
            pass

    with dbModel.atomic():
        for row in rows:
            if country.lower() == "us":
                update_sql = " UPDATE " + db_table + " SET us_price_lowest =%s, us_offers=%s WHERE isbn = %s"
            else:
                update_sql = " UPDATE  " + db_table + "  SET uk_price_lowest =%s, uk_offers=%s WHERE isbn = %s"

            dbModel.execute_sql(update_sql, [row[1], row[2], row[0]])


def get_prices_form_es(dbModel, db_table, isbns, country='us', condition='new'):
    earliest_date = (datetime.datetime.utcnow() - datetime.timedelta(hours=48))
    try:
        rows = []
        offers_info = offer_service_price_finder.find_offer_for_asins(isbns, country, condition)
        if offers_info is not None:
            for asin in isbns:
                try:
                    if asin not in offers_info or offers_info[asin] is None or 'product_price' not in offers_info[asin]:
                        continue

                    offer = offers_info[asin]
                    # sometimes, event query with new condition, used condition offers returned
                    if condition.lower() == "new" and offer['item_condition'] != 'new':
                        continue

                    if 'product_price' in offer:
                        offer_time_str = offer['time'][:19]
                        offer_time = datetime.datetime.strptime(offer_time_str, '%Y-%m-%dT%H:%M:%S')
                        if offer_time > earliest_date:
                            rows.append(
                                [asin, round(offer['product_price'] + offer['shipping_price'], 2), offer['offers']])
                            print asin, round(offer['product_price'] + offer['shipping_price'], 2)
                    else:
                        pass
                except:
                    print traceback.format_exc()

            if len(rows) > 0:
                with dbModel.atomic():
                    for row in rows:
                        if country.lower() == "us":
                            update_sql = " UPDATE " + db_table + " SET us_price = %s, us_offers=%s WHERE isbn = %s"
                        else:
                            update_sql = " UPDATE " + db_table + " SET  uk_price = %s, uk_offers=%s  WHERE isbn = %s"
                        dbModel.execute_sql(update_sql, [row[1], row[2], row[0]])

    except:
        print traceback.format_exc()


def get_product_info_from_es(dbModel, db_table, asins, region):
    data = elastic.get_data(asins, region)
    with dbModel.atomic() as transaction:
        for row in data['hits']['hits']:
            asin = row['_source']['asin']
            if 'title' not in row['_source'] or row['_source']['title'] is None:
                continue

            title = row['_source']['title']
            brand = row['_source']['brand'] if 'brand' in row['_source'] and row['_source']['brand'] is not None else ""
            brand = brand.replace('%', '').replace('"', "")[0:100]

            if region.lower() == "us":
                sql = "UPDATE " + db_table + " SET us_brand = %s,us_title=%s WHERE isbn = %s"
                dbModel.execute_sql(sql, [brand, title, asin])
            else:
                sql = "UPDATE " + db_table + " SET uk_brand= %s,uk_title= %s WHERE isbn = %s"
                dbModel.execute_sql(sql, [brand, title, asin])


def get_attributes(attrs_from_mws):
    attributes = {}
    for k, v in attrs_from_mws.iteritems():
        try:
            if 'value' in v:
                attributes[k] = v['value']
            else:
                if isinstance(v, list):
                    items = []
                    for item in v:
                        items.append(get_attributes(item))
                    attributes[k] = items
                else:
                    attributes[k] = get_attributes(v)
        except:
            attributes[k] = v

    return attributes


def get_product_info_mws(asins, product_api, region):
    chunks = [asins[x:x + 10] for x in xrange(0, len(asins), 10)]
    # print chunks

    product_dicts = {}
    for chunk in chunks:
        response = mws_product.get_matching_product(product_api, chunk, region=region)
        parsed_resp = response._response_dict

        if "ASIN" in parsed_resp['GetMatchingProductResult']:
            results = [parsed_resp['GetMatchingProductResult']]
        else:
            results = parsed_resp['GetMatchingProductResult']

        for product_dict in results:
            if "Error" in product_dict:
                print product_dict['Error']['Message']['value']
                continue

            asin = product_dict['ASIN']['value']
            try:
                product = product_dict['Product']
                # print product
                try:
                    title = product['AttributeSets']['ItemAttributes']['Title']['value']
                except:
                    title = ""

                if len(title) > 255:
                    title = title[:255]
                try:
                    brand = product['AttributeSets']['ItemAttributes']['Brand']['value']
                except:
                    brand = ""

                if len(brand) > 100:
                    brand = brand[:100]


                try:
                    format = product['AttributeSets']['ItemAttributes']['Format']['value']
                except:
                    format = ""


                try:
                    sales_ranking = int(product['SalesRankings']['SalesRank'][0]['Rank']['value'])
                except:
                    sales_ranking = 0

                try:
                    weight = product['AttributeSets']['ItemAttributes']['PackageDimensions']['Weight']['value']
                except:
                    weight = 0

                try:
                    height = product['AttributeSets']['ItemAttributes']['PackageDimensions']['Height'][
                        'value']
                except:
                    height = 0

                try:
                    length = product['AttributeSets']['ItemAttributes']['PackageDimensions']['Length'][
                        'value']
                except:
                    length = 0

                try:
                    width = product['AttributeSets']['ItemAttributes']['PackageDimensions']['Width'][
                        'value']
                except:
                    width = 0

                lwh = int(float(height) + float(length) + float(width))
                print asin, title, brand, sales_ranking, format

                attributes = product['AttributeSets']['ItemAttributes']
                # attributes = get_attributes(product['AttributeSets']['ItemAttributes'])
                # for k, v in product['AttributeSets']['ItemAttributes'].iteritems():
                #     try:
                #         if 'value' in v:
                #             attributes[k] = v['value']
                #         else:
                #             attributes[k] = v
                #     except:
                #         attributes[k] = value
                # print attributes
                ranks = []
                if 'SalesRankings' in product and 'SalesRank' in product['SalesRankings']:
                    if 'Rank' in product['SalesRankings']['SalesRank']:
                        ranks.append({'category': product['SalesRankings']['SalesRank']['ProductCategoryId']['value'],
                                      'rank': product['SalesRankings']['SalesRank']['Rank']['value']})
                    else:
                        for rank in product['SalesRankings']['SalesRank']:
                            try:
                                ranks.append({'category': rank['ProductCategoryId']['value'], 'rank': rank['Rank']['value']})
                            except:
                                pass

                product_dicts[asin] = {
                    'asin': asin,
                    'title': title,
                    'brand': brand,
                    'format': format,
                    'sales_rank': sales_ranking,
                    # 'weight': weight,
                    'lwh': lwh,
                    'attributes': attributes,
                    'sales_ranks': ranks
                }

                if 'Binding' in attributes:
                    product_dicts[asin]['binding'] = attributes['Binding']['value']

                if 'ProductGroup' in attributes:
                    product_dicts[asin]['ProductGroup'] = attributes['ProductGroup']['value']
                    # print product_dicts
                    # AttributeSets
                    # SalesRankings
            except:
                print traceback.format_exc()

        time.sleep(1)
    return product_dicts


def get_product_info(dbModel, db_table, asins, product_api, region):
    chunks = [asins[x:x + 10] for x in xrange(0, len(asins), 10)]
    # print chunks
    for chunk in chunks:
        response = mws_product.get_matching_product(product_api, chunk, region=region)
        parsed_resp = response._response_dict

        if "ASIN" in parsed_resp['GetMatchingProductResult']:
            results = [parsed_resp['GetMatchingProductResult']]
        else:
            results = parsed_resp['GetMatchingProductResult']

        with dbModel.atomic():
            for product_dict in results:
                if "Error" in product_dict:
                    print product_dict['Error']['Message']['value']

                    try:
                        asin = product_dict['ASIN']['value']
                        if region.lower() == "us":
                            sql = "UPDATE " + db_table + " SET us_brand = %s,us_title=%s WHERE isbn = %s"
                            dbModel.execute_sql(sql, ["None", "None", asin])
                        else:
                            sql = "UPDATE " + db_table + " SET uk_brand= %s,uk_title= %s WHERE isbn = %s"
                            dbModel.execute_sql(sql, ["None", "None", asin])
                    except:
                        pass

                    continue

                asin = product_dict['ASIN']['value']
                try:
                    product = product_dict['Product']
                    try:
                        title = product['AttributeSets']['ItemAttributes']['Title']['value']
                    except:
                        title = ""

                    if len(title) > 255:
                        title = title[:255]
                    try:
                        brand = product['AttributeSets']['ItemAttributes']['Brand']['value']
                    except:
                        brand = ""

                    if len(brand) > 100:
                        brand = brand[:100]

                    try:
                        sales_ranking = product['SalesRankings']['SalesRank'][0]['Rank']['value']
                    except:
                        sales_ranking = 0

                    try:
                        weight = product['AttributeSets']['ItemAttributes']['PackageDimensions']['Weight'][
                            'value']
                    except:
                        weight = 0

                    try:
                        height = product['AttributeSets']['ItemAttributes']['PackageDimensions']['Height'][
                            'value']
                    except:
                        height = 0

                    try:
                        length = product['AttributeSets']['ItemAttributes']['PackageDimensions']['Length'][
                            'value']
                    except:
                        length = 0

                    try:
                        width = product['AttributeSets']['ItemAttributes']['PackageDimensions']['Width'][
                            'value']
                    except:
                        width = 0

                    lwh = int(float(height) + float(length) + float(width))
                    print asin, title, brand, sales_ranking

                    if region.lower() == "us":
                        sql = "UPDATE " + db_table + " SET us_brand = %s,us_title=%s,us_weight= %s,us_lwh= %s WHERE isbn = %s"
                        dbModel.execute_sql(sql, [brand, title, weight, lwh, asin])
                    else:
                        sql = "UPDATE " + db_table + " SET uk_brand= %s,uk_title= %s,uk_weight= %s,uk_lwh= %s WHERE isbn = %s"
                        dbModel.execute_sql(sql, [brand, title, weight, lwh, asin])
                except:
                    print traceback.format_exc()

        time.sleep(1)


def clear_db_by_ingredients(region, database_connection, db_table):
    region = region.lower()
    database_name = "restricted_ingredients_"
    if region in eu:
        database_name = database_name + 'eu'
    else:
        database_name = database_name + region

    sql = "select name from " + database_name
    cursor = accountDB.execute_sql(sql)
    rows = cursor.fetchall()

    for row in rows:
        restricted_ingredient = row[0]
        sql = "update " + db_table + " set status = -10 where status>-10 and MATCH (us_title,uk_title) AGAINST (%s IN BOOLEAN MODE)"
        restricted_ingredient = restricted_ingredient.replace("-", " ").replace("(", " ").replace(")", " ")
        restricted_ingredient = " ".join(
            ['+' + p.replace("'", '').replace('"', '') for p in restricted_ingredient.split(" ") if len(p) > 0])

        print restricted_ingredient, sql
        try:
            database_connection.execute_sql(sql, [restricted_ingredient])
        except:
            print traceback.format_exc()


def clear_db_by_brands(region, database_connection, db_table):
    region = region.lower()
    database_name = "blacklist_brands"
    if region in eu:
        database_name = database_name + '_uk'
    elif region != 'us':
        database_name = database_name + region
    sql = "select brand from " + database_name
    cursor = accountDB.execute_sql(sql)
    rows = cursor.fetchall()

    for row in rows:
        brand = row[0]
        clear_by_brand(brand, database_connection, db_table)

    return

    if region != 'us':
        sql = "SELECT brand FROM blacklist_brands"
        cursor = accountDB.execute_sql(sql)
        rows = cursor.fetchall()
        for row in rows:
            brand = row[0]
            clear_by_brand(brand, database_connection, db_table)

    min_id = 0
    while True:
        sql = "select isbn,id from " + db_table + " where status = -10 and id > %s limit 100"
        cursor = database_connection.execute_sql(sql, [min_id])
        rows = cursor.fetchall()
        if len(rows) == 0:
            break
        with accountDB.atomic() as transaction:
            for row in rows:
                clean_asin(row[0], region)
                min_id = row[1]
            print min_id


def clear_by_brand(brand, database_connection, db_table):
    try:
        sql = "update " + db_table + " set status = -10 where (us_brand = %s or uk_brand = %s)"
        print brand + ": ", sql
        database_connection.execute_sql(sql, [brand, brand])
    except:
        print traceback.format_exc()
        pass


def clear_blacklist(region, database_connection, db_table):
    sync_blacklist_db(region)
    sync_delete_asins(region)

    blacllist_db_name = "blacklist_"
    if region in ['uk', 'de', 'fr', 'es', 'it', 'ca', 'au']:
        blacllist_db_name = blacllist_db_name + 'uk'
    else:
        blacllist_db_name = blacllist_db_name + region

    sql = "update " + db_table + " i join " + blacllist_db_name + " b on b.asin = i.isbn " \
                                                                  "set status = -5 where status > -5 and b.asin is not null"
    try:
        print sql
        database_connection.execute_sql(sql)
    except:
        print traceback.format_exc()

    deleted_db_table = "deleted_asins"
    if region in ['uk', 'de', 'fr', 'es', 'it', 'ca', 'au']:
        deleted_db_table = deleted_db_table + '_uk'
    elif region != 'us':
        deleted_db_table = deleted_db_table + "_" + region

    sql = "update " + db_table + " i join " + deleted_db_table + " b on b.asin = i.isbn " \
                                                                 "set status = -5 where status > -5 and b.asin is not null"
    try:
        print sql
        database_connection.execute_sql(sql)
    except:
        print traceback.format_exc()


def sync_blacklist_db(region):
    database_name = "data"
    region = region.lower()
    db_table = "blacklist_"
    if region in ['uk', 'de', 'fr', 'es', 'it', 'ca', 'au']:
        db_table = db_table + 'uk'
    else:
        db_table = db_table + region

    sql = "CREATE TABLE `" + db_table + "` (" \
                                        "`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT," \
                                        "`asin` VARCHAR(10) DEFAULT NULL," \
                                        "PRIMARY KEY (`id`)," \
                                        "UNIQUE KEY `asin` (`asin`)" \
                                        ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"
    try:
        dataDB.execute_sql(sql)
    except:
        if "exists" not in traceback.format_exc():
            print traceback.format_exc()

    sql = "mysqldump --opt --user=" + databaseUser + " --password=" + databasePW + " -h" + databaseHost + " " + \
          accountsDBName + " " + db_table + " | mysql -u " + databaseUser + " -p" + databasePW + " -h" + databaseHost + " " + database_name
    subprocess.call(sql, shell=True)

    # time.sleep(60)
    print sql


def sync_delete_asins(region):
    database_name = "data"
    region = region.lower()
    db_table = "deleted_asins"
    if region in ['uk', 'de', 'fr', 'es', 'it', 'ca', 'au']:
        db_table = db_table + '_uk'
    elif region != 'us':
        db_table = db_table + "_" + region

    sql = "CREATE TABLE `" + db_table + "` (" \
                                        "`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT," \
                                        "`asin` VARCHAR(10) DEFAULT NULL," \
                                        "PRIMARY KEY (`id`)," \
                                        "UNIQUE KEY `asin` (`asin`)" \
                                        ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"
    try:
        dataDB.execute_sql(sql)
    except:
        if "exists" not in traceback.format_exc():
            print traceback.format_exc()

    sql = "mysqldump --opt --user=" + databaseUser + " --password=" + databasePW + " -h" + databaseHost + " " + \
          accountsDBName + " " + db_table + " | mysql -u " + databaseUser + " -p" + databasePW + " -h" + databaseHost + " " + database_name
    subprocess.call(sql, shell=True)

    # time.sleep(60)
    print sql


def clean_asin(asin, region):
    region_key = region.lower() if region.lower() not in eu else 'uk'
    try:
        db_table = "deleted_asins_%s" % region_key
        sql = "INSERT IGNORE INTO " + db_table + " (asin) value ('" + asin + "')"
        print sql
        accountDB.execute_sql(sql)
    except:
        pass

    try:
        db_table = "blacklist_%s" % region_key
        sql = "insert ignore into " + db_table + " (asin) values ('" + asin + "')"
        accountDB.execute_sql(sql)
    except:
        pass
