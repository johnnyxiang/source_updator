import glob
import sys
import os
import traceback

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib.utils.simple_logger import SimpleLogger
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
import csv
from lib.utils.asin_matcher import AsinMatcher

logger = SimpleLogger.get_stream_logger('source_price_updator')
offer_service_price_finder = OfferServicePriceFinder()
asin_matcher = AsinMatcher()


def write_to_file(file_name, data):
    list_file = lib_dir + "data/%s.txt" % file_name
    with open(list_file, 'ab') as csvfile:
        for asin in data:
            if isinstance(asin, basestring):
                csvfile.write(asin + "\n")
            else:
                csvfile.write(",".join(asin) + "\n")


if __name__ == "__main__":

    last_id = 0
    size = 100
    found = 0

    files = ['701us/701us-jia-pro']
    for filename in files:
        dist_filename = filename + "_isbn"

        file_path = lib_dir + "data/%s.csv" % filename
        f = open(file_path)
        reader = csv.reader(f, delimiter=',')

        asins = [l for l in reader]
        new_rows = []
        found = 0
        index = 0
        if len(asins) > 0:
            chunks = [asins[x:x + 500] for x in xrange(0, len(asins), 500)]

            for chunk in chunks:
                try:
                    asins = [row[0] for row in chunk]

                    mappings = asin_matcher.match_by_api_v2(asins)
                    # print asins
                    rows = []
                    map = {}
                    for mapping in mappings:
                        try:
                            map[mapping['asin']] = mapping['isbn']
                        except:
                            pass
                    for row in chunk:
                        row.append(map[row[0]] if row[0] in map else "")
                        new_rows.append(row)
                    index = index + 1
                    found = len(new_rows)
                    print index, found
                except:
                    print traceback.format_exc()
                    pass

            if len(new_rows) > 0:
                write_to_file(dist_filename, new_rows)
