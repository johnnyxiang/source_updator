import argparse
import traceback

from lib.utils.simple_logger import SimpleLogger
from lib import mws_product
from lib.models import *
from local import sids
import sys
import os

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

logger = SimpleLogger.get_stream_logger('source_price_updator')


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default=None, help='Source ASIN country')
    parser.add_argument('-t', '--type', type=str, default="product", help='')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-o', '--offset', type=int, default=0, help='offset')
    parser.add_argument('-s', '--size', type=int, default=100, help='page size')
    parser.add_argument('-m', '--mode', type=str, default="asin", help='running mode')
    parser.add_argument('-f', '--min_id', type=int, default=0, help='min id')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":

    args = parse_args()

    print args

    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    database_name = args.account
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    offset = args.offset
    limit = args.limit
    pindex = 0

    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        logger.error("No account info for " + args.account + " found")
        sys.exit()

    if args.country is None or len(args.country) == 0:
        country = accountInfo.country.upper()
    else:
        country = args.country.upper()

    minId = args.min_id

    productApi = mws_product.getProductApi(accountInfo.seller_id, accountInfo.mws_access_key,
                                           accountInfo.mws_secret_key, country)

    found = 0
    while True:
        sql = "SELECT isbn,id FROM uk_asins_crawl WHERE id > " + str(minId)

        if country.lower() == "uk":
            price_field = 'uk_price'
            offer_field = 'uk_offers'
            title_field = 'uk_title'
        else:
            price_field = 'us_price'
            offer_field = 'us_offers'
            title_field = 'us_title'
        if args.type == "price":
            sql = sql + " and  " + price_field + "= 0"
        else:
            sql = sql + " and  " + title_field + " is null"

        sql = sql + " order by id asc limit " + str(
            args.size if args.limit == 0 or args.size < args.limit else args.limit)

        print sql

        cursor = accountDB.execute_sql(sql)
        rows = cursor.fetchall()

        if len(rows) == 0:
            break

        ItemIds = []
        for row in rows:
            asin = str(row[0]).zfill(10)
            minId = row[1]
            ItemIds.append({'id': minId, 'asin': asin})
            pindex = pindex + 1

        print "loading ", minId, '; total found ', found

        if len(ItemIds) > 0:
            try:
                asins = [item["asin"] for item in ItemIds]
                isbns = list(set(asins))

                if len(isbns) > 0:
                    try:
                        if args.type == 'product':
                            get_product_info(asins, mws_product, country)
                        else:
                            get_prices(asins, country, mws_product)

                    except:
                        print traceback.format_exc()

            except Exception as e:
                print traceback.format_exc()

        offset = offset + len(ItemIds)

        if limit != 0 and pindex >= limit:
            break
