import csv

import datetime

from lib.elastic_mapping import ElasticMapping
from lib.general import APP_ROOT_DIR

if __name__ == "__main__":
    # asin_isbn.txt
    es_obj = ElasticMapping(host='35.199.3.83', port=80)
    mapping_file = APP_ROOT_DIR + "/data/asin_isbn.txt"
    with open(mapping_file) as f:
        reader = csv.reader(f, delimiter='\t')
        mapping = {}
        index = 0
        found = 0
        for line in reader:
            index += 1
            mapping[line[0]] = line[1]
            if len(mapping) >= 50:
                to_add = es_obj.remove_existed(mapping.keys())
                found = found + len(to_add)
                print index, found
                if len(to_add) > 0:
                    for asin in to_add:
                        payload = {
                            'asin': asin,
                            'isbn': mapping[asin],
                            'filename': 'liu can',
                            'timestamp': datetime.datetime.utcnow()
                        }

                        es_obj.bulk_add_data(asin, payload=payload)

                mapping = {}

        es_obj.process_bulk()
