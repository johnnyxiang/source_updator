import sys
import argparse
import os
import datetime

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
from lib import mws_product
from lib.models import *
from local import sids

offer_service_price_finder = OfferServicePriceFinder()


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default=None, help='Source ASIN country')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-s', '--size', type=int, default=100, help='page size')
    parser.add_argument('-db', '--db_table', type=str, default='products', help='db table name')

    command_args = parser.parse_args()
    return command_args


def check_by_mws(isbns, productApi, country, db_table, ptype='p'):
    try:
        chunks = [isbns[x:x + 20] for x in xrange(0, len(isbns), 20)]
        current_time = time.time()
        offers_info = {}
        condition = 'new'
        for chunk in chunks:
            if len(chunk) > 0:
                try:
                    chunk_offer_info = mws_product.getPriceForAsinsFromMws(productApi, chunk, condition, country, product_type=ptype)
                    # print chunk_offer_info
                    for key, value in chunk_offer_info[condition].iteritems():
                        offers_info[key] = value
                    time.sleep(1)
                except:
                    print traceback.format_exc()
                    if "AccessDenied" in traceback.format_exc():
                        print "AccessDenied"
                        time.sleep(60 * 60 * 2)
                        sys.exit()
                        # offers_info.update(chunk_offer_info)

        print "fetch price for %d asins, took %s secs" % (len(isbns), time.time() - current_time)
        # print offers_info
        rows = []
        for asin in isbns:
            now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
            try:
                if asin not in offers_info or 'product_price' not in offers_info[asin]:
                    offer = {}
                    offer["product_code"] = "p" if ptype == "product" else "c"
                    offer["country_code"] = country
                    offer['time'] = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
                    offer['asin'] = asin
                    offer['offers'] = 0
                    offer['has_offer'] = 'n'

                    print asin, 'no offer info'
                    rows.append([asin, 0, now])
                    continue

                offer = offers_info[asin]
                # print offer

                if 'product_price' in offer:
                    # print pid, asin,offer['product_price'],offer['shipping_price'],offer['time']
                    offer["product_code"] = "p" if ptype == "product" else "c"
                    offer["country_code"] = country
                    offer['time'] = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
                    offer['asin'] = asin
                    # found = found + 1

                    print asin, round(offer['product_price'] + offer['shipping_price'], 2)

                    rows.append([asin, round(offer['product_price'] + offer['shipping_price'], 2), now])
                else:
                    print asin, 'no offer info'
                    rows.append([asin, 0, now])
            except Exception as e:
                print asin, 'no offer info'
                rows.append([asin, 0, now])
                print traceback.format_exc()

        current_time = time.time()

        with accountDBSave.atomic():
            for row in rows:
                update_sql = " update " + db_table + " set com_price = %s, com_price_last_checked=%s where asin = %s"
                shopifyDB.execute_sql(update_sql, [row[1], row[2], row[0]])
        print "saving price for %d asins, took %s secs" % (len(rows), time.time() - current_time)



    except:
        print traceback.format_exc()


if __name__ == "__main__":

    args = parse_args()
    print args

    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        print "No account info for " + args.account + " found"
        sys.exit()

    if args.country is None or len(args.country) == 0:
        country = accountInfo.country.upper()
    else:
        country = args.country.upper()

    db_table = args.db_table

    productApi = mws_product.getProductApi(accountInfo.seller_id, accountInfo.mws_access_key,
                                           accountInfo.mws_secret_key, country, auth_token=accountInfo.mws_auth_token)

    index_type = '%s_%s' % ('product_price', db_table)

    limit = args.limit
    pindex = 0
    page_size = args.size if args.limit == 0 or args.size < args.limit else args.limit
    while True:
        with accountDB.atomic() as transaction:
            last_id = get_last_id(index_type, country=country)
            sql = "SELECT asin,id FROM %s WHERE asin IS not NULL AND id > %s order by id asc limit %s" % (db_table, last_id, page_size)
            print sql
            cursor = shopifyDB.execute_sql(sql)
            rows = cursor.fetchall()

            if len(rows) == 0:
                break

            asins = [str(row[0]).zfill(10) for row in rows]

            min_id = rows[len(rows) - 1][1]
            try:
                save_last_id(index_type, min_id, country=country)
            except:
                break

            pindex = len(rows) + 1
            print "loading ", min_id, ';  '

        try:
            check_by_mws(asins, productApi, country, db_table)
            minId = rows[len(rows) - 1][1]
        except:
            if "QuotaExceeded" in traceback.format_exc() or 'RequestThrottled' in traceback.format_exc():
                time.sleep(60)
            else:
                minId = rows[len(rows) - 1][1]

            print traceback.format_exc()

        if limit != 0 and pindex >= limit:
            break
