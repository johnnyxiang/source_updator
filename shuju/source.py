import glob
import sys
import os
import traceback

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib.utils.simple_logger import SimpleLogger
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
import csv
from lib.utils.asin_matcher import AsinMatcher

logger = SimpleLogger.get_stream_logger('source_price_updator')
offer_service_price_finder = OfferServicePriceFinder()
asin_matcher = AsinMatcher(host='35.199.3.83', port=80, user=None, password=None)


def write_to_file(file_name, data):
    list_file = lib_dir + "data/us/%s.txt" % file_name
    with open(list_file, 'ab') as csvfile:
        for asin in data:
            if isinstance(asin, basestring):
                csvfile.write(asin + "\n")
            else:
                csvfile.write(",".join(asin) + "\n")


if __name__ == "__main__":

    last_id = 0
    size = 100
    found = 0

    # files = [f for f in glob.glob(lib_dir + "data/usbk-uploaded/new/sf/*.txt")]
    files = [lib_dir + "data/us/724us-problem.csv"]
    dist_filename = "724us-problem-mapping1"
    for file_path in files:
        filename = os.path.basename(file_path)

        f = open(file_path)
        reader = csv.reader(f, delimiter=',')
        asins = list(set([l[1] for l in reader]))
        found = 0
        index = 0
        if len(asins) > 0:
            chunks = [asins[x:x + 100] for x in xrange(0, len(asins), 100)]
            for chunk in chunks:
                isbns = list()
                try:
                    mappings = asin_matcher.match_by_api_v2(chunk)
                    # print asins
                    rows = []
                    for mapping in mappings:
                        try:
                            if mapping['isbn'].startswith("B"):
                                isbns.append([mapping['asin'], mapping['isbn']])
                        except:
                            print traceback.format_exc()
                            pass
                    index = index + 1
                    found = len(rows) + found
                    print '%d/%d' % (index, len(chunks))
                except:
                    print traceback.format_exc()
                    pass

                if len(isbns) > 0:
                    write_to_file(dist_filename, isbns)
