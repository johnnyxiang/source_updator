import csv
import re
import sys
import argparse
import traceback
import os

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)
from lib.elastic_product import ElasticProduct
from lib.models import *


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-c', '--country', type=str, default=None, help='Source ASIN country')

    command_args = parser.parse_args()
    return command_args


def write_to_file(list_file, data):
    with open(list_file, 'ab') as csvfile:
        for asin in data:
            if isinstance(asin, basestring):
                csvfile.write(asin + "\n")
            else:
                try:
                    row = u','.join(asin).encode('utf-8').strip()
                    csvfile.write(row + "\n")
                except:
                    pass


if __name__ == "__main__":
    args = parse_args()
    print args
    isbn_title_file = lib_dir + "data/usprods/us-isbn-titles.csv"
    asin_title_file = lib_dir + "data/usprods/us-titles.csv"
    mapping_file = lib_dir + "data/usprods/mapping.csv"
    problem_file = lib_dir + "data/usprods/problem.csv"
    pack_keywords = ['pack', 'pk', '2x', '3x', '4x', '5x', '6x', '8x', '10x', '12x']
    mapping = dict()
    with open(mapping_file) as f:
        reader = csv.reader(f, delimiter=',')
        for line in reader:
            mapping[line[0]] = line[1]

    isbn_titles = dict()
    with open(isbn_title_file) as f:
        reader = csv.reader(f, delimiter=',')
        for line in reader:
            if len(line) > 1:
                isbn_titles[line[0]] = line[1]

    pack_asins = []
    with open(asin_title_file) as f:
        reader = csv.reader(f, delimiter=',')
        index = 0
        for line in reader:
            if len(line) < 2:
                continue

            index += 1
            regex = re.compile('[\W_]+', re.UNICODE)
            for keyword in pack_keywords:
                if " " + keyword in line[1] or keyword + " " in line[1]:

                    asin = line[0]
                    isbn = mapping[asin] if asin in mapping else None
                    if isbn is not None and isbn in isbn_titles:
                        isbn_title = isbn_titles[isbn]
                        asin_title = line[1].lower()
                        asin_title_without_brand = asin_title_with_brand = asin_title
                        if line[2] is not None:
                            asin_title_with_brand = asin_title + " by " + line[2].lower()
                            asin_title_without_brand = asin_title.replace(" by " + line[2].lower(), "")

                        isbn_title = isbn_title.lower()
                        isbn_title = regex.sub('', isbn_title)
                        asin_title = regex.sub('', asin_title)
                        asin_title_with_brand = regex.sub('', asin_title_with_brand)
                        asin_title_without_brand = regex.sub('', asin_title_without_brand)

                        if isbn_title in [asin_title, asin_title_with_brand, asin_title_without_brand]:
                            break

                        pack_asins.append([asin, line[1], isbn, isbn_titles[isbn]])
                        print '%d/%d' % (len(pack_asins), index), keyword, asin, isbn
                        print line[1]
                        print isbn_titles[isbn], "\n"
                        break

    if len(pack_asins) > 0:
        write_to_file(problem_file, pack_asins)
