#!/usr/local/bin/python
# coding: utf-8
import argparse
import os

import sys

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib.elastic_product import ElasticProduct
from lib.models import dataDB


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-c', '--country', type=str, default=None, help='Source ASIN country')
    parser.add_argument('-t', '--type', type=str, default="product", help='')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-o', '--offset', type=int, default=0, help='offset')
    parser.add_argument('-s', '--size', type=int, default=100, help='page size')
    parser.add_argument('-m', '--mode', type=str, default="asin", help='running mode')
    parser.add_argument('-f', '--fwd', type=str, default='us', help='us,uk')
    parser.add_argument('-db', '--db_table', type=str, default='', help='db table name')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    elastic = ElasticProduct(host="35.199.3.83", port=80, http_auth=('elasticuser', 'KbersRiseUp153'))
    args = parse_args()

    print args

    country = args.country.upper()

    minId = 0

    if len(args.db_table) > 0:
        db_table = args.db_table
    elif args.fwd == 'us':
        db_table = 'us_asins_crawl'
    else:
        db_table = 'uk_asins_crawl'

    payloads = {}
    while True:
        if args.country == 'us':
            sql = "SELECT isbn,id,us_title,us_brand,us_weight,us_lwh FROM " + db_table + " WHERE us_title is not null and us_title <> 'None' and id > " + str(
                minId)
        else:
            sql = "SELECT isbn,id,uk_title,uk_brand,uk_weight,uk_lwh FROM " + db_table + " WHERE uk_title is not null and uk_title <> 'None' and id > " + str(
                minId)

        sql = sql + " order by id asc limit " + str(
            args.size if args.limit == 0 or args.size < args.limit else args.limit)

        print minId, sql

        cursor = dataDB.execute_sql(sql)
        rows = cursor.fetchall()
        if len(rows) == 0:
            break

        for row in rows:
            asin = row[0]
            payload = {
                'asin': asin,
                'title': row[2] + " by " + row[3],
                'brand': row[3],
                'weight': row[4],
                'lwh': row[5]
            }
            minId = row[1]
            elastic.bulk_add_data(asin, payload=payload, region=args.country)
