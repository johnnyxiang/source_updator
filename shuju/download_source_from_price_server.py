import sys
import os
import time
import traceback

from elasticsearch import Elasticsearch, NotFoundError, RequestError, ConnectionTimeout, TransportError, \
    ElasticsearchException

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib.utils.simple_logger import SimpleLogger

# Python version must be greater than or equal 2.7
if sys.version_info < (2, 7):
    print("ERROR: Repricing requires Python Version 2.7 or above...exiting.")
    sys.exit(1)
else:
    PY2 = bool(sys.version_info.major == 2)

logger = SimpleLogger.get_stream_logger('source_price_updator')

es_client = Elasticsearch(hosts='104.196.114.179', port=8080, http_auth=('elasticuser', 'KbersRiseUp153'))


def search(size=500):
    params = {
        'index': 'repricing_new',
        'scroll': '2m',
        'search_type': 'scan',
        'size': size
    }

    print params
    resp = None
    retry = 3
    while retry > 0:
        try:
            resp = es_client.search(**params)
            break
        except NotFoundError as e:
            break
        except RequestError as e:
            raise e
        except ConnectionTimeout:
            time.sleep(1)
        except (TransportError, ElasticsearchException) as e:
            print e
            resp = -1
            retry -= 1

            status_code = getattr(e, 'status_code', None)
            if status_code == 'N/A':
                time.sleep(3)
        except Exception as e:
            print e.message
            resp = -1
            retry -= 1
    if resp is None:
        return None

    if resp == -1:
        return False

    return resp


def write_to_file(file_name, data):
    list_file = lib_dir + "data/%s.txt" % file_name
    with open(list_file, 'ab') as csvfile:
        for row in data:
            try:
                if row is dict:
                    csvfile.write(",".join(row) + "\n")
                else:
                    csvfile.write(row + "\n")
            except:
                pass


if __name__ == "__main__":
    file_name = 'new-prime-isbn'
    size = 10000
    total = 0
    max = '200m'
    params = {
        'index': 'repricing_new',
        'scroll': max,
        'size': size
    }

    data = es_client.search(**params)
    sid = data['_scroll_id']
    scroll_size = data['hits']['total']
    total_size = scroll_size
    total_pages = int(total_size / size)
    page_no = 0
    # Start scrolling
    while scroll_size > 0:
        print "Scrolling..."
        data = es_client.scroll(scroll_id=sid, scroll=max)
        # Update the scroll ID
        sid = data['_scroll_id']
        # Get the number of results that we returned in the last scroll
        scroll_size = len(data['hits']['hits'])
        # print "scroll size: " + str(scroll_size)

        if data['hits']['total'] == 0:
            break

        rows = []
        for row in data['hits']['hits']:
            try:
                asin = row['_source']['asin']
                has_offer = row['_source']['has_offer']
                if has_offer != 'y':
                    continue

                prime = False
                if 'prime' in row['_source']:
                    prime = True if row['_source']['prime'] == 'y' else False
                if prime is False and 'is_prime' in row['_source']:
                    prime = row['_source']['is_prime']
                if prime is True:
                    rows.append(asin)
                    total = total + 1
            except:
                pass

        if len(rows) > 0:
            write_to_file(file_name, rows)

        page_no = page_no + 1
        print 'page %s/%s, total %s/%s' % (page_no, total_pages, total, total_size)
