import argparse
import datetime
import random
import traceback
import time

import sys

from lib.feedback import fetch_feedback_stats
from lib.general import regions, get_base_url, get_sales_channel
from lib.models import *
from config import *
from lib.utils.helper import get_browser, get_page_content_from_url, html_parser
from local import sids


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon feedback report')

    parser.add_argument('-r', '--region', type=str, default=None, help='KB Region')
    parser.add_argument('-c', '--country', type=str, default=None, help='marketplace')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()
    sql = "SELECT code,seller_id,country,region FROM seller_accounts WHERE status > 0 "
    if args.region is not None:
        sql += " and region_name = '%s'" % args.region

    if args.country is not None:
        sql += " and country = '%s'" % args.country

    print sql

    cursor = omsDB.execute_sql(sql)
    rows = cursor.fetchall()

    print "%s records found " % len(rows)
    for row in rows:
        if row[1] is None or len(row[1]) == 0:
            continue

        rand_time = random.randint(5, 30)
        print 'sleep %s seconds' % rand_time
        time.sleep(rand_time)

        browser = get_browser(profile=None, headless=True)
        country = row[2]
        if country.lower() == 'eu':
            countries = regions['EU']
        else:
            countries = [country]

        for country in countries:
            fetch_feedback_stats(country, row[0], row[1], browser=browser)
        browser.close()
