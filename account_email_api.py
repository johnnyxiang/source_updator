import argparse
import base64
import email
import re
import sys
from email.header import decode_header
from StringIO import StringIO
from googleapiclient import errors
from googleapiclient.discovery import build
from account_email import init_db, get_email_body, get_email_subject, parse_email_type_by_subject, get_email_date, search_message_bodies
from lib.google.storage import bucket
from lib.models import *

# If modifying these scopes, delete the file token.pickle.
from oauth2client.client import OAuth2Credentials

from lib.general import get_lock

SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']
client_id = "1032554645163-jsl5l0btqdrqd3utf7rl28tcev2smbjn.apps.googleusercontent.com"
client_secret = "C_62Yi10iEAXJfIpNp7NDrKX"
token_uri = "https://oauth2.googleapis.com/token"


def list_messages_matching_query(service, sellerAccount, user_id='me', query=''):
    try:
        response = service.users().messages().list(userId=user_id, q=query).execute()
        messages = []
        if 'messages' in response:
            messages = response['messages']

        for msg in messages:
            message = get_message(service, msg['id'])
            save_message(sellerAccount.code, message)

        while 'nextPageToken' in response:
            try:
                page_token = response['nextPageToken']
                response = service.users().messages().list(userId=user_id, q=query,
                                                           pageToken=page_token).execute()
                messages = response['messages']

                for msg in messages:
                    message = get_message(service, msg['id'])
                    save_message(sellerAccount.code, message)
            except:
                print traceback.format_exc()

                # return messages
    except:
        print traceback.format_exc()


def get_message(service, msg_id, user_id='me'):
    tried = 0
    while tried < 3:
        try:
            message = service.users().messages().get(userId=user_id, id=msg_id, format='raw').execute()
            return message
        except errors.HttpError, error:
            tried += 1
            time.sleep(10)
            print 'An error occurred: %s' % error


def list_history(service, start_history_id='1', user_id='me'):
    try:
        history = (service.users().history().list(userId=user_id,
                                                  startHistoryId=start_history_id)
                   .execute())
        changes = history['history'] if 'history' in history else []

        while 'nextPageToken' in history:
            page_token = history['nextPageToken']
            history = (service.users().history().list(userId=user_id,
                                                      startHistoryId=start_history_id,
                                                      pageToken=page_token).execute())
            if 'history' in history:
                changes.extend(history['history'])

        return changes
    except errors.HttpError, error:
        print 'An error occurred: %s' % error


def save_thread(service, thread, sellerAccount, user_id='me', start_history_id=0):
    tdata = service.users().threads().get(userId=user_id, id=thread['id'], format="minimal").execute()
    nmsgs = len(tdata['messages'])
    read = 1
    for message in tdata['messages']:
        if 'UNREAD' in message['labelIds']:
            read = 0
    try:
        email_thread = EmailThread.get(EmailThread.threadId == thread['id'])
    except:
        email_thread = EmailThread()
        email_thread.threadId = thread['id']

    email_thread.historyId = thread['historyId']
    email_thread.messageCount = nmsgs

    email_thread.account_code = sellerAccount.code
    email_thread.snippet = thread['snippet']
    email_thread.read = read
    email_thread.save()

    print email_thread.subject


def save_threads(service, threads, sellerAccount, user_id='me'):
    for thread in threads:
        save_thread(service, thread, sellerAccount, user_id)


def list_threads(service, sellerAccount, user_id='me'):
    start_history_id = 1
    for et in EmailThread.select().where(EmailThread.account_code == sellerAccount.code).order_by(EmailThread.historyId.desc()).limit(1):
        start_history_id = et.historyId

    response = service.users().threads().list(userId=user_id).execute()

    if 'threads' in response:
        threads = response['threads']
        for thread in threads:
            if int(thread['historyId']) <= start_history_id:
                return
            save_thread(service, thread, sellerAccount, user_id, start_history_id)

            # save_threads(service, threads, accountInfo, user_id)

    while 'nextPageToken' in response:
        try:
            page_token = response['nextPageToken']
            response = service.users().threads().list(userId=user_id, pageToken=page_token).execute()
            if 'threads' in response:
                threads = response['threads']
                for thread in threads:
                    if int(thread['historyId']) <= start_history_id:
                        return
                    save_thread(service, thread, sellerAccount, user_id, start_history_id)
                    # save_threads(service, threads, accountInfo, user_id)
        except:
            pass


def get_service(refresh_token):
    creds = OAuth2Credentials(None, client_id, client_secret, refresh_token, None, token_uri, "")

    if not creds.access_token or \
            (hasattr(creds, 'access_token_expired') and creds.access_token_expired):
        import httplib2

        http = httplib2.Http()
        creds.refresh(http)

    return build('gmail', 'v1', credentials=creds)


def parse_amazon_order_id(email_obj):
    if 'gmail' in email_obj.fromAddress.lower() and 'gmail' in email_obj.toAddress.lower():
        return None
    subject = email_obj.subject.replace("\n", " ")
    orderIds = re.findall(r"[0-9]{3}-[0-9]{7}-[0-9]{7}", subject)

    if orderIds is None or len(orderIds) == 0:
        content = email_obj.email
        lines = content.split('\n')

        for line in lines:
            orderIds = re.findall(r"[0-9]{3}-[0-9]{7}-[0-9]{7}", line)
            if orderIds is not None and len(orderIds) > 0:
                break

    # print orderIds
    if orderIds is not None and len(orderIds) > 0:
        return orderIds[0]

    if email_obj.in_reply_to is not None:
        try:
            last_email = Email.get(Email.messageId == email_obj.in_reply_to)
            return last_email.AmazonOrderId
        except:
            pass

    parts = email_obj.fromAddress.split('+')
    domain = email_obj.fromAddress.split('@')[1]
    buyer_email = parts[0] + '@' + domain
    if 'amazon' in domain.lower():
        try:
            order = AmazonOrder.get(AmazonOrder.BuyerEmail == buyer_email)
            if order is not None:
                return order.AmazonOrderId
        except:
            pass


skip_email_addresses = ['google.com', 'auto-communi']


def save_message(account_code, message):
    try:
        # print message
        msg_str = base64.urlsafe_b64decode(message['raw'].encode('ASCII'))
        mime_msg = email.message_from_string(msg_str)

        subject = get_email_subject(mime_msg)
        if "pricing error" in subject or 'DRAFT' in message['labelIds']:
            return
        from_list = email.utils.parseaddr(mime_msg['From'])
        to_list = email.utils.parseaddr(mime_msg['To'])
        for skip in skip_email_addresses:
            if skip.lower() in from_list[1].lower():
                return

        gmailId = message['id']
        try:
            email_obj = Email.get(Email.gmailId == gmailId)
        except:
            # print traceback.format_exc()
            email_obj = Email()
            email_obj.gmailId = gmailId

        email_obj.account_code = account_code
        email_obj.threadId = message['threadId']
        email_obj.historyId = message['historyId']
        email_obj.labelIds = ','.join(message['labelIds'])
        email_obj.snippet = message['snippet']
        body = get_email_body(mime_msg)
        email_obj.subject = subject
        email_obj.email = body[0:100000]

        if 'UNREAD' in message['labelIds']:
            email_obj.read = 0
        else:
            email_obj.read = 1

        email_obj.created_at = get_email_date(mime_msg)

        if "In-Reply-To" in mime_msg:
            email_obj.in_reply_to = mime_msg['In-Reply-To']

        store_names = []

        if sellerAccount.email_name is not None:
            store_names.append(sellerAccount.email_name.lower())
        if sellerAccount.name is not None:
            store_names.append(sellerAccount.name.lower())
        if sellerAccount.cs_name is not None:
            store_names.append(sellerAccount.cs_name.lower())

        if len(from_list[0]) == 0 or from_list[0].lower() in store_names:
            fromName = 'me'
        else:
            fromName = decode_header(from_list[0])[0][0]
        email_obj.fromName = fromName
        email_obj.fromAddress = from_list[1]
        email_obj.toName = to_list[0]
        email_obj.toAddress = to_list[1]
        email_obj.AmazonOrderId = parse_amazon_order_id(email_obj)

        email_obj.messageId = mime_msg["Message-ID"]
        email_obj.category = parse_email_type_by_subject(email_obj, labelIds=message['labelIds'])
        # email_obj.attachments = None
        if email_obj.attachments is None:
            attachment_urls = []
            for part in mime_msg.walk():
                attachment = parse_attachment(part)
                if attachment is not None:
                    blob = bucket.blob(attachment.name.strip('"'))
                    blob.upload_from_file(attachment)
                    blob.make_public()
                    attachment_urls.append(blob.public_url)

            email_obj.attachments = ",".join(attachment_urls)
        email_obj.save()
        print subject, message['labelIds'], email_obj.created_at

        try:
            save_thread1(email_obj, store_names=store_names)
        except:
            print traceback.format_exc()
            pass
    except:
        print traceback.format_exc()


def parse_attachment(message_part):
    content_disposition = message_part.get("Content-Disposition", None)
    if content_disposition:
        dispositions = content_disposition.strip().split(";")
        if bool(content_disposition and dispositions[0].lower() == "attachment"):
            file_data = message_part.get_payload(decode=True)
            attachment = StringIO(file_data)
            attachment.content_type = message_part.get_content_type()
            attachment.size = len(file_data)
            attachment.name = content_disposition.split('=')[1]
            attachment.create_date = None
            attachment.mod_date = None
            attachment.read_date = None

            for param in dispositions[1:]:
                name, value = param.split("=")
                name = name.lower()

                if name == "filename":
                    attachment.name = value
                elif name == "create-date":
                    attachment.create_date = value  # TODO: datetime
                elif name == "modification-date":
                    attachment.mod_date = value  # TODO: datetime
                elif name == "read-date":
                    attachment.read_date = value  # TODO: datetime
            return attachment

    return None


def save_thread1(email_obj, user_id='me', store_names=[]):
    threadId = email_obj.threadId
    thread = service.users().threads().get(userId=user_id, id=threadId, format="minimal").execute()
    nmsgs = len(thread['messages'])
    read = 1
    for message in thread['messages']:
        if 'UNREAD' in message['labelIds']:
            read = 0
    try:
        email_thread = EmailThread.get(EmailThread.threadId == threadId)

    except:
        email_thread = EmailThread()
        email_thread.threadId = threadId
        email_thread.account_code = email_obj.account_code

    if email_thread.created_at is None or email_thread.created_at >= email_obj.created_at:
        email_thread.created_at = email_obj.created_at
        email_thread.subject = email_obj.subject
        email_thread.category = email_obj.category
        email_thread.firstMsgId = email_obj.gmailId

        email_thread.fromName = 'me' if len(email_obj.fromName) == 0 else email_obj.fromName
    if email_thread.amazonOrderId is None:
        email_thread.amazonOrderId = email_obj.AmazonOrderId

    if email_obj.category is not None and email_obj.category.lower() != 'sent' and (
                        email_thread.category is None or len(email_thread.category) == 0 or email_thread.category.lower() == 'sent'):
        email_thread.category = email_obj.category

    if email_thread.updated_at is None or email_thread.updated_at < email_obj.created_at:
        email_thread.updated_at = email_obj.created_at

    if email_thread.people is not None:
        people = email_thread.people.split(', ')
        people = set([p.encode("utf-8") for p in people if 'utf' not in p.lower() and p.lower() not in store_names])
    else:
        people = set()

    people.add('me' if len(email_obj.fromName) == 0 else email_obj.fromName)

    email_thread.people = ', '.join(people)
    email_thread.historyId = thread['historyId']
    email_thread.messageCount = nmsgs
    if 'snippet' in thread:
        email_thread.snippet = thread['snippet']

    if email_thread.snippet is None:
        email_thread.snippet = email_obj.snippet

    email_thread.read = read
    email_thread.save()

    print email_thread.subject, email_thread.people


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-t', '--type', type=str, default="new", help='')
    parser.add_argument('-id', '--id', type=str, default=None, help='')
    parser.add_argument('-d', '--days', type=int, default=100, help='')
    parser.add_argument('-et', '--email_type', type=str, default="all", help='')
    parser.add_argument('-r', '--recent_mode', type=int, default=0, help='recent mode?')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()

    print args

    get_lock("account_email_api_%s_%s" % (args.account.lower(), args.type.lower()))

    database_name = args.account
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    sellerAccount = SellerAccount.get(code=args.account)

    if sellerAccount is None:
        print "No account info for " + args.account + " found"
        sys.exit()
    if sellerAccount.gmail_token is None or len(sellerAccount.gmail_token) == 0:
        print "No auth token  for " + args.account + " found"
        sys.exit()

    service = get_service(sellerAccount.gmail_token)

    if args.id is not None:
        message = get_message(service, args.id)
        save_message(sellerAccount.code, message)
    elif args.type == 'thread':
        list_threads(service, sellerAccount)
    elif args.type == 'all':
        list_messages_matching_query(service, sellerAccount)
    else:
        start_history_id = "1"
        for emailO in Email.select().where(Email.account_code == sellerAccount.code).order_by(Email.historyId.desc()).limit(1):
            start_history_id = emailO.historyId

        changes = list_history(service, start_history_id)

        for change in changes:
            if 'messages' in change:
                for msg in change['messages']:
                    message = get_message(service, msg['id'])
                    save_message(sellerAccount.code, message)
