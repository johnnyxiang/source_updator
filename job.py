import sys

from lib.general import get_lock
from lib.listing_report import get_inventory_report
from lib.models import *
import source_price_updator
import file_generator
import settlement
import finance
import argparse

from listing import parse_inventory_report
from local import *
from config import *


class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', type=str, default="all", help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='Country')
    parser.add_argument('-s', '--source_country', type=str, default="us", help='Source Country')
    parser.add_argument('-d', '--download', type=int, default=1, help='download inventory')
    parser.add_argument('-m', '--mode', type=str, default="production", help='running mode')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')
    parser.add_argument('-mc', '--min_change', type=float, default=4, help='min price to trigger auto repricing change')
    parser.add_argument('-sku', '--sku', type=str, default=None, help='sku pattern')
    parser.add_argument('-q', '--qty', type=int, default=3, help='upload qty')
    parser.add_argument('-up', '--update_price', type=int, default=1, help='update source price')
    parser.add_argument('-e', '--expired_only', type=int, default=-1, help='expired offers only')
    parser.add_argument('-t', '--types', type=str, default='product', help='product types')
    parser.add_argument('-sb', '--sync_blacklist', type=int, default=1, help='sync blacklist')
    parser.add_argument('-sp', '--sync_pro', type=int, default=0, help='sync pro table with listing table')
    command_args = parser.parse_args()
    return command_args


def process_account(account, mode, limit=15000, country=None, source_country="us", download_inventory=True, min_change=5,
                    sku=None, qty=3, update_price=True, expired_only=0, types='product', sync_blacklist=1, sync_pro=0):
    if country is None:
        country = account.country

    print account.name, mode
    if mode != "finance" and len(account.types) > 0:
        database_name = account.name
        databaseName = account.name
        database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)
        # download inventory
        if download_inventory is True:
            get_inventory_report(country, account.name, account.mws_access_key, account.mws_secret_key, account.seller_id,
                                 callback=parse_inventory_report, auth_token=account.mws_auth_token)

        for t in types.split(","):
            if update_price is True:
                args = {"account": account.name, "type": t, "condition": "all", "limit": 0, "country": country,
                        "offset": 0,
                        "mode": mode, "source_country": source_country, "sku": sku, "expired_only": expired_only}
                source_price_updator.main(Struct(**args))

            args = {"account": account.name, "type": t, "condition": "all", "limit": limit, "country": country,
                    "feed_type": "update", "mode": mode, "min_change": min_change,
                    "sku": sku, "qty": qty, "sync_blacklist": sync_blacklist, "sync_pro": sync_pro}
            file_generator.main(Struct(**args))

    if qty == 0 or sku is not None:
        return
    # settlement
    args = {"account": account.name}
    settlement.main(Struct(**args))

    # finance
    args = {"account": account.name, "posted_after": None, "posted_before": None}
    finance.main(Struct(**args))


if __name__ == "__main__":
    args = parse_args()
    print args

    get_lock("job_%s" % (args.account.lower()))

    download_inventory = True if args.download > 0 else False
    update_source_price = True if args.update_price > 0 else False
    if args.account == "all":
        for sid in sids:
            if sid in sids:
                account = Account.get(name=sid)
                process_account(account, args.mode, args.limit, None, args.source_country, download_inventory, args.min_change,
                                sku=args.sku, qty=args.qty, expired_only=args.expired_only, types=args.types,
                                sync_blacklist=args.sync_blacklist, sync_pro=args.sync_pro)
    else:
        if args.account not in sids:
            print "account not supported"
            sys.exit(0)

        account = Account.get(name=args.account)
        process_account(account, args.mode, args.limit, args.country, args.source_country, download_inventory, args.min_change,
                        sku=args.sku, qty=args.qty, update_price=update_source_price, expired_only=args.expired_only, types=args.types,
                        sync_blacklist=args.sync_blacklist, sync_pro=args.sync_pro)
