import sys
import os
import argparse
import platform
import traceback
from datetime import datetime
from lib.utils.simple_logger import SimpleLogger
from lib.utils.asin_matcher import AsinMatcher
import requests
from config import *
from lib.models import *
import time
import csv
import subprocess
import time


# Python version must be greater than or equal 2.7
from local import sids

if sys.version_info < (2, 7):
    print("ERROR: Repricing requires Python Version 2.7 or above...exiting.")
    sys.exit(1)
else:
    PY2 = bool(sys.version_info.major == 2)


logger = SimpleLogger.get_stream_logger('source_inactive')

asin_matcher = AsinMatcher()


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-sku', '--sku', type=str, default="", help='sku pattern')
    parser.add_argument('-l', '--limit', type=int, default=0, help='limit')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()

    logger.info(args)
    # if args.account not in sids:
    #     print "account not supported"
    #     sys.exit(0)
    region = args.country
    database_name = args.account
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    limit = args.limit
    pindex = 0

    accountInfo = Account.get(name=args.account)

    if accountInfo is None:
        logger.error("No account info for "+args.account +" found")
        sys.exit()

    db_table = "inactive_listings"
    if region.upper() != "US":
        db_table = db_table + "_" + args.country.lower()

    pindex = 0
    while True:
        current_time = time.time()
        sql = "select id,asin from "+db_table+" where mapped = 0 "
        if args.sku != "":
            sql = sql + " and sku like %s"
        sql = sql + " order by id asc limit 100 "
        if args.sku != "":
            cursor = database.execute_sql(sql,['%'+args.sku+'%'])
        else:
            cursor = database.execute_sql(sql)

        rows = cursor.fetchall()

        if len(rows) == 0:
            break

        ids = {}
        for row in rows:
            ids[row[1]] = row[0]

        asins = [row[1] for row in rows]

        #print rows[0][0], " fetch data took",int(time.time()-current_time)," seconds"
        #current_time = time.time()

        source_asins = asin_matcher.match_by_api(asins)

        #print rows[0][0], "get real asin took",int(time.time()-current_time)," seconds"
        #current_time = time.time()

        sql = "INSERT INTO "+db_table+"  (id,mapped) VALUES "
        
        values = []
        for asin in asins:

            mapped = -1
            if asin in source_asins and source_asins[asin] != None:
                mapped = 1

            id = ids[asin]
            values.append(" ("+str(id)+","+str(mapped)+")")
            
        sql = sql + ",".join(values)
        sql =  sql + " ON DUPLICATE KEY UPDATE mapped=VALUES(mapped);"  
        #print sql
        database.execute_sql(sql)
        
        
        pindex = pindex + len(rows)

        print rows[0][0], " took",int(time.time()-current_time)," seconds"
        if limit != 0 and pindex >= limit:
            break

