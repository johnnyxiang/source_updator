import argparse
import traceback
import urllib2
from lib.general import *
from lib.models import Account, AmazonOrder, omsDB
from lib.mws_feed import submit_invoice
from lib.utils.logger import get_logger
from lib.utils.utils import is_amazon_order
from local import *

rootDir = os.path.dirname(os.path.realpath(__file__))
invoiceFileDir = rootDir + "/invoice"
if not os.path.isdir(invoiceFileDir):
    os.makedirs(invoiceFileDir)

logger = get_logger(__name__)


def download_pdf(order_id):
    endpoint = 'https://oms.tmwarriors.com/api/orders/invoice/%s?pdf=1' % order_id
    feed_data = urllib2.urlopen(urllib2.Request(endpoint)).read()
    file_path = invoiceFileDir + '/' + order_id + '.pdf'
    with open(file_path, 'wb') as f:
        f.write(feed_data)

    return file_path


def download_credit_note_pdf(order_id):
    endpoint = 'https://oms.tmwarriors.com/api/orders/credit-note/%s?pdf=1' % order_id
    feed_data = urllib2.urlopen(urllib2.Request(endpoint)).read()
    file_path = invoiceFileDir + '/cn-' + order_id + '.pdf'
    with open(file_path, 'wb') as f:
        f.write(feed_data)

    return file_path


def get_invoice_id(order):
    last7 = order.AmazonOrderId.split('-')[-1]
    return 'INV-%s-%s' % (order.PurchaseDate.strftime("%y%m%d"), last7)


def get_credit_note_id(order):
    last7 = order.AmazonOrderId.split('-')[-1]
    return 'CN-%s-%s' % (order.PurchaseDate.strftime("%y%m%d"), last7)


def upload_invoice_for_order(order_id, account):
    if is_amazon_order(order_id):
        order = AmazonOrder.get(AmazonOrder.AmazonOrderId == order_id)
    else:
        order = order_id
        order_id = order.AmazonOrderId

    logger.info('uploading invoice for order %s' % order_id)
    file_path = download_pdf(order_id)
    if os.path.exists(file_path):
        options = {
            'metadata:orderid': order_id,
            'metadata:totalAmount': order.OrderTotal,
            'metadata:totalvatamount': 0,
            'metadata:invoicenumber': get_invoice_id(order),
            'metadata:documenttype': 'Invoice'
        }

        feed_options = ";".join(("{}={}".format(*i) for i in options.items()))
        # print feed_options

        selected_region = get_country_code(order.SalesChannel)
        logger.info("%s,%s" % (selected_region, order.SalesChannel))
        result = submit_invoice(selected_region, credential=account, file_path=file_path, feed_options=feed_options)
        logger.info(result)

        handle_result(order_id, result, 'invoice')


def upload_credit_note_for_order(order_id, account):
    if is_amazon_order(order_id):
        order = AmazonOrder.get(AmazonOrder.AmazonOrderId == order_id)
    else:
        order = order_id
        order_id = order.AmazonOrderId

    logger.info('uploading credit note for order %s' % order_id)
    file_path = download_credit_note_pdf(order_id)
    if os.path.exists(file_path):
        options = {
            'metadata:orderid': order_id,
            'metadata:totalAmount': order.OrderTotal,
            'metadata:totalvatamount': 0,
            'metadata:invoicenumber': get_invoice_id(order),
            'metadata:transactionid': get_credit_note_id(order),
            'metadata:documenttype': 'CreditNote'
        }

        feed_options = ";".join(("{}={}".format(*i) for i in options.items()))
        logger.debug(feed_options)

        selected_region = get_country_code(order.SalesChannel)
        logger.info("%s,%s" % (selected_region, order.SalesChannel))
        result = submit_invoice(selected_region, credential=account, file_path=file_path, feed_options=feed_options)
        logger.info(result)

        handle_result(order_id, result, 'credit')


def handle_result(order_id, response, upload_type='invoice'):
    for line in response.split('\n'):
        line_data = line.split('\t')
        if len(line_data) < 2:
            continue

        if line_data[2] == 'Error':
            update_status(order_id, 0, upload_type)
            return

    update_status(order_id, 1, upload_type)


def update_status(order_id, success, upload_type='invoice'):
    order = AmazonOrder.get(AmazonOrder.AmazonOrderId == order_id)
    if upload_type == 'invoice':
        order.invoiceSent = success
    else:
        order.creditNoteSent = success
    order.save()


def upload_invoice_for_account(account):
    orders = AmazonOrder.select().where(AmazonOrder.account_code == account.name, AmazonOrder.businessOrder == 1,
                                        AmazonOrder.invoiceSent == 0, AmazonOrder.OrderStatus == 'Shipped')
    logger.info("%s orders found to upload invoice for account  %s" % (len(orders), account.name))
    for order in orders:
        try:
            upload_invoice_for_order(order, account)
        except:
            logger.error(traceback.format_exc())

    sql = "SELECT amazon_order_id FROM orders o JOIN amazon_orders a ON a.AmazonOrderId = o.amazon_order_id " + \
          "WHERE o.account_code = %s AND o.is_refunded=1 AND creditNoteSent=0 AND businessOrder=1"
    cursor = omsDB.execute_sql(sql, [account.name])
    rows = cursor.fetchall()
    order_ids = [row[0] for row in rows]
    logger.info("%s orders found to upload credit note for account  %s" % (len(order_ids), account.name))
    if len(order_ids) == 0:
        return

    orders = AmazonOrder.select().where(AmazonOrder.AmazonOrderId << order_ids)

    for order in orders:
        try:
            upload_credit_note_for_order(order, account)
        except:
            logger.error(traceback.format_exc())


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-c', '--country', type=str, default=None, help='country of the inventory')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-id', '--order_id', type=str, default=None, help='order_id')
    parser.add_argument('-t', '--type', type=str, default='invoice', help='upload type: invoice or credit')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()

    if args.account not in sids:
        logger.error("account not supported: %s" % args.account)
        sys.exit(0)

    account_info = Account.get(name=args.account)

    if account_info is None:
        logger.error("No account info for " + args.account + " found")
        sys.exit()

    get_lock('upload_invoice_%s' % args.account)
    region = args.country
    if region is None:
        region = account_info.country

    if region.upper() not in regions['EU']:
        sys.exit()
    if args.order_id is not None:
        if args.type == 'credit':
            upload_credit_note_for_order(args.order_id, account_info)
        else:
            upload_invoice_for_order(args.order_id, account_info)
    else:
        upload_invoice_for_account(account_info)
