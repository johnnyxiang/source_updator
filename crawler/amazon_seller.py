import argparse
from multiprocessing import Process, Queue, current_process
import re
import os
import sys
from bs4 import BeautifulSoup
import threading

lock = threading.Lock()

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib.models import dataDB
from lib.utils.helper import Helper, get_page_content_from_url, html_parser, get_browser

_extract_asin_regexp = re.compile(r'(/dp/|/product-reviews/|/gp/product/|#isbn=)(?P<asin>[^/]+)/?')


def extract_asin(url):
    try:
        match = _extract_asin_regexp.search(url)
        if match is None:
            return None
        asin = str(match.group('asin'))
        if len(asin) == 10:
            return asin
    except:
        # warnings.warn('Error matching ASIN in URL {}'.format(url))
        raise

    return None


def download_from_merchant(merchant_id, country='uk'):
    table_name = '%s_asins_crawl_%s' % (country.lower(), merchant_id)

    try:
        sql = 'CREATE TABLE ' + table_name + ' LIKE us_asins_crawl'
        dataDB.execute_sql(sql)
    except:
        pass
    browser = get_browser()
    helper = Helper()
    if country == 'uk':
        domain = 'www.amazon.co.uk'
    else:
        domain = 'www.amazon.com'

    toplink = "https://" + domain + "/gp/search/other/ref=sr_in_a_-2?rh=i%3Amerchant-items&pickerToList=brandtextbin&me=" + merchant_id + "&indexField="

    print toplink

    allLinks = []
    for code in range(ord('a'), ord('z') + 1):
        brandABlink = toplink + chr(code)
        html = get_page_content_from_url(brandABlink, browser)
        soup = BeautifulSoup(html)
        links = helper.find_tags(soup, "#refinementList .a-link-normal")
        # print links
        for link in links:
            allLinks.append(helper.format_url(link["href"], toplink))

    print allLinks
    workers = 8
    work_queue = Queue()
    done_queue = Queue()
    processes = []

    for link in allLinks:
        link = link + "&lo=merchant-items"
        work_queue.put(link)

    for w in xrange(workers):
        browser = get_browser(profile=None)
        p = Process(target=worker, args=(table_name, work_queue, done_queue, browser))
        p.start()
        processes.append(p)
        work_queue.put('STOP')
    for p in processes:
        p.join()

    done_queue.put('STOP')
    for status in iter(done_queue.get, 'STOP'):
        print status


def worker(table_name, work_queue, done_queue, browser=None):
    try:
        for url in iter(work_queue.get, 'STOP'):
            try:
                download_by_link(table_name, url, browser)
            except:
                pass
            done_queue.put("%s - %s" % (current_process().name, url))
    except Exception, e:
        done_queue.put("%s failed on %s with: %s" % (current_process().name, url, e.message))
    return True


def download_by_link(table_name, link, browser=None):
    helper = Helper()
    page_limit = 2000000
    next = link
    root = link
    page_index = 0

    while next is not None:
        next_page = next
        print "\n\n", next_page, "\n\n"
        page_index = page_index + 1
        html = get_page_content_from_url(next_page, browser)
        mysoup = BeautifulSoup(html, html_parser)
        blinks = helper.find_tags(mysoup, "li.s-result-item")

        if len(blinks) == 0:
            blinks = helper.find_tags(mysoup, ".zg_itemImmersion .zg_itemWrapper a")

        asins = []
        for clink in blinks:
            try:
                try:
                    asin = extract_asin(clink["href"])
                except:
                    # print traceback.format_exc()
                    try:
                        asin = clink["data-asin"].strip()
                    except:
                        continue

                if asin is None:
                    continue
                asins.append(asin)
                # try:
                #     sql = "insert ignore into " + table_name + " (isbn) value (%s) "
                #     dataDB.execute_sql(sql, [asin])
                #
                print index, "-", asin
                index = index + 1
                # except:
                #     print asin, "existed"
            except:
                pass
        if len(asins) > 0:
            write_to_file(table_name, asins)
        next_page_num = helper.find_next_page(mysoup)
        # print nextPageNum

        if page_index >= page_limit:
            next = None
        else:
            if next_page_num is not None:
                next = root + "&pg=" + str(next_page_num)
                # time.sleep(random.expovariate(0.1))
            else:
                next = None


def write_to_file(merchant_id, data):
    lock.acquire()  # thread blocks at this line until it can obtain lock
    list_file = lib_dir + "/data/%s.txt" % merchant_id
    with open(list_file, 'ab') as csvfile:
        for asin in data:
            csvfile.write(asin + "\n")
    lock.release()


def parse_args():
    parser = argparse.ArgumentParser(description='Crawler.')
    parser.add_argument('-c', '--country', type=str, default='uk', help='country name')
    parser.add_argument('-id', '--merchant_id', type=str, default='', help='merchant id')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()
    download_from_merchant(args.merchant_id, args.country)
