import argparse
import csv
from multiprocessing import Process, Queue, current_process
import re
import os
import sys
from bs4 import BeautifulSoup
import threading

lock = threading.Lock()

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib.models import dataDB
from lib.utils.helper import Helper, get_page_content_from_url, html_parser, get_browser

_extract_asin_regexp = re.compile(r'(/dp/|/product-reviews/|/gp/product/|#isbn=)(?P<asin>[^/]+)/?')


def extract_asin(url):
    try:
        match = _extract_asin_regexp.search(url)
        if match is None:
            return None
        asin = str(match.group('asin'))
        if len(asin) == 10:
            return asin
    except:
        # warnings.warn('Error matching ASIN in URL {}'.format(url))
        raise

    return None


def download_by_keywords(country='us',process_no = 10):

    word_file = lib_dir + "/crawler/10000words.txt"

    table_name = '%s_asins_crawl_used_book' % args.country.lower()
    if country == 'uk':
        domain = 'www.amazon.co.uk'
    else:
        domain = 'www.amazon.com'

    try:
        sql = 'CREATE TABLE ' + table_name + ' LIKE us_asins_crawl'
        dataDB.execute_sql(sql)
    except:
        pass

    #https://www.amazon.com/s/
    allLinks = []
    with open(word_file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            keyword = row[0]
            #link = "https://" + domain + "/s/ref=sr_nr_p_n_condition-type_2?fst=as%3Aoff%2Cp90x%3A1&rh=n%3A283155%2Cn%3A4%2Ck%3A"+keyword+"%2Cp_n_condition-type%3A1294425011&keywords="+keyword+"&ie=UTF8&qid=1537465296&rnid=1294421011&lo=stripbooks"
            #link = "https://" + domain + "/s/ref=sr_ex_p_n_feature_browse-b_0?rh=n%3A283155%2Ck%3A"+keyword+"%2Cp_n_condition-type%3A1294425011&keywords="+keyword+"&ie=UTF8&qid=1537639970&lo=stripbooks"
            #link = "https://" + domain + "/s/ref=sr_nr_p_72_3?fst=as%3Aoff&rh=n%3A283155%2Ck%3A"+keyword+"%2Cp_n_condition-type%3A1294425011%2Cp_72%3A1250224011&keywords="+keyword+"&ie=UTF8&qid=1537639976&rnid=1250219011&lo=stripbooks"
            #hardcover
            #link = "https://www.amazon.com/s/ref=sr_nr_p_n_feature_browse-b_2?fst=as%3Aoff&rh=n%3A283155%2Ck%3A"+keyword+"%2Cp_n_availability%3A2245265011%2Cp_n_feature_browse-bin%3A2656020011&sort=relevanceexprank&keywords="+keyword+"&unfiltered=1&ie=UTF8&qid=1537794150&rnid=618072011&lo=stripbooks"
            #paperback
            link = "https://www.amazon.com/s/ref=sr_nr_p_n_feature_browse-b_4?fst=as%3Aoff&rh=n%3A283155%2Ck%3A"+keyword+"%2Cp_n_availability%3A2245265011%2Cp_n_feature_browse-bin%3A2656022011&sort=relevanceexprank&keywords="+keyword+"&unfiltered=1&ie=UTF8&qid=1537881691&rnid=618072011&lo=stripbooks"
            #link = "https://" + domain + "/s/ref=nb_sb_noss_1?url=search-alias%3Dstripbooks&field-keywords="+keyword+"&lo=stripbooks&rh=n%3A283155%2Ck%3A"+keyword+"&lo=stripbooks"
            allLinks.append(link)
    
    allLinks.reverse()
    workers = process_no
    work_queue = Queue()
    done_queue = Queue()
    processes = []

    for link in allLinks:
        work_queue.put(link)

    for w in xrange(workers):
        browser = get_browser(profile=None)
        p = Process(target=worker, args=(table_name, work_queue, done_queue, browser))
        p.start()
        processes.append(p)
        work_queue.put('STOP')
    for p in processes:
        p.join()

    done_queue.put('STOP')
    for status in iter(done_queue.get, 'STOP'):
        print status


def worker(table_name, work_queue, done_queue, browser=None):
    try:
        for url in iter(work_queue.get, 'STOP'):
            try:
                download_by_link(table_name, url, browser)
            except:
                pass
            done_queue.put("%s - %s" % (current_process().name, url))
    except Exception, e:
        done_queue.put("%s failed on %s with: %s" % (current_process().name, url, e.message))
    return True


def download_by_link(table_name, link, browser=None):
    helper = Helper()
    page_limit = 2000000
    next = link
    root = link
    page_index = 0
    index = 0
    while next is not None:
        next_page = next
        print "\n\n", next_page, "\n\n"
        page_index = page_index + 1
        html = get_page_content_from_url(next_page, browser)
        mysoup = BeautifulSoup(html, html_parser)
        blinks = helper.find_tags(mysoup, "li.s-result-item")

        if len(blinks) == 0:
            blinks = helper.find_tags(mysoup, ".zg_itemImmersion .zg_itemWrapper a")

        #asins = []
        for clink in blinks:
            try:
                try:
                    asin = extract_asin(clink["href"])
                except:
                    # print traceback.format_exc()
                    try:
                        asin = clink["data-asin"].strip()
                    except:
                        continue

                if asin is None:
                    continue
                #asins.append(asin)
                try:
                    sql = "insert into " + table_name + " (isbn) value (%s) "
                    dataDB.execute_sql(sql, [asin])

                    print index, "-", asin
                    index = index + 1
                except:
                    #print asin, traceback.format_exc()
                    print asin, "existed"
            except:
                pass
        # if len(asins) > 0:
        #     write_to_file(table_name, asins)
        next_page_num = helper.find_next_page(mysoup)
        # print nextPageNum

        if page_index >= page_limit:
            next = None
        else:
            if next_page_num is not None:
                next = root + "&pg=" + str(next_page_num)
                # time.sleep(random.expovariate(0.1))
            else:
                next = None


def write_to_file(merchant_id, data):
    lock.acquire()  # thread blocks at this line until it can obtain lock
    list_file = lib_dir + "/data/%s.txt" % merchant_id
    with open(list_file, 'ab') as csvfile:
        for asin in data:
            csvfile.write(asin + "\n")
    lock.release()


def parse_args():
    parser = argparse.ArgumentParser(description='Crawler.')
    parser.add_argument('-c', '--country', type=str, default='us', help='country name')

    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()
    download_by_keywords(args.country)

