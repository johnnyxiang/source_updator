import argparse
import csv
from multiprocessing import Process, Queue, current_process
import re
import os
import sys
import glob
from bs4 import BeautifulSoup
import threading

lock = threading.Lock()

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib.general import APP_ROOT_DIR
from lib.models import USAsin, UKAsin,dataDB
from lib.utils.helper import Helper, get_page_content_from_url, html_parser, get_browser

_extract_asin_regexp = re.compile(r'(/dp/|/product-reviews/|/gp/product/|#isbn=)(?P<asin>[^/]+)/?')


def extract_asin(url):
    try:
        match = _extract_asin_regexp.search(url)
        if match is None:
            return None
        asin = str(match.group('asin'))
        if len(asin) == 10:
            return asin
    except:
        # warnings.warn('Error matching ASIN in URL {}'.format(url))
        raise

    return None


def worker(country, save_to, work_queue, done_queue, browser=None):
    try:
        for url in iter(work_queue.get, 'STOP'):
            try:
                download_by_link(country, url, save_to, browser)
            except:
                pass
            done_queue.put("%s - %s" % (current_process().name, url))
    except Exception, e:
        done_queue.put("%s failed on %s with: %s" % (current_process().name, url, e.message))
    return True


def write_to_file(file_name, data):
    lock.acquire()  # thread blocks at this line until it can obtain lock
    list_file = lib_dir + "/data/%s.txt" % file_name
    with open(list_file, 'ab') as csvfile:
        for asin in data:
            csvfile.write(asin + "\n")
    lock.release()


def download_by_link(country, link, save_to, browser=None):
    helper = Helper()
    page_limit = 2000000
    next = link
    root = link
    index = 0
    page_index = 0

    if "db-" in save_to:
        table_name = save_to.replace("db-","")

        try:
            sql = 'CREATE TABLE ' + table_name + ' LIKE us_asins_crawl'
            dataDB.execute_sql(sql)
        except:
            pass

    else:
        table_name = None

    while next is not None:
        next_page = next
        print "\n\n", next_page, "\n\n"
        page_index = page_index + 1
        html = get_page_content_from_url(next_page, browser)
        mysoup = BeautifulSoup(html, html_parser)
        blinks = helper.find_tags(mysoup, "li.s-result-item")

        if len(blinks) == 0:
            blinks = helper.find_tags(mysoup, ".zg_itemImmersion .zg_itemWrapper a")

        asins = []
        for clink in blinks:
            try:
                try:
                    asin = extract_asin(clink["href"])
                except:
                    # print traceback.format_exc()
                    try:
                        asin = clink["data-asin"].strip()
                    except:
                        continue

                if asin is None:
                    continue

                if "db-" in save_to:
                    if table_name is not None:
                        try:
                            sql = "insert into " + table_name + " (isbn) value (%s) "
                            dataDB.execute_sql(sql, [asin])

                            print index, "-", asin
                            index = index + 1
                        except:
                            #print asin, traceback.format_exc()
                            print asin, "existed"
                    else:

                        try:
                            if country.lower() == 'us':
                                p = USAsin()
                            else:
                                p = UKAsin()

                            p.isbn = asin
                            p.save()

                            print index, "-", p.isbn
                            index = index + 1
                        except:
                            print asin, "existed"
                else:
                    asins.append(asin)
            except:
                pass

        if save_to != 'db' and len(asins):
            write_to_file(save_to, asins)

        next_page_num = helper.find_next_page(mysoup)
        # print nextPageNum

        if page_index >= page_limit:
            next = None
        else:
            if next_page_num is not None:
                next = root + "&pg=" + str(next_page_num)
                # time.sleep(random.expovariate(0.1))
            else:
                next = None


def download_asins(file, save_to='db', country='us',workers=8):
    links = []
    with open(file, 'rb') as f:
        reader = csv.reader(f)
        for row in reader:
            if len(row) == 0:
                break

            if row[0][0] == "#":
                continue
            links.append(row[0])

    work_queue = Queue()
    done_queue = Queue()
    processes = []
    # links.reverse()
    for link in links:
        # link = link + "&lo=stripbooks"
        work_queue.put(link)
        # download_by_link1(link, browser)
    # return
    for w in xrange(workers):
        browser = get_browser(profile=None)
        p = Process(target=worker, args=(country, save_to, work_queue, done_queue, browser))
        p.start()
        processes.append(p)
        work_queue.put('STOP')
    for p in processes:
        p.join()

    done_queue.put('STOP')
    for status in iter(done_queue.get, 'STOP'):
        print status

    pass


def parse_args():
    parser = argparse.ArgumentParser(description='Crawler.')
    parser.add_argument('-f', '--file', type=str, help='file name')
    parser.add_argument('-t', '--type', type=str, default='file', help='file name')
    parser.add_argument('-c', '--country', type=str, default='us', help='file name')
    parser.add_argument('-p', '--workers', type=int, default=5, help='works number')
    parser.add_argument('-s', '--save_to', type=str, default='file', help='save to file or db')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()
    if args.type == 'file':
        file = APP_ROOT_DIR + "/data/" + args.file + ".csv"
        download_asins(file, args.save_to, args.country,workers=args.workers)
    else:
        files = [f for f in glob.glob(APP_ROOT_DIR + "/data/" + args.file+"/*.*")]
        for file in files:
            if "db-" in args.save_to:
                save_to = os.path.basename(file)
            else:
                save_to = args.save_to
            print 'downloading file', file
            download_asins(file, save_to, args.country,workers=args.workers)
