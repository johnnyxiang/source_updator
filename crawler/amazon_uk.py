import argparse
import csv
import random
import urlparse
from multiprocessing import Process, Queue, current_process
import re
import os
import sys
from bs4 import BeautifulSoup

lib_dir = (os.path.abspath(os.path.join(os.path.dirname(__file__), '..')) + '/')
sys.path.insert(0, lib_dir)

from lib.general import APP_ROOT_DIR
from lib.models import UkAsin
from lib.utils.helper import Helper, get_page_content_from_url, html_parser, get_browser

_extract_asin_regexp = re.compile(r'(/dp/|/product-reviews/|/gp/product/|#isbn=)(?P<asin>[^/]+)/?')


def extract_asin(url):
    try:
        match = _extract_asin_regexp.search(url)
        if match is None:
            return None
        asin = str(match.group('asin'))
        if len(asin) == 10:
            return asin
    except:
        # warnings.warn('Error matching ASIN in URL {}'.format(url))
        raise

    return None


def download_by_authors(link, browser=None):
    helper = Helper()
    next = link

    parts = urlparse.urlparse(link)
    root = parts.scheme + "://" + parts.hostname
    browser.get(link)
    blinks = []

    while True:

        html = browser.page_source
        mysoup = BeautifulSoup(html, html_parser)

        blinks1 = helper.find_tags(mysoup, "li span.a-list-item .a-link-normal")
        blinks = blinks + blinks1
        try:
            nextPageLink = browser.find_element_by_css_selector(".pagn.pagnControls.next.pagnLink")

            if 'pagnDisabled' in nextPageLink.get_attribute('class'):
                break
            try:
                browser.execute_script("arguments[0].click();", nextPageLink)
            except:
                pass
        except:
            break

    # for link in blinks:
    #     author_link = root + link["href"]
    #     download_by_link1(author_link, browser)

    workers = 5
    work_queue = Queue()
    done_queue = Queue()
    processes = []

    # links.reverse()
    for link in blinks:
        author_link = root + link["href"]
        work_queue.put(author_link)

    for w in xrange(workers):
        browser = get_browser(profile=None)
        p = Process(target=worker, args=(work_queue, done_queue, browser))
        p.start()
        processes.append(p)
        work_queue.put('STOP')
    for p in processes:
        p.join()

    done_queue.put('STOP')
    for status in iter(done_queue.get, 'STOP'):
        print status


def worker(work_queue, done_queue, browser=None):
    try:
        for url in iter(work_queue.get, 'STOP'):
            try:
                download_by_link1(url, browser)
            except:
                pass
            done_queue.put("%s - %s" % (current_process().name, url))
    except Exception, e:
        done_queue.put("%s failed on %s with: %s" % (current_process().name, url, e.message))
    return True


def download_by_link1(link, browser=None):
    helper = Helper()
    pageLimit = 2000000
    next = link
    root = link
    index = 0
    pageIndex = 0

    while next != None:

        nextPage = next

        print "\n\n", nextPage, "\n\n"

        pageIndex = pageIndex + 1

        html = get_page_content_from_url(nextPage, browser)

        mysoup = BeautifulSoup(html, html_parser)

        blinks = helper.find_tags(mysoup, "li.s-result-item")

        if len(blinks) == 0:
            blinks = helper.find_tags(mysoup, ".zg_itemImmersion .zg_itemWrapper a")

        for clink in blinks:
            try:
                try:
                    asin = extract_asin(clink["href"])
                except:
                    # print traceback.format_exc()
                    try:
                        asin = clink["data-asin"].strip()
                    except:
                        continue

                if asin is None:
                    continue

                try:
                    p = UkAsin()
                    p.isbn = asin
                    p.save()

                    print index, "-", p.isbn
                    index = index + 1
                except:
                    print asin, "existed"

            except:
                pass

        nextPageNum = helper.find_next_page(mysoup)
        # print nextPageNum

        if pageIndex >= pageLimit:
            next = None
        else:
            if nextPageNum != None:
                next = root + "&pg=" + str(nextPageNum)
                # time.sleep(random.expovariate(0.1))
            else:
                next = None


def download_textbooks(file):
    links = []
    with open(file, 'rb') as f:
        reader = csv.reader(f)
        for row in reader:
            if len(row) == 0:
                break

            if row[0][0] == "#":
                continue
            links.append(row[0])

    workers = 5
    work_queue = Queue()
    done_queue = Queue()
    processes = []

    links.reverse()
    browser = get_browser(profile=None)
    for link in links:
        # link = link + "&lo=stripbooks"
        work_queue.put(link)
        # download_by_link1(link, browser)
    # return
    for w in xrange(workers):
        browser = get_browser(profile=None)
        p = Process(target=worker, args=(work_queue, done_queue, browser))
        p.start()
        processes.append(p)
        work_queue.put('STOP')
    for p in processes:
        p.join()

    done_queue.put('STOP')
    for status in iter(done_queue.get, 'STOP'):
        print status

    pass


def parse_args():
    parser = argparse.ArgumentParser(description='Crawler.')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()

    # textbook_url = "https://www.amazon.com/s/ref=lp_468224_ex_n_1?rh=n%3A283155%2Cn%3A%212349030011%2Cn%3A465600&bbn=465600&ie=UTF8&qid=1535126458"
    file = APP_ROOT_DIR + "/data/uk-bath.csv"
    download_textbooks(file)
    exit(0)
