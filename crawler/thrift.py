import argparse
import random
import urlparse
import warnings
from multiprocessing import Lock, Process, Queue, current_process
import re

import time
from bs4 import BeautifulSoup
from selenium.webdriver.support.select import Select

from lib.models import CrawlIsbn
from lib.utils.helper import Helper, get_page_content_from_url, html_parser, get_browser

_extract_asin_regexp = re.compile(r'(/dp/|/product-reviews/|/gp/product/|#isbn=)(?P<asin>[^/]+)/?')


def extract_asin(url):
    try:
        match = _extract_asin_regexp.search(url)
        if match is None:
            return None
        asin = str(match.group('asin'))
        if len(asin) == 10:
            return asin
    except:
        #warnings.warn('Error matching ASIN in URL {}'.format(url))
        raise

    return None


def worker(work_queue, done_queue, browser=None):
    try:
        for url in iter(work_queue.get, 'STOP'):
            try:
                download_by_link(url, browser)
            except:
                pass
            done_queue.put("%s - %s" % (current_process().name, url))
    except Exception, e:
        done_queue.put("%s failed on %s with: %s" % (current_process().name, url, e.message))
    return True


def download_by_link(link, browser=None):
    helper = Helper()
    next = link

    parts = urlparse.urlparse(link)
    root = parts.scheme + "://" + parts.hostname
    index = 0
    pageIndex = 0

    browser.get(link)

    select = Select(browser.find_element_by_id('BrowseItemsPerPage'))
    select.select_by_value("50")

    #BrowseItemsPerPage
    while next != None:

        nextPage = next

        print "\n\n", nextPage, "\n\n"

        pageIndex = pageIndex + 1
        blinks = []

        tried = 0
        while True:
            tried = tried + 1
            html = get_page_content_from_url(nextPage, browser)

            mysoup = BeautifulSoup(html, html_parser)

            blinks = helper.find_tags(mysoup, ".BrowseEntry")

            if len(blinks) > 0:
                break
            else:
                time.sleep(1)

            if tried > 10:
                break

        for link in blinks:

                try:
                    try:
                        asin = extract_asin(link["href"])
                    except:
                        continue

                    if asin is None:
                        continue

                    try:
                        p = CrawlIsbn()
                        p.isbn = asin
                        p.save()

                        print index, "-", p.isbn
                        index = index + 1
                    except:
                        print asin, "existed"

                        # pass
                except:
                    # print traceback.format_exc()
                    pass

        nextPage = browser.find_element_by_css_selector(".Browse-pagination.is-right")
        if 'is-disabled' in nextPage.get_attribute('class'):
            break
        try:
            browser.execute_script("arguments[0].click();", nextPage)
        except:
            pass
        #browser.execute_async_script("$('.Browse-pagination.is-right').click()")
        time.sleep(2)
        #print next


def parse_args():
    parser = argparse.ArgumentParser(description='Crawler.')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()
    url = "https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C15006%2C14641&b.pp=50&b.f.q=3"
    urls = [
            #'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C15006%2C12322&b.pp=50&b.f.q=3',
            #'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C15006%2C12212&b.pp=50&b.f.q=3',
            #'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C15006%2C15155&b.pp=50&b.f.q=3',
            #'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C15006%2C15367&b.pp=50&b.f.q=3',
            'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C15006%2C14643&b.pp=50&b.f.q=3',
            'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C12319%2C12322&b.pp=50&b.f.q=3',
            'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C12319%2C12321&b.pp=50&b.f.q=3',
            'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C12319%2C12996&b.pp=50&b.f.q=3',
            'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C12319%2C13006&b.pp=50&b.f.q=3',
            'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C12319%2C13976&b.pp=50&b.f.q=3',
            'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C12319%2C11847&b.pp=50&b.f.q=3',
            'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C12319%2C14657&b.pp=50&b.f.q=3',
            'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C14055&b.pp=50&b.f.q=3',
            'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C12996&b.pp=50&b.f.q=3',
            'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C15004&b.pp=50&b.f.q=3',
            'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C13005&b.pp=50&b.f.q=3',
            'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C13006%2C12639&b.pp=50&b.f.q=3',
            'https://www.thriftbooks.com/browse/#b.f.t%5B%5D=15395%2C13006&b.pp=50&b.f.q=3']
    browser = get_browser()
    try:
        for url in urls:
            download_by_link(url, browser)
    finally:
        pass
        browser.close()