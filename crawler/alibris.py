import argparse
import random
import urlparse
import warnings
from multiprocessing import Lock, Process, Queue, current_process
import re

import time
from bs4 import BeautifulSoup

from lib.models import CrawlIsbn
from lib.utils.helper import Helper, get_page_content_from_url, html_parser, get_browser

_extract_asin_regexp = re.compile(r'(/dp/|/product-reviews/|/gp/product/|/isbn/)(?P<asin>[^/]+)/?')


def extract_asin(url):
    try:
        match = _extract_asin_regexp.search(url)
        asin = str(match.group('asin'))
        if len(asin) == 10:
            return asin
    except:
        # warnings.warn('Error matching ASIN in URL {}'.format(url))
        raise

    return None


def worker(work_queue, done_queue, browser=None):
    try:
        for url in iter(work_queue.get, 'STOP'):
            try:
                download_by_link(url, browser)
            except:
                pass
            done_queue.put("%s - %s" % (current_process().name, url))
    except Exception, e:
        done_queue.put("%s failed on %s with: %s" % (current_process().name, url, e.message))
    return True


def download_by_link(link, browser=None):
    helper = Helper()
    next = link

    parts = urlparse.urlparse(link)
    root = parts.scheme + "://" + parts.hostname
    index = 0
    pageIndex = 0

    while next is not None:

        nextPage = next

        print "\n\n", nextPage, "\n\n"

        pageIndex = pageIndex + 1

        html = get_page_content_from_url(nextPage, browser)

        mysoup = BeautifulSoup(html, html_parser)

        blinks = helper.find_tags(mysoup, "#works li")

        if len(blinks) == 0:
            break

        for clink in blinks:
            links = helper.find_tags(clink, "a")
            for link in links:
                try:
                    try:
                        asin = extract_asin(link["href"])
                    except:
                        continue

                    if asin is None:
                        continue

                    try:
                        p = CrawlIsbn()
                        p.isbn = asin
                        p.save()

                        print index, "-", p.isbn
                        index = index + 1
                    except:
                        print asin, "existed"

                        # pass
                except:
                    # print traceback.format_exc()
                    pass

        nextPageLinks = helper.find_tags(mysoup, ".pagination a")
        if len(nextPageLinks) == 0:
            break

        nextPageLink = nextPageLinks[len(nextPageLinks) - 1]
        # print nextPageLink.text
        if 'next' not in nextPageLink.text.lower():
            break
        next = root + nextPageLink['href']
        # print next


def parse_args():
    parser = argparse.ArgumentParser(description='Crawler.')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()
    url = "https://www.alibris.com/booksearch?wquery=Accounting+textbook&eans=1&textbook=1&browse=3&qsort=&narrow=1&wtit=&wauth=&binding=&wquery=&wprice=5+-+15&wpub="
    urls = [
        'https://www.alibris.com/booksearch?wquery=Accounting+textbook&eans=1&textbook=1&browse=3&qsort=&narrow=1&wtit=&wauth=&binding=&wquery=&wprice=15+-+25&wpub=',
        'https://www.alibris.com/booksearch?wquery=Accounting+textbook&eans=1&textbook=1&browse=3&qsort=&narrow=1&wtit=&wauth=&binding=&wquery=&wprice=25+-+50&wpub=',
        'https://www.alibris.com/booksearch?wquery=Accounting+textbook&eans=1&textbook=1&browse=3&qsort=&narrow=1&wtit=&wauth=&binding=&wquery=&wprice=Over+50&wpub='
    ]
    browser = get_browser()
    try:
        for url in urls:
            download_by_link(url, browser)
    finally:
        browser.close()
