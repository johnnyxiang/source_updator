import sys
import argparse
import traceback

import datetime
from elasticsearch import Elasticsearch

from lib.general import APP_ROOT_DIR
from lib.models import accountDB
from lib.utils.simple_logger import SimpleLogger
from lib.utils.offer_service_price_finder import OfferServicePriceFinder
from local import sids

logger = SimpleLogger.get_stream_logger('source_price_updater')
offer_service_price_finder = OfferServicePriceFinder()
es_client = Elasticsearch(hosts='104.196.114.179', port=8080, http_auth=('elasticuser', 'KbersRiseUp153'))


def write_to_file(file_name, data):
    list_file = APP_ROOT_DIR + "/data/%s.txt" % file_name
    with open(list_file, 'ab') as csvfile:
        for row in data:
            try:
                csvfile.write(",".join(row) + "\n")
            except:
                pass


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-c', '--country', type=str, default="us", help='Source ASIN country')
    parser.add_argument('-d', '--condition', type=str, default="new", help='Source ASIN condition')
    parser.add_argument('-t', '--type', type=str, default="product", help='Product Type: product, book or cd')

    command_args = parser.parse_args()
    return command_args


def process(country, ptype, condition):
    condition = condition.lower()

    if ptype == "product":
        db_table = "source_price_products"
        product_code = 'p'
    elif ptype == "cd":
        db_table = "source_price_cds"
        product_code = 'g'
    else:
        db_table = "source_price_books"
        product_code = 'c'

    index_type = ptype

    country = country.lower()
    if country != "us":
        db_table = db_table + "_" + country

    if ptype == "book" and condition != 'all':
        db_table = db_table + "_" + condition

    file_name = db_table
    size = 10000
    total = 0
    max = '200m'

    es_index = 'repricing_new' if condition.lower() == 'new' else 'repricing_any'

    query = {
        "term": {"product_code": product_code}
    }
    params = {
        'index': es_index,
        'scroll': max,
        'size': size,
        'doc_type': country.lower(),
        'body': {
            'query': query
        }
    }

    data = es_client.search(**params)
    sid = data['_scroll_id']
    scroll_size = data['hits']['total']
    total_size = scroll_size
    total_pages = int(total_size / size)
    page_no = 0
    # Start scrolling
    while scroll_size > 0:
        try:
            data = es_client.scroll(scroll_id=sid, scroll=max)
            # Update the scroll ID
            sid = data['_scroll_id']
            # Get the number of results that we returned in the last scroll
            scroll_size = len(data['hits']['hits'])
            # print "scroll size: " + str(scroll_size)

            if data['hits']['total'] == 0:
                break

            rows = []
            for row in data['hits']['hits']:
                try:
                    isbn = row['_id']
                    time = row['_source']['time']
                    offer_time_str = time[:19]
                    offer_time_str = offer_time_str.replace('T', ' ')
                    rows.append([isbn, offer_time_str])
                    total = total + 1
                except:
                    print traceback.format_exc()
                    pass

            if len(rows) > 0:
                write_to_file(file_name, rows)
                # if accountDB.is_closed():
                #     accountDB.connect()
                #
                # with accountDB.atomic():
                #     for row in rows:
                #         update_sql = " update " + db_table + " set price_last_checked=%s where isbn = %s"
                #         accountDB.execute_sql(update_sql, [row[1], row[0]])

            page_no = page_no + 1
            print 'page %s/%s, total %s/%s' % (page_no, total_pages, total, total_size)
        except:
            pass


def sync_db_to_es(country, ptype, condition):
    if ptype == "product":
        db_table = "source_price_products"
        product_code = 'p'
    elif ptype == "cd":
        db_table = "source_price_cds"
        product_code = 'g'
    else:
        db_table = "source_price_books"
        product_code = 'c'

    country = country.lower()
    if country != "us":
        db_table = db_table + "_" + country

    if ptype == "book" and condition != 'all':
        db_table = db_table + "_" + condition

    last_id = 0
    size = 500
    total = 0
    while True:
        # with accountDB.atomic() as transaction:
        offers_to_save = {}
        try:
            if accountDB.is_closed():
                accountDB.connect()

            with accountDB.atomic():
                sql = "select isbn,price_last_checked,id,price  from %s where id > %s " % (db_table, last_id)
                sql = sql + " order by id asc limit %d" % size
                # print sql

                cursor = accountDB.execute_sql(sql)
                rows = cursor.fetchall()

                if len(rows) == 0:
                    break

                offers_in_db = {}
                for row in rows:
                    min_date = row[1]
                    last_id = row[2]
                    asin = row[0]
                    price = row[3]
                    offers_in_db[asin] = {'price': price, 'time': min_date}

                isbns = [str(row[0]).zfill(10) for row in rows]
                offers_info = offer_service_price_finder.find_offer_for_asins(isbns, country, condition)
                if offers_info is not None:
                    for asin in isbns:
                        try:
                            if asin not in offers_info or offers_info[asin] is None or 'product_price' not in offers_info[asin]:
                                continue

                            offer = offers_info[asin]
                            # sometimes, event query with new condition, used condition offers returned
                            if condition.lower() == "new" and offer['item_condition'].lower() != 'new':
                                continue

                            if 'product_price' in offer:
                                offer_time_str = offer['time'][:19]
                                offer_time = datetime.datetime.strptime(offer_time_str, '%Y-%m-%dT%H:%M:%S')
                                offer_in_db = offers_in_db[asin]
                                if offer_in_db['time'] - offer_time > datetime.timedelta(hours=12):
                                    offer['time'] = offer_in_db['time'].strftime('%Y-%m-%dT%H:%M:%S')
                                    price = offer_in_db['price']
                                    if price > 0:
                                        offer['has_offer'] = 'y'
                                        offer['product_price'] = price
                                        offer['shipping_price'] = 0
                                    else:
                                        offer['has_offer'] = 'n'
                                        offer['product_price'] = 0
                                        offer['shipping_price'] = 0
                                    offer['rating_str'] = offer['rate']
                                    offers_to_save[asin] = offer
                                    # print asin, condition, round(offer['product_price'] + offer['shipping_price'], 2), offer_time_str, \
                                    #     offer['time']
                            else:
                                pass
                        except Exception as e:
                            print traceback.format_exc()

                if len(offers_to_save) > 0:
                    total = total + len(offers_to_save)

                    try:
                        # threading.Thread(target=offer_service_price_finder.save_offers, args=(offers_to_save_es, country, cond))
                        offer_service_price_finder.save_offers(offers_to_save, country, condition)
                    except:
                        print traceback.format_exc()

                print last_id, total

        except:
            print traceback.format_exc()
            # break


if __name__ == "__main__":

    args = parse_args()
    print args

    condition = args.condition.lower()
    country = args.country.upper()
    ptype = args.type

    if ptype == "book":
        if condition not in ['used', 'new']:
            condition = "all"
    else:
        condition = "new"

    sync_db_to_es(country, ptype, condition)
