import argparse
import csv

import datetime

import StringIO
import requests

from lib.models import WarehouseTracking


def parse_args():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-d', '--tracking_date', type=str, default=None, help='Date')
    parser.add_argument('-b', '--days_back', type=int, default=3, help='days back')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":

    args = parse_args()

    if args.tracking_date is None:
        tracking_date = datetime.datetime.utcnow().strftime('%Y-%m-%d')
    else:
        tracking_date = args.tracking_date

    base = datetime.datetime.strptime(tracking_date, '%Y-%m-%d')
    date_list = [base - datetime.timedelta(days=x) for x in range(0, args.days_back)]
    for date in date_list:
        tracking_date = date.strftime('%Y-%m-%d')
        url = "http://dsi.dovertmt.club/index/tracking?date=%s" % tracking_date
        r = requests.get(url)

        f = StringIO.StringIO(r.content)
        reader = csv.reader(f, delimiter=",")

        row_no = 0
        for row in reader:
            if len(row) == 0 or row[0].lower() == 'account':
                continue
            row_no = row_no + 1
            try:
                amazon_order_id = row[1]
                try:
                    tracking_obj = WarehouseTracking.get(WarehouseTracking.amazon_order_id == amazon_order_id)
                except:
                    tracking_obj = WarehouseTracking()
                    tracking_obj.amazon_order_id = amazon_order_id

                tracking_obj.ship_date = datetime.datetime.strptime(tracking_date, '%Y-%m-%d')
                tracking_obj.carrier = row[3]
                tracking_obj.tracking = row[4].replace('#', '')
                tracking_obj.save()
                print tracking_date, row_no, row
            except:
                print row_no
