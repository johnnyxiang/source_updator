import string
import traceback

from lib.google.sheet_api import SheetAPI
from lib.models import *
import argparse
import datetime
from dateutil import parser

sheet_api = SheetAPI()

spreadsheets = {
    'dv': '1VNL3uMJagogGv69MbSuHf6HSLg1HKO-08ETNSz9oye4',
    'sf': '1NLWyF0js3jAFcJGAg6tx3b8EYt3Sz0VaK-EQwMrQlMQ'
}


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon inventory repricing.')
    parser.add_argument('-r', '--region', default='dv', help='region: dv, sf')
    parser.add_argument('-a', '--account', default=None, help='Account SID')
    parser.add_argument('-d', '--date_from', help='Date from')
    parser.add_argument('-ds', '--days_back', type=int, default=1, help='Days Back')
    command_args = parser.parse_args()
    return command_args


def count_emails(account_code, date_from, spreadsheet_id, debug=False):
    try:
        account_info = Account.get(name=account_code)
        email_address = account_info.email_address
    except:
        email_address = None
        pass

    database_name = account_code.lower()
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    start_of_day = datetime.datetime(date_from.year, date_from.month, date_from.day)
    end_of_day = start_of_day + datetime.timedelta(days=1)

    start_of_day = start_of_day + datetime.timedelta(hours=5)
    end_of_day = end_of_day + datetime.timedelta(hours=5)
    # emails = AccountEmail.select().where(
    #     (AccountEmail.fromAddress == email_address) | (AccountEmail.fromAddress.contains('auto-communication')),
    #     (AccountEmail.created_at >= start_of_day) & (AccountEmail.created_at < end_of_day))

    emails = AccountEmail.select().where(AccountEmail.fromAddress.contains("gmail"),
                                         (AccountEmail.created_at >= start_of_day) & (AccountEmail.created_at < end_of_day))
    total_counted = 0

    for email in emails:
        subject = ''.join(filter(lambda x: x in string.printable, email.subject))
        # print subject, email.fromAddress, email.toAddress
        # if from address and to address is the same
        if email.fromAddress.lower() == email.toAddress.lower():
            if debug:
                print 'ignored as from address and to address is the same'
            continue

        # if to a gmail, and (email_address is None or email.toAddress.lower() != email_address.lower())
        if 'mail.com' in email.toAddress.lower() or len(email.toAddress) == 0:
            if debug:
                print 'ignored as to a gmail address'
            continue

        if 'google.com' in email.toAddress.lower():
            if debug:
                print 'ignored as to a google address'
            continue

        if 'performance' in email.toAddress.lower() or 'seller' in email.toAddress.lower():
            continue

        if subject.lower().startswith('[important]'):
            if debug:
                print "ignored as it's a gray label email"
            continue

        feedback_subjects = [
            'We need your positive feedback',
            'are you happy with your',
            'you are satisfied with your recent',
            'Your recent Amazon order',
            'How do you feel about your transaction',
            'Thanks for your recent purchase',
            'will you rate your transaction',
            'Amazon Feedback Reminder',
            'Feedback request',
            'feedback'
            'Your recent order'
        ]
        feedback_request = False
        for keyword in feedback_subjects:
            if keyword.lower() in email.subject.lower():
                feedback_request = True
                break
        if feedback_request:
            if debug:
                print "ignored as it's a feedback request email"
            continue

        gray_subjects = [
            [u'Lieber', u'Ihre Amazon Bestellung'],
            [u'Estimado', u'acerca de su orden de Amazon'],
            [u'Bonjour', u'propos de votre commande Amazon'],
            [u'Gentile', u'riguardo il suo ordine Amazon'],
            ['Hello', 'Looking forward your response']
        ]
        is_gray = False
        for gray_subject in gray_subjects:
            if subject.lower().startswith(gray_subject[0].lower()) and gray_subject[1].lower() in subject.lower():
                is_gray = True
                break

        if is_gray is True:
            if debug:
                print "ignored as it's a gray label email"
            continue

        if subject == 'Your message to a buyer could not be delivered':
            continue

        # auto-reply
        auto_reply_keywords = [
            'contacting you about the delivery status',
            'We take customer service very seriously and will do our best to resolve any concern you may have',
            'regarding your Amazon order', 'We are proactively contacting the carrier to get further information',
            'Grazie per aver contattato team di servizio clienti di',
            'Kontaktaufnahme mit dem Kundenservice',
            'Por favor tenga en cuenta que este mensaje se',
            'Please note that this email is generated automatically',
            'Veuillez remarquer que cet e-mail est',
            'cannot be delivered because there was',
            'Wir setzen uns mit Ihnen bezglich des Lieferstatus Ihrer Bestellung in Verbindung und teilen Ihnen mit',
            'weil der Kufer sich dafr entschieden hat',
            'la contattiamo riguardo lo stato',
            'Estamos enviado este correo acerca',
            'statut de livraison de votre commande'

        ]
        auto_reply = False
        for keyword in auto_reply_keywords:
            if keyword.lower() in email.email.lower() and not subject.lower().startswith('re'):
                auto_reply = True
                break

        # auto
        # if 'auto' in email.fromAddress.lower():
        #     auto_reply = True

        if auto_reply is True:
            if debug:
                print "ignored as it's an auto-reply email"
            continue

        total_counted = total_counted + 1
        print 'counted, total %s so far' % total_counted
        print email.created_at, subject, email.fromAddress, email.toAddress
        # print email.email

    print 'total %s email replied for account %s on %s' % (total_counted, account_code, date_from.date())

    sheet_name = date_from.strftime('%m/%d')
    worksheet = sheet_api.create_new_sheet_if_not_existed(spreadsheet_id, sheet_name=sheet_name)

    accounts = sheet_api.col_values(worksheet, col=1)
    row = 0
    for acc in accounts:
        row = row + 1
        if acc.lower() == account_code.lower():
            break

    current_values = sheet_api.row_values(worksheet, row=row)

    cs_name = current_values[1] if len(current_values) > 1 else ''

    # worksheet.update_cell(row, 3, total_counted)
    sheet_api.update_cell(worksheet, row=row, col=3, value=total_counted)
    # sheet_api.append_or_insert_row(worksheet, key=account_info.name.upper(), values=[account_info.name.upper(), None, total_counted])

    ## save to db
    try:
        record = CSEmailReport.get(CSEmailReport.account == account_code, CSEmailReport.date == date_from.date())
    except:
        # print traceback.format_exc()
        record = CSEmailReport()
        record.account = account_code
        record.date = date_from.date()
    record.cs = cs_name
    record.emails = total_counted
    record.save()


if __name__ == "__main__":
    args = parse_args()

    spreadsheet_id = spreadsheets[args.region.lower()]
    if args.date_from is None:
        date_from = datetime.datetime.now() + datetime.timedelta(days=-1)
    else:
        date_from = parser.parse(args.date_from)

    if args.account is not None:
        date_list = [date_from - datetime.timedelta(days=x) for x in range(0, args.days_back)]
        for date in date_list:
            try:
                count_emails(args.account, date, spreadsheet_id=spreadsheet_id)
            except:
                print traceback.format_exc()
                pass
    else:
        template_sheet = sheet_api.get_worksheet(spreadsheet_id, sheet_name='Template')
        # template_sheet = sheet_api.service.open_by_key(spreadsheet_id).worksheet('Template')
        values_list = sheet_api.col_values(template_sheet, col=1)

        # days_back
        date_list = [date_from - datetime.timedelta(days=x) for x in range(0, args.days_back)]
        for date in date_list:
            for account in values_list:
                if account.lower() == 'account' or len(account) == 0:
                    continue
                try:
                    count_emails(account, date, spreadsheet_id=spreadsheet_id)
                except:
                    print traceback.format_exc()
                    pass
