import argparse
import datetime
import traceback
import urllib2
import xmltodict

from lib.models import *
import csv
import StringIO
from config import *
from local import *
from lib.listing_report import *
from dateutil import parser

feedback_file_dir = APP_ROOT_DIR + "/feedbacks"
if not os.path.isdir(feedback_file_dir):
    os.makedirs(feedback_file_dir)


def parse_performance_report(data, region, account_id):
    file_name = feedback_file_dir + "/%s-%s-%s.xml" % (account_id, region, datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"))

    w = open(file_name, "wb")
    w.write(data)
    w.close()

    parse_performance_report_file(region, account_id, file_name)

    return


def parse_performance_report_file(region, account_id, file_name):
    if os.path.isfile(file_name):
        file_name = file_name
    else:
        file_name = feedback_file_dir + "/" + file_name + ".xml"

    print file_name

    json_dict = json.loads(json.dumps(xmltodict.parse(open(file_name, "r"))))
    # print json

    performance_dict = dict()

    try:
        odr_section = json_dict['sellerPerformanceReports']['sellerPerformanceReport']['orderDefects']['orderDefectMetrics'][0]
        print odr_section
        odr_dict = {
            'odr': float(odr_section['orderWithDefects']['rate'].strip('%')),
            'odr_start': odr_section['timeFrame']['start'].split('T')[0],
            'odr_end': odr_section['timeFrame']['end'].split('T')[0],
            'orderCount': odr_section['orderCount'],
            'orderWithDefects': odr_section['orderWithDefects']['count'],
            'negativeFeedbacks': odr_section['negativeFeedbacks']['count'],
            'a_z_claims': odr_section['a_z_claims']['count'],
            'chargebacks': odr_section['chargebacks']['count']
        }
        performance_dict.update(odr_dict)
    except:
        pass

    try:
        ce_section = json_dict['sellerPerformanceReports']['sellerPerformanceReport']['customerExperience']['customerExperienceMetrics'][0]
        ce_dict = {
            'lateShipment': ce_section['lateShipment']['rate'].strip('%'),
            'preFulfillmentCancellation': ce_section['preFulfillmentCancellation']['rate'].strip('%'),
            'refunds': ce_section['refunds']['rate'].strip('%'),
        }
        performance_dict.update(ce_dict)
    except:
        pass

    try:
        trackingMetrics = json_dict['sellerPerformanceReports']['sellerPerformanceReport']['trackingInformation']['trackingMetrics']
        tracking_section = trackingMetrics[len(trackingMetrics)-1]
        tracking_dict = {
            'validTracking': tracking_section['validTracking']['rate'].strip('%'),
            'onTimeDelivery': tracking_section['onTimeDelivery']['rate'].strip('%')
        }
        performance_dict.update(tracking_dict)
    except:
        pass

    try:
        rt_section = \
            json_dict['sellerPerformanceReports']['sellerPerformanceReport']['buyerSellerContactResponseTimeMetrics'][
                'responseTimeMetrics'][0]
        rt_dict = {
            'averageResponseTimeInHours': rt_section['averageResponseTimeInHours'],
            'responseTimeGreaterThan24Hours': rt_section['responseTimeGreaterThan24Hours'],
            'noResponseForContactsOlderThan24Hours': rt_section['noResponseForContactsOlderThan24Hours']
        }
        performance_dict.update(rt_dict)
    except:
        pass

    try:
        performance_dict['intellectualPropertyData'] = \
            json_dict['sellerPerformanceReports']['sellerPerformanceReport']['intellectualPropertyData'][
                'defectCount']
        print performance_dict
    except:
        pass

    date = datetime.datetime.now().date()
    try:
        model_data = AccountPerformance.get(AccountPerformance.account_code == account_id, AccountPerformance.country == region,
                                            AccountPerformance.date == date)
    except:
        # print traceback.format_exc()
        model_data = AccountPerformance()
        model_data.account_code = account_id
        model_data.country = region
        model_data.SalesChannel = get_sales_channel(region)
        model_data.date = date

    for k, v in performance_dict.iteritems():
        setattr(model_data, k, v)
    model_data.save()

    print "\n", performance_dict


def parse_feedback_report(data, region, account_id):
    file_name = feedback_file_dir + "/%s-%s-%s.csv" % (account_id, region, datetime.datetime.now().strftime("%Y-%m-%d-%H-%M"))

    w = open(file_name, "wb")
    writer = csv.writer(w, delimiter=',')

    f = StringIO.StringIO(data)
    reader = csv.reader(f, delimiter="\t")

    for row in reader:
        writer.writerow(row)

    w.close()

    parse_feedback_report_file(region, account_id, file_name)

    return


date_formats = {
    'us': '%m/%d/%y',
    'uk': '%d/%m/%y',
    'de': '%d.%m.%y',
    'fr': '%d/%m/%y',
    'es': '%d/%m/%y',
    'it': '%d/%m/%y',
    'ca': '%d/%m/%y',
}


def parse_feedback_report_file(region, account_id, file):
    if os.path.isfile(file):
        file_name = file
    else:
        file_name = feedback_file_dir + "/" + file + ".csv"

    print file_name

    f = open(file_name, "r")
    reader = csv.reader(f, delimiter=",")

    rows = [row for row in reader]
    index = 0

    header = {'date': 0, 'rating': 1, 'comments': 2, 'order_id': 4}
    if len(rows) > 1:
        # delete existed, as some feedbacks may be removed
        row = rows[len(rows) - 1]
        date = row[header['date']]
        try:
            feedback_date = datetime.datetime.strptime(date, date_formats[region.lower()])
        except:
            feedback_date = parser.parse(date).date()

        date_string = feedback_date.strftime('%Y-%m-%d')
        # sql = "DELETE FROM feedbacks WHERE account_code = %s AND country = %s AND `date` >= %s"
        sql = "UPDATE feedbacks SET removed =1  WHERE account_code = %s AND country = %s AND `date` >= %s AND rating <=3"
        print sql, account_id, date_string, country
        omsDB.execute_sql(sql, [account_id, country, date_string])

    for row in rows:
        # if row[0] == "Date":
        index = index + 1
        if index == 1:
            i = 0
            for name in row:
                if 'Date' in name:
                    header['date'] = i
                elif name == 'Rating':
                    header['rating'] = i
                elif name == 'Comments':
                    header['comments'] = i
                elif name == 'Order ID':
                    header['order_id'] = i
                i = i + 1
            continue

        if len(row) < 7 and header['order_id'] > 6:
            header['order_id'] = 4
        date = row[header['date']]
        rating = row[header['rating']]
        comments = row[header['comments']]
        order_id = row[header['order_id']]

        try:
            feedback_date = datetime.datetime.strptime(date, date_formats[region.lower()])
        except:
            feedback_date = parser.parse(date).date()

        try:
            feedback = Feedback.get(Feedback.amazon_order_id == order_id)
        except:
            feedback = Feedback()
        feedback.amazon_order_id = order_id
        feedback.account_code = account_id
        feedback.removed = 0

        try:
            feedback.rating = rating
            feedback.comment = comments
            feedback.date = feedback_date
            feedback.country = region
            feedback.save()
        except:
            print traceback.format_exc()

        print date, order_id, rating, comments

    urllib2.urlopen("http://35.224.158.228/api/chargeback/update-stats").read()


def parse_args():
    parser = argparse.ArgumentParser(description='Amazon feedback report')

    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-c', '--country', type=str, default=None, help='marketplace')
    parser.add_argument('-d', '--days_back', type=int, default=30, help='days back')
    parser.add_argument('-f', '--file', type=str, default=None, help='File')
    parser.add_argument('-t', '--type', type=str, default='all', help='report type')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    # print datetime.datetime.strptime('07/01/19', date_formats['it'])
    args = parse_args()

    if args.account not in sids:
        print "account not supported"
        sys.exit(0)

    accountInfo = Account.get(name=args.account)
    databaseName = args.account
    database_name = args.account
    database.init(database_name, host=databaseHost, user=databaseUser, password=databasePW)

    country = args.country if args.country is not None and len(args.country) > 0 else accountInfo.country
    countries = country.split(",")
    if country.lower() == 'uk' and args.country is None:
        countries = ['uk', 'de', 'fr', 'es', 'it']
    print countries

    dt = datetime.datetime.utcnow() - datetime.timedelta(days=args.days_back)
    from_date = dt.strftime('%Y-%m-%dT%H:%M:%S')
    type = args.type
    # dt = datetime.datetime.utcnow() - datetime.timedelta(minutes=20)
    # todate = dt.strftime('%Y-%m-%dT%H:%M:%S')

    for country in countries:
        if args.file is not None:
            if type == 'feedback' or type == 'f' or type == 'all':
                parse_feedback_report_file(region=country, account_id=accountInfo.name, file=args.file)
            else:
                parse_performance_report_file(region=country, account_id=accountInfo.name, file_name=args.file)
        else:
            if type == 'feedback' or type == 'f' or type == 'all':
                get_report("_GET_SELLER_FEEDBACK_DATA_", country, accountInfo.name, accountInfo.mws_access_key,
                           accountInfo.mws_secret_key, accountInfo.seller_id, auth_token=accountInfo.mws_auth_token, start_date=from_date,
                           callback=parse_feedback_report)

            if type == 'performance' or type == 'p' or type == 'all':
                get_report("_GET_V1_SELLER_PERFORMANCE_REPORT_", country, accountInfo.name, accountInfo.mws_access_key,
                           accountInfo.mws_secret_key, accountInfo.seller_id, auth_token=accountInfo.mws_auth_token,
                           callback=parse_performance_report)
