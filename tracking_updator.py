import argparse
import socket
import datetime
import traceback
import random

from lib.general import get_lock
from lib.models import WarehouseTracking, OrderTracking, AmazonOrder
from lib.utils.helper import get_browser
import time
import sys
from multiprocessing import Process, Queue, current_process
from lib.utils.tracking_status_crawler import TrackingStatusCrawler, URLRejectedException


def update_tracking_info_by_order_id(order_id):
    tracking_obj = WarehouseTracking.get(WarehouseTracking.amazon_order_id == order_id)
    browser = get_browser()
    update_tracking_info_row(tracking_obj, browser)


def init_browser(profile=None):
    browser = get_browser(profile=profile, headless=True)
    return browser


def update_tracking_info_row(tracking_obj, browser=None):
    if len(tracking_obj.tracking) == 0:
        return

    tracking_status_crawler = TrackingStatusCrawler(carrier=tracking_obj.carrier, tracking_number=tracking_obj.tracking,
                                                    browser=browser, init_browser=init_browser)
    tracking_status_crawler.fetch()
    try:
        print tracking_status_crawler
    except:
        pass

    try:
        tracking_obj.shipment_status = tracking_status_crawler.shipping_status()
        tracking_obj.detail_tracking_info = tracking_status_crawler.description
        tracking_obj.postal_provider = tracking_status_crawler.postal_provider
        tracking_obj.last_tracking_status_updated_at = tracking_status_crawler.last_updated
        tracking_obj.last_checked = datetime.datetime.utcnow()
        tracking_obj.DHLProcessedDate = tracking_status_crawler.process_date
        if tracking_status_crawler.exception is True:
            tracking_obj.delivered = -1
        elif tracking_status_crawler.delivered:
            tracking_obj.delivered = 1
        else:
            tracking_obj.delivered = 0

        tracking_obj.save()
    except:
        print traceback.format_exc(), tracking_obj.shipment_status


def worker(work_queue, done_queue, browser=None):
    try:
        while True:
            if work_queue.empty():
                print "Done"
                break
            tracking = work_queue.get()
            if tracking == 'STOP':
                break

            try:
                update_tracking_info_row(tracking, browser)
                time.sleep(random.randrange(0, 2))
            except URLRejectedException as e:
                browser.close()
                time.sleep(2)
                browser = init_browser(profile=None)
                update_tracking_info_row(tracking, browser)
                time.sleep(random.randrange(0, 2))
            except:
                print traceback.format_exc()
                pass

    except Exception, e:
        print "%s failed  with: %s" % (current_process().name, e.message)
    return True


def check_by_multiple_process(trackings, max_worker=4):
    work_queue = Queue()
    done_queue = Queue()
    processes = []

    for tracking in trackings:
        work_queue.put(tracking)
    work_queue.put('STOP')

    workers = min(max_worker, len(trackings))

    print "fetch status for %s trackings, used %s works" % (len(trackings), workers)
    for w in xrange(workers):
        browser = init_browser(profile=None)
        p = Process(target=worker, args=(work_queue, done_queue, browser))
        p.start()
        processes.append(p)

    for p in processes:
        p.join()
        p.terminate()


def parse_args():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-a', '--account', help='Account SID')
    parser.add_argument('-d', '--tracking_date', type=str, default=None, help='Date')
    parser.add_argument('-c', '--carrier', type=str, default=None, help='Carrier')
    parser.add_argument('-u', '--updated', type=int, default=3, help='Date')
    parser.add_argument('-t', '--type', type=str, default='warehouse', help='Supplier tracking or warehouse tracking')
    command_args = parser.parse_args()
    return command_args


if __name__ == "__main__":
    args = parse_args()
    # update_tracking_info_by_order_id('111-6634516-4017803')
    #
    # browser = getBrowser(profile=None)
    start_date = datetime.datetime.utcnow() + datetime.timedelta(-60)
    last_checked = datetime.datetime.utcnow() + datetime.timedelta(hours=-args.updated)
    print args.type, start_date, last_checked

    get_lock("update_tracking_status_%s" % args.type.lower())

    if args.type == 'order':
        if args.account is not None:
            trackings = OrderTracking.select().join(AmazonOrder, on=(AmazonOrder.AmazonOrderId == OrderTracking.amazon_order_id)).where(
                OrderTracking.delivered != 1,
                OrderTracking.ship_date >= start_date,
                OrderTracking.last_checked <= last_checked,
                AmazonOrder.account_code == args.account)
        else:
            trackings = OrderTracking.select().where(OrderTracking.delivered != 1,
                                                     OrderTracking.last_checked <= last_checked,
                                                     OrderTracking.ship_date >= start_date)
    else:
        if args.carrier is not None:
            trackings = WarehouseTracking.select().where(WarehouseTracking.delivered != 1,
                                                         WarehouseTracking.ship_date >= start_date,
                                                         WarehouseTracking.last_checked <= last_checked,
                                                         WarehouseTracking.carrier == args.carrier)
        else:
            trackings = WarehouseTracking.select().where(WarehouseTracking.delivered != 1,
                                                         WarehouseTracking.last_checked <= last_checked,
                                                         WarehouseTracking.ship_date >= start_date)
    print 'processing %s trackings' % len(trackings)
    check_by_multiple_process(trackings)
